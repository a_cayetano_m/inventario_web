<?php 
session_start();
    require("../../System/Config.php");
    require("../../App/Config.php");
    require("../../App/Default.php");
    require("../../System/Core/Database/ODBC.php");
    
        $type='odbc';
        $instancia= new System\Core\Database\ODBC(CONFIG['database'][$type]['server'],CONFIG['database'][$type]['user'],CONFIG['database'][$type]['password'],CONFIG['database'][$type]['db']);
       
        //$fechas=$_GET['fechas'];
        $reporte=$_GET['reporte'];
        $instancia->createConexionODBC();
        $conexion=$instancia->getConexionODBC();
        $columnas=array();
        $columnas_0=['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
        $columnas_1=['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
        for($i=0;$i<count($columnas_0);$i++){
            for($j=0;$j<count($columnas_1);$j++){
                
                    $columnas[]=$columnas_0[$i].$columnas_1[$j];
                
            }
        }
        $columnas= array_merge($columnas_0,$columnas);
        if($reporte==1){ // Gestion de informacion ventas
            require("Reporte/EXCELSHEET/comercial-ventas.php");
        }else if($reporte==2){ // Gestion de informacion stock
            require("Reporte/EXCELSHEET/comercial-stock.php");
        }else if($reporte==3){ // Gestion de informacion ventas CSV
            require("Reporte/CSV/ventas.php");
        }

?>