<?php 
    ini_set('max_execution_time', 600);
    ini_set('memory_limit', '1024M');
    error_reporting(0);
    
    if(isset($_SESSION[CONFIG['session']['global']])){
        $ruc=$_SESSION[CONFIG['session']['global']]['USER']['RUC'];
        $tiendas=count($_SESSION[CONFIG['session']['global']]['TIENDAS']);
        $cast='INT';
        $prefix='STOCK_';
        $campos='';
        $action='';
        for($i=1;$i<=$tiendas; $i++){
            if($i==1){
                $campos=$campos.''.$cast.'('.$action.'('.$prefix.''.str_pad($i, 3, "0", STR_PAD_LEFT).')) as '.$prefix.''.str_pad($i, 3, "0", STR_PAD_LEFT).'';
            }else{
                $campos=$campos.','.$cast.'('.$action.'('.$prefix.''.str_pad($i, 3, "0", STR_PAD_LEFT).')) as '.$prefix.''.str_pad($i, 3, "0", STR_PAD_LEFT).'';
            }
        }     
        $sql="SELECT COMPRADOR,GRUPO,
        CATEGORIA,
        CODIGO_AS400 AS CODIGO_AS400,
        CODIGO_SAP AS CODIGO_SAP,
        DESCRIPCION,
        UNIDAD_BASE,
        ABC,
        ESTADO,
        INT(TRANSITO_TOTAL) AS TRANSITO_TOTAL,
        INT(STOCK_TOTAL) AS STOCK_TOTAL,".$campos." FROM  STLDTAPRV.PR001 WHERE PROVEEDOR='".$ruc."'";
    
        $data =$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar
        $instancia->disposeODBC();
        $cabeceras=DEFAULT_DATA['values']['exportar']['comercial']['stock']['cabeceras_export'];
        foreach($_SESSION[CONFIG['session']['global']]['TIENDAS'] as $k=>$v){
            $cabeceras[]=utf8_encode(trim($v['OPNOM']));
        }
        
        $array_tiendas=array();
        $array_campos=array();
        $array_campos_aux=array();
    
        if(count($data)>0){
            require_once '../PHPExcel.php';
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->getProperties()->setCreator("TaiLoy")
            ->setLastModifiedBy("Tai Loy")
            ->setTitle("Documento Office 2007 XLSX")
            ->setSubject("Solicitudes - ") //deberia agregarse la fecha
            ->setDescription("Detalle")
            ->setKeywords("office 2007 openxml")
            ->setCategory("File");
                // Reporte estilos 
            
            $letra=10;   
            $ancho=40;  

            array_values($data);
            $data_2=array();
            foreach($data as $k=>$v){  
                $data_2[]=array_values($v);
            }
            $arrayData = $data_2;
            $as = $objPHPExcel->getActiveSheet();
            
            $as->getProtection()->setSelectLockedCells(true);
            $as->getProtection()->setSelectUnlockedCells(true);
            $as->getProtection()->setFormatCells(true);
            $as->getProtection()->setFormatRows(true);
            $as->getProtection()->setInsertColumns(true);
            $as->getProtection()->setInsertRows(true);
            $as->getProtection()->setInsertHyperlinks(true);
            $as->getProtection()->setDeleteColumns(true);
            $as->getProtection()->setDeleteRows(true);
            $as->getProtection()->setSort(true);
            $as->getProtection()->setAutofilter(true);
            $as->getProtection()->setObjects(true);
            $as->getProtection()->setScenarios(true);
            $as->getProtection()->setSheet(true);
            $as->getProtection()->setPassword($ruc);

            $as->fromArray(
                $arrayData,  // The data to set
                NULL,        // Array values with this value will not be set
                'A9'         // Top left coordinate of the worksheet range where
                                //    we want to set these values (default is A1)
            );
            $default_style = array(
                'font' => array(
                    'name' => 'Verdana',
                    'color' => array('rgb' => '000000'),
                ),
                'alignment' => array(
                    'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER
                ),
                'borders' => array(
                    'allborders' => array(
                        'style' => \PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => 'AAAAAA')
                    )
                )
            );
            $as->getDefaultStyle()->applyFromArray($default_style);
            
            $titles = array($cabeceras);
            $title_style = array(
                'font' => array(
                    'bold' => true,
                    'color' => array('rgb' => 'FFFFFF'),
                ),
                'fill' => array(
                    'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                    'startcolor' => array('rgb' => '129249')
                ),
                'alignment' => array(
                    'wrap' => true,
                    'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                )
            );
            $text_style = array(
                'font' => array(
                    'bold' => true,
                    'color' => array('rgb' => 'ff0000'),
                )
            );
            

            $as->fromArray($titles, null, 'A8'); 
            
            $last_col = $as->getHighestColumn();
            
            $logo_style = array(
                'fill' => array(
                    'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                    'startcolor' => array('rgb' => 'ffffff')
                )
            );
            $as->getStyle('A1:'.$last_col.'7')->applyFromArray($logo_style);
            //LOGO	
            $objDrawing = new PHPExcel_Worksheet_Drawing();
            $objDrawing->setName('Tai Loy Proveedores');
            $objDrawing->setPath('../../App/assets/img/logo-proveedores.png');
            $objDrawing->setHeight(53);  
            $objDrawing->setCoordinates('A1');
            $objDrawing->setWorksheet($objPHPExcel->getActiveSheet(1));
    


            $as->getStyle('A8:'.$last_col.'8')->applyFromArray($title_style);
            $as->getStyle('A6')->applyFromArray($text_style);
            $as->getDefaultColumnDimension()->setWidth($ancho);
            $as->setCellValue("A6", '*El stock fisico total y los stocks por tienda NO incluyen tránsitos.');
            $name=date("Ymd-His");
            $as->setTitle('Stock-'.$name);

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="Stock-'.$name.'.xlsx"');
            header('Cache-Control: max-age=0');
            
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            ob_end_clean();
            $objWriter->save('php://output');


            die();












        }else{
            header('Location: '.BASE_URL.'comercial-stock');
        }
    }else{
        header('Location: '.BASE_URL.'comercial-stock');
    }
    
?>