<?php 
    ini_set('max_execution_time', 600);
    ini_set('memory_limit', '1024M');
    error_reporting(0);
    
    if(isset($_SESSION[CONFIG['session']['global']])){
        $ruc=$_SESSION[CONFIG['session']['global']]['USER']['RUC'];
        $fechas=explode("-",$_GET['fechas']);
        $fechas_inicio=explode("/",trim($fechas[0]));
        $fechas_fin=explode("/",trim($fechas[1]));
        $fechas_inicio=$fechas_inicio[2].$fechas_inicio[1].$fechas_inicio[0];
        $fechas_fin=$fechas_fin[2].$fechas_fin[1].$fechas_fin[0];
    
        $tiendas=count($_SESSION[CONFIG['session']['global']]['TIENDAS']);
        $cast='INT';
        $prefix='VENTAS_';
        $campos='';
        $action='SUM';
        for($i=1;$i<=$tiendas; $i++){
            if($i==1){
                $campos=$campos.''.$cast.'('.$action.'('.$prefix.''.str_pad($i, 3, "0", STR_PAD_LEFT).')) as '.$prefix.''.str_pad($i, 3, "0", STR_PAD_LEFT).'';
            }else{
                $campos=$campos.','.$cast.'('.$action.'('.$prefix.''.str_pad($i, 3, "0", STR_PAD_LEFT).')) as '.$prefix.''.str_pad($i, 3, "0", STR_PAD_LEFT).'';
            }
        }     
    
        $periodo_inicio='';
        $periodo_fin='';
        $periodos_generales=$_SESSION[CONFIG['session']['global']]['periodos_generales'];
    
        foreach($periodos_generales as $k=>$v){
            if($fechas_inicio>=$k && $fechas_inicio<date("Ymd",strtotime($k."+ 7 days"))){
                $periodo_inicio=$v;
            }
            if($fechas_fin>=$k && $fechas_fin<date("Ymd",strtotime($k."+ 7 days"))){
                $periodo_fin=$v;
            }
        }
        for($i=0;$i<=($periodo_fin-$periodo_inicio);$i++)
        {
            $periodos[]=$periodo_inicio+$i;
        }
        
        $periodos_anterior=array();
        foreach($periodos as $k=>$v){
            $valor=substr($v,0,4);
            $valor=(int)$valor;
            $valor=$valor-1;
            $valor=$valor.substr($v,4,2);
            $periodos_anterior[]=$valor;
        }
        
        //PERIODO SEMANA CERRADA
        $periodos=array_merge($periodos);
        $sql_where='';
        foreach($periodos as $k=>$v){
            if($k==0){
                if(count($periodos)>1){
                    $sql_where=$sql_where. " AND (PERIODO='".$v."'";
                }else{
                    $sql_where=$sql_where. " AND PERIODO='".$v."'";
                } 
            }else{
                if(($k+1)==count($periodos)){
                    $sql_where=$sql_where. " OR PERIODO='".$v."')";
                }else{
                    $sql_where=$sql_where. " OR PERIODO='".$v."'";
                }
            }
            
        }
    
        $sql="SELECT MIN(RANGO_INICIAL) AS RANGO_INICIAL,MAX(RANGO_FINAL) AS RANGO_FINAL,GRUPO,CATEGORIA,CODIGO_AS400 AS CODIGO_AS400,CODIGO_SAP AS CODIGO_SAP,DESCRIPCION,UNIDAD_BASE,ESTADO,INT(SUM(TOTAL_VENTAS)) as TOTAL_VENTAS,".$campos."
        FROM STLDTAPRV.PR002 WHERE PROVEEDOR='".$ruc."'";
    
        $order="  GROUP BY GRUPO,CATEGORIA,CODIGO_AS400,CODIGO_SAP,DESCRIPCION,UNIDAD_BASE,ESTADO ORDER BY CODIGO_AS400";
        $sql=$sql.$sql_where.$order;

       
        $data =$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar

        
        //PERIODO ANTERIOR SEMANA CERRADA
        $data_ant=array();
        if($_GET['check_anterior']==0){
            $periodos=array_merge($periodos_anterior);
            $sql_where='';
            foreach($periodos as $k=>$v){
                if($k==0){
                    if(count($periodos)>1){
                        $sql_where=$sql_where. " AND (PERIODO='".$v."'";
                    }else{
                        $sql_where=$sql_where. " AND PERIODO='".$v."'";
                    } 
                }else{
                    if(($k+1)==count($periodos)){
                        $sql_where=$sql_where. " OR PERIODO='".$v."')";
                    }else{
                        $sql_where=$sql_where. " OR PERIODO='".$v."'";
                    }
                }
                
            }
            $sql="SELECT MIN(RANGO_INICIAL) AS RANGO_INICIAL,MAX(RANGO_FINAL) AS RANGO_FINAL,GRUPO,CATEGORIA,CODIGO_AS400 AS CODIGO_AS400,CODIGO_SAP AS CODIGO_SAP,DESCRIPCION,UNIDAD_BASE,ESTADO,INT(SUM(TOTAL_VENTAS)) as TOTAL_VENTAS,".$campos."
            FROM STLDTAPRV.PR002 WHERE PROVEEDOR='".$ruc."'";
        
            $order="  GROUP BY GRUPO,CATEGORIA,CODIGO_AS400,CODIGO_SAP,DESCRIPCION,UNIDAD_BASE,ESTADO ORDER BY CODIGO_AS400";
            $sql=$sql.$sql_where.$order;
            $data_ant =$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar
    
        }
        
        $data=array_merge($data,$data_ant);
        $instancia->disposeODBC();
        $cabeceras=DEFAULT_DATA['values']['exportar']['comercial']['ventas']['cabeceras_export'];
        foreach($_SESSION[CONFIG['session']['global']]['TIENDAS'] as $k=>$v){
            $cabeceras[]=utf8_encode(trim($v['OPNOM']));
        }
        
        $array_tiendas=array();
        $array_campos=array();
        $array_campos_aux=array();
        if(count($data)>0){
            require_once '../PHPExcel.php';
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->getProperties()->setCreator("Tai Loy")
            ->setLastModifiedBy("Tai Loy")
            ->setTitle("Documento Office 2007 XLSX")
            ->setSubject("Solicitudes - ") //deberia agregarse la fecha
            ->setDescription("Detalle")
            ->setKeywords("office 2007 openxml")
            ->setCategory("File");
                // Reporte estilos 

            $letra=10;   
            $ancho=40;  

            array_values($data);
            $data_2=array();
            foreach($data as $k=>$v){  
                $data_2[]=array_values($v);
            }
            $arrayData = $data_2;
            $as = $objPHPExcel->getActiveSheet();

            $as->getProtection()->setSelectLockedCells(true);
            $as->getProtection()->setSelectUnlockedCells(true);
            $as->getProtection()->setFormatCells(true);
            $as->getProtection()->setFormatRows(true);
            $as->getProtection()->setInsertColumns(true);
            $as->getProtection()->setInsertRows(true);
            $as->getProtection()->setInsertHyperlinks(true);
            $as->getProtection()->setDeleteColumns(true);
            $as->getProtection()->setDeleteRows(true);
            $as->getProtection()->setSort(true);
            $as->getProtection()->setAutofilter(true);
            $as->getProtection()->setObjects(true);
            $as->getProtection()->setScenarios(true);
            $as->getProtection()->setSheet(true);
            $as->getProtection()->setPassword($ruc);
            
            $as->fromArray(
                $arrayData,  // The data to set
                NULL,        // Array values with this value will not be set
                'A9'         // Top left coordinate of the worksheet range where
                                //    we want to set these values (default is A1)
            );
            $default_style = array(
                'font' => array(
                    'name' => 'Verdana',
                    'color' => array('rgb' => '000000'),
                ),
                'alignment' => array(
                    'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER
                ),
                'borders' => array(
                    'allborders' => array(
                        'style' => \PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => 'AAAAAA')
                    )
                )
            );
            $as->getDefaultStyle()->applyFromArray($default_style);
            
            $titles = array($cabeceras);
            $title_style = array(
                'font' => array(
                    'bold' => true,
                    'color' => array('rgb' => 'FFFFFF'),
                ),
                'fill' => array(
                    'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                    'startcolor' => array('rgb' => '129249')
                ),
                'alignment' => array(
                    'wrap' => true,
                    'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                )
            );
            $text_style = array(
                'font' => array(
                    'bold' => true,
                    'color' => array('rgb' => 'ff0000'),
                )
            );
            

            $as->fromArray($titles, null, 'A8'); 
            
            $last_col = $as->getHighestColumn();
            
            $logo_style = array(
                'fill' => array(
                    'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                    'startcolor' => array('rgb' => 'ffffff')
                )
            );
            $as->getStyle('A1:'.$last_col.'7')->applyFromArray($logo_style);
            //LOGO	
            $objDrawing = new PHPExcel_Worksheet_Drawing();
            $objDrawing->setName('Tai Loy Proveedores');
            $objDrawing->setPath('../../App/assets/img/logo-proveedores.png');
            $objDrawing->setHeight(53);  
            $objDrawing->setCoordinates('A1');
            $objDrawing->setWorksheet($objPHPExcel->getActiveSheet(1));
    


            $as->getStyle('A8:'.$last_col.'8')->applyFromArray($title_style);
            $as->getStyle('A6')->applyFromArray($text_style);
            $as->getDefaultColumnDimension()->setWidth($ancho);
            $as->setCellValue("A6", '*Las ventas web con recojo en tienda figuran dentro de la venta de la tienda. La venta por tienda también incluye ventas mostrador y despachos de eventos que se dan por tienda');

            $name=date("Ymd-His");
            $as->setTitle('Ventas-'.$name);

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="Ventas-'.$name.'.xlsx"');
            header('Cache-Control: max-age=0');
            
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            ob_end_clean();
            $objWriter->save('php://output');
            exit;
        }else{
            header('Location: '.BASE_URL.'informes-reportes');
        }
    }else{
        header('Location: '.BASE_URL.'informes-reportes');
    }
   
?>