<?php 
    $ruc=$_SESSION[CONFIG['session']['global']]['USER']['RUC'];

    $sql="SELECT PERIODO,RANGO_INICIAL,RANGO_FINAL,GRUPO,CATEGORIA,CODIGO_AS400,CODIGO_SAP,
    DESCRIPCION,UNIDAD_BASE,TOTAL_VENTAS,TIENDA,VENTAS FROM STLDTAPRV.PR002 WHERE PROVEEDOR='".$ruc."'";
    $sql_where='';
    foreach($periodos as $k=>$v){
        if($k==0){
            if(count($periodos)>1){
                $sql_where=$sql_where. " AND (PERIODO='".$v."'";
            }else{
                $sql_where=$sql_where. " AND PERIODO='".$v."'";
            } 
        }else{
            if(($k+1)==count($periodos)){
                $sql_where=$sql_where. " OR PERIODO='".$v."')";
            }else{
                $sql_where=$sql_where. " OR PERIODO='".$v."'";
            }
        }
        
    }
    //$order=" order by CODIGO_SAP";
    $sql=$sql.$sql_where;
    //$sql=$sql.$order;
    $data =$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar
    $instancia->disposeODBC();
    $columnas_usar=DEFAULT_DATA['values']['exportar']['comercial']['ventas']['columnas_usar'];
    $columnas_nousar=DEFAULT_DATA['values']['exportar']['comercial']['ventas']['columnas_nousar'];
    $array_tiendas=array();
    $array_campos=array();
    $array_campos_aux=array();
    $count_tiendas=0;
    foreach($data as $key=>$value){
        $i=1;
        foreach($value as $k=>$v){
            if($k=='TOTAL_VENTAS'){
                $v=(int)$v;
            }
            if($k=='PERIODO'){//202230
                $v=substr($v, 0, 4)."-".substr($v, 4, 2);
            }
            if($k=='RANGO_INICIAL' || $k=='RANGO_FINAL'){ //20220718  
                $v=substr($v, 6, 2)."/".substr($v, 4, 2)."/".substr($v,0,4);
            }
            if($i>count($columnas_usar)){
                $array_tiendas[$value['CODIGO_SAP']][$key][]=$v;
            }else{
                $array_campos[$value['CODIGO_SAP']][$key][]=$v;
            }
            $i++;
        }
    }
    
    foreach($array_tiendas as $k=>$v){
        $inicial=$v[0][0];
        $count_tiendas=0;
        $tiend=array();
        for($i=1;$i<count($v);$i++){
            if($v[$i]==''){
                $count_tiendas=$i;
                break;
            }
            $tiend[]=$v[$i][0];
           
        }
        break;
    }
    array_unshift($tiend,$inicial);
    if(count($data)>0){
        require_once '../PHPExcel.php';
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setCreator("TaiLoy")
        ->setLastModifiedBy("Tailoy")
        ->setTitle("Documento Office 2007 XLSX")
        ->setSubject("Solicitudes - ") //deberia agregarse la fecha
        ->setDescription("Detalle")
        ->setKeywords("office 2007 openxml")
        ->setCategory("File");
        // Reporte estilos 
        
        $letra=10;   
        $ancho=40;    
        foreach($columnas_usar as $k=>$v){
            $objPHPExcel->getActiveSheet()->getColumnDimension($columnas[$k])->setWidth($ancho);
            $objPHPExcel->getActiveSheet()->getStyle($columnas[$k]."1")->getFont()->setBold(true);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue($columnas[$k]."1", $v);
        }
        $count_tiendas=count($tiend);
        $aux=count($columnas_usar);
        for($i=0;$i<$count_tiendas;$i++){
            $objPHPExcel->getActiveSheet()->getColumnDimension($columnas[$aux+$i])->setWidth($ancho);
            $objPHPExcel->getActiveSheet()->getStyle($columnas[$aux+$i]."1")->getFont()->setBold(true);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue($columnas[$aux+$i]."1", $tiend[$i]);
        }
        $aux=$aux+$count_tiendas;
        $objPHPExcel->getActiveSheet()->getProtection()->setSelectLockedCells(true);
        $objPHPExcel->getActiveSheet()->getProtection()->setSelectUnlockedCells(true);
        $objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
        $objPHPExcel->getActiveSheet()->getProtection()->setFormatRows(true);
        $objPHPExcel->getActiveSheet()->getProtection()->setInsertColumns(true);
        $objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
        $objPHPExcel->getActiveSheet()->getProtection()->setInsertHyperlinks(true);
        $objPHPExcel->getActiveSheet()->getProtection()->setDeleteColumns(true);
        $objPHPExcel->getActiveSheet()->getProtection()->setDeleteRows(true);
        $objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
        $objPHPExcel->getActiveSheet()->getProtection()->setAutofilter(true);
        $objPHPExcel->getActiveSheet()->getProtection()->setObjects(true);
        $objPHPExcel->getActiveSheet()->getProtection()->setScenarios(true);
        $objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
        $objPHPExcel->getActiveSheet()->getProtection()->setPassword($ruc);
        // -------------------------------------------CUERPO---------------------------------------------	
       
        $objPHPExcel->getActiveSheet()->getStyle('A1:'.$columnas[$aux-2].'1')->applyFromArray(
            array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('argb' => 'FF000000'),
                    ),
                ),
            )
        );
        
        $objPHPExcel->getActiveSheet()->getStyle('A1:'.$columnas[$aux-2].'1')->applyFromArray(
            array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'FFFF00')
                )
            )
        );	
        $objPHPExcel->getActiveSheet()->getStyle('A1:'.$columnas[$aux-2].'1')->applyFromArray(
            array(
                'font' => array(
                    'size' =>$letra
                )
            )
        );	
        
        $objPHPExcel->getActiveSheet()->getStyle('A1:'.$columnas[$aux-2].'1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $aux=0;
        foreach($array_campos as $key=>$value){
            foreach($value as $k=>$v){  
                for($j=0;$j<=count($v);$j++){
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($columnas[$j].($aux+2),$v[$j]);
                    $objPHPExcel->getActiveSheet()->getStyle($columnas[$j].($aux+2))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    $objPHPExcel->getActiveSheet()->getStyle($columnas[$j].($aux+2))->applyFromArray(
                        array(
                            'font' => array(
                                'size' =>$letra
                            )
                        )
                    );
                }
                $band=count($v);
            }
            $aux++;
            
        } 
        $aux=0;
       
        
        foreach($array_tiendas as $k=>$v){
            $band_aux=$band;
            foreach($v as $ke=>$va){
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue($columnas[$band_aux].($aux+2),$va[1]);
                $objPHPExcel->getActiveSheet()->getStyle($columnas[$band_aux].($aux+2))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $objPHPExcel->getActiveSheet()->getStyle($columnas[$band_aux].($aux+2))->applyFromArray(
                array('font' => array('size' =>$letra)));
                $band_aux++;
            }
            
            $aux++;
        }

        $styleThinBlackBorderOutline = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('argb' => 'FF000000'),),),);

        $objPHPExcel->getActiveSheet()->setTitle('Ventas-'.date("Ymd-His"));
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="archivo.xlsx"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        ob_end_clean();
        $objWriter->save('php://output');
        exit;
    }else{
        header('Location: '.BASE_URL.'informes-reportes');
    }
    
?>