<?php 

    $ruc=$_SESSION[CONFIG['session']['global']]['USER']['RUC'];
    $query="SELECT * FROM STLDTAPRV.PR001 WHERE PROVEEDOR='".$ruc."' order by CODIGO_SAP ";
   
    $data =$instancia->executeQueryShowODBC($query,$conexion); //funcion para reemplazar
    $instancia->disposeODBC();
    $columnas_usar=DEFAULT_DATA['values']['exportar']['comercial']['stock']['columnas_usar'];
    $columnas_nousar=DEFAULT_DATA['values']['exportar']['comercial']['stock']['columnas_nousar'];
    $array_tiendas=array();
    $array_campos=array();
    $array_campos_aux=array();

    foreach($data as $key=>$value){
        $i=1;
        foreach($value as $k=>$v){
            if($k=='STOCK_TOTAL'){
                $v=(int)$v;
            }
            if($i>count($columnas_usar)){
                $array_tiendas[$value['CODIGO_SAP']][$key][]=$v;
            }else{
                $array_campos[$value['CODIGO_SAP']][$key][]=$v;
            }
            $i++;
        }
    }
    if(count($data)>0){
        require_once '../PHPExcel.php';
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setCreator("TaiLoy")
        ->setLastModifiedBy("Tailoy")
        ->setTitle("Documento Office 2007 XLSX")
        ->setSubject("Solicitudes - ") //deberia agregarse la fecha
        ->setDescription("Detalle")
        ->setKeywords("office 2007 openxml")
        ->setCategory("File");
            // Reporte estilos 
        
        $letra=10;   
        $ancho=40;    
        foreach($columnas_usar as $k=>$v){
            $objPHPExcel->getActiveSheet()->getColumnDimension($columnas[$k])->setWidth($ancho);
            $objPHPExcel->getActiveSheet()->getStyle($columnas[$k]."1")->getFont()->setBold(true);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue($columnas[$k]."1", $v);
        }
        foreach($array_tiendas as $key=>$values){
            $aux=count($columnas_usar);
            foreach($values as $k=>$v){
                $objPHPExcel->getActiveSheet()->getColumnDimension($columnas[$aux])->setWidth($ancho);
                $objPHPExcel->getActiveSheet()->getStyle($columnas[$aux]."1")->getFont()->setBold(true);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue($columnas[$aux]."1", $v[0]);
                $aux++;
            } break; 
        }
        $objPHPExcel->getActiveSheet()->getProtection()->setSelectLockedCells(true);
        $objPHPExcel->getActiveSheet()->getProtection()->setSelectUnlockedCells(true);
        $objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
        $objPHPExcel->getActiveSheet()->getProtection()->setFormatRows(true);
        $objPHPExcel->getActiveSheet()->getProtection()->setInsertColumns(true);
        $objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
        $objPHPExcel->getActiveSheet()->getProtection()->setInsertHyperlinks(true);
        $objPHPExcel->getActiveSheet()->getProtection()->setDeleteColumns(true);
        $objPHPExcel->getActiveSheet()->getProtection()->setDeleteRows(true);
        $objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
        $objPHPExcel->getActiveSheet()->getProtection()->setAutofilter(true);
        $objPHPExcel->getActiveSheet()->getProtection()->setObjects(true);
        $objPHPExcel->getActiveSheet()->getProtection()->setScenarios(true);
        $objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
        $objPHPExcel->getActiveSheet()->getProtection()->setPassword($ruc);
        // -------------------------------------------CUERPO---------------------------------------------	
       
        $objPHPExcel->getActiveSheet()->getStyle('A1:'.$columnas[$aux-1].'1')->applyFromArray(
            array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('argb' => 'FF000000'),
                    ),
                ),
            )
        );
        $objPHPExcel->getActiveSheet()->getStyle('A1:'.$columnas[$aux-1].'1')->applyFromArray(
            array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'FFFF00')
                )
            )
        );	
        $objPHPExcel->getActiveSheet()->getStyle('A1:'.$columnas[$aux-1].'1')->applyFromArray(
            array(
                'font' => array(
                    'size' =>$letra
                )
            )
        );	

        $objPHPExcel->getActiveSheet()->getStyle('A1:'.$columnas[$aux-1].'1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $i=0;
        $aux=0;
        foreach($array_campos as $key=>$value){
            foreach($value[$i] as $k=>$v){  
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue($columnas[$k].($aux+2),$v);
                $objPHPExcel->getActiveSheet()->getStyle($columnas[$k].($aux+2))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $objPHPExcel->getActiveSheet()->getStyle($columnas[$k].($aux+2))->applyFromArray(
                    array(
                        'font' => array(
                            'size' =>$letra
                        )
                    )
                );	
            } 
            $band=count($value[$i]);
            foreach($array_tiendas[$key] as $k=>$v){  
                
                foreach($v as $ke=>$va){ 
                    if($ke>=1){ 
                       $objPHPExcel->setActiveSheetIndex(0)->setCellValue($columnas[$band].($aux+2),$va);
                       $objPHPExcel->getActiveSheet()->getStyle($columnas[$band].($aux+2))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                       $objPHPExcel->getActiveSheet()->getStyle($columnas[$band].($aux+2))->applyFromArray(
                        array(
                            'font' => array(
                                'size' =>$letra
                            )
                        )
                    );	
                    } 
                    
                } 
                $band++;
            } 
            
            $i=$i+count($value); 
            $aux++;
           
        } 
        $styleThinBlackBorderOutline = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('argb' => 'FF000000'),),),);

        $objPHPExcel->getActiveSheet()->setTitle('Ventas-'.date("Ymd-His"));
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="archivo.xlsx"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        ob_end_clean();
        $objWriter->save('php://output');
        exit;
    }else{
        header('Location: '.BASE_URL.'comercial-stock');
    }
    
?>