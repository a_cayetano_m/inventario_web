<?php 
  ini_set('max_execution_time', 600);
  date_default_timezone_set ( "America/Lima" );
  function generarCSV($arreglo, $ruta, $delimitador, $encapsulador){
      $file_handle = fopen($ruta, 'w');
      
      foreach ($arreglo as $linea) {
        fputcsv($file_handle, $linea, $delimitador, $encapsulador);
      }
      rewind($file_handle);
      fclose($file_handle);

  }
  if(isset($_SESSION[CONFIG['session']['global']])){
      $ruc=$_SESSION[CONFIG['session']['global']]['USER']['RUC'];
      $ruta =date("YmdHis").".csv";
      $password =$ruc;

      /*
      $arreglo[0] = array("Nombre","Apellido","Animal","Fruto");
      $arreglo[1] = array("Juan","Juarez","Jirafa","Jicama");
      $arreglo[2] = array("Maria","Martinez","Mono","Mandarina");
      $arreglo[3] = array("Esperanza","Escobedo","Elefante","Elote");
      */
      $fechas=explode("-",$_GET['fechas']);
      $fechas_inicio=explode("/",trim($fechas[0]));
      $fechas_fin=explode("/",trim($fechas[1]));
      $fechas_inicio=$fechas_inicio[2].$fechas_inicio[1].$fechas_inicio[0];
      $fechas_fin=$fechas_fin[2].$fechas_fin[1].$fechas_fin[0];

      $tiendas=count($_SESSION[CONFIG['session']['global']]['TIENDAS']);
      $cast='INT';
      $prefix='VENTAS_';
      $campos='';
      $action='SUM';
      for($i=1;$i<=$tiendas; $i++){
          if($i==1){
              $campos=$campos.''.$cast.'('.$action.'('.$prefix.''.str_pad($i, 3, "0", STR_PAD_LEFT).')) as '.$prefix.''.str_pad($i, 3, "0", STR_PAD_LEFT).'';
          }else{
              $campos=$campos.','.$cast.'('.$action.'('.$prefix.''.str_pad($i, 3, "0", STR_PAD_LEFT).')) as '.$prefix.''.str_pad($i, 3, "0", STR_PAD_LEFT).'';
          }
      }

      $periodo_inicio='';
      $periodo_fin='';
      $periodos_generales=$_SESSION[CONFIG['session']['global']]['periodos_generales'];

      foreach($periodos_generales as $k=>$v){
          if($fechas_inicio>=$k && $fechas_inicio<date("Ymd",strtotime($k."+ 7 days"))){
              $periodo_inicio=$v;
          }
          if($fechas_fin>=$k && $fechas_fin<date("Ymd",strtotime($k."+ 7 days"))){
              $periodo_fin=$v;
          }
      }
      for($i=0;$i<=($periodo_fin-$periodo_inicio);$i++)
      {
          $periodos[]=$periodo_inicio+$i;
      }

      $periodos_anterior=array();
      foreach($periodos as $k=>$v){
          $valor=substr($v,0,4);
          $valor=(int)$valor;
          $valor=$valor-1;
          $valor=$valor.substr($v,4,2);
          $periodos_anterior[]=$valor;
      }
      $periodos=array_merge($periodos_anterior,$periodos);
      $sql_where='';
      foreach($periodos as $k=>$v){
          if($k==0){
              if(count($periodos)>1){
                  $sql_where=$sql_where. " AND (PERIODO='".$v."'";
              }else{
                  $sql_where=$sql_where. " AND PERIODO='".$v."'";
              } 
          }else{
              if(($k+1)==count($periodos)){
                  $sql_where=$sql_where. " OR PERIODO='".$v."')";
              }else{
                  $sql_where=$sql_where. " OR PERIODO='".$v."'";
              }
          }
          
      }


      $sql="SELECT MIN(RANGO_INICIAL) AS RANGO_INICIAL,MAX(RANGO_FINAL) AS RANGO_FINAL,GRUPO,CATEGORIA,INT(CODIGO_AS400) AS CODIGO_AS400,INT(CODIGO_SAP) AS CODIGO_SAP,DESCRIPCION,UNIDAD_BASE,ESTADO,INT(SUM(TOTAL_VENTAS)) as TOTAL_VENTAS,".$campos."
      FROM STLDTAPRV.PR002 WHERE PROVEEDOR='".$ruc."'";

      $order="  GROUP BY GRUPO,CATEGORIA,CODIGO_AS400,CODIGO_SAP,DESCRIPCION,UNIDAD_BASE,ESTADO ORDER BY CODIGO_AS400 ";
      $sql=$sql.$sql_where.$order;
      $data =$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar
      $instancia->disposeODBC();

      $cabeceras=DEFAULT_DATA['values']['exportar']['comercial']['ventas']['cabeceras_export'];
      foreach($_SESSION[CONFIG['session']['global']]['TIENDAS'] as $k=>$v){
          $cabeceras[]=utf8_encode(trim($v['OPNOM']));
      }

      $data_csv=array();
      foreach($data as $k=>$v){
        $data_csv[]=array_values($v);
      }


      generarCSV($data_csv, $ruta, $delimitador = ';', $encapsulador = '"');
      $filename=$ruta;
      if(isset($password) && isset($filename) && $password!='' && $filename!='' ) {
		$zip = new ZipArchive();
		$zip_file_name=date("YmdHis").".zip";
		$zip->open($zip_file_name, ZIPARCHIVE::CREATE);
		$zip->setPassword($password);
	 
		$stream_opts = [
		"ssl" => [
			"verify_peer"=>false,
			"verify_peer_name"=>false,
			]
		];  

		$content = file_get_contents('./Proveedores/Services/Folder/Reporte/csv/'.$filename,
			   false, stream_context_create($stream_opts));
        print_r($content);
        die();
		$zip->addFromString($filename, $content);
		$zip->setEncryptionName($filename, ZipArchive::EM_AES_256);
		
		$zip->close();
		header("Content-Description: File Transfer"); 
		header("Content-Type: application/octet-stream"); 
		header(
		"Content-Disposition: attachment; filename=\""
		. $zip_file_name . "\""); 
		readfile ($zip_file_name);
		//unlink($zip_file_name);
	}
  }
  
?>