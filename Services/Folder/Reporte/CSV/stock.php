<?php 
  ini_set('max_execution_time', 600);
  date_default_timezone_set ( "America/Lima" );
  function generarCSV($arreglo, $ruta, $delimitador, $encapsulador){
      $file_handle = fopen($ruta, 'w');
      
      foreach ($arreglo as $linea) {
        fputcsv($file_handle, $linea, $delimitador, $encapsulador);
      }
      rewind($file_handle);
      fclose($file_handle);

  }
if(isset($_SESSION[CONFIG['session']['global']])){
    $ruc=$_SESSION[CONFIG['session']['global']]['USER']['RUC'];
    $ruta ="./Reporte/CSV/".date("YmdHis").".csv";
    $password =$ruc;

    $tiendas=count($_SESSION[CONFIG['session']['global']]['TIENDAS']);
    $cast='INT';
    $prefix='STOCK_';
    $campos='';
    $action='';
    for($i=1;$i<=$tiendas; $i++){
        if($i==1){
            $campos=$campos.''.$cast.'('.$action.'('.$prefix.''.str_pad($i, 3, "0", STR_PAD_LEFT).')) as '.$prefix.''.str_pad($i, 3, "0", STR_PAD_LEFT).'';
        }else{
            $campos=$campos.','.$cast.'('.$action.'('.$prefix.''.str_pad($i, 3, "0", STR_PAD_LEFT).')) as '.$prefix.''.str_pad($i, 3, "0", STR_PAD_LEFT).'';
        }
    }     
    $sql="SELECT COMPRADOR,GRUPO,
    CATEGORIA,
    INT(CODIGO_AS400) AS CODIGO_AS400,
    INT(CODIGO_SAP) AS CODIGO_SAP,
    DESCRIPCION,
    UNIDAD_BASE,
    ABC,
    ESTADO,
    INT(TRANSITO_TOTAL) AS TRANSITO_TOTAL,
    INT(STOCK_TOTAL) AS STOCK_TOTAL,".$campos." FROM  STLDTAPRV.PR001 WHERE PROVEEDOR='".$ruc."'";

    $data =$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplaza
    $instancia->disposeODBC();

    $cabeceras=DEFAULT_DATA['values']['exportar']['comercial']['stock']['cabeceras_export'];
    foreach($_SESSION[CONFIG['session']['global']]['TIENDAS'] as $k=>$v){
        $cabeceras[]=utf8_encode(trim($v['OPNOM']));
    }

    $data_csv=array();
    foreach($data as $k=>$v){
        $data_csv[]=array_values($v);
    }

    generarCSV($data_csv, $ruta, $delimitador = ';', $encapsulador = '"');
    header("Location: https://tlmkt.tailoy.com.pe/zip/export.php?filename=".$ruta."&password=".$ruc."");

    unlink($zip_file_name);
  }
  
?>