<?php

namespace System\Core;

use System\Core\Router;
use System\Core\Helper;

class App
{
    public static function router($url)
    {
        try {
            $action = Router::getAction($url);
            $controller = str_replace('Controller', '', $action["controller"]);

            $method = $action["method"];
            if (!Helper::ValidateController($controller)) {
                $controller = 'ErrorPage';
                $path_controller = "App/Controller/" . "{$controller}/";
                $controller .= 'Controller';
            } else {
                $path_controller = "App/Controller/" . "{$controller}/";
                $controller .= 'Controller';
            }

            $path = str_replace('/', "\\", $path_controller . "{$controller}");
            $controller = new $path();

            if (!Helper::ValidateMethod($controller, $action["method"])) {
                $method = 'index';
            }
            $json = file_get_contents('php://input');
            // Convertir objeto en Array
            $input = (array)json_decode($json);
            $post = $_POST;
            $data = NULL;
            //$data = !empty($_POST) ? $post : NULL;
            if (!empty($post)) {
                $data = $post;
            } else {
                if (!empty($input)) {
                    $data = $input;
                }
            }

            if (!empty($data)) {
                $response = $controller->$method($data);
                echo $response;
            } else {
                $response = $controller->$method();
                echo $response;
            }
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }
}
