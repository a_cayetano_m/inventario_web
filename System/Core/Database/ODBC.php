<?php 

    namespace System\Core\Database;
    
    class ODBC{

        private $server="";
        private $user="";
        private $password="";
        private $db="";
    
        private $cn = null;
        private static $instancia = null;
    
        public function __construct($server,$user,$password,$db){
            $this->server=$server;
            $this->user=$user;
            $this->password=$password;
            $this->db=$db;
        } 
        // -------------------------------------------ODBC--------------------------------------------
        public function createConexionODBC(){
            if(!$this->cn)
            {	
                $this->disposeODBC($this->cn);	
            }
            
            $this->cn = odbc_connect($this->server,$this->user,$this->password);
        }
        public function getConexionODBC(){
            if( is_null($this->cn) ){
                $this->cn =$this->createConexionODBC();
            }
            return $this->cn;
        }
        public function executeQueryODBC($sql,$cn){// executeQuery update, delete
            $rpta = null;
            $Prepare = odbc_prepare($cn, $sql);
            if( !odbc_execute($Prepare))
                {	$rpta = "Error: Ha ocurrido un error a nivel de la base de datos. ***Descripcion: ".odbc_errormsg();
                    $invalidos = array("`", "'");
                    $rpta = str_replace($invalidos,"",$rpta);
                }
            else
                {$rpta = true;}
            return $rpta;	
        }
        public function disposeODBC(){
            if( $this->cn ) 
                {	
                    
                    odbc_close( $this->cn );
                    
                    $this->cn = null;
                    
                }
        }
        public function executeQueryShowODBC($sql,$cn){ // executeQuery show data
            $lista = array();
            $rs = odbc_exec( $cn, $sql);
            try {
                if($rs){
                    while( $fila = odbc_fetch_array($rs) )
                        {	$lista[] = $fila;}

                    odbc_free_result( $rs );
                }
            } catch (\Exception  $e) {
                print_r($e);
            }
            return $lista;
        } 
        public function isConnectedODBC(){ // isConnected
            $rpta = FALSE;
            if( $this->cn) $rpta = TRUE;
            return $rpta;
        } 
    }




?>