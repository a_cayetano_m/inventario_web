<?php 

    namespace System\Core\Database;
    
    class MYSQLI{

        private $server="";
        private $user="";
        private $password="";
        private $db="";
    
        private $cn = null;
    
        public function __construct($server,$user,$password,$db){
            $this->server=$server;
            $this->user=$user;
            $this->password=$password;
            $this->db=$db;
        } 
        public function createConexionMysqli(){
            
            $this->cn = new \mysqli($this->server,$this->user,$this->password,$this->db);
            //$mysqli = new mysqli("localhost","root","","pedidos");
            if ($this->cn->connect_errno) {
               print_r("conexion fallida");
            }
        }
        public function getConexionMysqli(){
            if(is_null($this->cn) ){
                $this->cn =$this->createConexionMysqli();
            }
            return $this->cn;
        }
        public function executeQueryMysqli($sql,$cn){
            $loaddata = $cn->query($sql);
            if (!$loaddata) {
                die('Could not load data from file into table: ' .$cn->error);
            }
        
        }
       


    }




?>