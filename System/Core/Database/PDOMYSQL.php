<?php 

    namespace System\Core\Database;
    
    class PDOMYSQL{

        private $server="";
        private $user="";
        private $password="";
        private $db="";
    
        private $cn = null;
    
        public function __construct($server,$user,$password,$db){
            $this->server=$server;
            $this->user=$user;
            $this->password=$password;
            $this->db=$db;
        } 
        public function createConexionPDO(){
            /*$connect = new PDO("mysql:host=localhost;dbname=pedidos;", "root", "", array(
                PDO::MYSQL_ATTR_LOCAL_INFILE => true,
            ));*/
            try{
                $this->cn=new \PDO('mysql:host='.$this->server.'; dbname='.$this->db.'',$this->user,$this->password,array(\PDO::MYSQL_ATTR_LOCAL_INFILE => true));
                $this->cn->setAttribute(\PDO::ATTR_ERRMODE,\PDO::ERRMODE_EXCEPTION);
                $this->cn->exec("SET CHARACTER SET utf8");
            }catch(\PDOException $e){
                echo "La linea de error en la conexion es: ".$e->getLine();
            }
        }
        public function getConexionPDO(){
            if(is_null($this->cn) ){
                $this->cn =$this->createConexionPDO();
            }
            return $this->cn;
        }
        public function executeQueryPDO($sql,$cn){
            
            if($sql!=""){
                //$query=$cn->prepare($sql);
                //try{
                    $cn->exec($sql);
                    return true;
               // }catch(\PDOException $e){
                    //return false;
                   // return $e->getLine();
                //}
            }else{
                return false;
            }
        }
        public function executeQueryShowPDO($sql="",$cn,$value=array()){
            if($sql!=""){
                $query=$cn->prepare($sql);
                $query->execute($value);
                
                if($query->rowCount()==1){
                    return $query->fetch(\PDO::FETCH_ASSOC);
                }else{
                    return $query->fetchAll(\PDO::FETCH_ASSOC);
                }
            }else{
                return 0;
            }
        }
        public function executeQueryCountPDO($sql=""){
            if($sql!=""){
                $query=$this->cn->prepare($sql);
                $query->execute();
                return $query->rowCount();
            }else{
                return 0;
            }
        }
        public function executeQueryUpdatePDO($sql="",$value=array()){
            if($sql!=""){
                $query=$this->cn->prepare($sql);
                $query->execute($value);
                $query=null;
                return true;
            }else{
                return false;   
            }
        }
        public function executeQueryInsert($sql="",$value=array(),$table,$parameters){
            $query="";
            if($sql!="" && $parameters!=""){
                $query=$this->cn->prepare($sql);
                $array=array();
                foreach($value as $key=>$values){
                    if(in_array($table."_".$key,$parameters)){
                        $query->bindparam(":".$table."_".$key,$value[$key]);
                    }
                    
                }
                
                $query->execute();
                $query=null;
                return true;
            }else{
                return false;
            }
        }
        
        public function closeConnection(){
            $this->cn=null;
        }
    
        // FUNCIONES ODBC

        public function executeQueryShowODBC($sql,$cn){
            if($sql!=""){
                $query=$cn->prepare($sql);
                $query->execute();
                
                return $query->fetchAll(\PDO::FETCH_ASSOC);
            }else{
                return 0;
            }
        }
        public function executeQueryODBC($sql,$cn){
            if($sql!=""){
                $query=$cn->prepare($sql);
                $query->execute();
                return true;
            }else{
                return false;
            }
        }





    }




?>