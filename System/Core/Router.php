<?php
namespace System\Core;

class Router{

  private static $route=[];

  public function __construct(){

  }
  public static function add($route,$controller_method,$params=array()){
    
    $array_route=explode("@",$controller_method);
    if(empty($array_route[1])){
      $array_route[1]='index';
    }
    self::$route[$route]=["controller"=>$array_route[0],"method"=>$array_route[1]];
  }
  public static function getAction($route){
    $array_url=explode('/',$route);
    $uri=$route;
    
    if(array_key_exists($route,static::$route)){
      return self::$route[$route];
    }
    foreach (static::$route as $key => $val)
		{
			// Convert wild-cards to RegEx
      $key = str_replace(':any', '.+', str_replace(':num', '[0-9]+', $key));
      $dato=$val['controller']."/".$val['method'];
			if (preg_match('#^'.$key.'$#', $route))
			{
				// Do we have a back-reference?
				if (strpos($dato, '$') !== FALSE AND strpos($key, '(') !== FALSE)
				{
          $dato = preg_replace('#^'.$key.'$#', $dato, $route);
        }
        $dato= explode('/',$dato);
        return array("controller"=>$dato[0],"method"=>$dato[1]);
			
			}
		}
    
    /* VERSION ANTIGUA SIN PARAMETROS EN URL
    if(array_key_exists($route,static::$route)){
      return self::$route[$route];
    }
    else{
      throw new \Exception("The route $route was not found");
    }*/
  }

}