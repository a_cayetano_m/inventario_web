<?php
namespace System\Core;

use System\Core\Database\PDOMYSQL;
use System\Core\Database\ODBC;
use System\Core\Database\MYSQLI;
//use App\Model\Conexion\Conexion;

class Model
{

  public function __construct()
  {
    
  }
  public static function getIntancia($type){
    if($type=='pdomysql'){
      return new PDOMYSQL(CONFIG['database'][$type]['server'],CONFIG['database'][$type]['user'],CONFIG['database'][$type]['password'],CONFIG['database'][$type]['db']);
    }else if($type=='odbc'){
      return new ODBC(CONFIG['database'][$type]['server'],CONFIG['database'][$type]['user'],CONFIG['database'][$type]['password'],CONFIG['database'][$type]['db']);
    }else if($type=='mysqli'){
      return new MYSQLI(CONFIG['database'][$type]['server'],CONFIG['database'][$type]['user'],CONFIG['database'][$type]['password'],CONFIG['database'][$type]['db']);
    }
   
  }


}