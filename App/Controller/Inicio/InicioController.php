<?php
    namespace App\Controller\Inicio;

    use System\Core\Controller;
    use App\Model\Session\Session;
    use App\Helper\Helper as HelperMethod;
    use System\Core\Helper as HelperCore;

    class InicioController extends Controller{
        
        public function __construct(){
            $this->session=new Session();
            $this->session->init();
        }
        public function index(){
            if(!$this->session->active()){
                header("Location: ".BASE_URL."login"); 
            }else{
                $this->render(__CLASS__,"Inicio");
            }
        }
    }


?>