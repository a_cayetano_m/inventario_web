<?php

namespace App\Controller\Inventario;

use System\Core\Controller;
use App\Model\API\API;
use App\Helper\Helper as HelperMethod;
use System\Core\Helper as HelperCore;
use App\Model\Session\Session;
use App\Model\Files\Files;
use App\Model\INV16\INV16DA;
use App\Model\INV07\INV07DA;
use App\Model\INV17\INV17DA;
use App\Model\INV18\INV18DA;
use App\Model\INV19\INV19DA;
use App\Model\INV20\INV20DA;
use App\Model\INV05\INV05DA;
use App\Model\INV06\INV06DA;
use App\Model\INV00\INV00DA;
use App\Model\INV04\INV04DA;
use App\Model\OP69\OP69DA;
use App\Model\OP22\OP22DA;
use App\Model\PRODUCTOS\PRODUCTOSDA;
use App\Model\INV00\INV00;
use App\Model\LOCACION\LOCACIONDA;

class InventarioController extends Controller
{

    protected $session;
    protected $session_global;
    public function __construct()
    {
        $this->session = new Session();
        $this->session->init();
        date_default_timezone_set('America/Lima');
    }
    public function index()
    { //Crear Inventario
        if (!$this->session->active()) {
            header("Location: " . BASE_URL . "login");
        }
        $suc = ($_SESSION[CONFIG['session']['global']]['USER']['SUCURSAL']);
        $INV19DA = new INV19DA();
        $OP69DA = new OP69DA();
        $LOCACIONDA = new LOCACIONDA();
        $inventario = $INV19DA->getInventario($suc);
        $ifobibi = $LOCACIONDA->obtieneBiblioteca($suc);
        $ifoinv = $OP69DA->get_info_last_invt(trim($ifobibi[0]['BIBLIOTECA']));

        $dUser = array('Inventario' => $inventario, "infoinv" => $ifoinv);
        $this->render(__CLASS__, 'CreaInventario', $dUser);
    }

    public function index2()
    { //UsuarioZona
        if (!$this->session->active()) {
            header("Location: " . BASE_URL . "login");
            //$suc=trim($request['SUCURSAL']);
        }
        $suc = ($_SESSION[CONFIG['session']['global']]['USER']['SUCURSAL']);
        $INV18DA = new INV18DA();
        $INV17DA = new INV17DA();
        $INV19DA = new INV19DA();
        $INV20DA = new INV20DA();
        $INV04DA = new INV04DA();

        $qry1 = $INV19DA->getInventario($suc);
        $inv = trim($qry1[0]["INVENTARIO"]);

        $calculo = $INV18DA->getUsuarios2($suc, $inv);
        //die();
        foreach ($calculo as $key => $value) {
            $usuario = trim($value['USERNAME']);
            $leido = $INV04DA->muebleContadoInv($usuario, $suc, $inv);
            $leidoLinea = $INV04DA->lineaContadoInv($usuario, $suc, $inv);
            $lineaLeido = trim($leidoLinea[0]['TOTAL']);
            $muebleLeido = trim($leido[0]['TOTAL']);
            if ($lineaLeido != "0") {
                $inicio = $INV04DA->horaInicioInv($usuario, $suc, $inv);
                $fin = $INV04DA->horaUltimoInv($usuario, $suc, $inv);
                $horaInicio = trim($inicio[0]['FCREACION']);
                $horaFin = trim($fin[0]['FCREACION']);

                //calcular el estado
                $zonaTotal = $INV17DA->getZonas($suc, $inv, $usuario);
                $muebleTotalZonas = $INV04DA->totalMueble($suc, $inv, $zonaTotal);
                $mueTot = $muebleTotalZonas[0]["TOTAL"];
                $muebleLeidoZonas = $INV04DA->totalMuebleLeido($suc, $inv, $zonaTotal);
                $mueLei = $muebleLeidoZonas[0]["TOTAL"];
                if ($mueLei < $mueTot) {
                    $estado = 1;
                } else {
                    $estado = 2;
                }
                //actualizo los datos de la tabla zona - usuarios
                $INV18DA->update($suc, $usuario, $inv, $muebleLeido, $horaInicio, $horaFin, $estado, $lineaLeido);
            }
        }

        //die();
        $qry2 = $INV18DA->getUsuarios($suc, $inv);
        $locaciones = $INV20DA->getLocationInv($suc, $inv);
        //print_r($locaciones);
        //die();
        $dUser = array(
            'Inventario' => $qry2,
            'Locaciones' => $locaciones
        );

        $this->render(__CLASS__, 'UsuarioZona', $dUser);
    }
    public function index3()
    { //Seguimiento Inventario
        if (!$this->session->active()) {
            header("Location: " . BASE_URL . "login");
        }

        $suc = ($_SESSION[CONFIG['session']['global']]['USER']['SUCURSAL']);
        $INV20DA = new INV20DA();
        $INV19DA = new INV19DA();
        $INV04DA = new INV04DA();
        $INV06DA = new INV06DA();

        $qry1 = $INV19DA->getInventario($suc);
        $inv = trim($qry1[0]["INVENTARIO"]);
        //print_r($inv);
        //die();
        $zonas = $INV20DA->segMueble($suc, $inv);

        foreach ($zonas as $key => $value) {
            $tiempoFormato = '';
            $zona = trim($value['ZONA']);
            $muebleTotal = trim($value['MUEBLETOT']);
            $lineasTotal = trim($value['LINEASTOT']);
            //obtengo los muebles leidos de la zona
            $contadoM = $INV04DA->muebleContado($zona, $suc, $inv);
            $muebleContado = trim($contadoM[0]['TOTAL']);
            //evaluo el avance de la zona
            $avance = round((($muebleContado * 100) / $muebleTotal), 2);

            //obtengo el estado del avance
            switch (true) {
                case ($avance == 0):
                    $estado = 0;
                    break;
                case ($avance > 0 && $avance < 50):
                    $estado = 1;
                    break;
                case ($avance > 49 && $avance < 100):
                    $estado = 2;
                    break;
                case ($avance = 100):
                    $estado = 3;
                    break;
            }

            //obtengo las lineas contadas
            $contadoL = $INV04DA->lineaContado($zona, $suc, $inv);
            $lineaContado = trim($contadoL[0]['TOTAL']);
            if ($lineaContado > 0 && $estado == 0) {
                $estado = 1;
            }
            //obtengo la hora inicio
            $horaI = $INV04DA->horaInicio($zona, $suc, $inv);
            if (empty($horaI)) {
                $horaInicio = 0;
            } else {
                $horaInicio = trim($horaI[0]['FCREACION']);
            }

            if ($horaInicio == 0) {
                $tiempoTra = 0;
                $estimadoTer = 0;
                $invantariadores = 0;
            } else {

                if ($estado != "3") {
                    //calculo el tiempo transcurrido
                    $fechaHoraActual = date('Y-m-d H:i:s');
                    $timestamp1 = strtotime($horaInicio);
                    $timestamp2 = strtotime($fechaHoraActual);
                    $diferenciaSegundos = $timestamp2 - $timestamp1;
                    $tiempoTra = floor($diferenciaSegundos / 3600);
                    $horas = floor($diferenciaSegundos / 3600);
                    $minutos = floor(($diferenciaSegundos % 3600) / 60);
                    $tiempoFormato = sprintf('%02d:%02d', $horas, $minutos);
                    //calcular el estimado de termino
                    if ($tiempoTra > 0) {
                        $lineasPend = $lineasTotal - $lineaContado;
                        $velocidadLineasPorHora = $lineaContado / $tiempoTra;
                        $estimadoHorasRestantes = ($lineasPend / $velocidadLineasPorHora);

                        $horasEstimadas = floor($estimadoHorasRestantes);
                        $minutosEstimados = ($estimadoHorasRestantes - $horasEstimadas) * 60;
                        $tiempoEstimado = sprintf("%d:%02d", $horasEstimadas, $minutosEstimados);
                        $estimadoTer = $tiempoEstimado;
                    } else {
                        $estimadoTer = 0;
                    }

                    //obtener los inventariadores activos
                    $contadores = $INV04DA->invActivos($zona, $suc, $inv);
                    $invantariadores = trim($contadores[0]['TOTAL']);
                } else {
                    //calculo el tiempo transcurrido
                    $horaF = $INV06DA->horaFinal($suc, $zona);
                    $horaFinal = trim($horaF[0]['FCREACION']);
                    $timestamp1 = strtotime($horaInicio);
                    $timestamp2 = strtotime($horaFinal);
                    $diferenciaSegundos = $timestamp2 - $timestamp1;
                    $tiempoTra = floor($diferenciaSegundos / 3600);
                    $horas = floor($diferenciaSegundos / 3600);
                    $minutos = floor(($diferenciaSegundos % 3600) / 60);
                    $tiempoFormato = sprintf('%02d:%02d', $horas, $minutos);
                    //al haber terimnado esa zona ya no necesito calcular los 2 ultimos datos
                    $estimadoTer = 0;
                    $invantariadores = 0;
                }
            }
            $INV20DA->update($suc, $inv, $zona, $muebleContado, $avance, $estado, $lineaContado, $horaInicio, $tiempoFormato, $estimadoTer, $invantariadores);
        }
        $qry2 = $INV20DA->getData($suc, $inv);
        $qry3 = $INV20DA->avanceTotal($suc, $inv);
        $muebleTotal = trim($qry3[0]['MUEBLETOT']);
        $muebleContado = trim($qry3[0]['MUEBLECON']);
        $avanceTotal = trim($qry3[0]['AVANCE']);
        $avanceTotal = round($avanceTotal, 2);
        $horaTotal = trim($qry3[0]['FCREACION']);
        $lineasTotales = trim($qry3[0]['LINEASTOT']);
        $lineasContadas = trim($qry3[0]['LINEASCON']);
        if ($avanceTotal < 100 && $avanceTotal > 0) {
            $fechaHoraActual = date('Y-m-d H:i:s');
            $timestamp1 = strtotime($horaTotal);
            $timestamp2 = strtotime($fechaHoraActual);
            $diferenciaSegundos = $timestamp2 - $timestamp1;
            $tiempoTra = floor($diferenciaSegundos / 3600);
            $horas = floor($diferenciaSegundos / 3600);
            $minutos = floor(($diferenciaSegundos % 3600) / 60);
            $tiempoFormato2 = sprintf('%02d:%02d', $horas, $minutos);
            //calcular el estimado de termino
            if ($tiempoTra > 0) {
                $lineasPend = $lineasTotales - $lineasContadas;
                $velocidadLineasPorHora = $lineaContado / $tiempoTra;
                $estimadoHorasRestantes = ($lineasPend / $velocidadLineasPorHora);

                $horasEstimadas = floor($estimadoHorasRestantes);
                $minutosEstimados = ($estimadoHorasRestantes - $horasEstimadas) * 60;
                $tiempoEstimado = sprintf("%d:%02d", $horasEstimadas, $minutosEstimados);
                $estimadoTer2 = $tiempoEstimado;
            }
        } else {
            $horaFinal = $INV06DA->horaFinal2($suc);
            if (count($horaFinal) > 0) {
                $final = trim($horaFinal[0]['FCREACION']);
                $timestamp1 = strtotime($horaTotal);
                $timestamp2 = strtotime($final);
                $diferenciaSegundos = $timestamp2 - $timestamp1;
                $tiempoTra = floor($diferenciaSegundos / 3600);
                $horas = floor($diferenciaSegundos / 3600);
                $minutos = floor(($diferenciaSegundos % 3600) / 60);
                $tiempoFormato2 = sprintf('%02d:%02d', $horas, $minutos);
                //al haber terimnado esa zona ya no necesito calcular los 2 ultimos datos
                $estimadoTer2 = '00:00';
            } else {
                $estimadoTer2 = '00:00';
                $tiempoFormato2 = '00:00';
            }
        }
        $horaTotal = substr($horaTotal, 11, 8);
        $tot = array(
            'muebleTotal' => $muebleTotal,
            'muebleContado' => $muebleContado,
            'avanceTotal' => $avanceTotal,
            'horaTotal' => $horaTotal,
            'lineasTotales' => $lineasTotales,
            'lineasContadas' => $lineasContadas,
            'estimadoTer2' => $estimadoTer2,
            'tiempoFormato2' => $tiempoFormato2
        );

        //print_r($qry2);
        //die();
        $dUser = array(
            'Inventario' => $qry2,
            'Total' => $tot
        );

        $this->render(__CLASS__, 'SeguimientoInventario', $dUser);
    }
    public function index4()
    { //Locaciones Pendientes
        if (!$this->session->active()) {
            header("Location: " . BASE_URL . "login");
        }

        $suc = ($_SESSION[CONFIG['session']['global']]['USER']['SUCURSAL']);
        $INV19DA = new INV19DA();

        $qry1 = $INV19DA->getInventario($suc);
        if (count($qry1) > 0) {
            $inv = trim($qry1[0]["INVENTARIO"]);
            $INV04DA = new INV04DA();
            $qry2 = $INV04DA->locationsPend($suc, $inv);
            //print_r($qry2);
            //die();
            $dUser = array('Inventario' => $qry2);
            $this->render(__CLASS__, 'LocacionesPendientes', $dUser);
        } else {
            $dUser = array('Inventario' => $qry1);
            $this->render(__CLASS__, 'LocacionesPendientes', $dUser);
        }
    }
    public function index5()
    { //Sesion Usuarios
        if (!$this->session->active()) {
            header("Location: " . BASE_URL . "login");
        }
        $suc = ($_SESSION[CONFIG['session']['global']]['USER']['SUCURSAL']);
        $INV19DA = new INV19DA();

        $qry1 = $INV19DA->getInventario($suc);
        if (count($qry1) > 0) {
            $INV00DA = new INV00DA();
            $qry2 = $INV00DA->getUserActive($suc);
            //print_r($qry2);
            //die();
            $dUser = array('Inventario' => $qry2);
            $this->render(__CLASS__, 'SesionUsuarios', $dUser);
        } else {
            $dUser = array('Inventario' => $qry1);
            $this->render(__CLASS__, 'SesionUsuarios', $dUser);
        }
    }
    public function index6()
    { //Segundo Inventario 
        if (!$this->session->active()) {
            header("Location: " . BASE_URL . "login");
        }
        $suc = ($_SESSION[CONFIG['session']['global']]['USER']['SUCURSAL']);
        $INV19DA = new INV19DA();
        $INV16DA = new INV16DA();
        $INV05DA = new INV05DA();
        $OP69DA = new OP69DA();
        $inventario = $INV19DA->getInventario($suc);
        $inv = $inventario[0]['INVENTARIO'];
        if (strlen($suc) > 2) {
            $bib = "TIEND" . $suc;
        } else {
            $bib = "TIENDA" . str_pad($suc, 2, '0', STR_PAD_LEFT);
        }

        $valores = $INV16DA->getValues($suc);
        $contadores = $INV05DA->getContadores($suc);
        $as = $OP69DA->getStatus($bib, $inv);
        $dUser = array(
            'Inventario' => $as,
            'Datos' => $valores,
            'Contadores' => $contadores
        );
        $this->render(__CLASS__, 'SegundoConteo', $dUser);
    }

    public function iniciaInventario($request)
    {
        if (!$this->session->active()) {
            header("Location: " . BASE_URL . "login");
        }

        $OP69DA = new OP69DA();
        $INV04DA = new INV04DA();
        $INV05DA = new INV05DA();
        $INV18DA = new INV18DA();
        $INV19DA = new INV19DA();
        $INV20DA = new INV20DA();
        $INV00DA = new INV00DA();

        $inventario = trim($request['inventario']);
        $contadores = trim($request['contadores']);
        $suc = ($_SESSION[CONFIG['session']['global']]['USER']['SUCURSAL']);

        if (strlen($suc) > 2) {
            $biblioteca = "TIEND" . $suc;
        } else {
            $biblioteca = "TIENDA" . str_pad($suc, 2, '0', STR_PAD_LEFT);
        }
        $existe = $INV19DA->existInv($suc, $inventario);
        if (count($existe) < 1) {
            $valida = $OP69DA->validateInv($biblioteca, $inventario);

            if (count($valida) > 0) {
                //borrar usuarios para inventario
                $usrinv = $INV00DA->userInv($suc);
                if (count($usrinv) > 0) {
                    $INV00DA->deleteUsers($suc);
                }
                for ($i = 1; $i <= $contadores; $i++) {
                    $usuario = "INV" . str_pad($suc, 3, '0', STR_PAD_LEFT) . str_pad($i, 2, '0', STR_PAD_LEFT);
                    $id = trim($suc . str_pad($i, 2, '0', STR_PAD_LEFT));
                    $INV00DA->createUsers($suc, $usuario, $id);
                    $INV18DA->createUsersLocations($suc, $usuario, $inventario);
                }
                $INV19DA->createInventario($suc, $inventario);
                $INV05DA->createConteo1($suc, $inventario);
                $INV05DA->createConteo1_2($suc, $inventario);
                $INV05DA->createConteo1_3($suc, $inventario);
                $INV04DA->Create($suc, $inventario);
                $INV20DA->create($suc, $inventario);
                print_r(json_encode(array("validado" => true, 'tipo' => '1')));
            } else {
                print_r(json_encode(array("validado" => false, 'tipo' => '2')));
            }
        } else {
            print_r(json_encode(array("validado" => false, 'tipo' => '3')));
        }
    }
    public function cierraSesion($request)
    {
        if (!$this->session->active()) {
            header("Location: " . BASE_URL . "login");
        }
        $INV00DA = new INV00DA();

        $usuario = trim($request['usuario']);
        $INV00DA->refreshSession($usuario);
        print_r(json_encode(array("validado" => true, 'tipo' => '1')));
    }

    public function actualizaUsuario($request)
    {
        if (!$this->session->active()) {
            header("Location: " . BASE_URL . "login");
        }
        $suc = ($_SESSION[CONFIG['session']['global']]['USER']['SUCURSAL']);
        $INV18DA = new INV18DA();
        $INV17DA = new INV17DA();
        $INV19DA = new INV19DA();
        $INV00DA = new INV00DA();

        $qry1 = $INV19DA->getInventario($suc);
        $inv = trim($qry1[0]["INVENTARIO"]);

        $usuario = trim($request['usuario']);
        $nombre = trim($request['nombre']);
        $cargo = trim($request['cargo']);
        $INV00DA->updateName($suc, $usuario, $nombre, $cargo);

        unset($request['usuario']);
        unset($request['nombre']);
        unset($request['cargo']);

        foreach ($request as $key => $value) {
            if ($value != "") {
                $zon = trim(strtoupper($key));

                $existe = $INV18DA->exist($suc, $inv, $usuario, $zon);
                if (count($existe) < 1) {
                    $INV18DA->agregaZona($suc, $usuario, $inv, $zon);
                    $INV17DA->crea($suc, $usuario, $inv, $zon);
                }
            }/*else{
                    $zon = trim(strtoupper($key));
                    $INV18DA->quitaZona($suc,$usuario,$inv,$zon); 
                    $INV17DA->elimina($suc,$usuario,$inv,$zon);
                }*/
        }
        print_r(json_encode(array("validado" => true, 'tipo' => '1')));
    }

    public function segundoConteo($request)
    {
        if (!$this->session->active()) {
            header("Location: " . BASE_URL . "login");
        }
        $suc = ($_SESSION[CONFIG['session']['global']]['USER']['SUCURSAL']);
        $monto = trim($request['monto']);
        if (strlen($suc) > 2) {
            $bib = "TIEND" . $suc;
        } else {
            $bib = "TIENDA" . str_pad($suc, 2, '0', STR_PAD_LEFT);
        }
        $INV19DA = new INV19DA();
        $qry1 = $INV19DA->getInventario($suc);
        $inv = trim($qry1[0]["INVENTARIO"]);

        //CARGO VALORIZADO DE LAS DIFERENCIAS
        $alm = 'PICQ';
        $INV16DA = new INV16DA();
        $INV16DA->Create($bib, $suc, $alm, $monto, $inv);
        $valores = $INV16DA->getValues($suc);
        //CARGO TABLA PARA SEGUNDO CONTEO
        $INV07DA = new INV07DA();
        $INV07DA->Create1($suc);

        //ACTUALIZO EL TURNO DEL CONTEO
        $INV05DA = new INV05DA();
        $INV05DA->resetConteo($suc);
        $min = $valores[0]['MINIMO'];
        $max = $valores[0]['MAXIMO'];
        $INV05DA->updateConteo2($suc, $min, $max);
        $contadores = $INV05DA->getContadores($suc);

        $OP69DA = new OP69DA();
        $inventario = $INV19DA->getInventario($suc);

        print_r(json_encode(array("validado" => true, 'tipo' => '1')));
    }

    public function cargaContadores($request)
    {
        if (!$this->session->active()) {
            header("Location: " . BASE_URL . "login");
        }
        $suc = ($_SESSION[CONFIG['session']['global']]['USER']['SUCURSAL']);
        $contadores = trim($request['monto']);

        //CARGO VALORIZADO DE LAS DIFERENCIAS
        $alm = 'PICQ';
        $INV16DA = new INV16DA();
        $valores = $INV16DA->getValues($suc);

        //ACTUALIZO EL TURNO DEL CONTEO
        $INV05DA = new INV05DA();
        $INV05DA->updateContadores($suc, $contadores);
        $contadores = $INV05DA->getContadores($suc);
        $INV19DA = new INV19DA();
        $OP69DA = new OP69DA();
        $inventario = $INV19DA->getInventario($suc);
        $inv = $inventario[0]['INVENTARIO'];
        print_r(json_encode(array("validado" => true, 'tipo' => '1')));
    }

    public function locaciones()
    {
        if (!$this->session->active()) {
            header("Location: " . BASE_URL . "login");
        }
        $suc = ($_SESSION[CONFIG['session']['global']]['USER']['SUCURSAL']);
        $INV19DA = new INV19DA();
        $INV16DA = new INV16DA();
        $INV05DA = new INV05DA();
        $OP69DA = new OP69DA();
        $inventario = $INV19DA->getInventario($suc);
        $inv = $inventario[0]['INVENTARIO'];
        if (strlen($suc) > 2) {
            $bib = "TIEND" . $suc;
        } else {
            $bib = "TIENDA" . str_pad($suc, 2, '0', STR_PAD_LEFT);
        }

        $valores = $INV16DA->getValues($suc);
        $contadores = $INV05DA->getContadores($suc);
        $as = $OP69DA->getStatus($bib, $inv);
        $dUser = array(
            'Inventario' => $as,
            'Datos' => $valores,
            'Contadores' => $contadores
        );
        $this->render(__CLASS__, 'SegundoConteo', $dUser);
    }
}
