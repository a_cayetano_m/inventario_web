<?php
    namespace App\Controller\Locacion;

    use System\Core\Controller;
    use App\Model\API\API;
    use App\Helper\Helper as HelperMethod;
    use System\Core\Helper as HelperCore;
    use App\Model\Session\Session;
    use App\Model\Files\Files;
    use App\Model\INV16\INV16DA;
    use App\Model\INV07\INV07DA;
    use App\Model\INV05\INV05DA;
    use App\Model\INV04\INV04DA;
    use App\Model\OP69\OP69DA;
    use App\Model\PRODUCTOS\PRODUCTOSDA;

    class LocacionController extends Controller{

        public function __construct(){
            $this->session=new Session();
            $this->session->init();
            $this->Files=new Files();
			date_default_timezone_set ( 'America/Lima' );
        }
        public function index(){
            if(!$this->session->active()){
                header("Location: ".BASE_URL."login");
            }

            $OP69DA = NEW OP69DA();
            $bib = 'TIENDA46';
            $sql = $OP69DA->getInventario($bib);

            $dUser=array('Inventario'=>$sql);

            $this->render(__CLASS__,'IniciarLocacion',$dUser);
        }

        public function EnviarLocaciones(){
            if(!$this->session->active()){
                header("Location: ".BASE_URL."login");
            }

            //CARGO VALORIZADO DE LAS DIFERENCIAS
            $bib = 'TIENDA11';
            $suc = 11;
            $alm = 'TSJM';
            $INV16DA = NEW INV16DA();
            $INV16DA->Create($bib,$suc,$alm);

            //CARGO TABLA PARA SEGUNDO CONTEO
            $INV07DA = NEW INV07DA();
            $INV07DA->Create1($suc);
            $INV07DA->Create2($suc);

            //ACTUALIZO EL TURNO DEL CONTEO
            $INV05DA = NEW INV05DA();
            $INV05DA->resetConteo($suc);
            $INV05DA->updateConteo2($suc);

            $OP69DA = NEW OP69DA();
            $bib = 'TIENDA11';
            $sql = $OP69DA->getInventario($bib);

            $dUser=array('Inventario'=>$sql);

            $this->render(__CLASS__,'IniciarLocacion',$dUser);
        }

        public function CargarLocacion(){
            if(!$this->session->active()){
                header("Location: ".BASE_URL."login");
            }

           
            //CARGO VALORIZADO DE LAS DIFERENCIAS
            $suc = 58;
            $INV04DA = NEW INV04DA();
            $INV04DA->Create($suc);

            $OP69DA = NEW OP69DA();
            $bib = 'TIENDA58';
            $sql = $OP69DA->getInventario($bib);

            $dUser=array('Inventario'=>$sql);

            $this->render(__CLASS__,'IniciarLocacion',$dUser);
        }
    }



?>