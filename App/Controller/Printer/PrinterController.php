<?php
    namespace App\Controller\Printer;

    use System\Core\Controller;
    use App\Helper\Helper as HelperMethod;
    use System\Core\Helper as HelperCore;
    use App\Model\Session\Session;
    class PrinterController extends Controller{

        protected $session;
        protected $session_global;
        public function __construct(){
			date_default_timezone_set ( 'America/Lima' );
        }
        public function index(){
        }
        public function printLocation($request){
            if(DEFAULT_DATA['Auth']['token']==$request['TOKEN']){
               
                $location=$request['LOCATION'];
                $sucursal=$request['SUCURSAL'];
                $myfile = fopen("//172.16.0.5/vtxt/INVTIENDA".$sucursal.".txt", "w");
                $txt = "^XA\n^FO190,10^BY4,4,4^BCN,140,,,,^ADN,80,55^FD".$location."^FS\n^XZ\n";
                fwrite($myfile, $txt);
                fclose($myfile);
                $response=array(
                    'MSG'=>"Impresión realizada ",
                    'VALIDATE'=>'0',
                );
                print_r(json_encode($response));
            }
        }
        public function printZoneLocation($request){
            if(DEFAULT_DATA['Auth']['token']==$request['TOKEN']){
                $location=explode(",",$request['LOCATION']);
                
                $sucursal=$request['SUCURSAL'];
                
                if(count($location)==1){
                    foreach($location as $k=>$v){
                        $txt = "^XA\n^FO190,10^BY4,4,4^BCN,140,,,,^ADN,80,55^FD".$v."^FS\n^XZ\n";
                    }
                }else{
                    $txt="";
                    foreach($location as $k=>$v){
                        $txt = $txt."^XA\n^FO190,10^BY4,4,4^BCN,140,,,,^ADN,80,55^FD".$v."^FS\n^XZ\n\n";
                    }
                }
                $myfile = fopen("//172.16.0.5/vtxt/INVTIENDA".$sucursal.".txt", "w");
                fwrite($myfile, $txt);
                fclose($myfile);
                $response=array(
                    'MSG'=>"Impresión realizada ",
                    'VALIDATE'=>'0',
                );
                print_r(json_encode($response));
            }
            
           
            
            
        }

    }

?>