<?php
    namespace App\Controller\Api;
    
    use System\Core\Controller;
    use App\Helper\Helper as HelperMethod;
    use System\Core\Helper as HelperCore;
    use App\Model\Session\Session;
    use App\Model\INV00\INV00;
    use App\Model\INV00\INV00DA;
    use App\Model\INV01\INV01;
    use App\Model\INV01\INV01DA;
    use App\Model\INV02\INV02;
    use App\Model\INV02\INV02DA;
    use App\Model\PRODUCTOS\PRODUCTOSDA;
    use App\Model\INV03\INV03;
    use App\Model\INV03\INV03DA;
    use App\Model\INV04\INV04DA;
    use App\Model\INV05\INV05DA;
    use App\Model\INV06\INV06;
    use App\Model\INV06\INV06DA;
	use App\Model\INV24\INV24DA;
    use App\Model\INV07\INV07DA;
    use App\Model\INV19\INV19DA;
    use App\Model\OP22\OP22DA;
    use App\Model\OP01\OP01DA;
    use App\Model\LOCACION\LOCACION;
    use App\Model\LOCACION\LOCACIONDA;    
    class ApiController extends Controller{
        
        protected $session;
        protected $session_global;
        public function __construct(){
			date_default_timezone_set ( 'America/Lima' );
        }
        public function index(){
        }
        public function getAuth($request){
            if(DEFAULT_DATA['Auth']['token']==$request['TOKEN']){
                $INV00DA=new INV00DA();
                $INV00=new INV00();
                $INV01DA=new INV01DA();
                $INV00DA=new INV00DA();
                $INV05DA=new INV05DA();

                $INV00->USERNAME=$request['USERNAME'];
                $INV00->PASSWORD=$request['PASSWORD'];

                $user=$INV00DA->getUser($INV00);
                $locaciones=$INV01DA->getInfoLocaciones();
                $a_zonas=trim($locaciones[0]['VALOR']);
                $a_muebles=trim($locaciones[1]['VALOR']);
                $a_nivel=trim($locaciones[2]['VALOR']);
                $a_separacion=trim($locaciones[3]['VALOR']);

                if(count($user)>0){
                    if($user[0]['TYPE']==1 || $user[0]['TYPE']==2){ //USUARIO DE TIPO INVENTARIO
                        $user=$INV00DA->getUser($INV00);
                    }else{
                        $user=$INV00DA->getUserComplete($INV00);
                    }
                    $conteos=$INV05DA->getConteos($user[0]['SUCURSAL']);
                    if(count($conteos)>0){
                        $conteos=$conteos[0]['CONTEO'];
                    }
                    $response=array(
                        'MSG'=>'Autorizado',
                        'VALIDATE'=>'1',
                        'ID'=>$user[0]['COD'],
                        'USERNAME'=>$user[0]['USERNAME'],
                        'NAME'=>$user[0]['NAME'],
                        'LASTNAME'=>$user[0]['LASTNAME'],
                        'EMAIL'=>$user[0]['EMAIL'],
                        'TYPE'=> $user[0]['TYPE'],
                        'SUCURSAL'=> $user[0]['SUCURSAL'],
                        'CODTIENDA'=>$user[0]['CODTIENDA'],
                        'DESTIENDA'=> $user[0]['DESTIENDA'],
                        'SESION'=> $user[0]['SESION'],
                        'ZONAS'=>$a_zonas,
                        'MUEBLES'=>$a_muebles,
                        'NIVEL'=>$a_nivel,
                        'SEPARACION'=>$a_separacion,
                        'CONTEOS'=>$conteos
                    );
                }else{
                    $response=array(
                        'MSG'=>'Usuario incorrecto',
                        'VALIDATE'=>'0',
                    );
                }
            }else{
                $response=array(
                    'MSG'=>DEFAULT_DATA['Message']['invalido'],
                    'VALIDATE'=>'0',
                );
            }
            print_r(json_encode($response));
            $INV00DA->updateSesion($INV00);
        }
        public function closeSesion($request){
            if(DEFAULT_DATA['Auth']['token']==$request['TOKEN']){
                $INV00DA=new INV00DA();
                $INV00=new INV00();
                $INV01DA=new INV01DA();
                $INV00DA=new INV00DA();
                $INV05DA=new INV05DA();

                $INV00->USERNAME=$request['USERNAME'];

                $user=$INV00DA->closeSession($INV00);
                $response=array(
                    'MSG'=>'Usuario incorrecto',
                    'VALIDATE'=>'0',
                );
            }
            print_r(json_encode($response));
        }
        public function actualizaDescripcion($request){
            if(DEFAULT_DATA['Auth']['token']==$request['TOKEN']){
                $INV00DA=new INV00DA();
                $INV00=new INV00();
                $INV01DA=new INV01DA();
                $INV00DA=new INV00DA();
                $INV05DA=new INV05DA();
                $PRODUCTOSDA=new PRODUCTOSDA();

                $articulo=trim($request['MATERIAL']);
                if (strlen($articulo)>6) {
                    $qry2 = $PRODUCTOSDA->actualizaDescEAN($articulo);
                    if(count($qry2)>0){
                        foreach ($qry2 as $key=>$value) {
                            $qry2[$key]["DESCRIPCION"] = trim(utf8_encode($qry2[$key]["DESCRIPCION"]));
                            $qry2[$key]["EAN"] = trim(utf8_encode($qry2[$key]["EAN"]));
                            $qry2[$key]["UNIDAD"] = trim(utf8_encode($qry2[$key]["UNIDAD"]));
                            $qry2[$key]["CODIGO"] = trim(utf8_encode($qry2[$key]["CODIGO"]));
                            $qry2[$key]["FACTOR"] = trim(utf8_encode($qry2[$key]["FACTOR"]));
                        }
                        $response=array(
                            'MSG'=>"Autorizado",
                            'VALIDATE'=>'1',
                            'DATA'=>$qry2
                        );
                    }else{
                        $response=array(
                            'MSG'=>"ERROR",
                            'VALIDATE'=>'0',
                        );
                    }
                } else {
                    $qry1 = $PRODUCTOSDA->actualizaDesc($articulo);
                    foreach ($qry1 as $key=>$value) {
                        $qry1[$key]["DESCRIPCION"] = trim(utf8_encode($qry1[$key]["DESCRIPCION"]));
                        $qry1[$key]["EAN"] = trim(utf8_encode($qry1[$key]["EAN"]));
                        $qry1[$key]["UNIDAD"] = trim(utf8_encode($qry1[$key]["UNIDAD"]));
                        $qry1[$key]["CODIGO"] = trim(utf8_encode($qry1[$key]["CODIGO"]));
                        $qry1[$key]["FACTOR"] = trim(utf8_encode($qry1[$key]["FACTOR"]));
                    }
                    if(count($qry1)>0){
                        $response=array(
                            'MSG'=>'',
                            'VALIDATE'=>'1',
                            'DATA'=>$qry1
                        );
                    }else{
                        $response=array(
                            'MSG'=>"ERROR",
                            'VALIDATE'=>'0',
                        );
                    }
                }
            }
            print_r(json_encode($response));
        }
        public function createLocation($request){
            
            if(DEFAULT_DATA['Auth']['token']==$request['TOKEN']){
                $locations=explode(",",$request['LOCATION']);
                $INV02DA=new INV02DA();
                $obj=array();
                $suc=$request['SUCURSAL'];
                foreach($locations as $k=>$v){
                    $consulta=trim($v);
                    $qry = $INV02DA->validaExistente($consulta,$suc);
                    if ( count($qry) == 0 ) {
                        unset($obj);
                        $INV02=new INV02();
                        $INV02->LOCACION=trim($v);
                        $INV02->ZONA=substr($v,0,1);
                        $INV02->MUEBLE=substr($v,1,2);
                        $INV02->NIVEL=substr($v,4,1);
                        $INV02->SEPARACION=substr($v,5,2);
                        $INV02->STATUS="1";
                        $INV02->SUCURSAL=$request['SUCURSAL'];
                        $INV02->CODTIENDA=$request['CODTIENDA'];
                        $INV02->USERNAME=$request['USERNAME'];
                        $INV02->FCREACION=date("Y-m-d H:i:s");
                        $obj[]=$INV02;
                        $INV02DA->create($obj);
                        //$var = $var + 1;
                    }
                }
                $response=array(
                    'MSG'=>"Ubicaciones enviadas",
                    'VALIDATE'=>'1',
                );
                /*if($var > 1){
                    $response=array(
                        'MSG'=>"Ubicaciones enviadas",
                        'VALIDATE'=>'1',
                    );
                }else{
                    $response=array(
                        'MSG'=>"Error al enviar",
                        'VALIDATE'=>'0',
                    );
                }*/
            }else{
                $response=array(
                    'MSG'=>DEFAULT_DATA['Message']['invalido'],
                    'VALIDATE'=>'0',
                );
            }
            print_r(json_encode($response));
        }
        
        public function validateLocations($request)
		{
			$rsp['MSG'] = DEFAULT_DATA['Message']['invalido'];
			$rsp['VALIDATE'] = 0;
			$rsp['RTA'] = [];

			if (DEFAULT_DATA['Auth']['token'] == $request['TOKEN']) {
				$locations = explode(",", $request['LOCATION']);
				$INV02DA = new INV02DA();
				$INV24DA = new INV24DA();
				$INV02 = new INV02();
				$sucu = $request['SUCURSAL'];
				$loca = $request['VALUE'];
				$INV02->SUCURSAL = $sucu;
				$INV02->LOCACION = $loca;
				$locations = $INV02DA->validateLocation($INV02);
				if (count($locations) > 0) {
					$rsp['MSG'] = "OK";
					$rsp['VALIDATE'] = 1;
					$rsp['RTA'] = $INV24DA->showProductsLocations(array('SUCURSAL'=>$sucu,'LOCACION'=>$loca));
				} else {
					$rsp['MSG'] = "No existe ubicación";
				}
			}
			print_r(json_encode($rsp));
		}
        public function showLocations($request)
		{
			if (DEFAULT_DATA['Auth']['token'] == $request['TOKEN']) {
				$locations = explode(",", $request['LOCATION']);
				$INV02DA = new INV02DA();
				$INV02 = new INV02();
				$INV02->SUCURSAL = $request['SUCURSAL'];
				$INV02->LOCACION = $request['VALUE'];
				$locations = $INV02DA->getLocations($INV02);
				if (count($locations) > 0) {
					$a_location = "";
					foreach ($locations as $k => $v) {
						if ($k == 0) {
							$a_location = trim($v['LOCACION']);
						} else {
							$a_location = $a_location . "," . trim($v['LOCACION']);
						}
					}
					$response = array(
						'MSG' => "",
						'VALIDATE' => '1',
						'LOCATIONS' => $a_location
					);
				} else {
					$response = array(
						'MSG' => "Sin locaciones asignadas",
						'VALIDATE' => '0',
					);
				}
			} else {
				$response = array(
					'MSG' => DEFAULT_DATA['Message']['invalido'],
					'VALIDATE' => '0',
				);
			}
			print_r(json_encode($response));
		}
        public function deleteLocations($request){
            if(DEFAULT_DATA['Auth']['token']==$request['TOKEN']){
                $INV02DA=new INV02DA();
                $params=array(
                    'SUCURSAL'=>$request['SUCURSAL'],
                    'LOCATIONS'=>$request['LOCATIONS']
                );
                
                if($INV02DA->deleteLocations($params)){
                    $response=array(
                        'MSG'=>"Guardado",
                        'VALIDATE'=>'1',
                    );
                }else{
                    $response=array(
                        'MSG'=>"Error al borrar",
                        'VALIDATE'=>'0',
                    );
                }
            }else{
                $response=array(
                    'MSG'=>DEFAULT_DATA['Message']['invalido'],
                    'VALIDATE'=>'0',
                );
            }
            print_r(json_encode($response));
        }
        public function showDataInitial(){
            if(DEFAULT_DATA['Auth']['token']!=$request['TOKEN']){
                $INV03DA=new INV03DA();
                $params=array(
                    'SUCURSAL'=>$request['SUCURSAL']
                );
                $dataInitial=$INV03DA->showDataInitial($params);
                
                $data="";
                $as=array();
                $ean=array();
                foreach($dataInitial as $k=>$v){

                    

                }
                $response=array(
                    'MSG'=>'',
                    'VALIDATE'=>'1',
                    'DATA'=>$dataInitial
                );
            }else{
                $response=array(
                    'MSG'=>DEFAULT_DATA['Message']['invalido'],
                    'VALIDATE'=>'0',
                );
            }
            print_r(json_encode($response));
        }
        public function showProduct($request){
            //header('Content-Type: application/json; charset=utf8');
            $LOCACIONDA=new LOCACIONDA();
            if(DEFAULT_DATA['Auth']['token']==$request['TOKEN']){
                $PRODUCTOSDA=new PRODUCTOSDA();
                $INV03DA=new INV03DA();
                $sucursal = $request['SUCURSAL'];
                $material = $request['MATERIAL'];
                $locacion = $request['LOCATION'];
                $params=array(
                    'SUCURSAL'=>intval($sucursal),
                    'MATERIAL'=>$material,
                    'LOCACION'=>$locacion
                );
                $exist=$INV03DA->ExistProduct($params);
                $data=$PRODUCTOSDA->showProduct($params);
                if($exist[0]['CONT']==0){                    
                    foreach ($data as $key=>$value) {
                        //$data[$key]["DESCRIPCION"] = mb_convert_encoding($value["DESCRIPCION"], "UTF-8", "auto");
                        //$data[$key]["DESCRIPCION"] = Normalizer::normalize($value["DESCRIPCION"], Normalizer::FORM_D);
                        //$data[$key]["DESCRIPCION"] = iconv('UTF-8', 'ASCII//TRANSLIT', $value["DESCRIPCION"]);
                        //$data[$key]["DESCRIPCION"] = preg_replace("/[^a-zA-Z0-9\s]/", "",iconv('UTF-8', 'ASCII//TRANSLIT', $value["DESCRIPCION"]));
                        $data[$key]["DESCRIPCION"] = utf8_encode($data[$key]["DESCRIPCION"]);
                    }
                    if(count($data)>0){
                        $response=array( // Si existe el EAN
                            'MSG'=>'',
                            'VALIDATE'=>'1',
                            'DATA'=>$data[0]
                        );
                    }else{
                        $response=array(
                            'MSG'=>'',
                            'VALIDATE'=>'0',
                            'DATA'=>''
                        );
                    }
                }else{                    
                    $qblb = $LOCACIONDA->obtieneBiblioteca($sucursal);
                    $params['biblioteca']  = $qblb[0]["BIBLIOTECA"];
                    
                    $params['cod_art'] = !empty($data[0]['CODIGO_AS']) ? $data[0]['CODIGO_AS'] : 0;
                    $_exist = $LOCACIONDA->validar_locacion($params);
                    if(!$_exist) {
                        $LOCACIONDA->validar_locacion($params);
                    }

                    $response=array(
                        'MSG'=>'',
                        'VALIDATE'=>'2',
                        'DATA'=>''
                    );
                }
            }else{
                $response=array(
                    'MSG'=>DEFAULT_DATA['Message']['invalido'],
                    'VALIDATE'=>'0',
                );
            }
            print_r(json_encode($response));
        }
        public function saveLocationMaterial($request){
            //-3501179503691-1
           // print_r($request['CODTIENDA']);
           //MATERIAL-->COD_AS-COD_EAN-INDICADOR
            //-3501179503691-1,-025215500039-1,213133-2050000381393-1,213122-025215503030-1,613-7751324004511-1,1086-9783849906122-1,1089-3013010010898-1,1100-17750277000783-1,1097-9781503722163-1,1100-7750277000786-1,110--0
            $response=array(
                'MSG'=>DEFAULT_DATA['Message']['invalido'],
                'VALIDATE'=>'0',
            );
            //print_r($request);
            if(DEFAULT_DATA['Auth']['token']==$request['TOKEN']){                
                $INV03DA=new INV03DA();
                $PRODUCTOSDA=new PRODUCTOSDA();
                $LOCACIONDA=new LOCACIONDA();

                $obj=array();
                $obj_locaciones_prod=array();

                $ar=explode(",",$request['MATERIAL']);
                $location=trim($request['LOCATION']);
                $date=date("Y-m-d H:i:s");
                $username=trim($request['USERNAME']);
                $sucursal=intval($request['SUCURSAL']);

                $qblb = $LOCACIONDA->obtieneBiblioteca($sucursal);
                $biblio = trim($qblb[0]["BIBLIOTECA"]);

                $cod_as_arr = [];
                $cod_ea_arr = [];
                $desgosa = [];
                $c_ean = 0;
                $c_as = 0;
                $es_in = 0;

                foreach($ar as $vv) {
                    $desgosa = explode("-",$vv);
                    $c_as = !empty($desgosa[0]) ? (int)$desgosa[0] : 0;
                    $c_ean = !empty($desgosa[1]) ? (int)$desgosa[1] : 0;
                    $es_in = $desgosa[0];
                    if($c_as) {
                        array_push($cod_as_arr,$c_as);                            
                    }

                    if($c_ean && !empty($es_in)) {
                        array_push($cod_ea_arr,$c_ean);
                    }
                }
                //echo "<pre>";print_r($cod_as_arr); echo "</pre>";
                //echo "<pre>";print_r($cod_ea_arr); echo "</pre>";
                //die();
                //Validar codigos EAN
                /*if(!empty($cod_ea_arr)) {
                    //Buscar codigos AS con parametros de codigo EAN
                    $valores_as = $PRODUCTOSDA->buscar_codigo_as_desde_ean($cod_ea_arr);
                    if(!empty($valores_as)) {
                        //Agregar los nuevos codigos AS
                        foreach($valores_as as $c_e_a_n => $zz) {
                            array_push($cod_as_arr,$zz);
                            array_push($cod_ea_arr,$c_e_a_n);
                        }
                    }
                }*/
                //Validar si existe registros de cod as
                if(!empty($cod_as_arr)) {
                    $send['sucu'] = $sucursal;
                    $send['loca'] = $location;
                    $send['c_as'] = $cod_as_arr;
                    //Eliminar todo los codigos AS de la tabla inv03
                    $INV03DA->delete_locaciones_inv($send);
                    //Agregar locaciones
                    $INV03DA->add_locaciones($send);
                    //Actualizar indicaciones
                    if(!empty($cod_ea_arr)) {
                        $send['c_ea'] = $cod_ea_arr;
                        $INV03DA->upd_locacion_indicador($send);
                        unset($send['c_ea']);
                    }                    

                    $send['ALM']=str_pad($sucursal, 3, "0", STR_PAD_LEFT);
                    $send['ZONA']=substr($location,0,1);
                    $send['MUEBLE']=substr($location,1,2);
                    $send['NIVEL']=substr($location,4,3);

                    //Eliminar todo los codigo de la tabla TMR correspondiente a su biblioteca
                    $valor = $LOCACIONDA->delete_locaciones($biblio, $send);
                    //AgregCDNIVar locacion en TMR
                    $valor = $LOCACIONDA->add_loca_ciones($biblio, $send);

                    if($valor) {
                        $response=array(
                            'MSG'=>"Enviado correctamente",
                            'VALIDATE'=>'1',
                        );
                    }
                } else{
                    $response=array(
                        'MSG'=>"Debe ingresar materiales ",
                        'VALIDATE'=>'0',
                    );
                }            
            }
            print_r(json_encode($response));
        }


        /*public function saveLocationMaterial($request){
            
            if(DEFAULT_DATA['Auth']['token']==$request['TOKEN']){
                $INV03DA=new INV03DA();
                $obj=array();
                $ar_as=explode(",",$request['MATERIAL_AS']);
                $ar_ean=explode(",",$request['MATERIAL_EAN']);
                $location=trim($request['LOCATION']);
                $date=date("Y-m-d H:i:s");
                $username=trim($request['USERNAME']);
                $sucursal=intval($request['SUCURSAL']);
                $ar_ean_format='';
                $ar_as_format='';
                foreach($ar_ean as $k=>$v){
                    if(count($ar_ean)==1){
                        $ar_ean_format=$ar_ean_format."'".$v."'";
                    }
                    else if($k==0){
                        $ar_ean_format=$ar_ean_format."'".$v."'";
                    }else {
                        $ar_ean_format=$ar_ean_format.",'".$v."'";
                    }
                    
                   
                }
                $band=false;
                if($request['MATERIAL_AS']!=''){
                    $prodsAS=$INV03DA->searchProductAS($request['MATERIAL_AS'],$request['SUCURSAL']);
                   
                    foreach($prodsAS as $k=>$v){
                        $params=array(
                            'SUCURSAL'=>intval($request['SUCURSAL']),
                            'COD_EAN'=>'',
                            'COD_AS'=>$v['CODIGO_AS'],
                            'LOCACION'=>$location
                        );
                       
                            $INV03=new INV03();
                            $INV03->LOCACION= $location;
                            $INV03->COD_AS=$v['CODIGO_AS'];
                            $INV03->COD_EAN='';
                            $INV03->UNIDAD_BASE=$v['UNIDAD_BASE'];
                            $INV03->FACTOR=1;
                            $INV03->DESCRIPCION=$v['DESCRIPCION'];
                            $INV03->SUCURSAL=$sucursal;
                            $INV03->USERNAME=$username;
                            $INV03->FCREACION=$date;
                            $obj[]=$INV03;
                        
                    }
                }
               
                if($request['MATERIAL_EAN']!=''){
                    $prodsEAN=$INV03DA->searchProductEAN($ar_ean_format,$request['SUCURSAL']);
                  
                    foreach($prodsEAN as $k=>$v){
                        $params=array(
                            'SUCURSAL'=>intval($request['SUCURSAL']),
                            'COD_EAN'=>$v['EAN'],
                            'COD_AS'=>$v['CODIGO_AS'],
                            'LOCACION'=>$location
                        );
                       
                            $INV03=new INV03();
                            $INV03->LOCACION= $location;
                            $INV03->COD_EAN=$v['EAN'];
                            $INV03->UNIDAD_BASE=$v['UNIDAD_BASE'];
                            $INV03->FACTOR=intval($v['EAN_FACTOR']);
                            $INV03->DESCRIPCION=$v['DESCRIPCION'];
                            $INV03->COD_AS=$v['CODIGO_AS'];
                            $INV03->SUCURSAL=$sucursal;
                            $INV03->USERNAME=$username;
                            $INV03->FCREACION=$date;
                            $obj[]=$INV03;
                       
                       
                    }
                }
                if(count($obj)>0){
                    if($INV03DA->create($obj)){
                        $response=array(
                            'MSG'=>"Enviado correctamente",
                            'VALIDATE'=>'1',
                        );
                    }else{
                        $response=array(
                            'MSG'=>"Error al enviar",
                            'VALIDATE'=>'0',
                        );
                    }
                }else if($band){
                    $response=array(
                        'MSG'=>"Enviado correctamente",
                        'VALIDATE'=>'1',
                    );
                }else{
                    $response=array(
                        'MSG'=>"Debe ingresar materiales ",
                        'VALIDATE'=>'0',
                    );
                }
               
            }else{
                $response=array(
                    'MSG'=>DEFAULT_DATA['Message']['invalido'],
                    'VALIDATE'=>'0',
                );
            }
            print_r(json_encode($response));
        }*/
        public function showProductSucursal($request){
          
            if(DEFAULT_DATA['Auth']['token']==$request['TOKEN']){
                $PRODUCTOSDA=new PRODUCTOSDA();
                $INV03DA=new INV03DA();
                $params=array(
                    'SUCURSAL'=>intval($request['SUCURSAL']),
                    'LOCACION'=>trim($request['LOCACION']),
                );
                $products=$INV03DA->showProductsLocations($params);
                if(count($products)>0){
                    $a_as='';
                    $a_ean='';
                    foreach($products as $k=>$v){
                        if($k==(count($products)-1)){
                            if($v['COD_AS']!=''){
                                $a_as=$a_as.$v['COD_AS'];
                            }
                            if($v['COD_EAN']!=''){
                                $a_ean=$a_ean.$v['COD_EAN'];
                            }
                        }else{
                            if($v['COD_AS']!=''){
                                $a_as=$a_as.$v['COD_AS'].",";
                            }
                            if($v['COD_EAN']!=''){
                                $a_ean=$a_ean.$v['COD_EAN'].",";
                            }
                        }
                    }
                    
                    $params=array(
                        'SUCURSAL'=>intval($request['SUCURSAL']),
                        'MATERIAL'=>$a_as,
                        'MATERIAL_EAN'=>$a_ean,
                    );
                    $data=$PRODUCTOSDA->showDetailsProduct($params);
                    $response=array(
                        'MSG'=>'',
                        'VALIDATE'=>'1',
                        'DATA'=>$data
                    );
                }else{
                    $response=array(
                        'MSG'=>'',
                        'VALIDATE'=>'0',
                    );
                }
            }else{
                $response=array(
                    'MSG'=>DEFAULT_DATA['Message']['invalido'],
                    'VALIDATE'=>'0',
                );
            }
            print_r(json_encode($response));
        }
        public function deleteContentLocation($request){
            if(DEFAULT_DATA['Auth']['token']==$request['TOKEN']){
                $INV03DA=new INV03DA();
                $params=array(
                    'SUCURSAL'=>intval($request['SUCURSAL']),
                    'LOCACION'=>trim($request['LOCATION']),
                    'ZONA'=>substr(trim($request['LOCATION']),0,1),
                    'MUEBLE'=>substr(trim($request['LOCATION']),1,2),
                    'NIVEL'=>substr(trim($request['LOCATION']),4,3),
                    'ALM'=>str_pad($request['SUCURSAL'], 3, "0", STR_PAD_LEFT)
                );
                $LOCACIONDA=new LOCACIONDA();
                $biblioteca = $LOCACIONDA->obtieneBiblioteca($params['SUCURSAL']);
                $biblio = !empty($biblioteca[0]['BIBLIOTECA']) ? $biblioteca[0]['BIBLIOTECA'] : 0;
                if($INV03DA->deleteContentLocation($params)){
                    $LOCACIONDA->deleteLocation($params,$biblio);
                    $response=array(
                        'MSG'=>'Se borró correctamente',
                        'VALIDATE'=>'1',
                    );
                }else{
                    $response=array(
                        'MSG'=>'Error al borrar contenido',
                        'VALIDATE'=>'0',
                    );
                }
            }else{
                $response=array(
                    'MSG'=>DEFAULT_DATA['Message']['invalido'],
                    'VALIDATE'=>'0',
                );
            }
            print_r(json_encode($response));
        }
        public function deleteLocation($request){
            if(DEFAULT_DATA['Auth']['token']==$request['TOKEN']){
                $INV02DA=new INV02DA();
                $LOCACIONDA=new LOCACIONDA();
                $params=array(
                    'SUCURSAL'=>intval($request['SUCURSAL']),
                    'LOCACION'=>trim($request['LOCATION']),
                    'ZONA'=>substr(trim($request['LOCATION']),0,1),
                    'MUEBLE'=>substr(trim($request['LOCATION']),1,2),
                    'NIVEL'=>substr(trim($request['LOCATION']),4,3),
                    'ALM'=>str_pad($request['SUCURSAL'], 3, "0", STR_PAD_LEFT)
                );
                
                
                if($INV02DA->deleteLocation($params)){
                    $LOCACIONDA->deleteLocation($params,$request['CODTIENDA']);
                    $response=array(
                        'MSG'=>'Se borró correctamente',
                        'VALIDATE'=>'1',
                    );
                }else{
                    $response=array(
                        'MSG'=>'Error al borrar contenido',
                        'VALIDATE'=>'0',
                    );
                }
            }else{
                $response=array(
                    'MSG'=>DEFAULT_DATA['Message']['invalido'],
                    'VALIDATE'=>'0',
                );
            }
            print_r(json_encode($response));
        }

        public function deleteLocationUnidad($request){
            if(DEFAULT_DATA['Auth']['token']==$request['TOKEN']){
                $INV03DA=new INV03DA();
                $LOCACIONDA=new LOCACIONDA();
                $params=array(
                    'SUCURSAL'=>intval($request['SUCURSAL']),
                    'LOCACION'=>trim($request['LOCATION']),
                    'ZONA'=>substr(trim($request['LOCATION']),0,1),
                    'MUEBLE'=>substr(trim($request['LOCATION']),1,2),
                    'NIVEL'=>substr(trim($request['LOCATION']),4,3),
                    'ALM'=>str_pad($request['SUCURSAL'], 3, "0", STR_PAD_LEFT),
                    'AS'=>str_pad(str_pad(trim($request['AS']), 6, "0", STR_PAD_LEFT),15," ",STR_PAD_LEFT),
                    'AS_NOFORMAT'=>trim($request['AS']),
                );
                $biblioteca = $LOCACIONDA->obtieneBiblioteca($params['SUCURSAL']);
                $biblio = !empty($biblioteca[0]['BIBLIOTECA']) ? $biblioteca[0]['BIBLIOTECA'] : 0;
                if($INV03DA->deleteContentLocationUnidad($params)){
                    $LOCACIONDA->deleteLocationUnidad($params,$biblio);
                    $response=array(
                        'MSG'=>'Se borró correctamente',
                        'VALIDATE'=>'1',
                    );
                }else{
                    $response=array(
                        'MSG'=>'Error al borrar contenido',
                        'VALIDATE'=>'0',
                    );
                }
            }else{
                $response=array(
                    'MSG'=>DEFAULT_DATA['Message']['invalido'],
                    'VALIDATE'=>'0',
                );
            }
            print_r(json_encode($response));
        }

        /* inventario fisico */

        public function locationsInv($request)
		{
			if (DEFAULT_DATA['Auth']['token'] == $request['TOKEN']) {
				$INV04DA = new INV04DA();
				$INV19DA = NEW INV19DA();
				$params = array(
					'SUCURSAL' => $request['SUCURSAL'],
					'USERNAME' => trim($request['USERNAME']),
					'CONTEO' => trim($request['CONTEO']),
				);

				$numinv = (!empty($params['SUCURSAL'])) ? $INV19DA->lastInv($params['SUCURSAL']) : NULL;
				$params['INVENTARIO'] = $numinv;
				
				$qry1 = $INV04DA->consultaObtenerLocAsignado($params);
				if (count($qry1) < 1) { //REVISO SI EN EL INV04 NO TIENE NINGUNA LOCACIÓN ASIGNADA
					$qry2 = $INV04DA->consultaObtenerNiveles($params);
					if (count($qry2) > 0) { //REVISO QUE EN LA TABLA ZONA - USUARIO TENGA PENDIENTE DE CONTAR
						$zona = trim($qry2[0]["ZONA"]);
						$nivsup = trim($qry2[0]["NIVSUP"]);
						$nivinf = trim($qry2[0]["NIVINF"]);
						$suc = $request['SUCURSAL'];
						$user = trim($request['USERNAME']);
						$qry3 = $INV04DA->consultaExistenLocPend($zona, $nivinf, $nivsup, $suc, $numinv);

						if (count($qry3) < 1) { // VEO QUE SI LO QUE ME SALE PENDIENTE YA ESTA CONTADO Y SINO ACTUALIZO ESTADO ZONA-USUARIO
							$INV04DA->actualizaEstadoZonaNivel($suc, $user, $zona, $nivinf, $nivsup, $numinv); //SI YA SE TERMINO ACTUALIZO EL ESTADO
							$qry4 = $INV04DA->consultaObtenerNiveles($params); //VERIFICO LA SIGUIENTE ZONA
							if (count($qry4) > 0) { //SI ENCUENTRA UNA PENDIENTE
								$zona3 = trim($qry2[0]["ZONA"]);
								$nivsup3 = trim($qry2[0]["NIVSUP"]);
								$nivinf3 = trim($qry2[0]["NIVINF"]);
								$qry5 = $INV04DA->consultaExistenLocPend($zona3, $nivinf3, $nivsup3, $suc, $numinv); //BUSCO ESA NUEVA PENDIENTE EN LA TABLA INV04
								if (count($qry5) < 1) { //SI ES QUE NO ENCUENTRA NADA ES QUE ESE USUARIO YA TERMINO
									$INV04DA->actualizaEstadoZonaNivel($suc, $user, $zona3, $nivinf3, $nivsup3, $numinv); //ACTUALIZO EL ESTADO
								} else { //SI ENCUENTRA ALGO, LO ASIGNA AL USUARIO
									$zona4 = trim($qry5[0]["ZONA"]);
									$nivel4 = trim($qry5[0]["NIVEL"]);
									$INV04DA->asignoLocacionUsuario($suc, $user, $zona4, $nivel4, $numinv);
								}
							} else {
							}
						} else {
							$zona2 = trim($qry3[0]["ZONA"]);
							$nivel = trim($qry3[0]["NIVEL"]);
							$INV04DA->asignoLocacionUsuario($suc, $user, $zona2, $nivel, $numinv);
						}
					} else {
					}
				} else {

				}
				$a_locations = $INV04DA->getLocationInv($params);
                $locaciones = [];
				foreach ($a_locations as $k => $v) {
                    array_push($locaciones, $v['LOCACION']);
				}
				$response = array(
					'MSG' => '',
					'VALIDATE' => '1',
					'DATA' => implode(",",$locaciones)
				);                         
			} else {
				$response = array(
					'MSG' => DEFAULT_DATA['Message']['invalido'],
					'VALIDATE' => '0',
				);
			}
			print_r(json_encode($response));
		}

        public function locationsInv2($request){
            if(DEFAULT_DATA['Auth']['token']==$request['TOKEN']){
                $INV04DA=new INV04DA();
                $INV05DA=new INV05DA();
                $INV07DA=new INV07DA();
                $params=array(
                    'SUCURSAL'=>intval($request['SUCURSAL']),
                    'USERNAME'=>trim($request['USERNAME']),
                    'CONTEO'=>trim($request['CONTEO']),
                );
                $suc = trim($request['SUCURSAL']);
                $user = trim($request['USERNAME']);

                $a_locations=$INV07DA->getLocationInv($suc,$user);
                if (count($a_locations)>0) {
                    $locations="";
                    foreach($a_locations as $k=>$v){
                        if($k==0){
                            $locations=$locations.$v['LOCACION'];
                        }else{
                            $locations=$locations.",".$v['LOCACION'];
                        }
                    }
                }else{
                    //$firstcod = $INV07DA->getFirstCod($suc);
                    //$cod = $firstcod[0]['COD_AS'];
                    $sql = $INV05DA->getContadores($suc);
                    $contadores = $sql[0]['CONTADORES'];
                    $INV07DA->update($suc,$cod,$user,$contadores);
                    $a_locations=$INV07DA->getLocationInv($suc,$user);
                    $locations="";
                    foreach($a_locations as $k=>$v){
                        if($k==0){
                            $locations=$locations.$v['LOCACION'];
                        }else{
                            $locations=$locations.",".$v['LOCACION'];
                        }
                    }
                }
                $response=array(
                    'MSG'=>'',
                    'VALIDATE'=>'1',
                    'DATA'=>$locations
                );
                //}                            
            }else{
                $response=array(
                    'MSG'=>DEFAULT_DATA['Message']['invalido'],
                    'VALIDATE'=>'0',
                );
            }
            print_r(json_encode($response));
        }

        public function getLocationMaterial($request){
            
            if(DEFAULT_DATA['Auth']['token']==$request['TOKEN']){
                $INV04DA=new INV04DA();
                $params=array(
                    'SUCURSAL'=>$request['SUCURSAL'],
                    'USERNAME'=>trim($request['USERNAME']),
                );

                $conteo=trim(strval($request['CONTEO']));
                switch ($conteo) {
                    case '1':
                        /*$qry1= $INV04DA->consultaObtenerLocAsignado($params);
                        if(count($qry1 < 1)){//REVISO SI EN EL INV04 NO TIENE NINGUNA LOCACIÓN ASIGNADA
                            $qry2 = $INV04DA->consultaObtenerNiveles($params);
                            if(count($qry2)>0){//REVISO QUE EN LA TABLA ZONA - USUARIO TENGA PENDIENTE DE CONTAR
                                $zona = trim($qry2[0]["ZONA"]);
                                $nivsup = trim($qry2[0]["NIVSUP"]);
                                $nivinf = trim($qry2[0]["NIVINF"]);
                                $suc = intval($request['SUCURSAL']);
                                $user = trim($request['USERNAME']);
                                $qry3 = $INV04DA->consultaExistenLocPend($zona,$nivinf,$nivsup,$suc);
                            
                                if (count($qry3) < 1) {// VEO QUE SI LO QUE ME SALE PENDIENTE YA ESTA CONTADO Y SINO ACTUALIZO ESTADO ZONA-USUARIO
                                    $INV04DA->actualizaEstadoZonaNivel($suc,$user,$zona,$nivinf,$nivsup);//SI YA SE TERMINO ACTUALIZO EL ESTADO
                                    $qry4 = $INV04DA->consultaObtenerNiveles($params);//VERIFICO LA SIGUIENTE ZONA
                                    if(count($qry4)>0){//SI ENCUENTRA UNA PENDIENTE
                                        $zona3 = trim($qry2[0]["ZONA"]);
                                        $nivsup3 = trim($qry2[0]["NIVSUP"]);
                                        $nivinf3 = trim($qry2[0]["NIVINF"]);
                                        $qry5 = $INV04DA->consultaExistenLocPend($zona3,$nivinf3,$nivsup3,$suc);//BUSCO ESA NUEVA PENDIENTE EN LA TABLA INV04
                                        if (count($qry5) < 1){//SI ES QUE NO ENCUENTRA NADA ES QUE ESE USUARIO YA TERMINO
                                            $INV04DA->actualizaEstadoZonaNivel($suc,$user,$zona3,$nivinf3,$nivsup3);//ACTUALIZO EL ESTADO
                                        }else {//SI ENCUENTRA ALGO, LO ASIGNA AL USUARIO
                                            $zona4 = trim($qry5[0]["ZONA"]);
                                            $nivel4 = trim($qry5[0]["NIVEL"]);
                                            $INV04DA->asignoLocacionUsuario($suc,$user,$zona4,$nivel4);
                                        }
                                    }else{
                                    }
                                }else{
                                    $zona2 = trim($qry3[0]["ZONA"]);
                                    $nivel = trim($qry3[0]["NIVEL"]);
                                    $INV04DA->asignoLocacionUsuario($suc,$user,$zona2,$nivel);
                                }
                            }else{
                            }
                        }else{}*/
                        $materials=$INV04DA->getLocationMaterialFirstCount($params);
                        foreach ($materials as $key=>$value) {
                                $materials[$key]["DESCRIPCION"] = utf8_encode($materials[$key]["DESCRIPCION"]);
                        //        $materials[$key]["up.DESCRIPCION"] = preg_replace("/[^a-zA-Z0-9\s]/", "",$materials["up.DESCRIPCION"]);
                        }
                        break;
                    case '2':
                        $materials=$INV04DA->getLocationMaterialSecondCount($params);
                        //print_r($materials);
                        //die();
                        foreach ($materials as $key=>$value) {
                            $materials[$key]["DESCRIPCION"] = utf8_encode($materials[$key]["DESCRIPCION"]);
                        //        $materials[$key]["up.DESCRIPCION"] = preg_replace("/[^a-zA-Z0-9\s]/", "",$materials["up.DESCRIPCION"]);
                        }
                        break;
                }
                $response=array(
                    'MSG'=>'',
                    'VALIDATE'=>'1',
                    'DATA'=>$materials
                );
            }else{
                $response=array(
                    'MSG'=>DEFAULT_DATA['Message']['invalido'],
                    'VALIDATE'=>'0',
                );
            }
            print_r(json_encode($response));
        }
        public function createFirstCont($request){
           
            //1089-2-1,1097-1-1,1100-25-1,9783849906122-1-0
            if(DEFAULT_DATA['Auth']['token']==$request['TOKEN']){
                $INV06DA=new INV06DA();
                $INV04DA=new INV04DA();
                $INV19DA=new INV19DA();
                $OP22DA=new OP22DA();
                $OP01DA=new OP01DA();
                $PRODUCTOSDA=new PRODUCTOSDA();

                //OBTENGO SUCURSAL
                $suc = $request['SUCURSAL'];
                //OBTENGO USUARIO
                $user = trim($request['USERNAME']);
                //DEFINO BILBIOTECA, CAMBIAR A QUE SE ENVIE DESDE EL RF
                if(strlen($suc)>2){
                    $bib = "TIEND".$suc;
                }else{
                    $bib = "TIENDA".str_pad($suc, 2, '0', STR_PAD_LEFT);
                }
                //OBTENGO EL NRO DE INVENTARIO ACTIVO
                $consulta = $INV19DA->getInventario($suc);
                $inv = trim($consulta[0]["INVENTARIO"]);
                $contador = 0;
                //OBTENGO LA ULTIMA PAGINA DEL INVENTARIO POR SI ES NECESARIO CREAR UNO NUEVO
                $consulta2 = $OP22DA->getPagina($bib,$inv);
                if (count($consulta2)>0) {
                    $pagina = $consulta2[0]['PAGINA'];    
                }else{
                    $pagina = 0;
                }
                //GUARDO LA LOCACION ENVIADA
                $locacion = trim($request['LOCATION']);

                $params=array(
                    'SUCURSAL'=>$request['SUCURSAL'],
                    'USERNAME'=>trim($request['USERNAME']),
                    'MATERIAL'=>explode(",",trim($request['MATERIAL'])),
                    'LOCATION'=>trim($request['LOCATION']),
                );
                $date=date("Y-m-d H:i:s");
                $obj=array();
                $ar_as=array();
                foreach($params['MATERIAL'] as $k=>$v){

                    $v=explode('-',$v);
                    $cod=$v[0];
                    $cantidad=$v[1];
                    $indicador=$v[2];

                    $INV06=new INV06();
                    $INV06->LOCACION=trim($request['LOCATION']);
                    $INV06->USERNAME=trim($request['USERNAME']);
                    $INV06->SUCURSAL=$request['SUCURSAL'];
                    $INV06->CANTIDAD=$cantidad;
                    $INV06->INDICADOR=$indicador;
                    
                    if(strlen($v[0])>6){
                        $params=array(
                            'SUCURSAL'=>intval($request['SUCURSAL']),
                            'MATERIAL_EAN'=>$v[0],
                        );
                        $data=$PRODUCTOSDA->showDetailsProductAllBaseEAN($params);
                        if(count($data)>0){
                            $INV06->COD_AS=$data[0]['CODIGO_AS'];
                        }
                    }else{
                        $INV06->COD_AS=$cod;
                    }
                    $pos=array_search($INV06->get('COD_AS'),$ar_as);
                    if($pos==false){
                        $ar_as[]=$INV06->get('COD_AS');
                        $INV06->FCREACION=$date;
                        $obj[]=$INV06;
                    }else{
                        $obj[$pos]->CANTIDAD= $obj[$pos]->get('CANTIDAD')+$cantidad;
                    }
                    
                }
                //BORRO LA LOCACION ENVIADA
                $INV06DA->delete($suc,$locacion);
                if($INV06DA->create($obj)){
                    $params=array(
                        'SUCURSAL'=>$request['SUCURSAL'],
                        'USERNAME'=>trim($request['USERNAME']),
                        'LOCATION'=>trim($request['LOCATION']),
                        'CONTEO'=>1,
                    );
                    $INV04DA->updateCont($params);
                    $response=array(
                        'MSG'=>"Enviado con exito",
                        'VALIDATE'=>'1',
                    );
                }else{
                    $response=array(
                        'MSG'=>"Error al enviar",
                        'VALIDATE'=>'0',
                    );
                }
            }else{
                $response=array(
                    'MSG'=>DEFAULT_DATA['Message']['invalido'],
                    'VALIDATE'=>'0',
                );
            }
            //INCIO LA MAGIA PARA CARGAR AL INVENTARIO :)

            //OBTENGO LA LOCACION ENVIADA E INSERTADA AL INV06
            $dataContada = $INV06DA->getDataInventario($suc,$locacion);
            $pagina = $pagina + 1;
            foreach ($dataContada as $key => $value) {
                $locContada = trim($value['LOCACION']);
                $locCodAs = trim($value['COD_AS']);
                $locCantidad = trim($value['CANTIDAD']);

                $valido = $OP22DA->validateExist($bib,$inv,$locContada,$locCodAs);
                if(count($valido)>0){
                    //EXISTE EL REGISTRO, HAGO EL UPDATE CHINGON
                    $fecha = date("Ymd");
                    $hora = date("His");
                    $OP22DA->update($bib,$inv,$locContada,$locCodAs,$locCantidad,$user,$fecha,$hora);
                }else{
                    //NO EXISTE Y TENGO QUE CREAR EL REGISTRO
                    //PRIMERO REVISO QUE EL CODIGO AS EXISTA
                    $existeCod = $OP01DA->existCod($locCodAs);
                    if (count($existeCod)>0) {
                        $contador = $contador + 1;
                        $fecha = date("Ymd");
                        $hora = date("His");
                        $OP22DA->insert($bib,$inv,$locContada,$locCodAs,$locCantidad,$user,$fecha,$hora,$pagina,$contador,$suc);
                    }
                }
            }
            print_r(json_encode($response));
        }
        public function createSecondCont($request){
           
            //1089-2-1,1097-1-1,1100-25-1,9783849906122-1-0
            if(DEFAULT_DATA['Auth']['token']==$request['TOKEN']){
                $INV06DA=new INV06DA();
                $INV04DA=new INV04DA();
                $INV07DA=new INV07DA();
                $INV19DA=new INV19DA();
                $OP22DA=new OP22DA();
                $OP01DA=new OP01DA();
                $PRODUCTOSDA=new PRODUCTOSDA();

                //OBTENGO SUCURSAL
                $suc = trim($request['SUCURSAL']);
                //OBTENGO USUARIO
                $user = trim($request['USERNAME']);
                //DEFINO BILBIOTECA, CAMBIAR A QUE SE ENVIE DESDE EL RF
                if(strlen($suc)>2){
                    $bib = "TIEND".$suc;
                }else{
                    $bib = "TIENDA".str_pad($suc, 2, '0', STR_PAD_LEFT);
                }
                //OBTENGO EL NRO DE INVENTARIO ACTIVO
                $consulta = $INV19DA->getInventario($suc);
                $inv = trim($consulta[0]["INVENTARIO"]);
                $contador = 0;
                //OBTENGO LA ULTIMA PAGINA DEL INVENTARIO POR SI ES NECESARIO CREAR UNO NUEVO
                $consulta2 = $OP22DA->getPagina($bib,$inv);
                $pagina = $consulta2[0]['PAGINA'];
                //GUARDO LA LOCACION ENVIADA
                $locacion = trim($request['LOCATION']);

                $params=array(
                    'SUCURSAL'=>intval($request['SUCURSAL']),
                    'USERNAME'=>trim($request['USERNAME']),
                    'MATERIAL'=>explode(",",trim($request['MATERIAL'])),
                    'LOCATION'=>trim($request['LOCATION']),
                );
                $date=date("Y-m-d H:i:s");
                $obj=array();
                $ar_as=array();
                foreach($params['MATERIAL'] as $k=>$v){
                    $cod_as = $params['MATERIAL'];
                    $v=explode('-',$v);
                    $cod=$v[0];
                    $cantidad=$v[1];
                    $indicador=$v[2];

                    $INV06=new INV06();
                    $INV06->LOCACION=trim($request['LOCATION']);
                    $INV06->USERNAME=trim($request['USERNAME']);
                    $INV06->SUCURSAL=intval($request['SUCURSAL']);
                    $INV06->CANTIDAD=$cantidad;
                    $INV06->INDICADOR=$indicador;
                    
                    if(strlen($v[0])>6){
                        $params=array(
                            'SUCURSAL'=>intval($request['SUCURSAL']),
                            'MATERIAL_EAN'=>$v[0],
                        );
                        $data=$PRODUCTOSDA->showDetailsProductAllBaseEAN($params);
                        if(count($data)>0){
                            $INV06->COD_AS=$data[0]['CODIGO_AS'];
                        }
                    }else{
                        $INV06->COD_AS=$cod;
                    }
                    $pos=array_search($INV06->get('COD_AS'),$ar_as);
                    if($pos==false){
                        $ar_as[]=$INV06->get('COD_AS');
                        $INV06->FCREACION=$date;
                        $obj[]=$INV06;
                    }else{
                        $obj[$pos]->CANTIDAD= $obj[$pos]->get('CANTIDAD')+$cantidad;
                    }
                    
                }
                //BORRO LA LOCACION ENVIADA
                $INV06DA->delete_conteo2($suc,$locacion,$cod_as);
                if($INV06DA->create_conteo2($obj)){
                    $params=array(
                        'SUCURSAL'=>intval($request['SUCURSAL']),
                        'USERNAME'=>trim($request['USERNAME']),
                        'LOCATION'=>trim($request['LOCATION']),
                        'CONTEO'=>1,
                    );
                    $INV04DA->updateCont2($params);
                    $response=array(
                        'MSG'=>"Enviado con exito",
                        'VALIDATE'=>'1',
                    );
                }else{
                    $response=array(
                        'MSG'=>"Error al enviar",
                        'VALIDATE'=>'0',
                    );
                }
            }else{
                $response=array(
                    'MSG'=>DEFAULT_DATA['Message']['invalido'],
                    'VALIDATE'=>'0',
                );
            }
            //INCIO LA MAGIA PARA CARGAR AL INVENTARIO :)

            //OBTENGO LA LOCACION ENVIADA E INSERTADA AL INV08
            $dataContada = $INV06DA->getDataInventario2($suc,$locacion);
            $pagina = $pagina + 1;
            foreach ($dataContada as $key => $value) {
                $locContada = trim($value['LOCACION']);
                $locCodAs = trim($value['COD_AS']);
                $locCantidad = trim($value['CANTIDAD']);
                $INV07DA->updateConteo($suc,$locCodAs,$locacion);

                $valido = $OP22DA->validateExist($bib,$inv,$locContada,$locCodAs);
                if(count($valido)>0){
                    //EXISTE EL REGISTRO, HAGO EL UPDATE CHINGON
                    $fecha = date("Ymd");
                    $hora = date("His");
                    $OP22DA->update_conteo2($bib,$inv,$locContada,$locCodAs,$locCantidad,$user,$fecha,$hora);
                }
            }
            print_r(json_encode($response));
        }

		/**
		 * Guardar pre inventario formato de data
		 * LOCATION=>U01-A01
		 * [USERNAME] => ALMTIE8
		 * [SUCURSAL] => 88
		 * [MATERIAL]=>,13013017224912,69~,778988632581,55~,1033354250752,19~,10194735107848,23~,17804915527184,100
		 * Codigo_as,Codigo_ean,cantiad concatenado por (~)
		 */
		public function createPreInventario($request) {        
			$a_id = [];
			$upd = [];
			$pre = [];
			if (DEFAULT_DATA['Auth']['token'] == $request['TOKEN']) {
				$INV24DA = new INV24DA();
				$PRODUCTOSDA = new PRODUCTOSDA();
				$LOCACIONDA = new LOCACIONDA();

				$sucursal = trim($request['SUCURSAL']);
				$username = trim($request['USERNAME']);            
				$material = explode("~", trim($request['MATERIAL']));            
				$location = trim($request['LOCATION']);

				$qblb = $LOCACIONDA->obtieneBiblioteca($sucursal);
				$biblio = $qblb[0]["BIBLIOTECA"];

				$env_loca['ALM'] = str_pad($sucursal, 3, "0", STR_PAD_LEFT);
				$env_loca['ZONA'] = substr($location, 0, 1);
				$env_loca['MUEBLE'] = substr($location, 1, 2);
				$env_loca['NIVEL'] = substr($location, 4, 3);
				$env_loca['LOCA'] = $location;
				$env_loca['SUCU'] = $sucursal;

				$send_inv['LOCACION'] = $location;
				$send_inv['USERNAME'] = $username;
				$send_inv['SUCURSAL'] = $sucursal;

				$idaum = 0;
				$form_ = [];
				/**
					*Obtener la cantidad y el codigo y guardarlo en un array
				**/
				foreach ($material as $vv) {
					$v = explode(',', $vv);
					$cod = !empty($v[0]) ? $v[0] : NULL;
					$cantidad = !empty($v[1]) ? $v[1] : 0;
					$form_[$cod] = $cantidad;
				}
				/**
					*Si existe articulos
				**/
				if(count($form_)>0) {
					//Optener todo los articulos agregados
					$up_date = $INV24DA->pre_inv_act($sucursal,$location);
					if(!empty($up_date)) {
						$idaum = 0;
						$idau_ = 0;
						$fo_tr = [];
						/**
						*Formar array para actualizar y editar
						**/
						foreach($up_date as $_cd_ => $_vv_) {
							$date = date("Y-m-d H:i:s");
							$__coa = (int)trim($_cd_);
							$__cant = !empty($_vv_['CANTIDAD']) ? $_vv_['CANTIDAD'] : 0;
							$__cant_ = !empty($form_[$__coa]) ? $form_[$__coa] : 0;
							$__fec = !empty($_vv_['FCREACION']) ? $_vv_['FCREACION'] : $date;
							$__use = !empty($_vv_['USERNAME']) ? $_vv_['USERNAME'] : $username;
							//Si existe articulos en la db siempre editar
							if(isset($form_[$__coa])) {
								$upd[$idaum] = $send_inv;
								$upd[$idaum]['COD_AS'] = $__coa;
								$upd[$idaum]['CANTIDAD'] = $__cant_;
								$upd[$idaum]['FCREACION'] = $__fec;
								$upd[$idaum]['USERNAME'] = $__use;
								$upd[$idaum]['INDICADOR'] = 0;
								
								$upd[$idaum]['USERMOD'] = $username;
								$upd[$idaum]['FECMODI'] = $date;
								$idaum++;
								unset($form_[$__coa]);								
							} else {
								//Si no coinciden articulo pero existe en la DB
								$fo_tr = $send_inv;
								$fo_tr['COD_AS'] = $__coa;
								$fo_tr['CANTIDAD'] = $__cant;
								$fo_tr['USERNAME'] = $__use;
								$fo_tr['FCREACION'] = $__fec;
								$fo_tr['INDICADOR'] = 0;
								//Registro tiene fecha de modificacion se forma array upd
								if(!empty($_vv_['USERMOD'])) {
									$fo_tr['USERMOD'] = $_vv_['USERMOD'];
									$fo_tr['FECMODI'] = $_vv_['FECMODI'];
									$idaum = count($upd);
									$upd[$idaum] = $fo_tr;
								} else {
									//Se forman registros solo de ingresos
									$idau_ = count($pre);
									$pre[$idau_] = $fo_tr;

								}
								$idau_++;
							}
						}
					}
					//Articulos
					if(!empty($form_)) {
						$idau_ = count($pre);
						foreach($form_ as $_cd_=> $_vv_) {
							$date = date("Y-m-d H:i:s");
							$pre[$idau_] = $send_inv;
							$pre[$idau_]['COD_AS'] = (int)$_cd_;
							$pre[$idau_]['CANTIDAD'] = $_vv_;
							$pre[$idau_]['FCREACION'] = $date;
							$pre[$idau_]['INDICADOR'] = 0;
							$idau_++;
						}						
					}
				}
				/*print_r("\n---upd---");
				print_r($upd);
				print_r("\n---PRE---");
				print_r($pre);
				print_r("\n---PRE---");*/
				//die();
				//BORRO PRE INVENTARIO
				$INV24DA->delete($sucursal, $location);
				
				if(count($pre) >0) {
					if($INV24DA->create_($pre)) {
						$response = array(
							'MSG' => "Enviado con exito",
							'VALIDATE' => '1',
						);
					} else {
						$response = array(
							'MSG' => "Error al enviar",
							'VALIDATE' => '0',
						);
					}              
				}

				if(count($upd)>0) {
					if($INV24DA->update($upd)) {
						$response = array(
							'MSG' => "Enviado con exito",
							'VALIDATE' => '1',
						);
					} else {
						$response = array(
							'MSG' => "Error al enviar",
							'VALIDATE' => '0',
						);
					}
				}
				
				//BORRO LOCACIONADAS
				$LOCACIONDA->deleteLocation($env_loca,$biblio);
				//AGREGO LOCACIONADAS
				$LOCACIONDA->add_location($env_loca,$biblio); 
			} else {
				$response = array(
					'MSG' => DEFAULT_DATA['Message']['invalido'],
					'VALIDATE' => '0',
				);
			}
			print_r(json_encode($response));
		}
		public function createPreInventario_($request) {
        
			$a_id = [];
			$upd = [];
			$pre = [];
			//print_r($request); die();
			if (DEFAULT_DATA['Auth']['token'] == $request['TOKEN']) {
				$INV24DA = new INV24DA();
				$PRODUCTOSDA = new PRODUCTOSDA();
				$LOCACIONDA = new LOCACIONDA();

				$sucursal = trim($request['SUCURSAL']);
				$username = trim($request['USERNAME']);            
				$material = explode("~", trim($request['MATERIAL']));            
				$location = trim($request['LOCATION']);

				$qblb = $LOCACIONDA->obtieneBiblioteca($sucursal);
				$biblio = $qblb[0]["BIBLIOTECA"];

				$env_loca['ALM'] = str_pad($sucursal, 3, "0", STR_PAD_LEFT);
				$env_loca['ZONA'] = substr($location, 0, 1);
				$env_loca['MUEBLE'] = substr($location, 1, 2);
				$env_loca['NIVEL'] = substr($location, 4, 3);
				$env_loca['LOCA'] = $location;
				$env_loca['SUCU'] = $sucursal;

				$send_inv['LOCACION'] = $location;
				$send_inv['USERNAME'] = $username;
				$send_inv['SUCURSAL'] = $sucursal;

				$idaum = 0;
				$form_ = [];
				foreach ($material as $vv) {
					$v = explode(',', $vv);
					$cod = !empty($v[0]) ? $v[0] : NULL;
					$cantidad = !empty($v[1]) ? $v[1] : 0;
					$form_[$cod] = $cantidad;
					//$env_i["'".$cod."'"] = $cod;
				}
				//print_r("FORM");
				//print_r($form_);
				if(count($form_)>0) {
					$up_date = $INV24DA->pre_inv_act_($sucursal,$location);
					//print_r("----UP_DATE");
					//print_r($up_date);
					//print_r("----");
					if(!empty($up_date)) {
						$idaum = 0;
						$idau_ = 0;
						$fo_tr = [];
						foreach($up_date as $_cd_ => $_vv_) {
							$date = date("Y-m-d H:i:s");
							$__coa = (int)trim($_cd_);
							$__cant = !empty($_vv_['CANTIDAD']) ? $_vv_['CANTIDAD'] : 0;
							$__cant_ = !empty($form_[$__coa]) ? $form_[$__coa] : 0;
							$__fec = !empty($_vv_['FCREACION']) ? $_vv_['FCREACION'] : $date;
							$__use = !empty($_vv_['USERNAME']) ? $_vv_['USERNAME'] : $username;
							
							if(isset($form_[$__coa])) {
								$upd[$idaum] = $send_inv;
								$upd[$idaum]['COD_AS'] = $__coa;
								$upd[$idaum]['CANTIDAD'] = $__cant_;
								$upd[$idaum]['FCREACION'] = $__fec;
								$upd[$idaum]['USERNAME'] = $__use;
								$upd[$idaum]['INDICADOR'] = 0;
								
								$upd[$idaum]['USERMOD'] = $username;
								$upd[$idaum]['FECMODI'] = $date;
								$idaum++;
								unset($form_[$__coa]);								
							} else {
								$fo_tr = $send_inv;
								$fo_tr['COD_AS'] = $__coa;
								$fo_tr['CANTIDAD'] = $__cant;
								$fo_tr['USERNAME'] = $__use;
								$fo_tr['FCREACION'] = $__fec;
								$fo_tr['INDICADOR'] = 0;
								if(!empty($_vv_['USERMOD'])) {
									$fo_tr['USERMOD'] = $_vv_['USERMOD'];
									$fo_tr['FECMODI'] = $_vv_['FECMODI'];
									$idaum = count($upd);
									$upd[$idaum] = $fo_tr;
								} else {
									$idau_ = count($pre);
									$pre[$idau_] = $fo_tr;

								}
								$idau_++;
							}
						}
					}

					if(!empty($form_)) {
						$idau_ = count($pre);
						foreach($form_ as $_cd_=> $_vv_) {
							$date = date("Y-m-d H:i:s");
							$pre[$idau_] = $send_inv;
							$pre[$idau_]['COD_AS'] = $_cd_;
							$pre[$idau_]['CANTIDAD'] = $_vv_;
							$pre[$idau_]['FCREACION'] = $date;
							$pre[$idau_]['INDICADOR'] = 0;
							$idau_++;
						}						
					}
				}
				/*print_r("\n---upd---");
				print_r($upd);
				print_r("\n---PRE---");
				print_r($pre);
				print_r("\n---PRE---");*/
				//die();
				//BORRO PRE INVENTARIO
				$INV24DA->delete($sucursal, $location);
				
				if(count($pre) >0) {
					if($INV24DA->create_($pre)) {
						$response = array(
							'MSG' => "Enviado con exito",
							'VALIDATE' => '1',
						);
					} else {
						$response = array(
							'MSG' => "Error al enviar",
							'VALIDATE' => '0',
						);
					}              
				}

				if(count($upd)>0) {
					if($INV24DA->update_($upd)) {
						$response = array(
							'MSG' => "Enviado con exito",
							'VALIDATE' => '1',
						);
					} else {
						$response = array(
							'MSG' => "Error al enviar",
							'VALIDATE' => '0',
						);
					}
				}
				
				//BORRO LOCACIONADAS
				$LOCACIONDA->deleteLocation($env_loca,$biblio);
				//AGREGO LOCACIONADAS
				$LOCACIONDA->add_location($env_loca,$biblio); 
			} else {
				$response = array(
					'MSG' => DEFAULT_DATA['Message']['invalido'],
					'VALIDATE' => '0',
				);
			}
			print_r(json_encode($response));
    }

	public function verArticulo($request) {
		$rsp['MSG'] = 'ERROR';
		$rsp['VALIDATE'] = 0;
		$rsp['RTA'] = '';
		//print_r(DEFAULT_DATA['Auth']['token']);
		//print_r($request);
		if (DEFAULT_DATA['Auth']['token'] == $request['TOKEN'] && !empty($request['CODIGO'])) {
			$INV24DA = new INV24DA();
			$data = $INV24DA->getProducts($request);
			//print_r($data);
			$rsp['VALIDATE'] = (count($data)) ? '1' : $rsp['VALIDATE'];
			$rsp['MSG'] = (count($data)) ? 'OK' : $rsp['MSG'];
			$rsp['RTA'] = $data;
		}
		print_r(json_encode($rsp));
	}

	public function ver_articulo($request) {
		$rsp['MSG'] = 'ERROR';
		$rsp['VALIDATE'] = 0;
		$rsp['RTA'] = '';
		//print_r(DEFAULT_DATA['Auth']['token']);
		//print_r($request);
		if (DEFAULT_DATA['Auth']['token'] == $request['TOKEN'] && !empty($request['CODIGO'])) {
			$INV24DA = new INV24DA();
			$data = $INV24DA->ver_articulo($request);
			//print_r($data);
			$rsp['VALIDATE'] = (count($data)) ? '1' : $rsp['VALIDATE'];
			$rsp['MSG'] = (count($data)) ? 'OK' : $rsp['MSG'];
			$rsp['RTA'] = $data;
		}
		print_r(json_encode($rsp));
	}
}

?>