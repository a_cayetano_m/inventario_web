<?php
namespace App\Controller\ErrorPage;

use System\Core\Controller;

class ErrorPageController extends Controller
{
  public $path_inicio;

  public function __construct()
  {
    
  }
  
  public function index()
  {
    $this->render(__CLASS__,"ErrorPage");
    
  }
}

?>