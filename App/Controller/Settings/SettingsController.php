<?php
    namespace App\Controller\Settings;

    use System\Core\Controller;

    use App\Model\PROV10\PROV10;
    use App\Model\PROV10\PROV10DA;
    use App\Model\PROV00\PROV00;
    use App\Model\PROV00\PROV00DA;
    use App\Model\PROV02\PROV02;
    use App\Model\PROV02\PROV02DA;
    use App\Model\PROV01\PROV01;
    use App\Model\PROV01\PROV01DA;
    use App\Model\PROV03\PROV03;
    use App\Model\PROV03\PROV03DA;
    use App\Model\PROV12\PROV12;
    use App\Model\PROV12\PROV12DA;
    use App\Model\PROV09\PROV09;
    use App\Model\PROV09\PROV09DA;
    use App\Model\PROV11\PROV11;
    use App\Model\PROV11\PROV11DA;
    use App\Model\PROV13\PROV13;
    use App\Model\PROV13\PROV13DA;
    use App\Model\PROV14\PROV14;
    use App\Model\PROV14\PROV14DA;
    use App\Model\PROV15\PROV15;
    use App\Model\PROV15\PROV15DA;
    use App\Model\PROV08\PROV08;
    use App\Model\API\API;
    use App\Model\PROV08\PROV08DA;
    use App\Helper\Helper as HelperMethod;
    use System\Core\Helper as HelperCore;
    use App\Model\Session\Session;
    use App\Model\Files\Files;

    class SettingsController extends Controller{

        public function __construct(){
            $this->session=new Session();
            $this->session->init();
            $this->Files=new Files();
			date_default_timezone_set ( 'America/Lima' );
        }
        public function index(){
            if(!$this->session->active()){
                header("Location: ".BASE_URL."login");
            }
            $this->render(__CLASS__,'Settings');
        }
        public function Settings($request){
            if(!$this->session->active()){
                header("Location: ".BASE_URL."login");
            }
            switch ($request['url_type']) {
                case 'User/index':
                    $response=$this->ShowAllUser();
                    $this->render(__CLASS__,$request['url_type'],$response);
                    break;
                case 'Role/index':
                    $response=$this->ShowAllRole();
                    $this->render(__CLASS__,$request['url_type'],$response);
                    break;
                case 'Menu/index':
                    $response=$this->ShowAllMenu();
                    $this->render(__CLASS__,$request['url_type'],$response);
                    break;
                case 'Submenu/index':
                    $response=$this->ShowAllSubmenu();
                    $this->render(__CLASS__,$request['url_type'],$response);
                    break;
                case 'Program/index':
                    $response=$this->ShowAllPrograms();
                    $this->render(__CLASS__,$request['url_type'],$response);
                    break;
               
            }
        }
        public function OptionsRoleView($request){
            if(!$this->session->active()){
                header("Location: ".BASE_URL."login");
            }
            switch ($request['type']) {
                case 'create':
                    $response=$this->ShowMenu();
                    $this->render(__CLASS__,"Role/".$request['url_type'],$response);
                    break;
                case 'show':
                    $response=array_merge($this->ShowMenuXrole($request['idRole']),array('menu'=>$this->ShowMenu()));
                    $this->render(__CLASS__,"Role/".$request['url_type'],$response);
                    break;
                case 'update':
                   /* $response=$this->ShowActionsXrole($request['idRole']);
                    $this->render(__CLASS__,"Role/".$request['url_type'],$response);*/
                    break;
                case 'delete':
                    break;
                   
                
            }
           
        }
        public function OptionsRole($request){
            if(!$this->session->active()){
                header("Location: ".BASE_URL."login");
            }
            switch ($request['type']) {
                case 'create':
                    $response=$this->CreateRole($request);
                    if($response){
                        print_r(json_encode(array('validate'=>true,'msg'=>DEFAULT_DATA['messages']['settings']['roles']['create'])));
                    }else{
                        print_r(json_encode(array('validate'=>false,'msg'=>DEFAULT_DATA['messages']['settings']['roles']['err_create'])));
                    }
                    break;
                case 'show':
                    break;
                case 'update':
                    
                    $response=$this->UpdateRole($request);
                    if($response){
                        print_r(json_encode(array('validate'=>true,'msg'=>DEFAULT_DATA['messages']['settings']['roles']['update'])));
                    }else{
                        print_r(json_encode(array('validate'=>false,'msg'=>DEFAULT_DATA['messages']['settings']['roles']['err_update'])));
                    }
                    break;
                case 'delete':
                    $response=$this->DeleteRole($request);
                    if($response){
                        print_r(json_encode(array('validate'=>true,'msg'=>DEFAULT_DATA['messages']['settings']['roles']['delete'])));
                    }else{
                        print_r(json_encode(array('validate'=>false,'msg'=>DEFAULT_DATA['messages']['settings']['roles']['err_delete'])));
                    }
                    break;
            }
           
        }
        public function OptionsMenuView($request){
            if(!$this->session->active()){
                header("Location: ".BASE_URL."login");
            }
            switch ($request['type']) {
                case 'create':
                    $this->render(__CLASS__,"Menu/".$request['url_type']);
                    break;
                case 'show':
                    $response=$this->ShowXmenu($request['idMenu']);
                    $this->render(__CLASS__,"Menu/".$request['url_type'],$response);
                    break;
                case 'update':
                   
                    break;
                case 'delete':
                    break;
                   
                
            }
           
        }
        public function OptionsMenu($request){
            if(!$this->session->active()){
                header("Location: ".BASE_URL."login");
            }
            switch ($request['type']) {
                case 'create':
                    $response=$this->CreateMenu($request);
                    if($response){
                        print_r(json_encode(array('validate'=>true,'msg'=>DEFAULT_DATA['messages']['settings']['menu']['create'])));
                    }else{
                        print_r(json_encode(array('validate'=>false,'msg'=>DEFAULT_DATA['messages']['settings']['menu']['err_create'])));
                    }
                    break;
               
                case 'show':
                    break;
                case 'update':
                    $response=$this->UpdateMenu($request);
                    if($response){
                        print_r(json_encode(array('validate'=>true,'msg'=>DEFAULT_DATA['messages']['settings']['menu']['update'])));
                    }else{
                        print_r(json_encode(array('validate'=>false,'msg'=>DEFAULT_DATA['messages']['settings']['menu']['err_update'])));
                    }
                    break;
                case 'delete':
                    $response=$this->DeleteMenu($request);
                    if($response){
                        print_r(json_encode(array('validate'=>true,'msg'=>DEFAULT_DATA['messages']['settings']['menu']['delete'])));
                    }else{
                        print_r(json_encode(array('validate'=>false,'msg'=>DEFAULT_DATA['messages']['settings']['menu']['err_delete'])));
                    }
                    break;
            }
           
        }
        public function OptionsSubmenuView($request){
            if(!$this->session->active()){
                header("Location: ".BASE_URL."login");
            }
            switch ($request['type']) {
                case 'create':
                    $response=$this->ShowAllMenu($request);
                    $this->render(__CLASS__,"Submenu/".$request['url_type'],$response);
                    break;
                case 'show':
                    $response=array($this->ShowXsubmenu($request['idSubmenu']),$this->ShowAllMenu($request));
                    $this->render(__CLASS__,"Submenu/".$request['url_type'],$response);
                    break;
                case 'update':
                   
                    break;
                case 'delete':
                    break;
                   
                
            }
           
        }
        public function OptionsSubmenu($request){
            if(!$this->session->active()){
                header("Location: ".BASE_URL."login");
            }
            switch ($request['type']) {
                case 'create':
                    $response=$this->CreateSubmenu($request);
                    if($response){
                        print_r(json_encode(array('validate'=>true,'msg'=>DEFAULT_DATA['messages']['settings']['submenu']['create'])));
                    }else{
                        print_r(json_encode(array('validate'=>false,'msg'=>DEFAULT_DATA['messages']['settings']['submenu']['err_create'])));
                    }
                    break;
               
                case 'show':
                    break;
                case 'update':
                    $response=$this->UpdateSubmenu($request);
                    if($response){
                        print_r(json_encode(array('validate'=>true,'msg'=>DEFAULT_DATA['messages']['settings']['submenu']['update'])));
                    }else{
                        print_r(json_encode(array('validate'=>false,'msg'=>DEFAULT_DATA['messages']['settings']['submenu']['err_update'])));
                    }
                    break;
                case 'delete':
                    $response=$this->DeleteSubmenu($request);
                    if($response){
                        print_r(json_encode(array('validate'=>true,'msg'=>DEFAULT_DATA['messages']['settings']['submenu']['delete'])));
                    }else{
                        print_r(json_encode(array('validate'=>false,'msg'=>DEFAULT_DATA['messages']['settings']['submenu']['err_delete'])));
                    }
                    break;
            }
           
        }
        public function OptionsUserView($request){ 
            if(!$this->session->active()){
                header("Location: ".BASE_URL."login");
            }
            switch ($request['type']) {
                case 'create':
                   
                    $area=$this->ShowArea(); //AREAS EN GENERAL
                    $status=$this->ShowStatus(); //ESTADOS EN GENERAL
                    $role=$this->ShowAllRole('1'); //ROLES EN GENERAL
                    $submenu=$this->ShowMenuAllSubmenu();
                    $supervisor=$this->ShowAllSuperior();
                    $programs=$this->ShowAllPrograms();
                    $porcentajes=array('porcentajes'=>DEFAULT_DATA['values']['porcentajes']);
                    $tipo=array('tipos'=>DEFAULT_DATA['values']['tipos']);
                    $response=array_merge($area,$status,$role,$submenu,$supervisor,$programs,$porcentajes,$tipo);
                    $this->render(__CLASS__,"User/".$request['url_type'],$response);
                    break;
                case 'show':
                    $area=$this->ShowArea();
                    $status=$this->ShowStatus();
                    $role=$this->ShowAllRole('1');
                    $submenu=$this->ShowMenuAllSubmenu();
                    $supervisor=$this->ShowAllSuperior();
                    $programs=$this->ShowAllPrograms();
                    //$sucursal=$this->ShowAllSucursal();
                    $porcentajes=array('porcentajes'=>DEFAULT_DATA['values']['porcentajes']);
                    $tipo=array('tipos'=>DEFAULT_DATA['values']['tipos']);
                    $roles_asignados=$this->ShowXuser($request['idUser']);//MIS ROLES
                    $response=array_merge($area,$status,$role,$submenu,$supervisor,$programs,$porcentajes,$tipo,array("values"=>$roles_asignados));
                    $this->render(__CLASS__,"User/".$request['url_type'],$response);
                    break;
                case 'list':
                    $response=$this->ShowAllUser();
                    $this->render(__CLASS__,"User/".$request['url_type'],$response);
                    break;
                case 'update':
                    break;
                case 'delete':
                    break;
                
            }
           
        }
        public function OptionsUser($request){
            
            if(!$this->session->active()){
                header("Location: ".BASE_URL."login");
            }
            switch ($request['type']) {
                case 'create':
                    $response=$this->CreateUser($request);
                    
                    if($response){
                        print_r(json_encode(array('validate'=>true,'msg'=>DEFAULT_DATA['messages']['settings']['user']['create'])));
                    }else{
                        print_r(json_encode(array('validate'=>false,'msg'=>DEFAULT_DATA['messages']['settings']['user']['err_create'])));
                    }
                    break;
               
                case 'show':
                    break;
                case 'update':
                    $response=$this->UpdateUser($request);
                    if($response){
                        print_r(json_encode(array('validate'=>true,'msg'=>DEFAULT_DATA['messages']['settings']['user']['update'])));
                    }else{
                        print_r(json_encode(array('validate'=>false,'msg'=>DEFAULT_DATA['messages']['settings']['user']['err_update'])));
                    }
                    break;
                case 'delete':
                    $response=$this->DeleteUser($request);
                    if($response){
                        print_r(json_encode(array('validate'=>true,'msg'=>DEFAULT_DATA['messages']['settings']['user']['delete'])));
                    }else{
                        print_r(json_encode(array('validate'=>false,'msg'=>DEFAULT_DATA['messages']['settings']['user']['err_delete'])));
                    }
                    break;
               /* case 'submenu':
                    $ECM12DA=new ECM12DA();
                    $submenu=$ECM12DA->ShowAllSubmenu();  
                    print_r($submenu);
                    die();
                    if(count($submenu)>0){  
                        print_r(json_encode(array('validate'=>true,'msg'=>'','submenu'=>$submenu)));
                    }else{
                        print_r(json_encode(array('validate'=>false)));
                    }
                    break;*/
            }
           
        }
        public function OptionsProgramView($request){ 
            if(!$this->session->active()){
                header("Location: ".BASE_URL."login");
            }
            switch ($request['type']) {
                case 'create':
                    $status=$this->ShowStatus(); //ESTADOS EN GENERAL
                    $proveedores=$this->ShowRucs(); //PROVEEDORES EN GENERAL
                    $response=array_merge($status,$proveedores);
                    $this->render(__CLASS__,"Program/".$request['url_type'],$response);
                    break;
                case 'show':
                    $status=$this->ShowStatus(); //ESTADOS EN GENERAL
                    $proveedores=$this->ShowRucs(); //PROVEEDORES EN GENERAL
                    $program=array($this->ShowXPrograms($request['id']));
                    $proveedores_asignados=$this->ShowRucXPrograms($request['id']);
                    $response=array_merge($program,$status,$proveedores,$proveedores_asignados);
                    $this->render(__CLASS__,"Program/".$request['url_type'],$response);
                    break;
                case 'update':
                    break;
                case 'delete':
                    break;
                
            }
           
        }
        public function OptionsProgram($request){
            
            if(!$this->session->active()){
                header("Location: ".BASE_URL."login");
            }
            switch ($request['type']) {
                case 'create':
                    $response=$this->CreatePrograms($request);
                    
                    if($response){
                        print_r(json_encode(array('validate'=>true,'msg'=>DEFAULT_DATA['messages']['settings']['program']['create'])));
                    }else{
                        print_r(json_encode(array('validate'=>false,'msg'=>DEFAULT_DATA['messages']['settings']['program']['err_create'])));
                    }
                    break;
               
                case 'show':
                    break;
                case 'update':
                    $response=$this->UpdatePrograms($request);
                    if($response){
                        print_r(json_encode(array('validate'=>true,'msg'=>DEFAULT_DATA['messages']['settings']['program']['update'])));
                    }else{
                        print_r(json_encode(array('validate'=>false,'msg'=>DEFAULT_DATA['messages']['settings']['program']['err_update'])));
                    }
                    break;
                case 'delete':
                    break;
            }
           
        }
        
        /* -----------------------------------------ROLE---------------------------------------------- */
        protected function ShowAllRole($status=''){
            $PROV14DA=new PROV14DA();
            $PROV15DA=new PROV15DA();
            $data=$PROV14DA->ShowAllRole();
            if($status==''){
                $data=$PROV14DA->ShowAllRole();
            }else{
                $data=$PROV14DA->ShowAllRoleXstatus($status);
            }
            
            $array_roles=array();
            $response=array();
            if(count($data)>0){
                if($status==''){
                    foreach($data as $k=>$v){
                        if(!array_key_exists($v['CODROLE'],$array_roles)){
                            $array_roles[$v['CODROLE']]=array("codrole"=>$v['CODROLE'],"namerole"=>utf8_encode($v['NAMEROLE']),"estrole"=>$v['ESTROLE']);
                        }
                    }
                }else{
                    foreach($data as $k=>$v){
                        if(!array_key_exists($v['CODROLE'],$array_roles)){
                            $array_roles[$v['NAMEROUTE']][$v['CODROLE']]=array("codrole"=>$v['CODROLE'],"namerole"=>utf8_encode($v['NAMEROLE']),"estrole"=>$v['ESTROLE'],"nameruta"=>utf8_encode($v['NAMEROUTE']));
                        }
                    }
                }
                $response=array("roles"=>$array_roles);
            }
            return $response;
        }
        
        protected function ShowMenuXrole($idRole){
            $PROV14DA=new PROV14DA();
            $data=$PROV14DA->ShowMenuXrole($idRole);
            $array_roles=array();
            $response=array();
            if(count($data)>0){
                foreach($data as $k=>$v){
                    if(!array_key_exists($v['CODROLE'],$array_roles)){
                        $array_roles=array("codrole"=>$v['CODROLE'],"namerole"=>utf8_encode($v['NAMEROLE']),"estrole"=>$v['ESTROLE'],"nameroute"=>$v['ROUTENAME'],"routecod"=>$v['ROUTECOD']);
                    }
                }
                $response=array("roles"=>$array_roles);
            }
            return $response;
        }
        protected function ShowMenu(){
            $PROV10DA=new PROV10DA();
            $data=$PROV10DA->ShowMenu();
            $array_menu=array();
            $response=array();
            if(count($data)>0){
                foreach($data as $k=>$v){
                    $response[]=array("routecod"=>$v['ROUTECODE'],"nameroute"=>utf8_encode($v['NAMEROUTE']));
                }
                
            }
            return $response;
        }
        protected function CreateRole($request){
            if(!$this->session->active()){
                header("Location: ".BASE_URL."login");
            }
            $PROV14DA=new PROV14DA();
            $PROV15DA=new PROV15DA();
            $PROV14=new PROV14();
            $newcoderole=$PROV14DA->NewCode();
            $PROV14->MKRLCOD=$newcoderole;
            $PROV14->MKRLNOM=mb_strtolower($request['ipt_role_name']);
            $PROV14->MKRLEST=$request['ipt_role_est'];
            $PROV14->MKRFCR=date("Y-m-d H:i:s");
            $PROV14->MKMECOD=$request['ipt_menu'];
            $response=$PROV14DA->CreateRole($PROV14);
            return $response;
        }
        protected function UpdateRole($request){
            if(!$this->session->active()){
                header("Location: ".BASE_URL."login");
            }
            $PROV14DA=new PROV14DA();
            $PROV15DA=new PROV15DA();
            $PROV14=new PROV14();
            $PROV14->MKRLCOD=$request['ipt_role_code'];
            $PROV14->MKRLNOM=mb_strtolower($request['ipt_role_name']);
            $PROV14->MKRLEST=$request['ipt_role_est'];
            $PROV14->MKMECOD=$request['ipt_menu'];
            $response=$PROV14DA->UpdateRole($PROV14);
            return $response;
        }
        protected function DeleteRole($request){
            if(!$this->session->active()){
                header("Location: ".BASE_URL."login");
            }
            $PROV14DA=new PROV14DA();
            $PROV15DA=new PROV15DA();
            $response=$PROV15DA->Validate($request['ipt_role_code']);
            if(count($response)==0){
                $PROV14DA->DeleteRole($request['ipt_role_code']);
                return true;
            }else{
                return false;
            }
           
        }
        protected function ShowAllMenu(){
            $PROV10DA=new PROV10DA();
            $data=$PROV10DA->ShowAllMenu();
            $response=array();
            $array_menu=array();
            if(count($data)>0){
                foreach($data as $k=>$v){
                    $array_menu[]=array("codmenu"=>$v['CODMENU'],"namemenu"=>utf8_encode($v['NAMEMENU']),"ordenmenu"=>$v['ORDENMENU']);
                }
                $response=array("menu"=>$array_menu);
            }
            return $response;
        }
        protected function ShowXmenu($idMenu){
            $PROV10DA=new PROV10DA();
            $data=$PROV10DA->ShowXmenu($idMenu);
            $array_menu=array();
            $response=array();
            if(count($data)>0){
                foreach($data as $k=>$v){
                    $array_menu=array("codmenu"=>$v['CODMENU'],"namemenu"=>utf8_encode($v['NAMEMENU']),"ordenmenu"=>$v['ORDENMENU'],"iconmenu"=>$v['ICONMENU'],"routemenu"=>$v['ROUTEMENU'],"typemenu"=>$v['TYPEMENU']);
                }
                $response=array("menu"=>$array_menu);
            }
            return $response;
        }
        protected function CreateMenu($request){
            if(!$this->session->active()){
                header("Location: ".BASE_URL."login");
            }
            $PROV10DA=new PROV10DA();
            $PROV10=new PROV10();
            $newcodemenu=$PROV10DA->NewCode();
            $PROV10->MKMECOD=$newcodemenu;
            $PROV10->MKMENOM=$request['ipt_menu_name'];
            $PROV10->MKMEORD=$request['ipt_menu_orden'];
            $PROV10->MKMEICON=$request['ipt_icon_name'];
            $PROV10->MKMERU=$request['ipt_route_name'];
            $PROV10->MKMETYP=$request['ipt_type_name'];
            $PROV10->MKMEFCR=date("Y-m-d H:i:s");

            $response=$PROV10DA->CreateMenu($PROV10);
            if($response){
                $this->Files->Create($request['ipt_route_name'],$request['ipt_menu_name']); //CREAR FILES POR MENU
            }
            return $response;
        }
        protected function UpdateMenu($request){
            if(!$this->session->active()){
                header("Location: ".BASE_URL."login");
            }
            $PROV10DA=new PROV10DA();
            $PROV10=new PROV10();
            $PROV10->MKMECOD=$request['ipt_menu_cod'];
            $PROV10->MKMENOM=$request['ipt_menu_name'];
            $PROV10->MKMEORD=$request['ipt_menu_orden'];
            $PROV10->MKMEICON=$request['ipt_icon_name'];
            $PROV10->MKMERU=$request['ipt_route_name'];
            $PROV10->MKMEFCR=date("Y-m-d H:i:s");
            $PROV10->MKMETYP=$request['ipt_type_name'];
            $response=$PROV10DA->UpdateMenu($PROV10);
            return $response;
        }
        protected function DeleteMenu($request){
            if(!$this->session->active()){
                header("Location: ".BASE_URL."login");
            }
            $PROV10DA=new PROV10DA();
            $response=$PROV10DA->Validate($request['ipt_menu_code']);
            
            if(count($response)==0){
                $PROV10DA->DeleteMenu($request['ipt_menu_code']);
                return true;
            }else{
                return false;
            }
           
        }
        protected function CreateSubmenu($request){
            if(!$this->session->active()){
                header("Location: ".BASE_URL."login");
            }
            $PROV12DA=new PROV12DA();
            $PROV10DA=new PROV10DA();
            $PROV12=new PROV12();
            $newcodemenu=$PROV12DA->NewCode();
            $PROV12->MKSMCOD=$newcodemenu;
            $PROV12->MKSMNOM=$request['ipt_submenumenu_name'];
            $PROV12->MKSMRUT=$request['ipt_submenuroute_name'];
            if($request['slc_menu_name']==''){
                $PROV12->MKMECOD=0;
            }else{
                $PROV12->MKMECOD=$request['slc_menu_name'];
            }
            $PROV12->MKSMORD=$request['ipt_submenumenu_orden'];
            $PROV12->MKSMICON=$request['ipt_submenuicon_name'];
            $PROV12->MKSMDES=$request['ipt_submenudes_name'];
            $PROV12->MKSMFCR=date("Ymd");
            $PROV12->MKSMHCR=date("His");
            $routemenuname=$PROV10DA->getRoute($request['slc_menu_name'])[0]['ROUTEMENU'];
            $response= $PROV12DA->CreateSubmenu($PROV12);
            
            if($response){
                $this->Files->CreateView($routemenuname,$request['ipt_submenuroute_name'],$request['ipt_submenudes_name']); //CREAR FILES POR SUBMENU y MENU
            }
            
            return $response;
        }
        protected function ShowAllSubmenu(){
            $PROV12DA=new PROV12DA();
            $data=$PROV12DA->ShowAllSubmenu();
            $response=array();
            $array_submenu=array();
            if(count($data)>0){
                foreach($data as $k=>$v){
                    /*$array_submenu[]=array("codsubmenu"=>$v['CODSUBMENU'],"namesubmenu"=>utf8_encode($v['NAMESUBMENU']),"codmenu"=>$v['CODMENU'],"routesubmenu"=>utf8_encode($v['ROUTESUBMENU']),"ordensubmenu"=>$v['ORDENSUBMENU'],"iconsubmenu"=>$v['ICONSUBMENU']);*/
                    $array_submenu[]=array("codsubmenu"=>$v['CODSUBMENU'],"namesubmenu"=>utf8_encode($v['NAMESUBMENU']),"ordensubmenu"=>$v['ORDENSUBMENU'],"dessubmenu"=>utf8_encode($v['DESSUBMENU']));
                }
                $response=array("submenu"=>$array_submenu);
            }
            return $response;
        }
        protected function ShowXsubmenu($idSubmenu){
            $PROV12DA=new PROV12DA();
            $data=$PROV12DA->ShowXsubmenu($idSubmenu);
            $array_submenu=array();
            $response=array();
            if(count($data)>0){
                foreach($data as $k=>$v){
                    $array_submenu=array("codsubmenu"=>$v['CODSUBMENU'],"namesubmenu"=>utf8_encode($v['NAMESUBMENU']),"ordensubmenu"=>$v['ORDENSUBMENU'],"codmenu"=>$v['CODMENU'],"iconsubmenu"=>$v['ICONSUBMENU'],"routesubmenu"=>$v['ROUTESUBMENU'],"dessubmenu"=>$v['DESSUBMENU']);
                }
                $response=array("submenu"=>$array_submenu);
            }
            return $response;
        }
        protected function UpdateSubmenu($request){
            if(!$this->session->active()){
                header("Location: ".BASE_URL."login");
            }
            $PROV12DA=new PROV12DA();
            $PROV12=new PROV12();
            if($request['slc_menu_name']==''){
                $PROV12->MKMECOD=0;
            }else{
                $PROV12->MKMECOD=$request['slc_menu_name'];
            }
            $PROV12->MKSMCOD=$request['ipt_submenu_cod'];
            $PROV12->MKSMNOM=$request['ipt_submenumenu_name'];
            
            $PROV12->MKSMORD=$request['ipt_submenumenu_orden'];
            $PROV12->MKSMICON=$request['ipt_submenuicon_name'];
            $PROV12->MKSMRUT=$request['ipt_submenuroute_name'];
            $response=$PROV12DA->UpdateSubmenu($PROV12);
            return $response;
        }
        protected function DeleteSubmenu($request){
         
            if(!$this->session->active()){
                header("Location: ".BASE_URL."login");
            }
            $PROV12DA=new PROV12DA();
            $PROV12DA->DeleteSubmenu($request['ipt_submenu_code']);
            return true;
           /* $response=$PROV12DA->Validate($request['ipt_submenu_code'])[0]['ECMECOD'];
            if($response=='0'){
              
                return true;
            }else{
                return false;
            }*/
           
        }
        protected function ShowAllUser(){
            $PROV00DA=new PROV00DA();
            $data=$PROV00DA->ShowAllUsers();
            $response=array();
            $array_user_aux=array();
            if(count($data)>0){
                foreach($data as $k=>$v){
                    if(!in_array($v['CODUSER'],$array_user_aux)){
                        $array_user[]=array(
                            "coduser"=>$v['CODUSER'],
                            "nameuser"=>utf8_encode($v['NAMEUSER']),
                            "appuser"=>$v['APPUSER'],
                            "ruc"=>$v['RUC'],
                            "razon"=>utf8_encode($v['RAZON']),
                            "status"=>$v['EST'],
                            "email"=>$v['EMAIL'],
                            "user"=>$v['USER']);
                        $array_user_aux[]=$v['CODUSER'];
                    }
                    
                }
                $response=array("users"=>$array_user);
            }
            return $response;
        }
        
        protected function ShowArea(){
            $PROV09DA=new PROV09DA();
            $data=$PROV09DA->ShowArea();
            $response=array();
            $array_area=array();
            if(count($data)>0){
                foreach($data as $k=>$v){
                    $array_area[]=array("codare"=>$v['CODARE'],"nameare"=>utf8_encode($v['NAMEARE']));
                }
                $response=array("area"=>$array_area);
            }
            return $response;
        }
        protected function ShowRucs(){
            $PROV00DA=new PROV00DA();
            $data=$PROV00DA->ShowRucs();
            $response=array();
            $array_data=array();
            if(count($data)>0){
                foreach($data as $k=>$v){
                    $array_data[]=array("ruc"=>$v['RUC'],"razon"=>$v['RAZON']);
                }
                $response=array("proveedores"=>$array_data);
            }
            return $response;
        }
        protected function ShowStatus(){
            $PROV11DA=new PROV11DA();
            $data=$PROV11DA->ShowStatus();
            $response=array();
            $array_status=array();
            if(count($data)>0){
                foreach($data as $k=>$v){
                    $array_status[]=array("codest"=>$v['CODEST'],"nameest"=>utf8_encode($v['NAMEEST']));
                }
                $response=array("status"=>$array_status);
            }
            return $response;
        }
        protected function ShowMenuAllSubmenu(){
            $PROV12DA=new PROV12DA();
            $data=$PROV12DA->ShowAllSubmenu();
            $array_submenu=array();
            if(count($data)>0){
                foreach($data as $key=>$value){
                    $array_submenu[]=array("codsubmenu"=>$value['CODSUBMENU'],"dessubmenu"=>$value['DESSUBMENU']);
                }
            }
            return array("submenu"=>$array_submenu);
        }
        protected function ShowAllSuperior(){
            $PROV08DA=new PROV08DA();
            $data=$PROV08DA->ShowAllSuperior();
            $array_sup=array();
            if(count($data)>0){
                foreach($data as $key=>$value){
                    $array_sup[]=array("codsup"=>$value['CODSUP'],"nomsup"=>$value['NOMSUP']);
                }
            }
            return array("supervisor"=>$array_sup);
        }
       /* protected function ShowAllSucursal(){
            $DATGENE=new DATGENE();
            $data=$DATGENE->getAll();
            $array_suc=array();
            if(count($data)>0){
                foreach($data as $key=>$value){
                    $array_suc[]=array("codsuc"=>$value['COD'],"nomsuc"=>$value['SUCURSAL']);
                }
            }
            return array("sucursal"=>$array_suc);
        }*/
        protected function CreateUser($request,$usercod=''){
            if(!$this->session->active()){
                header("Location: ".BASE_URL."login");
            }
            
          

            $PROV00DA=new PROV00DA();
            $PROV01DA=new PROV01DA();
            $PROV13DA=new PROV13DA();
            $PROV15DA=new PROV15DA();
            $PROV03DA=new PROV03DA();
            $PROV00=new PROV00();
            $PROV01=new PROV01();
            $API=new API();
            $email=utf8_encode(trim($request['ipt_email']));
            $user=utf8_encode(trim($request['ipt_user']));
            $exists=$PROV00DA->existUser($user);
            $exists_email=$PROV00DA->existEmail($email);
            if($exists['cantidad']==0 && $exists_email['cantidad']==0){
                if($usercod==''){
                    $usercod=$PROV00DA->NewCode();
                }
                
                $array_obj_user=array();
                $array_obj_submenu=array();
                $array_obj_sup=array();
                $array_obj_roles=array();
                $array_obj_programs=array();
                if(count($request['slc_role'])>0){
                    foreach($request['slc_role'] as $key=>$value){
                        $PROV15=new PROV15();
                        $PROV15->MKRLCOD=$value;
                        $PROV15->MKUCOD=$usercod;
                        $array_obj_roles[]=$PROV15;
                    }
                    
                }
                if(count($request['slc_programs_user'])>0){
                    foreach($request['slc_programs_user'] as $key=>$value){
                        $PROV01=new PROV01();
                        $PROV01->PUCOD=$usercod;
                        $PROV01->PPCOD=$value;
                        $PROV01->PURUC=$request['ipt_dni'];
                        $array_obj_programs[]=$PROV01;
                    }
                    
                }
                $PROV00->PUCOD=$usercod;
                $PROV00->PURUC=$request['ipt_dni']; 
                $PROV00->PURAZON=$request['ipt_razon']; 
                $PROV00->PUSER=$request['ipt_user'];
                $PROV00->PUNOM=$request['ipt_name'];
                $PROV00->PUAPP=$request['ipt_app'];
                $PROV00->PUMAI=$request['ipt_email'];
                $PROV00->PUPAS=$request['ipt_pass'];
                $PROV00->PUARE=$request['slc_area'];
                $PROV00->PUFER=date("Y-m-d H:i:s");
                $PROV00->PUEST=$request['slc_status'];
                $PROV00->PUTLF=$request['ipt_tlf'];
                $PROV00->PUPRCJ=$request['slc_porcetaje'];
                $PROV00->PUTYPE=$request['slc_tipo'];
                $PROV00->PULMIN=$request['ipt_limit_min'];
                $PROV00->PULMAX=$request['ipt_limit_max'];
                $array_obj_user[]=$PROV00;
                
                if(count($request['slc_menu_user'])>0){
                    foreach($request['slc_menu_user'] as $key=>$value){
                        $PROV13=new PROV13();
                        $PROV13->MKSMCOD=$value;
                        $PROV13->MKUCOD=$usercod;
                        $array_obj_submenu[]=$PROV13;
                    }
                }
                
                $response=$PROV00DA->CreateMassiveUser($array_obj_user);
                if($response){
                    /*if(isset($request['slc_sup_user']) && count($request['slc_sup_user'])>0){
                        foreach($request['slc_sup_user'] as $key=>$value){
                            $PROV03=new PROV03();
                            $PROV03->SUPCOD=$value;
                            $PROV03->MKUCOD=$usercod;
                            $array_obj_sup[]=$PROV03;
                        }
                        $PROV03DA->insertMasivo($array_obj_sup);
                    }*/
                    $response=$PROV13DA->CreateMassive($array_obj_submenu);
                    if($response){
                        $PROV15DA->CreateMassive($array_obj_roles);
                        $PROV01DA->CreateMassive($array_obj_programs);
    
                       
                        
                        return true;
                    }else{
                        $PROV00DA->DeleteUser($usercod);
                        return false;
                    }
                }else{
                    return false;
                }
            }else{
                return false;
            }
            
           
        }
        protected function ShowXuser($iduser){
            $PROV00DA=new PROV00DA();
            $data=$PROV00DA->ShowXuser($iduser);
           
            $array_user=array();
            $array_area=array();
            $array_est=array();
            $array_role=array();
            $array_submenu=array();
            $array_sup=array();
            $array_program=array();
            /*$array_role_aux=array();
            $array_submenu_aux=array();*/
            
            $response=array();
            if(count($data)>0){
                foreach($data as $k=>$v){
                    $array_user=array("usercod"=>$v['USERCOD'],"userdni"=>$v['USERDNI'],"userrazon"=>$v['USERRAZON'],"user"=>$v['USER'],"username"=>$v['USERNAME'],"userpass"=>$v['USERPASS'],"userapp"=>$v['USERAPP'],"useremail"=>$v['USERMAIL'],"usertlf"=>$v['USERTLF'],"porcentaje"=>$v['PORCENTAJE_PROVEEDOR'],"tipo_proveedor"=>$v['TIPO_PROVEEDOR'],"limite_min"=>$v['LIMITE_MIN'],"limite_max"=>$v['LIMITE_MAX']);
                    
                    if(!in_array($v['ROLECODE'],$array_role)){
                        $array_role[]=$v['ROLECODE'];
                    }
                    if(!in_array($v['CODSUBMENU'],$array_submenu)){
                        $array_submenu[]=$v['CODSUBMENU'];
                    }
                    if(!in_array($v['ESTCOD'],$array_est)){
                        $array_est[]=$v['ESTCOD'];
                    }
                    if(!in_array($v['AREACOD'],$array_area)){
                        $array_area[]=$v['AREACOD'];
                    }
                    if(!in_array($v['CODPROGRAM'],$array_program)){
                        $array_program[]=$v['CODPROGRAM'];
                    }
                    /*if($v['CODSUP']!='' && !in_array($v['CODSUP'],$array_sup)){
                        $array_sup[]=$v['CODSUP'];
                    }*/
                }
                $response=array("user"=>$array_user,"role"=>$array_role,"submenu"=>$array_submenu,"area"=>$array_area,"status"=>$array_est,"sup"=>$array_sup,"program"=>$array_program);
            }
            return $response;
        }
        protected function UpdateUser($request){
            if(!$this->session->active()){
                header("Location: ".BASE_URL."login");
            }
            $PROV00DA=new PROV00DA();
            $PROV01DA=new PROV01DA();
            $PROV03DA=new PROV03DA();
            $PROV13DA=new PROV13DA();
            $PROV15DA=new PROV15DA();
            $PROV00=new PROV00();
            $PROV01=new PROV01();
            $PROV00->PUCOD=$request['ipt_cod'];
            $PROV00->PUSER=$request['ipt_user'];
            $PROV00->PURUC=$request['ipt_dni'];
            $PROV00->PURAZON=$request['ipt_razon'];
            $PROV00->PUNOM=$request['ipt_name'];
            $PROV00->PUAPP=$request['ipt_app'];
            $PROV00->PUMAI=$request['ipt_email'];
            $PROV00->PUPAS=$request['ipt_pass'];
            $PROV00->PUARE=$request['slc_area'];
            $PROV00->PUEST=$request['slc_status'];
            $PROV00->PUTLF=$request['ipt_tlf'];
            $PROV00->PUPRCJ=$request['slc_porcetaje'];
            $PROV00->PUTYPE=$request['slc_tipo'];
            $PROV00->PULMIN=$request['ipt_limit_min'];
            $PROV00->PULMAX=$request['ipt_limit_max'];
            
            $array_obj_submenu=array();
            $array_obj_roles=array();
            $array_obj_programs=array();
            //$PROV00->MKUSUP=$request['slc_sup_user'];
            if($PROV00DA->UpdateUser($PROV00)){
                $response=$PROV13DA->DeleteByUser($request['ipt_cod']);
                if($response){
                    foreach($request['slc_menu_user'] as $key=>$value){
                        $PROV13=new PROV13();
                        $PROV13->MKSMCOD=$value;
                        $PROV13->MKUCOD=$request['ipt_cod'];
                        $array_obj_submenu[]=$PROV13;
                    }
                    if($PROV13DA->CreateMassive($array_obj_submenu)){
                        $response=$PROV15DA->DeleteByUser($request['ipt_cod']);
                        if($response){
                            foreach($request['slc_role'] as $key=>$value){
                                $PROV15=new PROV15();
                                $PROV15->MKRLCOD=$value;
                                $PROV15->MKUCOD=$request['ipt_cod'];
                                $array_obj_roles[]=$PROV15;
                            }
                            $PROV15DA->CreateMassive($array_obj_roles);
                        }
                        $response=$PROV01DA->DeleteByUser($request['ipt_cod']);
                        if($response){
                            foreach($request['slc_programs_user'] as $key=>$value){
                                $PROV01=new PROV01();
                                $PROV01->PUCOD=$request['ipt_cod'];
                                $PROV01->PPCOD=$value;
                                $PROV01->PURUC=$request['ipt_dni'];
                                $array_obj_programs[]=$PROV01;
                            }
                            $PROV01DA->CreateMassive($array_obj_programs);
                        }
                    }
                    /* if(isset($request['slc_sup_user']) && $request['slc_sup_user']!='' ){
                        foreach($request['slc_sup_user'] as $key=>$value){
                            $PROV03=new $PROV03();
                            $PROV03->SUPCOD=$value;
                            $PROV03->MKUCOD=$request['ipt_cod'];
                            $array_obj_sup[]=$PROV03;
                        }
                        $PROV03DA->insertMasivo($array_obj_sup);
                    }*/
                }
               


            }
            return $response;
        }

        /* PROGRAMS */ 

        protected function ShowAllPrograms(){
            $PROV02DA=new PROV02DA();
            $data=$PROV02DA->ShowAllPrograms();
            $response=array();
            $array_programs=array();
            if(count($data)>0){
                foreach($data as $k=>$v){
                    $array_programs[]=array("cod"=>$v['CODPROGRAM'],"descripcion"=>utf8_encode($v['DESCRIPCION']),"status"=>$v['PRGRAMEST'],"orden"=>$v['ORDEN']);
                }
                $response=array('programs'=>$array_programs);
            }
            return $response;
        }
        protected function ShowXPrograms($id){
            $PROV02DA=new PROV02DA();
            $data=$PROV02DA->ShowXPrograms($id);
            $array_programs=array();
            $response=array();
            if(count($data)>0){
                $array_programs=array("cod"=>$data[0]['CODPROGRAM'],"descripcion"=>utf8_encode($data[0]['DESCRIPCION']),"status"=>$data[0]['PRGRAMEST'],"orden"=>$data[0]['ORDEN']);
                $response=array('programs'=>$array_programs);
            }
            return $response;
        }
        protected function ShowRucXPrograms($id){
            $PROV01DA=new PROV01DA();
            $data=$PROV01DA->ShowRucXPrograms($id);
            $array_data=array();
            $response=array();
            if(count($data)>0){
                foreach($data as $k=>$v){
                    $array_data[]=$v['RUC'];
                }
                $response=array('rucs'=>$array_data);
            }
            return $response;
        }
        protected function UpdatePrograms($request){
            if(!$this->session->active()){
                header("Location: ".BASE_URL."login");
            }
            
            $PROV02DA=new PROV02DA();
            $PROV01DA=new PROV01DA();
            $PROV00DA=new PROV00DA();
            $PROV02=new PROV02();
            $PROV02->PPCOD=$request['ipt_cod'];
            $PROV02->PPDES=$request['ipt_description'];
            $PROV02->PPORD=$request['ipt_orden'];
            $PROV02->PPEST=$request['slc_status'];
            $response=$PROV02DA->UpdatePrograms($PROV02);

            $PROV01DA->DeletePrograms($request['ipt_cod']);
            $array_obj_user=array();
            foreach($request['slc_proveedores'] as $k=>$v){
                $usuarios=$PROV00DA->getUsersByRuc($v);
                foreach($usuarios as $key=>$value){
                    $PROV01=new PROV01();
                    $PROV01->PUCOD=$value['CODIGO'];
                    $PROV01->PPCOD=$request['ipt_cod'];
                    $PROV01->PURUC=$v;
                    $array_obj_user[]=$PROV01;
                }
            }
            if( $PROV01DA->CreateMassive($array_obj_user)){
                $response=true;
            }else{
                $response=false;
            }
            

            return $response;
        }
        protected function CreatePrograms($request,$usercod=''){
            if(!$this->session->active()){
                header("Location: ".BASE_URL."login");
            }
            
            $PROV02=new PROV02();
            $PROV02DA=new PROV02DA();
            $PROV00DA=new PROV00DA();
            $PROV01DA=new PROV01DA();
            $newcode=$PROV02DA->NewCode();
            $PROV02->PPCOD=$newcode;
            $PROV02->PPORD=$request['ipt_orden'];
            $PROV02->PPDES=$request['ipt_description'];
            $PROV02->PPEST=$request['slc_status'];
            $response= $PROV02DA->CreatePrograms($PROV02);

            $array_obj_user=array();
            foreach($request['slc_proveedores'] as $k=>$v){
                $usuarios=$PROV00DA->getUsersByRuc($v);
                foreach($usuarios as $key=>$value){
                    $PROV01=new PROV01();
                    $PROV01->PUCOD=$value['CODIGO'];
                    $PROV01->PPCOD=$newcode;
                    $PROV01->PURUC=$v;
                    $array_obj_user[]=$PROV01;
                }
            }
            if( $PROV01DA->CreateMassive($array_obj_user)){
                $response=true;
            }else{
                $response=false;
            }
            return $response;
           
        }
        public function validateUser($request){
            if(!$this->session->active()){
                header("Location: ".BASE_URL."login");
            }
            $PROV00DA=new PROV00DA();
            $user=utf8_encode(trim($request['user']));
            $exists=$PROV00DA->existUser($user);
            $validate=false;
            if($exists['cantidad']!=0){
                $validate=true;
            }
            print_r(json_encode(array('validate'=>$validate)));
        }

        /* SEND EMAILS */
        public function SendMail($request){

            switch ($request['option']) {
                case 'register':
                    $API=new API();
                    $PROV00DA=new PROV00DA();
                    // SET DATA PARA ENVIO DE CORREO

                    $user=$PROV00DA->ShowXuser($request['id']);
                   

                    $tipo=DEFAULT_DATA['values']['tipos'][$user[0]['TIPO_PROVEEDOR']]['descripcion'];
                    $porcentaje=DEFAULT_DATA['values']['porcentajes'][$user[0]['PORCENTAJE_PROVEEDOR']]['descripcion'];
                    $limit_min=$user[0]['LIMITE_MIN'];
                    $limit_max=$user[0]['LIMITE_MAX'];
                    $tipo_proveedor=$user[0]['TIPO_PROVEEDOR'];
                    $name=$user[0]['USERNAME'];
                    $app=$user[0]['USERAPP'];
                    $username=$user[0]['USER'];
                    $pass=$user[0]['USERPASS'];
                    $ruc=$user[0]['USERDNI'];
                    $razon=$user[0]['USERRAZON'];
                    $email=$user[0]['USERMAIL'];

                    $params=array('name'=>$name,'app'=>$app,'user'=>$username,'password'=>$pass,'ruc'=>$ruc,'razon'=>$razon,'tipo'=>$tipo,'porcentaje'=>$porcentaje,'limit_max'=>$limit_max,'limit_min'=>$limit_min);
                    require('./App/View/Mail/registro.php');
                    require('./App/View/Mail/notificacion.php');
                    // Envio de correo confirmación registro a proveedor
                    $API->sendEmail( 
                        array(
                            array( 'email'=>$email,'name'=>$razon)
                        ),
                        array(
                            'asunto'=>'Tai Loy - Portal Proveedores',
                            'body'=>$registro //Es una variable dentro de la vista View/Mail/registro.php
                        )
                    ); 
                    // Envio de correo notificacion a Tai loy
                    $API->sendEmail( 
                        DEFAULT_DATA['values']['notificaciones']['destinatarios'],
                        array(
                            'asunto'=>'Fee Comercial '.$params['name'].' '.$params['app'].'',
                            'body'=>$notificacion //Es una variable dentro de la vista View/Mail/notificacion.php
                        )
                    ); 
                    $PROV00DA->ShowXuser($request['id']);
                    print_r(json_encode(array('validate'=>true,'user'=>$username,'ruc'=>$ruc,'razon'=>$razon)));
                    break;
                
                default:
                    break;
            }
            
        }
        
    }



?>