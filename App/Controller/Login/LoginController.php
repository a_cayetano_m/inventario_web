<?php
    namespace App\Controller\Login;
    
    use System\Core\Controller;
    use App\Helper\Helper;
    use App\Model\Session\Session;
    use App\Model\INV00\INV00;
    use App\Model\INV00\INV00DA;
    use App\Model\API\API;
    use App\Model\INV15\INV15DA;
    use App\Model\INV19\INV19DA;
    use App\Model\OP69\OP69DA;
    class LoginController extends Controller{
        
        protected $session;
        protected $session_global;
        public function __construct(){
            $this->session=new Session();
            $this->session->init();
            $this->session_global=Helper::get_session_global();
        }
        public function index(){
            if($this->session->active()){
                header("Location: ".BASE_URL."inicio");
            }
            
            $this->render(__CLASS__,"Login");
        }
        public function login(){
            if(!$this->session->active()){
                $this->render(__CLASS__,"Login");
            }
            header("Location: ".BASE_URL."comercial-ventas");
        }
        public function infouser(){

            if(!$this->session->active()){
                header("Location: ".BASE_URL."login");
            }else{
               
                $user=array('ID'=>$_SESSION[CONFIG['session']['global']]['USER']['ID'],'DNI'=>$_SESSION[CONFIG['session']['global']]['USER']['DNI'],'NOMBRE'=>$_SESSION[CONFIG['session']['global']]['USER']['NOMBRE'],'APELLIDO'=>$_SESSION[CONFIG['session']['global']]['USER']['APELLIDO'],'EMAIL'=>$_SESSION[CONFIG['session']['global']]['USER']['EMAIL'],'AREA'=>$_SESSION[CONFIG['session']['global']]['USER']['AREA'],'TIPO'=>$_SESSION[CONFIG['session']['global']]['USER']['TIPO']); 
                
                print_r(json_encode(array("user"=>$user)));
            }
        }
        public function crearsession($request){
            $user=$request['user'];
            $pass=$request['pass'];

            $INV00   = new INV00();
            $INV00DA = new INV00DA();
            $INV15DA = new INV15DA();
            $INV19DA = new INV19DA();
            $OP69DA  = new OP69DA();

            $arrayMenu=array(); //arreglo con la informacion del menu
            $arrayMenuIcon=array(); //arreglo con icons del menu
            $arrayMenuIden=array(); //arreglo con los identificadores(id) del menu
            $tempMenu=''; //valor temporal para ver cambio de menu

            $INV00->USERNAME=$user;
            $INV00->PASSWORD=$pass;

            $user=$INV00DA->getUser($INV00);
            if(count($user)>0 ){
                if((int)$user[0]['TYPE']==1){
                    $sucu = !empty($user[0]['SUCURSAL']) ? $user[0]['SUCURSAL'] : 0;
                    $user=$INV00DA->getUserComplete($INV00);
                    $bib = !empty($user[0]['CODTIENDA']) ? trim($user[0]['CODTIENDA']) : NULL;
                    
                    if(!empty($sucu)) {
                        $registro = $INV19DA->verificarInventario($sucu);
                        if(!empty($registro[0]['INVENTARIO'])) {
                            $inv = !empty($registro[0]['INVENTARIO']) ? $registro[0]['INVENTARIO'] : NULL;
                            //Para produccion
                            $estinva = $OP69DA->getinv($bib, $inv);
                            //Para pruebas
                            //$estinva = 8;
                            /**
                             * 6 = Cancelado y 8 Terminado
                             */
                            if( $estinva == 6 || $estinva==8 ) {
                                $INV19DA->update_inventario($sucu,$inv,$estinva);
                            }
                        }
                    }
                }
                $role=$INV15DA->getRole($user[0]['COD']);
                $this->session->sessionArray($this->session_global,array()); //Se inicializa el array que contendra la informacion session del sistema
                foreach($user as $key=>$item){
                    
                    $arrayUser=array(
                        'COD'=>$item['COD'],
                        'NOM'=>utf8_encode($item['NOM']),
                        'APP'=>utf8_encode($item['APP']),
                        'EMAIL'=>$item['EMAIL'],
                        'PUSER'=>$item['USERNAME'],
                        'PUEST'=>$item['EST'],
                        'ROLE'=>$role,
                        'TIPO'=>$item['TYPE'],
                        'DESTIENDA'=>$item['DESTIENDA'],
                        'SUCURSAL'=>$item['SUCURSAL'],
                    );
                    if($tempMenu==''){
                        if($item['SUBMENU']!='null'){
                            $tempMenu=$item['TIPOMENU'];
                            $arrayMenu[utf8_encode($item['MENU'])]=array(array("RUTAMENU"=>utf8_encode($item['RUTAMENU']),"SUBMENU"=>utf8_encode($item['SUBMENU']),"RUTASUBMENU"=>utf8_encode($item['RUTASUBMENU'])));
                        }else{
                            $arrayMenu[utf8_encode($item['MENU'])]=array(array("RUTAMENU"=>utf8_encode($item['RUTAMENU']),"RUTASUBMENU"=>utf8_encode($item['RUTASUBMENU']),"SUBMENU"=>utf8_encode($item['SUBMENU'])));
                        }
                        $arrayMenuIcon[utf8_encode($item['MENU'])]=array(utf8_encode($item['ICONMENU']));
                        $arraySubMenuIcon[utf8_encode($item['SUBMENU'])]=array(utf8_encode($item['ICONSUBMENU']));
                    }else{
                            if($tempMenu==$item['TIPOMENU']){
                                if($item['SUBMENU']!='null'){
                                    $tempMenu=$item['TIPOMENU'];
                                    array_push($arrayMenu[utf8_encode($item['MENU'])],array("RUTAMENU"=>utf8_encode($item['RUTAMENU']),"SUBMENU"=>utf8_encode($item['SUBMENU']),"RUTASUBMENU"=>utf8_encode($item['RUTASUBMENU'])));
                                }else{
                                    $arrayMenu[utf8_encode($item['MENU'])]=array(array("RUTAMENU"=>utf8_encode($item['RUTAMENU']),"RUTASUBMENU"=>utf8_encode($item['RUTASUBMENU'])));
                                }
                                $arrayMenuIcon[utf8_encode($item['MENU'])]=array(utf8_encode($item['ICONMENU']));
                                $arraySubMenuIcon[utf8_encode($item['SUBMENU'])]=array(utf8_encode($item['ICONSUBMENU']));
                            }else{
                                if($item['SUBMENU']!='null'){
                                    $tempMenu=$item['TIPOMENU'];
                                    $arrayMenu[utf8_encode($item['MENU'])]=array(array("RUTAMENU"=>utf8_encode($item['RUTAMENU']),"SUBMENU"=>utf8_encode($item['SUBMENU']),"RUTASUBMENU"=>utf8_encode($item['RUTASUBMENU'])));
                                }else{
                                    $arrayMenu[utf8_encode($item['MENU'])]=array(array("RUTAMENU"=>utf8_encode($item['RUTAMENU']),"RUTASUBMENU"=>utf8_encode($item['RUTASUBMENU']),"SUBMENU"=>utf8_encode($item['SUBMENU'])));
                                }
                                $arrayMenuIcon[utf8_encode($item['MENU'])]=array(utf8_encode($item['ICONMENU']));
                                $arraySubMenuIcon[utf8_encode($item['SUBMENU'])]=array(utf8_encode($item['ICONSUBMENU']));
                            }
                    }
                }
                $arrayUser['MENU']=$arrayMenu;
                $arrayUser['ICONMENU']=$arrayMenuIcon;
                $arrayUser['ICONSUBMENU']=$arraySubMenuIcon;

                $this->session->set($this->session_global,$arrayUser,'USER');

                print_r(json_encode(array('validate'=>1,'msg'=>"Bienvenido ".$arrayUser['RAZON'])));
            
            }else{
                print_r(json_encode(array('validate'=>3,'msg'=>"Verifique usuario o contraseña")));
            }
           
        }
       
        public function cerrarsession(){
            session_destroy();
            $_SESSION=array();
            if(count($_SESSION)>0){
                print_r(json_encode(array('validate'=>false)));
            }else{
                print_r(json_encode(array('validate'=>true)));
            }
            
        }
        public function PrimerCambioPass(){
            
            if(!$this->session->active()){
                header("Location: ".BASE_URL."login");
            }
            $cambiopass=$_SESSION[CONFIG['session']['global']]['USER']['PASSCHANGE'];
            if($cambiopass==''){
                print_r(json_encode(array('validate'=>false)));
            }else{
                print_r(json_encode(array('validate'=>true)));
            }
        }
        /* SEND EMAILS */
        public function RecuperarPass($request){
           
            $API=new API();
            $PROV00DA=new PROV00DA();
            // SET DATA PARA ENVIO DE CORREO

            $user=$PROV00DA->getUsersByEmail($request['email'],'');
            
            if(count($user)>0){
                $tipo=DEFAULT_DATA['values']['tipos'][$user[0]['TIPO_PROVEEDOR']]['descripcion'];
                $porcentaje=DEFAULT_DATA['values']['porcentajes'][$user[0]['PORCENTAJE_PROVEEDOR']]['descripcion'];
                $limit_min=$user[0]['LIMITE_MIN'];
                $limit_max=$user[0]['LIMITE_MAX'];
                $tipo_proveedor=$user[0]['TIPO_PROVEEDOR'];
                $name=$user[0]['USERNAME'];
                $app=$user[0]['USERAPP'];
                $username=$user[0]['USER'];
                $pass=$user[0]['USERPASS'];
                $ruc=$user[0]['USERDNI'];
                $razon=$user[0]['USERRAZON'];
                $email=$user[0]['USERMAIL'];
    
                $params=array('name'=>$name,'app'=>$app,'user'=>$username,'password'=>$pass,'ruc'=>$ruc,'razon'=>$razon,'tipo'=>$tipo,'porcentaje'=>$porcentaje,'limit_max'=>$limit_max,'limit_min'=>$limit_min);
                require('./App/View/Mail/recuperarPass.php');
                // Envio de correo confirmación registro a proveedor
                $API->sendEmail( 
                    array(
                        array( 'email'=>$email,'name'=>$razon)
                    ),
                    array(
                        'asunto'=>'Tai Loy - Portal Proveedores',
                        'body'=>$registro //Es una variable dentro de la vista View/Mail/registro.php
                    )
                ); 
                print_r(json_encode(array('validate'=>true,'message'=>'Se envió un correo con las credenciales')));
                
            }else{
                print_r(json_encode(array('validate'=>false,'message'=>'Email no registrado')));
            }
            
          
            
        }
         /* SEND EMAILS */
        public function CambiarPass($request){
            
            $API=new API();
            $PROV00DA=new PROV00DA();
            // SET DATA PARA ENVIO DE CORREO

            $user=$PROV00DA->getUsersByEmail($request['email'],$request['pass_old']);
           
            if(count($user)>0 ){
                if(trim($request['pass_old'])==trim(utf8_encode($user[0]['USERPASS']))){
                    $tipo=DEFAULT_DATA['values']['tipos'][$user[0]['TIPO_PROVEEDOR']]['descripcion'];
                    $porcentaje=DEFAULT_DATA['values']['porcentajes'][$user[0]['PORCENTAJE_PROVEEDOR']]['descripcion'];
                    $limit_min=$user[0]['LIMITE_MIN'];
                    $limit_max=$user[0]['LIMITE_MAX'];
                    $tipo_proveedor=$user[0]['TIPO_PROVEEDOR'];
                    $name=$user[0]['USERNAME'];
                    $app=$user[0]['USERAPP'];
                    $username=$user[0]['USER'];
                    $pass=$request['pass_new'];
                    $ruc=$user[0]['USERDNI'];
                    $razon=$user[0]['USERRAZON'];
                    $email=$user[0]['USERMAIL'];
        
                    $params=array('name'=>$name,'app'=>$app,'user'=>$username,'password'=>$pass,'ruc'=>$ruc,'razon'=>$razon,'tipo'=>$tipo,'porcentaje'=>$porcentaje,'limit_max'=>$limit_max,'limit_min'=>$limit_min);
                    require('./App/View/Mail/cambiarPass.php');
                    $PROV00DA->UpdateByEmail($email,$pass);
                    // Envio de correo confirmación registro a proveedor
                    $API->sendEmail( 
                        array(
                            array( 'email'=>$email,'name'=>$razon)
                        ),
                        array(
                            'asunto'=>'Tai Loy - Portal Proveedores',
                            'body'=>$registro //Es una variable dentro de la vista View/Mail/registro.php
                        )
                    ); 
                    print_r(json_encode(array('validate'=>true,'message'=>'Se envió un correo con las credenciales')));
                }else{
                    print_r(json_encode(array('validate'=>true,'message'=>'La contraseña actual no coincide')));
                }
            }else{
                print_r(json_encode(array('validate'=>false,'message'=>'Email no registrado')));
            }
          
            
        }

    }
?>