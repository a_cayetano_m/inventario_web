<?php 
    Namespace App\Http;
    
    use System\Core\Router;
    

    /*--------------------------------API --------------------------------*/
    Router::add("getAuth/","ApiController@getAuth");
    Router::add("closeSesion/","ApiController@closeSesion");
    Router::add("actualizaDescripcion/","ApiController@actualizaDescripcion");
    Router::add("createLocation/","ApiController@createLocation");
    Router::add("showLocations/","ApiController@showLocations");
    Router::add("deleteLocations/","ApiController@deleteLocations");
    Router::add("deleteLocation/","ApiController@deleteLocation");
    Router::add("deleteLocationUnidad/","ApiController@deleteLocationUnidad");
    Router::add("validateLocations/","ApiController@validateLocations");
    
    Router::add("showDataInitial/","ApiController@showDataInitial");
    Router::add("showProduct/","ApiController@showProduct");
    Router::add("saveLocationMaterial/","ApiController@saveLocationMaterial");
    Router::add("showProductSucursal/","ApiController@showProductSucursal");
    Router::add("deleteContentLocation/","ApiController@deleteContentLocation");

    /*Inventario*/
    Router::add("locationsInv/","ApiController@locationsInv");
    Router::add("locationsInv2/","ApiController@locationsInv2");
    Router::add("getLocationMaterial/","ApiController@getLocationMaterial");
    Router::add("createFirstCont/","ApiController@createFirstCont");
    Router::add("createSecondCont/","ApiController@createSecondCont");
	Router::add("createPreInventario/","ApiController@createPreInventario");
	Router::add("createPreInventario_/","ApiController@createPreInventario_");
	Router::add("verArticulo/","ApiController@verArticulo");
	Router::add("ver_articulo/","ApiController@ver_articulo");

    /*-------------------------Pantalla---------------------------------*/
    Router::add("login","LoginController@");
    Router::add("/","LoginController@");
    Router::add("inicio","InicioController@");
    Router::add("inicio/","InicioController@");
    Router::add("cerrarsession","LoginController@cerrarsession");

    /*-------------------------Login---------------------------------*/
    Router::add("crearsession","LoginController@crearsession");

    /*-------------------------Printer---------------------------------*/
    Router::add("printLocation/","PrinterController@printlocation");
    Router::add("printZoneLocation/","PrinterController@printZoneLocation");
    
    
    /*------------------------Locacion--------------------------------*/
    Router::add("Locacion-inilocacion","LocacionController@");
    Router::add("EnviarLocaciones","LocacionController@EnviarLocaciones");
    Router::add("CargarLocacion","LocacionController@CargarLocacion");
    /*------------------------Inventario--------------------------------*/
    Router::add("Inventario-creaInv","InventarioController@");
    Router::add("Inventario-userZon","InventarioController@index2");
    Router::add("Inventario-segInv","InventarioController@index3");
    Router::add("Inventario-locPen","InventarioController@index4");
    Router::add("Inventario-userSes","InventarioController@index5");
    Router::add("Inventario-segcon","InventarioController@index6");
    Router::add("Inventario-loca","InventarioController@locaciones");
    Router::add("iniciaInventario","InventarioController@iniciaInventario");
    Router::add("cierraSesion","InventarioController@cierraSesion");
    Router::add("actualizaUsuario","InventarioController@actualizaUsuario");
    Router::add("segundoConteo","InventarioController@segundoConteo");
    Router::add("tercerConteo","InventarioController@tercerConteo");
    Router::add("cargaContadores","InventarioController@cargaContadores");
    

    /*-------------------------Route---------------------------------*/
    /*-------------------------------- SETTINGS --------------------------------*/
     //--- RETURN VIEW ----
    Router::add("settings","SettingsController@");
    Router::add("setting","SettingsController@Settings");
    Router::add("optionsroleview","SettingsController@OptionsRoleView");
    Router::add("optionsmenuview","SettingsController@OptionsMenuView");
    Router::add("optionssubmenuview","SettingsController@OptionsSubmenuView");
    Router::add("optionsuserview","SettingsController@OptionsUserView");
    Router::add("optionsprogramview","SettingsController@OptionsProgramView");
    //--- RETURN DATA  ---
    Router::add("optionsrole","SettingsController@OptionsRole");
    Router::add("optionsmenu","SettingsController@OptionsMenu");
    Router::add("optionssubmenu","SettingsController@OptionsSubmenu");
    Router::add("optionsuser","SettingsController@OptionsUser");

    Router::add("optionsuser","SettingsController@OptionsUser");
    
    

    
?>   


