<!-- Modal Cambiar contraseña-->
<div class="modal fade" id="md_change" tabindex="-1" role="dialog" aria-labelledby="md_changeLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="container">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                            <div class="row d-flex justify-content-sm-center align-items-center">
                                <div class="col-4 d-flex justify-content-end">
                                    <img class="img-fluid mt1"  src="./App/assets/img/logo-tailoy.jpg" alt="Tai Loy">
                                </div>
                                <div class="col-8 mb-2">
                                    <h1 class="titulo-principal">PORTAL DE</h1>
                                    <h1 class="titulo-principal sub text-center">PROVEEDORES</h1>
                                </div>
                            </div>
                            <div class="row mt-4">
                                <div class="col-10 offset-1 d-flex justify-content-center">
                                    <h4 class="label-login text-center">Cambiar contraseña</h4>
                                </div>
                            </div>    
                            <div class="row login-card mt-3">
                                <div class="col-12 col-md-10 offset-md-1">
                                    <div class="form-group">
                                        <label for="ipt_pass_old"
                                            class="label-login">Contraseña actual</label>
                                        <input type="password" id="ipt_pass_old" name="ipt_pass_old"
                                            class="form-control">
                                    </div>
                                </div>
                                <div class="col-12 col-md-10 offset-md-1">
                                    <div class="form-group mb-4">
                                        <label for="ipt_pass_new"
                                            class="label-login">Nueva Contraseña</label>
                                        <input type="password" name="ipt_pass_new" id="ipt_pass_new"
                                            class="form-control">
                                    </div>
                                </div>
                                <div class="col-12 col-md-10 offset-md-1">
                                    <div class="form-group mb-4">
                                        <label for="ipt_mail_change"
                                            class="label-login">Correo</label>
                                        <input type="text" name="ipt_mail_change" id="ipt_mail_change"
                                            class="form-control">
                                    </div>
                                </div>
                                <div class="col-12 col-md-10 offset-md-1">
                                    <button name="btn_send_change" id="btn_send_change" class="btn btn-block login-btn mb-4">
                                        <span class="spinner-border spinner-border-sm icono-loading-2 d-none"
                                            style="    margin-bottom: 2px;" role="status"
                                            aria-hidden="true"></span>
                                        Enviar
                                    </button>
                                </div>
                                <div class="col-12 col-md-10 offset-md-1 text-center">
                                    <span id="message-2" class="text-success font-weight-bold"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>