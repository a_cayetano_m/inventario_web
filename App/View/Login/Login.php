<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Tai Loy - Provedores</title>
    <!--Favicon-->
    <link rel="shortcut icon" href="https://s3-sa-east-1.amazonaws.com/tailoy-web/logo/icon.gif" />
    <link href="https://fonts.googleapis.com/css?family=Karla:400,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.materialdesignicons.com/4.8.95/css/materialdesignicons.min.css">
    <link href="./App/assets/css/bootstrap/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" href="./App/assets/css/login-css/login.css">
    <!-- Toastr -->
    <link rel="stylesheet" href="./App/assets/adminlte/plugins/toastr/toastr.min.css">
</head>

<body>

    <main class="d-flex align-items-center min-vh-100 py-3 py-md-0">
        <div class="container">
            <div class="row d-flex justify-content-center">
                <div class="col-12 col-md-12 col-lg-12">
                    <div class="card login-card">
                        <div class="row no-gutters">
                            <div class="col-12 col-sm-12 col-md-12">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-12 col-md-6 offset-md-3">
                                            <div class="row d-flex justify-content-center">
                                                <div
                                                    class="offset-1 col-10 col-sm-8 offset-sm-2 col-md-12 offset-md-0 col-lg-8 mb-4">
                                                    <h1 class="titulo-principal">SISTEMA DE CONTROL</h1>
                                                    <h1 class="titulo-principal sub text-center">INVENTARIO</h1>
                                                </div>
                                                  <div class=" col-12 text-center mb-4">
                                                    <div id="div-mobile" class="col-12 col-md-8 col-xl-6 d-flex align-items-center">
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <img class="img-inicio img-fluid"
                                                                    src="./App/assets/img/Grupo 7.png" alt="Tai Loy">
                                                            </div>
                                                            <div class="col-4">
                                                                <img class="img-inicio img-fluid"
                                                                    src="./App/assets/img/Grupo 9.png" alt="Tai Loy">
                                                            </div>
                                                            <div class="col-4">
                                                                <img class="img-inicio img-fluid"
                                                                    src="./App/assets/img/Grupo 11.png" alt="Tai Loy">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <img class="img-logo img-fluid  mt-4"
                                                        src="./App/assets/img/logo-tailoy.jpg" alt="Tai Loy">
                                                </div>
                                                <div class="col-12 col-lg-8">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="form-group">
                                                                <label for="ipt_user"
                                                                    class="label-login">Usuario</label>
                                                                <input type="user" id="ipt_user" name="ipt_user"
                                                                    class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-12">
                                                            <div class="form-group mb-4">
                                                                <label for="ipt_pass"
                                                                    class="label-login">Contraseña</label>
                                                                <input type="password" name="ipt_pass" id="ipt_pass"
                                                                    class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <button name="btn_login" id="btn_login"
                                                                class="btn btn-block login-btn mb-4">
                                                                <span
                                                                    class="spinner-border spinner-border-sm icono-loading d-none"
                                                                    style="    margin-bottom: 2px;" role="status"
                                                                    aria-hidden="true"></span>
                                                                Ingresar
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-6 d-flex justify-content-end">
                                                        <a href="" data-toggle="modal" data-target="#md_rest" class="forgot-password-link">Recuperar contraseña</a>
                                                        </div>
                                                        <div class="col-6 d-flex justify-content-start">
                                                        <a href="" data-toggle="modal" data-target="#md_change" class="forgot-password-link">Cambiar contraseña</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include './App/View/Login/CambiarPass.php'; ?>
        <?php include './App/View/Login/RecuperarPass.php'; ?>
    </main>
    <script src="./App/assets/js/bootstrap/jquery-3.4.1.min.js"></script>
    <script src="./App/assets/js/bootstrap/popper.min.js"></script>
    <script src="./App/assets/js/bootstrap/bootstrap.min.js"></script>
    <!-- Toastr -->
    <script src="./App/assets/adminlte/plugins/toastr/toastr.min.js"></script>
    <script src="./App/assets/js/sweetalert2/sweetalert2.js"></script>
    <script src="./App/assets/js/global_access.js"></script>
    <script src="./App/assets/js/login.js"></script>
</body>

</html>