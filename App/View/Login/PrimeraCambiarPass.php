<!-- Modal Cambiar contraseña-->
<div class="modal fade" id="md_first_change" tabindex="-1" role="dialog" aria-labelledby="md_first_changeLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="container">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="row d-flex justify-content-sm-center align-items-center">
                        <div class="col-4 d-flex justify-content-end">
                            <img class="img-fluid mt1" src="./App/assets/img/logo-tailoy.jpg" alt="Tai Loy">
                        </div>
                        <div class="col-8 mb-2">
                            <h1 class="titulo-principal">PORTAL DE</h1>
                            <h1 class="titulo-principal sub text-center">PROVEEDORES</h1>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-10 offset-1 d-flex justify-content-center">
                            <h4 class="label-login text-center">Cambiar contraseña</h4>
                        </div>
                    </div>
                    <div class="row login-card mt-3">
                        <div class="col-12 col-md-10 offset-md-1">
                            <div class="form-group">
                                <label for="ipt_pass_old" class="label-login">Contraseña actual</label>
                                <input type="password" id="ipt_pass_old" name="ipt_pass_old" class="form-control">
                            </div>
                        </div>
                        <div class="col-12 col-md-10 offset-md-1">
                            <div class="form-group mb-4">
                                <label for="ipt_pass_new" class="label-login">Nueva Contraseña</label>
                                <input type="password" name="ipt_pass_new" id="ipt_pass_new" class="form-control">
                            </div>
                        </div>
                        <div class="col-12 col-md-10 offset-md-1">
                            <div class="form-group mb-4">
                                <label for="ipt_pass_new_confirm" class="label-login">Confirmar contraseña</label>
                                <input type="password" name="ipt_pass_new_confirm" id="ipt_pass_new_confirm"
                                    class="form-control">
                            </div>
                        </div>
                        <div class="col-12 col-md-10 offset-md-1">
                            <button name="btn_send_change_confirm" id="btn_send_change_confirm"
                                class="btn btn-block login-btn mb-4">
                                <span class="spinner-border spinner-border-sm icono-loading-2 d-none"
                                    style="    margin-bottom: 2px;" role="status" aria-hidden="true"></span>
                                Enviar
                            </button>
                        </div>
                        <div class="col-12 col-md-10 offset-md-1 text-center">
                            <span id="message" class="text-success font-weight-bold"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
$("#btn_send_change_confirm").on("click", function() {
   
    if ($("#ipt_pass_new").val() == $("#ipt_pass_new_confirm").val()) {
        $(".icono-loading-2").removeClass("d-none");
        $("#ipt_mail_change").removeClass('is-invalid')
        let parametros = {
            'pass_old': $("#ipt_pass_old").val(),
            'pass_new': $("#ipt_pass_new").val(),
            'pass_new_confirm': $("#ipt_pass_new_confirm").val(),
            'email': $("#ipt_email").val(),
        }
        $.ajax({
            type: "POST",
            url: url_global + 'CambiarPass',
            dataType: "json",
            async: true,
            data: parametros,
            success: function(response) {
                if (response.validate) {
                    $("#message").addClass("text-success")
                    $("#ipt_pass_old").val('')
                    $("#ipt_pass_new").val('')
                    $("#ipt_pass_new_confirm").val('')
                } else {
                    $("#message").addClass("text-danger")
                }
                $(".icono-loading-2").addClass("d-none");
                $("#message").html(response.message);
                setInterval(
                    function () {
                        $('#md_first_change').modal('hide');
                }, 1000);
            }
        });
    } else {
        $("#ipt_pass_new_confirm").addClass('is-invalid')
    }
});
</script>