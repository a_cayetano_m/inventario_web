<!-- Layouts -->
<?php include './App/View/Layouts/url.php'; ?>
<!DOCTYPE html>
<html>

<!-- Header -->
<?php include $backurlview.'Layouts/header.php'; ?>

<!-- /.Header -->
<link rel="stylesheet" href="<?php echo $backurl;?>css/infoproveedores/style.css">

<body class="hold-transition sidebar-mini">
    <div class="wrapper">
        <!-- Navbar -->
        <?php include $backurlview.'Layouts/navbar.php'; ?>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <?php include $backurlview.'Layouts/menu.php'; ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <!--<div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0">Dashboard</h1>

                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="../index" class="breadcrumb-label">Inicio</a></li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>-->
            <!-- /.content-header -->
            <br>
            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">
                    <div class="row">
                        
                    </div>
                </div><!-- /.container-fluid -->
            </section>
            <!-- /.content -->

        </div>
        <!-- /.content-wrapper -->
        <footer class="main-footer">
        </footer>
    </div>
    <!-- ./wrapper -->

    <!-- Footer -->
    <?php include $backurlview.'Layouts/footer.php'; ?>

    <!--  JS -->
    <script src="<?php echo $backurl;?>js/ubicaciones/functions.js"></script>
    <script src="<?php echo $backurl;?>js/ubicaciones/events.js"></script>
    <script src="<?php echo $backurl;?>js/ubicaciones/validate.js"></script>
    <!-- /.Footer -->

</body>

</html>