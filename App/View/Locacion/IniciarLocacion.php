
<!-- Layouts -->
<?php include './App/View/Layouts/url.php'; ?>
<!DOCTYPE html>
<html>

<!-- Header -->
<?php include $backurlview.'Layouts/header.php'; ?>
<!-- /.Header -->

<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
  <?php include $backurlview.'Layouts/navbar.php'; ?>
  <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <?php include $backurlview.'Layouts/menu.php'; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Configuración</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item">Configuración</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title"></h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <!-- Content -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-primary card-outline card-outline-tabs">
                            <div class="card-header p-0 border-bottom-0 div_tabs"  id="div_tabs">
                                </br>
                                <div class="col-6 d-flex justify-content-center">
                                    <form method="POST" action="CargarLocacion">
                                        <button class="btn btn-success f-subtitle" type="submit">Cargar Usuarios</button>
                                    </form>
                                </div>
                                </br>
                                <div class="col-6 d-flex justify-content-center">
                                    <form method="POST" action="EnviarLocaciones">
                                        <button class="btn btn-success f-subtitle" type="submit">Cargar 2do Conteo</button>
                                    </form>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="tab-content" id="div_tab_content_general">
                                        
                                </div>
                            </div>
                            <!-- /.card -->
                        </div>
                    </div>
                    <div class="card-body">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>N° Inventario</th>
                                            <th>Almacen</th>
                                            <th>Fecha</th>
                                            <th>Estado</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach($params['Inventario'] as $k_Usuario=>$v_Usuario){ 
                                        ?>  
                                            <tr>
                                            <td id="ci-<?php echo $k_Usuario; ?>"> <?php echo utf8_encode($v_Usuario['INVENTARIO']) ?></td>
                                            <td> <?php echo utf8_encode($v_Usuario['ALMACEN']) ?></td>
                                            <td id="mu-<?php echo $k_Usuario; ?>"> <?php echo utf8_encode($v_Usuario['FECHA']) ?></td>
                                            <td id="mo-<?php echo $k_Usuario; ?>"> <?php 
                                            if ($v_Usuario['ESTADO'] == "0") {
                                              echo "GENERADO";
                                            }
                                            if ($v_Usuario['ESTADO'] == "1") {
                                              echo "IMPRESION";
                                            }
                                            if ($v_Usuario['ESTADO'] == "2") {
                                              echo "SALDOS";
                                            }
                                            if ($v_Usuario['ESTADO'] == "3") {
                                              echo "1ra.CONSOLIDACION";
                                            }
                                            if ($v_Usuario['ESTADO'] == "4") {
                                              echo "2da.CONSOLIDACION";
                                            }
                                            if ($v_Usuario['ESTADO'] == "5") {
                                              echo "3ra.CONSOLIDACION";
                                            }
                                            if ($v_Usuario['ESTADO'] == "6") {
                                              echo "TERMINADO";
                                            }
                                            ?></td>
                                            <td>
                                            <?php if($v_Usuario['ESTADO'] <> "6") { ?>
                                            <button name="btn_visualizar" id="bv_<?php echo $k_Usuario; ?>"
                                            class="btn btn-success fw-bold f-subtitle" style="font-size: var(--font-extra);" type="button" value="">
                                              <?php if ($v_Usuario['ESTADO'] == "0") {
                                                  echo "Gen. Ticket"; } 
                                                  if ($v_Usuario['ESTADO'] == "1") {
                                                    echo "Saldos"; }
                                                  if ($v_Usuario['ESTADO'] == "2") {
                                                    echo "1° Consol."; }
                                                  if ($v_Usuario['ESTADO'] == "3") {
                                                    echo "2° Consol."; }
                                                  if ($v_Usuario['ESTADO'] == "4") {
                                                    echo "3° Consol."; }
                                                  if ($v_Usuario['ESTADO'] == "5") {
                                                    echo "Terminar"; }?>
                                            <?php } ?>
                                            </button>
                                            </td>
                                            </tr>
                                        <?php }?>
                                    </tbody>
                                </table>
                            </div>
                    <!-- /.row -->
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <!-- <div class="float-right d-none d-sm-block">
      <b>Version</b> 3.0.5
    </div>
    <strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io">AdminLTE.io</a>.</strong> All rights
    reserved.-->
  </footer>
                                                
  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->


<!-- Footer -->
<?php include $backurlview.'Layouts/footer.php'; ?>


</body>

</html>
