<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Tai Loy - Proveedores</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Style personalizado -->
    <link rel="stylesheet" href="<?php echo $backurl;?>css/inicio/style.css">
    
    <!-- Style bootstrap -->
    <link rel="stylesheet" href="<?php echo $backurl;?>css/bootstrap/bootstrap.min.css">
    
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
   
    <!--Favicon-->
    <link rel="shortcut icon" href="https://s3-sa-east-1.amazonaws.com/tailoy-web/logo/icon.gif" />


    
    
</head>