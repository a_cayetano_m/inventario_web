

<!-- jQuery -->
<script src="<?php echo $backurl; ?>adminlte/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo $backurl; ?>adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- BostrapSelect -->
<script src="<?php echo $backurl;?>js/bootstrap/bootstrap-select.min.js"></script>

<!--Sweet Alert -->
<script src="<?php echo $backurl;?>js/sweetalert2/sweetalert2.js"></script>

<!-- Global JS -->
<script src="<?php echo $backurl; ?>js/global_access.js"></script>

<!-- Global JS -->
<script src="<?php echo $backurl; ?>js/global.js"></script>

<!-- JS Custom -->
<script src="<?php echo $backurl; ?>js/Inicio/functions.js"></script>
<script src="<?php echo $backurl; ?>js/Inicio/events.js"></script>

