<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Tai Loy - Proveedores</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo $backurl;?>adminlte/plugins/fontawesome-free/css/all.min.css">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="<?php echo $backurl; ?>adminlte/plugins/overlayScrollbars/zcss/OverlayScrollbars.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- DataTables -->
    <link rel="stylesheet"
        href="<?php echo $backurl;?>adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet"
        href="<?php echo $backurl;?>adminlte/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
    <!-- daterange picker -->
    <link rel="stylesheet" href="<?php echo $backurl;?>adminlte/plugins/daterangepicker/daterangepicker.css">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="<?php echo $backurl;?>adminlte/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- Bootstrap Color Picker -->
    <link rel="stylesheet" href="<?php echo $backurl;?>adminlte/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css">
    <!-- Tempusdominus Bbootstrap 4 -->
    <link rel="stylesheet" href="<?php echo $backurl;?>adminlte/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo $backurl;?>adminlte/dist/css/adminlte.min.css">
    <!-- summernote -->
    <link rel="stylesheet" href="<?php echo $backurl;?>adminlte/plugins/summernote/summernote-bs4.css">
   
    <!-- Style personalizado -->
    <link rel="stylesheet" href="<?php echo $backurl;?>css/estilos.css">
    
    <!-- Toastr -->
    <link rel="stylesheet" href="<?php echo $backurl;?>adminlte/plugins/toastr/toastr.min.css">

    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
   
    <!--Favicon-->
    <link rel="shortcut icon" href="https://s3-sa-east-1.amazonaws.com/tailoy-web/logo/icon.gif" />

    <!-- BostrapSelect -->
    <link rel="stylesheet" href="<?php echo $backurl;?>css/bootstrap/bootstrap-select.min.css">
    
    <!-- Custom Admin LTE -->
    <link rel="stylesheet" href="<?php echo $backurl;?>css/estilos.css">
    
    
</head>