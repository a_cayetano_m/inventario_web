<!-- jQuery -->
<script src="<?php echo $backurl; ?>adminlte/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo $backurl; ?>adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- overlayScrollbars -->
<script src="<?php echo $backurl; ?>adminlte/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>

<!-- DataTables -->
<script src="<?php echo $backurl; ?>adminlte/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo $backurl; ?>adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo $backurl; ?>adminlte/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo $backurl; ?>adminlte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="<?php echo $backurl; ?>datatable/js/dataTables.fixedColumns.min.js"></script>


<!-- InputMask -->
<script src="<?php echo $backurl; ?>adminlte/plugins/moment/moment.min.js"></script>
<script src="<?php echo $backurl; ?>adminlte/plugins/inputmask/min/jquery.inputmask.bundle.min.js"></script>
<!-- date-range-picker -->
<script src="<?php echo $backurl; ?>adminlte/plugins/daterangepicker/daterangepicker.js"></script>
<!-- bootstrap color picker -->
<script src="<?php echo $backurl; ?>adminlte/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="<?php echo $backurl; ?>adminlte/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Bootstrap Switch -->
<script src="<?php echo $backurl; ?>adminlte/plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
<!-- Loading -->
<script src="<?php echo $backurl; ?>js/loadingoverlay/loadingoverlay.min.js"></script>


<!-- ChartJS -->
<!--<script src="https://cdn.jsdelivr.net/npm/chart.js@3.0.0/dist/chart.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@2.0.0"></script>-->

<!-- Apexchart -->
<script src="<?php echo $backurl; ?>ApexChart/Apexchart.js"></script>


<!-- AdminLTE App -->
<script src="<?php echo $backurl; ?>adminlte/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo $backurl; ?>adminlte/dist/js/demo.js"></script>

<!-- Toastr -->
<script src="<?php echo $backurl; ?>adminlte/plugins/toastr/toastr.min.js"></script>
<!-- Summernote -->
<script src="<?php echo $backurl; ?>adminlte/plugins/summernote/summernote-bs4.min.js"></script>
<script src="<?php echo $backurl; ?>js/summernote-ES.js"></script>

<!-- BostrapSelect -->
<script src="<?php echo $backurl; ?>js/bootstrap/bootstrap-select.min.js"></script>

<!--Sweet Alert -->
<script src="<?php echo $backurl; ?>js/sweetalert2/sweetalert2.js"></script>

<!-- Global JS -->
<script src="<?php echo $backurl; ?>js/global_access.js"></script>

<!-- Global JS -->
<script src="<?php echo $backurl; ?>js/global.js"></script>

<script>
    console.log("tabal");
    var tbl_seguimiento = $("#tbl_seguimiento").DataTable({
        "searching": false,
        "responsive": false,
        fixedHeader: true,
        "order": [],
        "language": {
            "zeroRecords": "No hay registros para mostrar",
            "lengthMenu": "Registros por página  _MENU_ ",
            "info": "",
            "infoEmpty": "",
            "emptyTable": "",
            "infoFiltered": "(filtrado de _MAX_ registros)",
            "search": "Filtrar: ",
            "paginate": {
                "first": "Primero",
                "last": "Último",
                "next": "Siguiente",
                "previous": "Anterior"
            },
        }
    });

    var tbl_cabecera = $("#tbl_cabecera").DataTable({
        "paging": false, // Deshabilitar paginación
        "searching": false, // Deshabilitar la función de búsqueda/filtro
        "lengthChange": false, // Deshabilitar la opción de cambiar la cantidad de registros por página
        "responsive": true,
        "order": [],
        "language": {
            "zeroRecords": "No hay registros para mostrar",
            "lengthMenu": "Registros por página  _MENU_ ",
            "info": "",
            "infoEmpty": "",
            "emptyTable": "",
            "infoFiltered": "(filtrado de _MAX_ registros)",
            "search": "Filtrar: ",
            "paginate": {
                "first": "Primero",
                "last": "Último",
                "next": "Siguiente",
                "previous": "Anterior"
            },
        }
    });

    var tbl_zona = $("#tbl_zona").DataTable({
        "scrollX": true,
        "searching": false,
        "responsive": false,
        "fixedHeader": true,
        "fixedColumns": {
            "leftColumns": 1 // Número de columnas que deseas inmovilizar a la izquierda
        },
        "order": [],
        "language": {
            "zeroRecords": "No hay registros para mostrar",
            "lengthMenu": "Registros por página  _MENU_ ",
            "info": "",
            "infoEmpty": "",
            "emptyTable": "",
            "infoFiltered": "(filtrado de _MAX_ registros)",
            "search": "Filtrar: ",
            "paginate": {
                "first": "Primero",
                "last": "Último",
                "next": "Siguiente",
                "previous": "Anterior"
            },
        }
    });

    var locapen = $("#locapen").DataTable({
        "searching": true,
        "paging": false,
        "responsive": false,
        "fixedHeader": true,
        "order": [],
        "columns": [{
                "searchable": false
            },
            {
                "searchable": false
            },
            null
        ],
        "language": {
            "zeroRecords": "No hay registros para mostrar",
            "lengthMenu": "Registros por página  _MENU_ ",
            "info": "",
            "infoEmpty": "",
            "emptyTable": "",
            "infoFiltered": "(filtrado de _MAX_ registros)",
            "search": "Filtrar: ",
            "paginate": {
                "first": "Primero",
                "last": "Último",
                "next": "Siguiente",
                "previous": "Anterior"
            },
        }
    });

    $(document).on('click', '#ul_menu_p a.submenu', function(e) {
        $.LoadingOverlay("show", {
            image: "",
            fontawesome: "fa fa-spinner fa-spin",
        });
    });
    if(document.location.host=='localhost') {
        $("#ul_menu_p a.submenu").each(function() {
            let href = $(this).attr("href");
            let new_href = href.replace("InvFisico", "inventario_web");  
            $(this).attr({'href':new_href})
        });
    }
    
</script>