<aside class="main-sidebar sidebar-dark-primary elevation-4 navbar-white">
    <!-- Brand Logo -->
    <a id="logo_general" href="" class="brand-link navbar-success ">
      <div class="">
        <img src="<?php echo $backurl; ?>/img/logo-navbar.png" alt="Tai Loy" class="brand-image elevation-3">
          <span class="text-light brand-text font-weight-bold">INVENTARIO FISICO</span>
      </div>
    </a>

    <!-- Sidebar -->
    <div class="sidebar navbar-white">
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul id="ul_menu_p" class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          
          <?php $menu=$_SESSION[CONFIG['session']['global']]['USER']['MENU']; 
                $iconmenu=$_SESSION[CONFIG['session']['global']]['USER']['ICONMENU'];
                $iconsubmenu=$_SESSION[CONFIG['session']['global']]['USER']['ICONSUBMENU'];
                ?>

          <?php foreach($menu as $key=>$value){ ?> 
            <?php if(count($value)==1 && $value[0]['SUBMENU']=='null'){ ?>
              <li class="nav-item has-treeview" >
                <?php if($optMenu==$value[0]['RUTASUBMENU']){
                          $activeMenu='active';
                      }
                      else{
                          $activeMenu='';
                      } ?>
                <?php if($value[0]['RUTASUBMENU']=='cerrarsesion'){ ?>
                  <a id="a_cerrarsesion" class="nav-link <?php echo $activeMenu; ?>">
                    <i class="nav-icon <?php echo $iconmenu[$key][0];?>"></i>
                    <p>
                      <?php echo $key; ?>
                    </p>
                  </a>
                <?php }else{ ?>
                  <?php if($value[0]['RUTAMENU']==$value[0]['RUTASUBMENU']){?>
                    <a href="<?php echo BASE_URL.$value[0]['RUTAMENU'];?>" class="nav-link <?php echo $activeMenu; ?> " id="<?php echo $value[0]['RUTASUBMENU']; ?>">
                      <i class="nav-icon <?php echo $iconmenu[$key][0];?>"></i>
                      <p>
                        <?php echo $key; ?>
                      </p>
                    </a>
                  <?php }else{?> 
                    <a href="<?php echo BASE_URL.$value[0]['RUTAMENU']."-".$value[0]['RUTASUBMENU']; ?>" class="nav-link <?php echo $activeMenu; ?> " id="<?php echo $value[0]['RUTASUBMENU']; ?>">
                      <i class="nav-icon <?php echo $iconmenu[$key][0];?>"></i>
                      <p>
                        <?php echo $key; ?>
                      </p>
                    </a>
                  <?php } ?>
                <?php } ?>
              </li> 
            <?php }else{ ?>

               <?php if(explode('-',$optMenu)[0]==$value[0]['RUTAMENU']){$activeMenu='active';}else{$activeMenu='';} ?>
              <li class="nav-item has-treeview" id="">
                <a href="" class="nav-link <?php echo $activeMenu; ?>">
                  <i class="nav-icon <?php echo $iconmenu[$key][0];?>"></i>
                  <p>
                    <?php echo $key; ?>
                    <i class="fas fa-angle-left right"></i>
                  </p>
                </a>
                <?php foreach($value as $k=>$v){
                  $s_u_b = ' submenu';
                  ?>

                  <?php if($optMenu==($v['RUTAMENU']."-".$v['RUTASUBMENU'])){$activeSubMenu=' active';}else{$activeSubMenu='';} ?>
                  <ul class="nav nav-treeview" id="<?php echo 'ul_'.$v['RUTAMENU'].'_'.$v['RUTASUBMENU']; ?>">
                    <li class="nav-item">
                      <a href="<?php echo BASE_URL.$v['RUTAMENU']."-".$v['RUTASUBMENU']; ?>" class="nav-link<?php echo $s_u_b.$activeSubMenu; ?>" >
                        <i class="<?php echo $iconsubmenu[$v['SUBMENU']][0];?> nav-icon"></i>
                        <p><?php echo $v['SUBMENU']; ?></p>
                      </a>
                    </li>
                  </ul>
                <?php } ?>  
              </li>
            <?php  } ?>
          <?php } ?>  
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>