<!-- Layouts -->
<?php include './App/View/Layouts/url.php'; ?>
<!DOCTYPE html>
<html>

<!-- Header -->
<?php include $backurlview . 'Layouts/header.php'; ?>
<!-- /.Header -->

<body class="hold-transition sidebar-mini">
  <div class="wrapper">
    <!-- Navbar -->
    <?php include $backurlview . 'Layouts/navbar.php'; ?>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <?php include $backurlview . 'Layouts/menu.php'; ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1>Creación Inventario</h1>
            </div>
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item">Creación Inventario</li>
              </ol>
            </div>
          </div>
        </div><!-- /.container-fluid -->
      </section>

      <!-- Main content -->
      <section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header">
                  <h3 class="card-title"></h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                  <!-- Content -->
                  <?php if (count($params['Inventario']) < 1) { ?>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="card card-primary card-outline card-outline-tabs">
                          <div class="card-header p-0 border-bottom-0 div_tabs" id="div_tabs">
                            </br>
                            <div style="display: flex;justify-content: center;align-items: center;">
                              <h4 style="margin-right: 10px;">Ingrese Nro. de Inventario : </h4>
                              <input type="number" id="nroInventario" name="nroInventario" placeholder="Ingrese nro Inventario">
                            </div>
                            </br>
                            <div style="display: flex;justify-content: center;align-items: center;">
                              <h4 style="margin-right: 16px;">Ingrese Cant. Contadores : </h4>
                              <input type="number" id="cantContadores" name="cantContadores" placeholder="Ingrese nro Contadores">
                            </div>
                            </br>
                            <div style="display: flex;justify-content: center;align-items: center;">
                              <button class="btn btn-success f-subtitle" type="submit" onclick="crea()">Crear Inventario</button>
                            </div>
                            </br>
                          </div>
                          <div class="card-body">
                            <div class="tab-content" id="div_tab_content_general">

                            </div>
                          </div>
                          <!-- /.card -->
                        </div>
                      </div>
                    </div>
                    <?php } else {
                    foreach ($params['Inventario'] as $k_Usuario => $v_Usuario) { ?>
                      <div style="display: flex;justify-content: center;align-items: center;">
                        <h4 style="margin-right: 10px;">Invetario Iniciado - NRO : <?php echo utf8_encode($v_Usuario['INVENTARIO']) ?></h4>
                      </div>
                  <?php }
                  } ?>
                  <!-- /.card-body -->
                </div>
                <!-- /.card -->
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->
          </div>
          <!-- /.container-fluid -->
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <footer class="main-footer">
      <!-- <div class="float-right d-none d-sm-block">
      <b>Version</b> 3.0.5
    </div>
    <strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io">AdminLTE.io</a>.</strong> All rights
    reserved.-->
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
      <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
  </div>
  <!-- ./wrapper -->


  <!-- Footer -->
  <?php include $backurlview . 'Layouts/footer.php'; ?>


</body>
<script>
  function crea() {

    var inventario = $('#nroInventario').val().trim();
    var contadores = $('#cantContadores').val().trim();

    console.log(inventario);
    console.log(contadores);

    if (inventario == "") {
      Swal.fire({
        text: "INGRESAR NRO DE INVENTARIO",
        icon: 'error',
        showConfirmButton: true,
      });
    } else {
      if (contadores == "") {
        Swal.fire({
          text: "INGRESAR CANT. DE CONTADORES",
          icon: 'error',
          showConfirmButton: true,
        });
      } else {
        Swal.fire({
          title: 'Se va a iniciar el Inventario : ' + inventario,
          text: "Con la siguiente cantidad de contadores : " + contadores,
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Si',
          cancelButtonText: 'No'
        }).then((result) => {
          $.ajax({
            type: "POST",
            url: "./iniciaInventario",
            dataType: 'html',
            data: {
              "inventario": inventario,
              "contadores": contadores
            },
            beforeSend: function() {
              $.LoadingOverlay("show", {
                image: "",
                fontawesome: "fa fa-spinner fa-spin",
              });
            },
            complete: function() {
              $.LoadingOverlay("hide");
            },
          }).done(function(response) { // respuesta
            response = JSON.parse(response);
            if (response.validado) {
              $.LoadingOverlay("show", {
                image: "",
                fontawesome: "fa fa-spinner fa-spin",
              });
              window.location.href = "./Inventario-userZon"
            } else {
              if (response.tipo == '2') {
                Swal.fire({
                  text: "ERROR EN NRO DE INVENTARIO",
                  icon: 'error',
                  showConfirmButton: true,
                });
              }

              if (response.tipo == '3') {
                Swal.fire({
                  text: "INVENTARIO YA FUE INGRESADO ANTERIORMENTE",
                  icon: 'error',
                  showConfirmButton: true,
                });
              }
            }
          });
        })
      }
    }

  }
</script>

</html>