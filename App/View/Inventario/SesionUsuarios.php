
<!-- Layouts -->
<?php include './App/View/Layouts/url.php'; ?>
<!DOCTYPE html>
<html>

<!-- Header -->
<?php include $backurlview.'Layouts/header.php'; ?>
<!-- /.Header -->

<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
  <?php include $backurlview.'Layouts/navbar.php'; ?>
  <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <?php include $backurlview.'Layouts/menu.php'; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Sesiones Usuarios</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item">Sesiones Usuarios</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title"></h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <!-- Content -->
                <?php if(count($params['Inventario']) > 0){?>
                  <div class="card-body">
                      <table id="example1" class="table table-bordered table-striped">
                          <thead>
                              <tr>
                                  <th>USUARIO</th>
                                  <th>ESTADO</th>
                                  <th></th>
                              </tr>
                          </thead>
                          <tbody>
                              <?php foreach($params['Inventario'] as $k_Usuario=>$v_Usuario){ 
                              ?>  
                                  <tr>
                                  <td id="us-<?php echo $k_Usuario; ?>"> <?php echo utf8_encode($v_Usuario['USERNAME']) ?></td>
                                  <td id="es-<?php echo $k_Usuario; ?>"> 
                                  <?php if($v_Usuario['SESION'] == "0"){
                                          echo "INACTIVO";
                                  }
                                        if($v_Usuario['SESION'] == "1"){
                                          echo "ACTIVO";
                                  } ?></td>
                                  <td>
                                  <?php if($v_Usuario['SESION'] == "1"){?>
                                    <button name="btn_visualizar" id="btn_<?php echo $k_Usuario; ?>" onclick="cerrarSesion(<?php echo $k_Usuario;?>)" class="btn btn-danger fw-bold f-subtitle" style="font-size: var(--font-extra);" type="button" value="">Cerrar Sesion</button>
                                    <?php } ?>
                                  </td>
                                  </tr>
                              <?php }?>
                          </tbody>
                      </table>
                  </div>
                <?php }else {?>
                  <div style="display: flex;justify-content: center;align-items: center;">
                    <h4 style="margin-right: 10px;">No se ha iniciado el Inventario</h4>
                  </div>
                <?php }?>    
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <!-- <div class="float-right d-none d-sm-block">
      <b>Version</b> 3.0.5
    </div>
    <strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io">AdminLTE.io</a>.</strong> All rights
    reserved.-->
  </footer>
                                                
  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->


<!-- Footer -->
<?php include $backurlview.'Layouts/footer.php'; ?>


</body>
<script>
  function cerrarSesion(key){
      var usuario = $('#us-'+key).html().trim();
      console.log(usuario);
          $.ajax({
              type: "POST",
              url: "./cierraSesion",
              dataType : 'html',
              data: {
                  "usuario":usuario
              },
          }).done(function(response) { // respuesta
            window.location.href = "./Inventario-userSes"
          });
    }
</script>

</html>
