<!-- Layouts -->
<?php include './App/View/Layouts/url.php'; ?>
<!DOCTYPE html>
<html>
<style>
  .tr-reducido td {
    padding: 0.3rem;
    vertical-align: BOTTOM;
    padding: 0.3rem;
    vertical-align: TOP;
  }
</style>
<!-- Header -->
<?php include $backurlview . 'Layouts/header.php'; ?>
<!-- /.Header -->

<body class="hold-transition sidebar-mini">
  <div class="wrapper">
    <!-- Navbar -->
    <?php include $backurlview . 'Layouts/navbar.php'; ?>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <?php include $backurlview . 'Layouts/menu.php'; ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1>Zona - Usuario</h1>
            </div>
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item">Zona - Usuario</li>
              </ol>
            </div>
          </div>
        </div><!-- /.container-fluid -->
      </section>

      <!-- Main content -->
      <section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header">
                  <h3 class="card-title"></h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                  <!-- Content -->
                  <?php if (count($params['Inventario']) > 0) { ?>
                    <div>
                      <table id="tbl_zona" class="table table-bordered table-striped table-responsive">
                        <thead>
                          <tr>
                            <th>Usuario</th>
                            <th>Nombre</th>
                            <th>Cargo</th>
                            <?php foreach ($params['Locaciones'] as $k_Usuario => $v_Usuario) { ?>
                              <th><?php echo utf8_encode($v_Usuario['ZONA']) ?></th>
                            <?php } ?>
                            <th>Mb. Leido</th>
                            <th>Lin. Leido</th>
                            <th>Inicio</th>
                            <th>Ultm. Carga</th>
                            <th>Status</th>
                            <th>Usuario</th>
                            <th></th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php foreach ($params['Inventario'] as $k_Usuario => $v_Usuario) {
                          ?>
                            <tr>
                              <td style="vertical-align: top;" id="usr-<?php echo $k_Usuario; ?>"> <?php echo utf8_encode($v_Usuario['USERNAME']) ?></td>
                              <td style="padding: 0.3rem; vertical-align: BOTTOM; vertical-align: top;"><input id="nom-<?php echo $k_Usuario; ?>" type="text" style="width: 10em;" value="<?php echo utf8_encode($v_Usuario['NAME']); ?>"></td>
                              <td style="padding: 0.3rem; vertical-align: BOTTOM; vertical-align: top;"><input id="car-<?php echo $k_Usuario; ?>" type="text" style="width: 10em;" value="<?php echo utf8_encode($v_Usuario['LASTNAME']); ?>"></td>
                              <?php foreach ($params['Locaciones'] as $k_zona => $v_zona) {
                                $zona1 = utf8_encode($v_zona['ZONA']);
                                $estado = utf8_encode($v_Usuario[$zona1]); ?>
                                <td style="padding: 0.3rem; vertical-align: bottom; vertical-align: top;">
                                  <?php if ($estado == "") { ?>
                                    <input id="<?php echo utf8_encode($v_zona['ZONA']); ?>-<?php echo $k_Usuario; ?>" type="text" maxlength="1" style="width: 1.2em;" value="<?php $zona = utf8_encode($v_zona['ZONA']);
                                                                                                                                                                            echo utf8_encode($v_Usuario[$zona]); ?>">
                                  <?php } else {
                                    $zona = utf8_encode($v_zona['ZONA']);
                                    echo utf8_encode($v_Usuario[$zona]);
                                  } ?>
                                </td>
                              <?php } ?>
                              <td style="padding: 0.3rem; vertical-align: BOTTOM; vertical-align: top;"> <?php echo utf8_encode($v_Usuario['MUEBLELEI']) ?></td>
                              <td style="padding: 0.3rem; vertical-align: BOTTOM; vertical-align: top;"> <?php echo utf8_encode($v_Usuario['LINEACON']) ?></td>
                              <td style="padding: 0.3rem; vertical-align: BOTTOM; vertical-align: top;"> <?php echo utf8_encode($v_Usuario['HORAINICIO']) ?></td>
                              <td style="padding: 0.3rem; vertical-align: BOTTOM; vertical-align: top;"> <?php echo utf8_encode($v_Usuario['HORAULTCARGA']) ?></td>
                              <td style="padding: 0.3rem; vertical-align: BOTTOM; vertical-align: top;"> <?php
                                                                                                          if ($v_Usuario['STATUS'] == "0") {
                                                                                                            echo "NO INICIADO";
                                                                                                          }
                                                                                                          if ($v_Usuario['STATUS'] == "1") {
                                                                                                            echo "EN CURSO";
                                                                                                          }
                                                                                                          if ($v_Usuario['STATUS'] == "2") {
                                                                                                            echo "FINALIZADO";
                                                                                                          }
                                                                                                          ?></td>
                              <td style="vertical-align: top;" id="usr-<?php echo $k_Usuario; ?>"> <?php echo utf8_encode($v_Usuario['USERNAME']) ?></td>
                              <td style="padding: 0.3rem; vertical-align: BOTTOM; vertical-align: top;">
                                <button name="btn_visualizar" id="btn_<?php echo $k_Usuario; ?>" onclick="actualizaZona(<?php echo $k_Usuario; ?>)" class="btn btn-success fw-bold f-subtitle" style="font-size: var(--font-extra);" type="button" value="">Actualizar</button>
                              </td>
                            </tr>
                          <?php } ?>
                        </tbody>

                      </table>
                    </div>
                  <?php } else { ?>
                    <div style="display: flex;justify-content: center;align-items: center;">
                      <h4 style="margin-right: 10px;">No se ha iniciado el Inventario</h4>
                    </div>
                  <?php } ?>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <footer class="main-footer">
      <!-- <div class="float-right d-none d-sm-block">
      <b>Version</b> 3.0.5
    </div>
    <strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io">AdminLTE.io</a>.</strong> All rights
    reserved.-->
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
      <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
  </div>
  <!-- ./wrapper -->


  <!-- Footer -->
  <?php include $backurlview . 'Layouts/footer.php'; ?>


</body>

<script>
  function actualizaZona(key) {
    var usuario = $('#usr-' + key).html().trim();
    var nombre = $('#nom-' + key).val();
    var cargo = $('#car-' + key).val();
    var a = $('#A-' + key).val();
    var b = $('#B-' + key).val();
    var c = $('#C-' + key).val();
    var d = $('#D-' + key).val();
    var e = $('#E-' + key).val();
    var f = $('#F-' + key).val();
    var g = $('#G-' + key).val();
    var h = $('#H-' + key).val();
    var i = $('#I-' + key).val();
    var j = $('#J-' + key).val();
    var k = $('#K-' + key).val();
    var l = $('#L-' + key).val();
    var m = $('#M-' + key).val();
    var n = $('#N-' + key).val();
    var o = $('#O-' + key).val();
    var p = $('#P-' + key).val();
    var q = $('#Q-' + key).val();
    var r = $('#R-' + key).val();
    var s = $('#S-' + key).val();
    var t = $('#T-' + key).val();
    var u = $('#U-' + key).val();
    var v = $('#V-' + key).val();
    var w = $('#W-' + key).val();
    var x = $('#X-' + key).val();
    var y = $('#Y-' + key).val();
    var z = $('#Z-' + key).val();

    $.ajax({
      type: "POST",
      url: "./actualizaUsuario",
      dataType: 'html',
      beforeSend: function() {
        $.LoadingOverlay("show", {
          image: "",
          fontawesome: "fa fa-spinner fa-spin",
        });
      },
      complete: function() {
        $.LoadingOverlay("hide");
      },
      data: {
        "usuario": usuario,
        "nombre": nombre,
        "cargo": cargo,
        "a": a,
        "b": b,
        "c": c,
        "d": d,
        "e": e,
        "f": f,
        "g": g,
        "h": h,
        "i": i,
        "j": j,
        "k": k,
        "l": l,
        "m": m,
        "n": n,
        "o": o,
        "p": p,
        "q": q,
        "r": r,
        "s": s,
        "t": t,
        "u": u,
        "v": v,
        "w": w,
        "x": x,
        "y": y,
        "z": z
      },
    }).done(function(response) { // respuesta
      window.location.href = "./Inventario-userZon"
    });
  }
</script>

</html>