
<!-- Layouts -->
<?php include './App/View/Layouts/url.php'; ?>
<!DOCTYPE html>
<html>

<!-- Header -->
<?php include $backurlview.'Layouts/header.php'; ?>
<!-- /.Header -->

<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
  <?php include $backurlview.'Layouts/navbar.php'; ?>
  <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <?php include $backurlview.'Layouts/menu.php'; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Locaciones Pendientes</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item">Locaciones Pendientes</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title"></h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <?php if(count($params['Inventario']) > 0){?>
                  <div class="card-body">
                      <table id="locapen" class="table table-bordered table-striped">
                          <thead>
                              <tr>
                                  <th>LOCACIONES</th>
                                  <th>USUARIO</th>
                                  <th>CANT. LINEAS</th>
                              </tr>
                          </thead>
                          <tbody>
                              <?php foreach($params['Inventario'] as $k_Usuario=>$v_Usuario){ 
                              ?>  
                                  <tr>
                                  <td id="lo-<?php echo $k_Usuario; ?>"> <?php echo utf8_encode($v_Usuario['LOCACION']) ?></td>
                                  <?php if($v_Usuario['USERNAME'] == 'inv'){ ?>
                                    <td id="us-<?php echo $k_Usuario; ?>"> SIN USUARIO</td>
                                  <?php }else { ?>  
                                    <td id="us-<?php echo $k_Usuario; ?>"> <?php echo utf8_encode($v_Usuario['USERNAME']) ?></td>
                                  <?php } ?>
                                  <td id="lo-<?php echo $k_Usuario; ?>"> <?php echo utf8_encode($v_Usuario['TOTAL']) ?></td>
                                  </tr>
                              <?php }?>
                          </tbody>
                      </table>
                  </div>
                <?php }else {?>
                  <div style="display: flex;justify-content: center;align-items: center;">
                    <h4 style="margin-right: 10px;">No se ha iniciado el Inventario</h4>
                  </div>
                <?php }?> 
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <!-- <div class="float-right d-none d-sm-block">
      <b>Version</b> 3.0.5
    </div>
    <strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io">AdminLTE.io</a>.</strong> All rights
    reserved.-->
  </footer>
                                                
  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->


<!-- Footer -->
<?php include $backurlview.'Layouts/footer.php'; ?>


</body>

</html>
