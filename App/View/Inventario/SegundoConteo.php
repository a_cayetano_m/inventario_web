
<!-- Layouts -->
<?php include './App/View/Layouts/url.php'; ?>
<!DOCTYPE html>
<html>

<!-- Header -->
<?php include $backurlview.'Layouts/header.php'; ?>
<!-- /.Header -->

<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
  <?php include $backurlview.'Layouts/navbar.php'; ?>
  <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <?php include $backurlview.'Layouts/menu.php'; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Creación Inventario</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item">Creación Inventario</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title"></h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <!-- Content -->
                <?php
                if($params['Inventario'][0]['RGEST'] > 2){ 
                  if($params['Datos'][0]['MAXIMO'] < 1){?> 
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-primary card-outline card-outline-tabs">
                            <div class="card-header p-0 border-bottom-0 div_tabs"  id="div_tabs">
                                </br>
                                <div style="display: flex;justify-content: center;align-items: center;">
                                  <h4 style="margin-right: 10px;">Ingrese Monto Diferencia : </h4>
                                  <input type="number" id="monto" name="nroInventario" placeholder="Monto + -">
                                </div>
                                </br> 
                                <div style="display: flex;justify-content: center;align-items: center;">
                                      <button class="btn btn-success f-subtitle" type="submit" onclick="crea()">Segundo Conteo</button>
                                </div>
                                </br>
                            </div>
                            <div class="card-body">
                                <div class="tab-content" id="div_tab_content_general">
                                        
                                </div>
                            </div>
                            <!-- /.card -->
                        </div>
                    </div>
                </div>
                <?php } else { ?>
                  <div class="row">
                    <div class="col-md-12">
                        <div class="card card-primary card-outline card-outline-tabs">
                            <div class="card-header p-0 border-bottom-0 div_tabs"  id="div_tabs">
                                </br>
                                <div style="display: flex;justify-content: center;align-items: center;">
                                  <h4 style="margin-right: 10px;">Monto Maximo : </h4>
                                  <h4 style="margin-right: 10px;"> <?php echo $params['Datos'][0]['MAXIMO']?> </h4>
                                </div>
                                </br> 
                                <div style="display: flex;justify-content: center;align-items: center;">
                                  <h4 style="margin-right: 10px;">Monto Mínimo : </h4>
                                  <h4 style="margin-right: 10px;"><?php echo $params['Datos'][0]['MINIMO']?> </h4>
                                </div>
                                </br> 
                                <div style="display: flex;justify-content: center;align-items: center;">
                                  <h4 style="margin-right: 10px;">Cantidad Códigos : </h4>
                                  <h4 style="margin-right: 10px;"><?php echo $params['Datos'][0]['CANTIDAD_REGISTROS']?> </h4>
                                </div>
                                </br> 
                                <div style="display: flex;justify-content: center;align-items: center;">
                                  <h4 style="margin-right: 10px;">Ingrese Cant. Códigos : </h4>
                                  <input type="number" id="Contadores" name="nroInventario" placeholder=" <?php echo $params['Contadores'][0]['CONTADORES']?>" value="<?php echo $params['Contadores'][0]['CONTADORES']?> ">
                                </div>
                                </br> 
                                <div style="display: flex;justify-content: center;align-items: center;">
                                      <button class="btn btn-success f-subtitle" type="submit" onclick="creaContadores()">Cargar Contadores</button>
                                </div>
                                </br>
                            </div>
                            <div class="card-body">
                                <div class="tab-content" id="div_tab_content_general">
                                        
                                </div>
                            </div>
                            <!-- /.card -->
                        </div>
                    </div>
                </div>
                <?php } } else { 
                        foreach($params['Inventario'] as $k_Usuario=>$v_Usuario){ ?>
                        <div style="display: flex;justify-content: center;align-items: center;">
                          <h4 style="margin-right: 10px;">Pasar a Primera consolidación el Inventario en AS400</h4>
                        </div>
                <?php }} ?>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <!-- <div class="float-right d-none d-sm-block">
      <b>Version</b> 3.0.5
    </div>
    <strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io">AdminLTE.io</a>.</strong> All rights
    reserved.-->
  </footer>
                                                
  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->


<!-- Footer -->
<?php include $backurlview.'Layouts/footer.php'; ?>


</body>
<script>

    function crea(){
      
      var monto = $('#monto').val().trim();

      if (monto == "") {
        Swal.fire({
                    text: "INGRESAR MONTO",
                    icon: 'error',
                    showConfirmButton: true,
                });
      } else {
          Swal.fire({
            title: 'Se va a iniciar el Segundo Conteo',
            text: "Con el monto +- : "+monto,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si',
            cancelButtonText: 'No'
          }).then((result) => {
            $.ajax({
                      type: "POST",
                      url: "./segundoConteo",
                      dataType : 'html',
                      data: {
                          "monto":monto,
                      },
                  }).done(function(response) { // respuesta
                    response = JSON.parse(response);
                    if(response.validado){
                      window.location.href = "./Inventario-segcon"
                    }else{
                      if(response.tipo == '2'){
                          Swal.fire({
                              text: "ERROR EN NRO DE INVENTARIO",
                              icon: 'error',
                              showConfirmButton: true,
                          });
                      }

                      if(response.tipo == '3'){
                          Swal.fire({
                              text: "INVENTARIO YA FUE INGRESADO ANTERIORMENTE",
                              icon: 'error',
                              showConfirmButton: true,
                          });
                      }
                    }
                  });
          })
      }

    }

    function creaContadores(){
      
      var monto = $('#Contadores').val().trim();

      if (monto == "") {
        Swal.fire({
                    text: "INGRESAR NRO CONTADORES",
                    icon: 'error',
                    showConfirmButton: true,
                });
      } else {
          Swal.fire({
            title: 'Se agruparan los códigos del segundo Conteo',
            text: "Con la cantidad de : "+monto,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si',
            cancelButtonText: 'No'
          }).then((result) => {
            $.ajax({
                      type: "POST",
                      url: "./cargaContadores",
                      dataType : 'html',
                      data: {
                          "monto":monto,
                      },
                  }).done(function(response) { // respuesta
                    response = JSON.parse(response);
                    if(response.validado){
                      window.location.href = "./Inventario-segcon"
                    }else{
                      if(response.tipo == '2'){
                          Swal.fire({
                              text: "ERROR EN NRO DE INVENTARIO",
                              icon: 'error',
                              showConfirmButton: true,
                          });
                      }

                      if(response.tipo == '3'){
                          Swal.fire({
                              text: "INVENTARIO YA FUE INGRESADO ANTERIORMENTE",
                              icon: 'error',
                              showConfirmButton: true,
                          });
                      }
                    }
                  });
          })
      }

    }
  </script>
</html>
