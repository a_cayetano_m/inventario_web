
<!-- Layouts -->
<?php include './App/View/Layouts/url.php'; ?>
<!DOCTYPE html>
<html>
<style>
  @keyframes blink {
    0% { opacity: 1; }
    50% { opacity: 0; }
    100% { opacity: 1; }
  }

  .cabeceraH5{
    font-weight: bold;
    margin: 0;
    font-size: 16px;
    font-family: Arial, sans-serif;
    color: #333;
    white-space: nowrap;
    display: inline-block; 
    margin: 0;
    color: white;
  }
  .cabeceraDIV{
    font-family: Arial, sans-serif;
    display: inline-block;
    margin-bottom: 10px;    
    padding: 10px;    
    border: 1px solid #ccc;    
    border-radius: 5px;
  }
  .progress-bar-container{
    display: inline-block;
    width: 200px;
    height: 25px;
    background-color: #8D8C8B;
    position: relative;
    border: 1px solid white;    
    border-radius: 5px;
  }
  .progress-bar{
    height: 100%;
    position: absolute;
    top: 0;
    left: 0;
    max-width: 100%;
    border: 1px solid white;    
    border-radius: 5px;
  }
  
</style>
<?php
  $avanceTotal = $params['Total']['avanceTotal'];
  $style = '';
  if ($avanceTotal == 0) {
    $style = 'background-color: #FF5B41;';
  } elseif ($avanceTotal < 50) {
    $style = 'background-color: #FF5B41;';
  } elseif ($avanceTotal > 50) {
    if($avanceTotal < 100){
      $style = 'background-color: #EEE000;';
    }else{
      $style = 'background-color: #9BDF11;';
    }
  }
?>
<!-- Header -->
<?php include $backurlview.'Layouts/header.php'; ?>
<!-- /.Header -->

<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
  <?php include $backurlview.'Layouts/navbar.php'; ?>
  <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <?php include $backurlview.'Layouts/menu.php'; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Seguimiento Inventario</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item">Seguimiento Inventario</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header" style="text-align:center;">
              <?php if(count($params['Total']) > 0){?>
                <h5 style="font-weight: bold;">Avance General: 
                  <span class="progress-bar-container">
                    <div class="progress-bar" style="width: <?php echo ($params['Total']['avanceTotal']) ?>%;
                      <?php
                        if ($params['Total']['avanceTotal'] == 0) {
                          echo 'background-color: #FF5B41;';
                        } elseif ($params['Total']['avanceTotal'] < 50) {
                          echo 'background-color: #FF5B41;';
                        } elseif ($params['Total']['avanceTotal'] > 50 ) {
                          if ($params['Total']['avanceTotal'] < 100) {
                            echo 'background-color: #EEE000;';
                          }else {
                            echo 'background-color: #9BDF11;';
                          } 
                        }
                      ?>">
                    </div>
                    <p style="text-align:center;color:white;margin-bottom: 0px; font-weight: bold; position: absolute; top: 0; left: 0; width: 100%; z-index: 2;"><?php echo ($params['Total']['avanceTotal']) ?>%</p>
                  </span>
                </h5>
                <div class="cabeceraDIV" style="<?php echo $style; ?>">
                  <h5 class="cabeceraH5">Muebles Totales  = <?php echo utf8_encode($params['Total']['muebleTotal']) ?></h5>
                  <h5 class="cabeceraH5">&nbsp;&nbsp;&nbsp;Muebles Contados = <?php echo utf8_encode($params['Total']['muebleContado']) ?></h5>
                </div>
                <div class="cabeceraDIV" style="<?php echo $style; ?>">
                  <h5 class="cabeceraH5">Lineas Totales = <?php echo utf8_encode($params['Total']['lineasTotales']) ?></h5>
                  <h5 class="cabeceraH5">&nbsp;&nbsp;&nbsp;Lineas Contados = <?php echo utf8_encode($params['Total']['lineasContadas']) ?></h5>
                </div>
                <div class="cabeceraDIV" style="<?php echo $style; ?>">
                  <h5 class="cabeceraH5">Inicio  = <?php echo utf8_encode($params['Total']['horaTotal']) ?></h5>
                  <h5 class="cabeceraH5">&nbsp;&nbsp;&nbsp;Horas Transcurridas = <?php echo utf8_encode($params['Total']['tiempoFormato2']) ?></h5>
                  <h5 class="cabeceraH5">&nbsp;&nbsp;&nbsp;Estimado Termino = <?php echo utf8_encode($params['Total']['estimadoTer2']) ?></h5>
                </div>
              <?php }?> 
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <!-- Content -->
                <?php if(count($params['Inventario']) > 0){?>
                  <div>
                      <table id="tbl_seguimiento" class="table table-bordered table-striped table-responsive">
                          <thead>
                              <tr>
                                  <th>Zona</th>
                                  <th>Mbl. Tot.</th>
                                  <th>Mbl. Con</th>
                                  <th>Avnc. Mue.</th>
                                  <th>Estado</th>
                                  <th>Lin. Tot.</th>
                                  <th>Lin. Con.</th>
                                  <th>Avnc. Lin.</th>
                                  <th>Hr. Ini.</th>
                                  <th>Tmp. Transc.</th>
                                  <th>Estm. Term.</th>
                                  <th>Inv. Curso</th>
                              </tr>
                          </thead>
                          <tbody>
                              <?php foreach($params['Inventario'] as $k_Usuario=>$v_Usuario){ 
                              ?>  
                                  <tr>
                                  <td id="zon-<?php echo $k_Usuario; ?>"> <?php echo utf8_encode($v_Usuario['ZONA']) ?></td>
                                  <td id="mt-<?php echo $k_Usuario; ?>"> <?php echo utf8_encode($v_Usuario['MUEBLETOT']) ?></td>
                                  <td id="mc-<?php echo $k_Usuario; ?>"> <?php echo utf8_encode($v_Usuario['MUEBLECON']) ?></td>
                                  <td id="av-<?php echo $k_Usuario; ?>"> <?php echo utf8_encode($v_Usuario['AVANCE']) ?>%</td>
                                  <td id="es-<?php echo $k_Usuario; ?>"> 
                                    <?php if ($v_Usuario['ESTADO'] == "0"){
                                            echo '<p style="color: red;font-weight: bold;">SIN INICIAR</p>';
                                            $color='btn-warning';
                                          }
                                          if ($v_Usuario['ESTADO'] == "1"){
                                            echo '<p style="color: red;font-weight: bold;">EN CURSO</p>';
                                          }
                                          if ($v_Usuario['ESTADO'] == "2"){
                                            echo '<p style="color: yellow;font-weight: bold;">EN CURSO</p>';
                                          }
                                          if ($v_Usuario['ESTADO'] == "3"){
                                            echo '<p style="color: green;font-weight: bold;">TERMINADO</p>';
                                          } ?></td>
                                  <td id="e-<?php echo $k_Usuario; ?>"> <?php echo utf8_encode($v_Usuario['LINEASTOT']) ?></td>
                                  <td id="f-<?php echo $k_Usuario; ?>"> <?php echo utf8_encode($v_Usuario['LINEASCON']) ?></td>
                                  <td id="z-<?php echo $k_Usuario; ?>"> <?php echo utf8_encode($v_Usuario['AVANCE2']) ?>%</td>
                                  <td id="g-<?php echo $k_Usuario; ?>"> <?php echo utf8_encode($v_Usuario['HORAINI']) ?></td>
                                  <td id="h-<?php echo $k_Usuario; ?>"> <?php echo utf8_encode($v_Usuario['TIEMPOTRA']) ?></td>
                                  <td id="i-<?php echo $k_Usuario; ?>"> <?php echo utf8_encode($v_Usuario['ESTIMADOTER']) ?></td>
                                  <td id="j-<?php echo $k_Usuario; ?>"> <?php echo utf8_encode($v_Usuario['INVENTARIODORCUR']) ?></td>
                                  </tr>
                              <?php }?>
                          </tbody>
                      </table>
                  </div>
                  <?php }else {?>
                  <div style="display: flex;justify-content: center;align-items: center;">
                    <h4 style="margin-right: 10px;">No se ha iniciado el Inventario</h4>
                  </div>
                <?php }?>    
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <!-- <div class="float-right d-none d-sm-block">
      <b>Version</b> 3.0.5
    </div>
    <strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io">AdminLTE.io</a>.</strong> All rights
    reserved.-->
  </footer>
                                                
  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->


<!-- Footer -->
<?php include $backurlview.'Layouts/footer.php'; ?>


</body>

</html>
