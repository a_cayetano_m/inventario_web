<!-- CREATE SUBMENU -->
<?php if(count($params)>0){?>
<div class="tab-pane text-left fade show" id="vert-tabs-plus" role="tabpanel">
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <div class="row">
                    <input class="form-control" type="hidden" id="ipt_cod" value="<?php echo $params['values']['user']['usercod'] ?>">
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <label class="text-dark lb-ipt" for="ipt_dni">Ruc</label>
                        <input class="form-control " type="text" placeholder="RUC" id="ipt_dni" value="<?php echo $params['values']['user']['userdni'] ?>">
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <label class="text-dark lb-ipt" for="ipt_razon">Razon Social</label>
                        <input class="form-control " type="text" placeholder="Usuario" id="ipt_razon" maxlength='100' value="<?php echo $params['values']['user']['userrazon'] ?>">
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <label class="text-dark lb-ipt" for="ipt_user">Usuario</label>
                        <input class="form-control " type="text" placeholder="Usuario" id="ipt_user" maxlength='50' value="<?php echo $params['values']['user']['user'] ?>">
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <label class="text-dark lb-ipt" for="ipt_pass">Contraseña</label>
                        <input class="form-control " type="text" placeholder="Contraseña" id="ipt_pass" value="<?php echo $params['values']['user']['userpass'] ?>">
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <label class="text-dark lb-ipt" for="ipt_name">Nombre</label>
                        <input class="form-control " type="text" placeholder="Nombre" id="ipt_name" value="<?php echo utf8_encode($params['values']['user']['username']); ?>">
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <label class="text-dark lb-ipt" for="ipt_app">Apellido</label>
                        <input class="form-control " type="text" placeholder="Apellido" id="ipt_app" value="<?php echo utf8_encode($params['values']['user']['userapp']); ?>">
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <label class="text-dark lb-ipt" for="ipt_email">Email</label>
                        <input class="form-control" type="text" placeholder="Correo" id="ipt_email" value="<?php echo $params['values']['user']['useremail'] ?>">
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <label class="text-dark lb-ipt" for="ipt_tlf">Teléfono</label>
                        <input class="form-control" type="text" placeholder="Teléfono" id="ipt_tlf" value="<?php echo $params['values']['user']['usertlf'] ?>">
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <label class="text-dark lb-ipt" for="slc_status">Estado</label>
                        <select name="status" id="slc_status" class="form-control  selectpicker">
                            <option value="" selected>Seleccione estado</option>
                            <?php foreach($params['status'] as $key=>$value){ ?> 
                                <?php if(in_array($value['codest'],$params['values']['status'])){ ?> 
                                    <option selected value="<?php echo $value['codest']; ?>"><?php echo utf8_encode($value['nameest']); ?></option>
                                <?php }else{ ?>
                                    <option value="<?php echo $value['codest']; ?>"><?php echo utf8_encode($value['nameest']); ?></option>
                                    <?php } ?>
                                <?php } ?>
                        </select>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <label class="text-dark lb-ipt" for="slc_area">Área</label>
                        <select name="area" id="slc_area" class="form-control selectpicker">
                            <option value="" selected>Seleccione área</option>
                            <?php foreach($params['area'] as $key=>$value){ ?> 
                                <?php if(in_array($value['codare'],$params['values']['area'])){ ?> 
                                    <option selected value="<?php echo $value['codare']; ?>"><?php echo utf8_encode($value['nameare']); ?></option>
                                <?php }else{ ?>
                                    <option value="<?php echo $value['codare']; ?>"><?php echo utf8_encode($value['nameare']); ?></option>
                                    <?php } ?>
                                <?php } ?>
                        </select>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <label class="text-dark lb-ipt" for="slc_porcetaje">Porcentaje</label>
                        <select name="porcentaje" id="slc_porcetaje" class="form-control  selectpicker">
                            <option value="" >Seleccione Porcentaje</option>
                            <?php foreach($params['porcentajes'] as $key=>$value){ ?> 
                                <?php if($value['cod']==$params['values']['user']['porcentaje']){ ?> 
                                    <option selected value="<?php echo $value['cod']; ?>"><?php echo utf8_encode($value['descripcion']); ?></option>
                                <?php }else{ ?>
                                    <option value="<?php echo $value['cod']; ?>"><?php echo utf8_encode($value['descripcion']); ?></option>
                                    <?php } ?>
                                <?php } ?>
                        </select>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <label class="text-dark lb-ipt" for="slc_tipo">Tipo</label>
                        <select name="tipo" id="slc_tipo" class="form-control  selectpicker">
                            <option value="" >Seleccione Tipos</option>
                            <?php foreach($params['tipos'] as $key=>$value){ ?> 
                                <?php if($value['cod']==$params['values']['user']['tipo_proveedor']){ ?> 
                                    <option selected value="<?php echo $value['cod']; ?>"><?php echo $value['descripcion']; ?></option>
                                <?php }else{ ?>
                                    <option value="<?php echo $value['cod']; ?>"><?php echo $value['descripcion']; ?></option>
                                    <?php } ?>
                                <?php } ?>
                        </select>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <label class="text-dark lb-ipt" for="ipt_limit_min">Límite mínimo</label>
                        <input class="form-control " min='0' max='9999' type="number" placeholder="Límite mínimo" id="ipt_limit_min" value="<?php echo $params['values']['user']['limite_min'] ?>">
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <label class="text-dark lb-ipt" for="ipt_limit_max">Límite máximo</label>
                        <input class="form-control " min='0' max='9999' type="number" placeholder="Límite máximo" id="ipt_limit_max" value="<?php echo $params['values']['user']['limite_max'] ?>">
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                        <label class="text-dark lb-ipt" for="slc_role">Vistas</label>
                        <select multiple name="role" id="slc_role" class="form-control selectpicker" size="12">
                            <option value="" disabled>Seleccione rol</option>
                                <?php foreach($params['roles'] as $key=>$value){ ?> 
                                    <option disabled value=""><?php echo utf8_encode("--------".$key."--------"); ?></option>
                                    <?php foreach($value as $k=>$v){ ?> 
                                        <?php 
                                            if(in_array($v['codrole'],$params['values']['role'])){ ?> 
                                                <option selected value="<?php echo $v['codrole']; ?>"><?php echo utf8_encode($v['namerole']); ?></option>
                                            <?php }else{ ?>
                                                <option value="<?php echo $v['codrole']; ?>"><?php echo utf8_encode($v['namerole']); ?></option>
                                            <?php } ?>
                                    <?php } ?>
                                   
                                <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                        <label class="text-dark lb-ipt" for="slc_menu_user">Menu de opciones</label>
                        <select multiple name="menu" id="slc_menu_user" class="form-control selectpicker" size="12">
                            <option disabled value="">Seleccione Opciones</option>
                            <?php foreach($params['submenu'] as $key=>$value){ ?> 
                            <?php if(in_array($value['codsubmenu'],$params['values']['submenu'])){ ?> 
                                <option selected value="<?php echo $value['codsubmenu']; ?>"><?php echo utf8_encode($value['dessubmenu']); ?></option>
                            <?php }else{ ?>
                                <option value="<?php echo $value['codsubmenu']; ?>"><?php echo utf8_encode($value['dessubmenu']); ?></option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                        <label class="text-dark lb-ipt" for="slc_programs_user">Programas</label>
                        <select multiple name="programs" id="slc_programs_user" class="form-control selectpicker" size="12">
                            <?php foreach($params['programs'] as $k=>$v){?> 
                           
                            <?php if(in_array($v['cod'],$params['values']['program'])){ ?> 
                                <option selected value="<?php echo $v['cod']; ?>"><?php echo $v['descripcion']; ?></option>
                            <?php }else{ ?>
                                <option value="<?php echo $v['cod']; ?>"><?php echo $v['descripcion']; ?></option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <br>
                <div class="row  d-flex justify-content-center"><button id="btn_user_update" class="btn btn-success btn-sm">Guardar</button></div>
                
            </div>
        </div>
    </div>
</div>
<?php } ?>

<script>
    $('.selectpicker').selectpicker();
</script> 