<!-- CREATE USER -->
<div class="tab-pane text-left fade show" id="vert-tabs-plus" role="tabpanel">
    <div class="row">
        <div class="col-sm-6 col-md-10 offset-1">
            <div class="form-group">
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <label class="text-dark lb-ipt" for="ipt_dni">Ruc</label>
                        <input maxlength='15' class="form-control" type="text" placeholder="RUC" id="ipt_dni">
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <label class="text-dark lb-ipt" for="ipt_razon">Razon Social</label>
                        <input maxlength='100' class="form-control" type="text" placeholder="RAZON" id="ipt_razon">
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <label class="text-dark lb-ipt" for="ipt_user" >Usuario</label>
                        <input maxlength='50' class="form-control" type="text" onblur="validateuser()" placeholder="Usuario" id="ipt_user">
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <label class="text-dark lb-ipt" for="ipt_pass">Contraseña</label>
                        <input class="form-control" type="text" placeholder="Contraseña" id="ipt_pass">
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <label class="text-dark lb-ipt" for="ipt_name">Nombre</label>
                        <input class="form-control" type="text" placeholder="Nombre" id="ipt_name">
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <label class="text-dark lb-ipt" for="ipt_app">Apellido</label>
                        <input class="form-control" type="text" placeholder="Apellido" id="ipt_app">
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <label class="text-dark lb-ipt" for="ipt_email">Email</label>
                        <input class="form-control " type="text" placeholder="Correo" id="ipt_email">
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <label class="text-dark lb-ipt" for="ipt_tlf">Teléfono</label>
                        <input class="form-control " type="text" placeholder="Teléfono" id="ipt_tlf">
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <label class="text-dark lb-ipt" for="slc_status">Estado</label>
                        <select name="status" id="slc_status" class="form-control  selectpicker">
                            <option value="" selected>Seleccione estado</option>
                            <?php foreach($params['status'] as $key=>$value){ ?> 
                                <option value="<?php echo $value['codest']; ?>"><?php echo $value['nameest']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <label class="text-dark lb-ipt" for="slc_area">Área</label>
                        <select name="area" id="slc_area" class="form-control  selectpicker">
                            <option value="" selected >Seleccione área</option>
                            <?php foreach($params['area'] as $key=>$value){ ?> 
                                <option value="<?php echo $value['codare']; ?>"><?php echo $value['nameare']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <label class="text-dark lb-ipt" for="slc_porcetaje">Porcentaje</label>
                        <select name="porcentaje" id="slc_porcetaje" class="form-control  selectpicker">
                            <option value="" selected >Seleccione Porcentaje</option>
                            <?php foreach($params['porcentajes'] as $key=>$value){ ?> 
                                <option value="<?php echo $value['cod']; ?>"><?php echo $value['descripcion']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <label class="text-dark lb-ipt" for="slc_tipo">Tipo</label>
                        <select name="tipo" id="slc_tipo" class="form-control  selectpicker">
                            <option value="" selected >Seleccione Tipo</option>
                            <?php foreach($params['tipos'] as $key=>$value){ ?> 
                                <option value="<?php echo $value['cod']; ?>"><?php echo $value['descripcion']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <label class="text-dark lb-ipt" for="ipt_limit_min">Límite mínimo</label>
                        <input class="form-control " min='0' max='9999' type="number" placeholder="Límite mínimo" id="ipt_limit_min">
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <label class="text-dark lb-ipt" for="ipt_limit_max">Límite máximo</label>
                        <input class="form-control " min='0' max='9999' type="number" placeholder="Límite máximo" id="ipt_limit_max">
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                        <label class="text-dark lb-ipt" for="slc_role">Vistas</label>
                        <select multiple name="role" id="slc_role" class="form-control selectpicker" size="12">
                            <?php foreach($params['roles'] as $key=>$value){ ?> 
                                <option disabled value=""><?php echo utf8_encode("--------".$key."--------"); ?></option>
                                <?php foreach($value as $k=>$v){ ?> 
                                    <option value="<?php echo $v['codrole']; ?>"><?php echo utf8_encode($v['namerole']); ?></option>
                                <?php } ?>
                                
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                        <label class="text-dark lb-ipt" for="slc_menu_user">Menu de opciones</label>
                        <select multiple name="menu" id="slc_menu_user" class="form-control selectpicker" size="12">
                            <?php foreach($params['submenu'] as $k=>$v){?> 
                            <option value="<?php echo $v['codsubmenu']; ?>"><?php echo utf8_encode($v['dessubmenu']); ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                        <label class="text-dark lb-ipt" for="slc_programs_user">Programas</label>
                        <select multiple name="programs" id="slc_programs_user" class="form-control selectpicker" size="12">
                            <?php foreach($params['programs'] as $k=>$v){?> 
                            <option value="<?php echo $v['cod']; ?>"><?php echo $v['descripcion']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <br>
                <div class="row  d-flex justify-content-center"><button id="btn_user_add" class="btn btn-success btn-sm">Guardar</button></div>
                
            </div>
        </div>
    </div>
</div>

<script>
    
    $('.selectpicker').selectpicker();
</script> 