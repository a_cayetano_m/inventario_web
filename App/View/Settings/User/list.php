<!-- LIST USER -->
<?php if(count($params)>0){?>
<div class="tab-pane text-left fade show" id="vert-tabs-plus" role="tabpanel">
    <div class="row">
        <div class="col-md-12">
            <table id="tbl_users" class="table table-responsive tbl_all table-bordered table-striped text-center">
                <thead>
                   <th>Cód</th>
                   <th>Usuario</th>
                   <th>Ruc</th>
                   <th>Razon Social</th>
                   <th>Estado</th>
                   <th></th>
                </thead>
                <tbody>
                    <?php foreach($params['users'] as $k=>$v){ ?> 
                        <tr>
                            <td><?php echo $v['coduser']; ?></td>
                            <td><?php echo $v['user']; ?></td>
                            <td><?php echo $v['ruc']; ?></td>
                            <td><?php echo $v['razon']; ?></td>
                            <?php if($v['status']=='1'){ 
                                $class="badge badge-success";
                                $estado='Activo';
                             }else {
                               $class="badge badge-danger";
                               $estado='Inactivo';
                             } ?>
                            <td><span class="<?php echo $class; ?>"><?php echo $estado; ?></span></td>
                            <td><span style="cursor:pointer" onclick="SendEmail('<?php echo $v['coduser']; ?>','SendMail','register','<?php echo $v['user']; ?>')"><i class="far fa-envelope"></i></span></td>
                        </tr>
                    <?php } ?>
                    
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php } ?>

<script>
/*---------------------------------------------- DATATABLES -------------------------------------------- */
var font_size='13px';
$("#tbl_users").css('font-size',font_size);
$(".pagination ").css('font-size',font_size);

var tbl_users=$("#tbl_users").DataTable({
    scrollCollapse: true,
    paging:         true,
    "searching": true,
    "order": [],
    "language": {
        "zeroRecords": "No hay registros para mostrar",
        "lengthMenu": "Registros por página  _MENU_ ",
        "info": "",
        "infoEmpty": "",
        "emptyTable": "",
        "infoFiltered": "(filtrado de _MAX_ registros)",
        "search":"Buscar: ",
        "paginate": {
            "first":      "Primero",
            "last":       "Último",
            "next":       "Siguiente",
            "previous":   "Anterior"
        },
    }
} );
</script>