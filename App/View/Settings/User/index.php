<div class="container">
    <div class="row">
        <div class="col-12 col-sm-6 col-md-5 col-lg-4">
            <div class="nav flex-column nav-tabs h-100" id="vert-tabs-tab" role="tablist" aria-orientation="vertical">
                <div class="container" style="padding-right: 0px;">
                    <div class="nav-link d-flex justify-content-center">
                    <i style="cursor:pointer" id="vert-tabs-tab-plus" onclick="OptionUser('create','plus','optionsuserview')" class="fas fa-plus-circle text-success  mr-3"></i>
                        <i style="cursor:pointer" id="vert-tabs-tab-plus" onclick="OptionUser('list','plus','optionsuserview')" class="fas fa-list text-success "></i>
                    </div>
                </div>
                <?php if(count($params)>0){ ?> 
                   <div class="container " style="padding-right: 0px;">
                        <?php $i=0;foreach($params['users'] as $key=>$value){ ?> 
                            <?php if($i==0){ ?>
                                <div class="nav-link" id="vert-tabs-tab-<?php echo $value['coduser']; ?>">
                                    <div class="row d-flex justify-content-center">
                                        <div class="col-10 col-md-8 col-lg-8">
                                            <li class=" lb-ipt text-dark font-weight-bold cursor-pointer" onclick="OptionUser('show','<?php echo $value['coduser']; ?>','optionsuserview')"><?php echo utf8_encode($value['nameuser'].' '.$value['appuser']) ; ?>
                                            </li>
                                        </div>
                                    </div>
                                </div>
                            <?php }else{ ?> 
                                <div class="nav-link" id="vert-tabs-tab-<?php echo $value['coduser']; ?>">
                                    <div class="row d-flex justify-content-center">
                                        <div class="col-10 col-md-8 col-lg-8">
                                            <li class=" lb-ipt text-dark font-weight-bold cursor-pointer" onclick="OptionUser('show','<?php echo $value['coduser']; ?>','optionsuserview')"><?php echo utf8_encode($value['nameuser'].' '.$value['appuser']) ; ?></li>
                                        </div>
                                    </div>
                                </div>
                            <?php } $i++; ?>
                        <?php } ?>       
                   </div>
                <?php } ?>
            </div>
        </div>
        <div class="col-12 col-sm-6 col-md-7 col-lg-8">
            <br>
            <div id="content_user">  
            </div>
        </div>
    </div>
</div>