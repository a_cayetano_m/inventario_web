<!-- CREATE MENU -->
<div class="tab-pane text-left fade show" id="vert-tabs-plus" role="tabpanel">
    <div class="row">
        <div class="col-sm-12 col-md-11 offset-md-1">
            <div class="form-group">
                <div class="row">
                    <br>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-6">
                        <label for="ipt_menu_name" class="labelform">Nombre</label>
                        <input class="form-control form-control-sm" type="text" id="ipt_menu_name">
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <label for="ipt_route_name" class="labelform">Ruta</label>
                        <input class="form-control form-control-sm" type="text" id="ipt_route_name">
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <label for="ipt_menu_orden" class="labelform">Orden</label>
                        <input class="form-control form-control-sm" type="text" placeholder="Orden" id="ipt_menu_orden"> 
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <label for="ipt_icon_name" class="labelform">Icono</label>
                        <input class="form-control form-control-sm" type="text" placeholder="Icono" id="ipt_icon_name"> 
                    </div>
                    <div class="col-12 col-sm-12 col-md-6">
                        <label for="ipt_type_name" class="labelform">Tipo menu</label>
                        <div class="form-group">
                            <select class="form-control form-control-sm" id="ipt_type_name">
                                <option selected disabled value="">Seleccione</option>
                                <option value="0">Default</option>
                                <option value="1">Options</option>
                            </select>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row  d-flex justify-content-center"><button id="btn_menu_add" class="btn btn-success btn-sm">Guardar</button></div>
            </div>
        </div>
    </div>
</div>