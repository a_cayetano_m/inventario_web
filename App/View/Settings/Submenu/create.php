<!-- CREATE SUBMENU -->
<div class="tab-pane text-left fade show" id="vert-tabs-plus" role="tabpanel">
    <div class="row">
        <div class="col-sm-12 col-md-11 offset-md-1">
            <div class="form-group">
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <label for="ipt_submenu_name" class="labelform">Nombre</label>
                        <input class="form-control form-control-sm" type="text" id="ipt_submenu_name">
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <label for="ipt_submenu_orden" class="labelform">Orden</label>
                        <input class="form-control form-control-sm" type="text" id="ipt_submenu_orden">
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <label for="ipt_submenuicon_name" class="labelform">Icono</label>
                        <input class="form-control form-control-sm" type="text" id="ipt_submenuicon_name">
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <label for="ipt_submenuroute_name" class="labelform">Ruta</label>
                        <input class="form-control form-control-sm" type="text" id="ipt_submenuroute_name">
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <label for="ipt_submenudes_name" class="labelform">Descripción</label>
                        <input class="form-control form-control-sm" type="text" id="ipt_submenudes_name">
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <label for="ipt_submenudes_name" class="labelform">Menu</label>
                        <?php if(count($params)>0){ ?>
                            <div class="form-group">
                                <select class="form-control form-control-sm" id="slc_menu_name">
                                    <option value="">Seleccione</option>
                                    <?php foreach($params['menu'] as $k=>$v){?> 
                                    <option value="<?php echo $v['codmenu']; ?>"><?php echo $v['namemenu']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <br>
                <div class="row  d-flex justify-content-center"><button id="btn_submenu_add" class="btn btn-success btn-sm">Guardar</button></div>
            </div>
        </div>
    </div>
</div>