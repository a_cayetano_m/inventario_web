<!-- CREATE ROLE -->
<div class="tab-pane text-left fade show" id="vert-tabs-plus" role="tabpanel">
    <div class="row">
        <div class="col-sm-12 col-md-11 offset-md-1">
            <div class="form-group">
                <div class="row">
                    <br>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-6">
                        <label for="ipt_role_name" class="labelform">Nombre</label>
                        <input class="form-control form-control-sm" type="text" id="ipt_role_name">
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-6">
                        <label for="ipt_role_est" class="labelform">Estado</label>
                        <div class="form-group">
                            <select class="form-control form-control-sm " id="ipt_role_est">
                                <option value="">Seleccione</option>
                                <option value="1">Activo</option>
                                <option value="0">Inactivo</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 ">
                        <div class="row">
                            <div class="col-12">
                                <label for="ipt_menu" class="labelform">Menú</label>
                                <div class="form-group">
                                    <select class="form-control form-control-sm " id="ipt_menu">
                                        <option value="" selected disabled>Seleccione</option>
                                        <?php foreach($params as $k=>$v){?> 
                                            <option value="<?php echo $v['routecod'];?>"><?php echo $v['nameroute'];?></option>
                                        <?php } ?> 
                                    </select>
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>
                <br>
                <div class="row  d-flex justify-content-center">
                    <button id="btn_role_add" class="btn btn-success btn-sm">Guardar</button>
                </div>
            </div>
        </div>
    </div>
</div>