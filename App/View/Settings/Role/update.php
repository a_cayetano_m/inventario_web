

<!-- SHOW MENU -->
<?php if(count($params)>0){?> 
<div class="tab-pane text-left fade show" id="vert-tabs-plus" role="tabpanel">
    <div class="row">
        <div class="col-sm-12 col-md-11 offset-md-1">
            <div class="form-group">
                <div class="row">
                    <br>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-6">
                        <label for="ipt_role_name" class="labelform">Nombre</label>
                        <input class="form-control form-control-sm" type="text" id="ipt_role_name" value="<?php echo $params['roles']['namerole']?>">
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-6">
                        <label for="ipt_role_est" class="labelform">Estado</label>
                        <div class="form-group">
                            <select class="form-control form-control-sm " placeholder="Role" id="ipt_role_est">
                                <?php if($params['roles']['estrole']=='1'){ ?> 
                                    <option value="1" checked>Activo</option>
                                    <option value="0">Inactivo</option>
                                <?php }else{ ?>
                                    <option value="1">Activo</option>
                                    <option value="0" checked>Inactivo</option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-11 offset-md-1 col-lg-12 offset-lg-0">
                        <?php $i=0;foreach($params['actions_select'] as $key=>$value){?> 
                            <div class="tab-pane text-left fade show" id="vert-tabs-<?php echo $key; ?>" role="tabpanel">
                                <div class="row d-flex justify-content-center">
                                    <?php foreach($params['actions'] as $k=>$v){?> 
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <div class="form-group">
                                                <div class="custom-control custom-checkbox" id="div_role_actions">
                                                    <?php if(in_array($v['actcod'],$value)){ ?>
                                                        <input class="custom-control-input" type="checkbox" id="<?php echo "ipt_".$v['actcod'];?>" value="<?php echo $v['actcod'];?>" checked>
                                                        <label for="<?php echo "ipt_".$v['actcod'];?>" class="custom-control-label"><?php echo $v['nameact'];?></label>
                                                    <?php }else{ ?> 
                                                        <input class="custom-control-input" type="checkbox" id="<?php echo "ipt_".$v['actcod'];?>" value="<?php echo $v['actcod'];?>">
                                                        <label for="<?php echo "ipt_".$v['actcod'];?>" class="custom-control-label"><?php echo $v['nameact'];?></label>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>   
                                </div>
                            </div>  
                        <?php $i++; } ?>
                    </div>
                </div>
                <br>
                <div class="row  d-flex justify-content-center">
                    <button id="btn_role_update" class="btn btn-success btn-sm">Guardar</button>
                </div>
            </div>
        </div>
    </div>
</div>
<?php } ?> 