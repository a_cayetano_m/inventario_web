
<div class="container">
    <div class="row">
        <div class="col-12 col-sm-5 col-md-5 col-lg-5 col-xl-4">
            <div class="nav flex-column nav-tabs h-100" id="vert-tabs-tab" role="tablist" aria-orientation="vertical">
                <div class="container" style="padding-right: 0px;">
                    <div class="nav-link d-flex justify-content-center" id="vert-tabs-tab-plus" onclick="OptionRole('create','plus','optionsroleview')"><i class="fas fa-plus-circle text-success"></i>
                    </div>
                </div>
                <?php if(count($params)>0){ ?> 
                    <div class="container" style="padding-right: 0px;">
                    <?php $i=0;foreach($params['roles'] as $key=>$value){ ?> 
                        <?php if($i==0){ ?>
                            <div class="nav-link" id="vert-tabs-tab-<?php echo $value['codrole']; ?>">
                                <div class="row">
                                    <div class="col-10 col-md-8 col-lg-8">
                                        <li class=" text-dark font-weight-bold cursor-pointer" onclick="OptionRole('show','<?php echo $value['codrole']; ?>','optionsroleview')"><?php echo $value['namerole'];?>
                                        </li>
                                    </div>
                                    <div class="col-2 col-md-2 col-lg-2">
                                        <i class="fas fa-trash-alt text-danger cursor-pointer" onclick="DeleteRole('delete','<?php echo $value['codrole']; ?>','optionsrole')"></i>
                                    </div>
                                </div>
                            </div>
                        <?php }else{ ?> 
                            <div class="nav-link" id="vert-tabs-tab-<?php echo $value['codrole']; ?>">
                                <div class="row">
                                    <div class="col-10 col-md-8 col-lg-8">
                                        <li class=" text-dark font-weight-bold cursor-pointer" onclick="OptionRole('show','<?php echo $value['codrole']; ?>','optionsroleview')"><?php echo $value['namerole'];?>
                                        </li>
                                    </div>
                                    <div class="col-2 col-md-2 col-lg-2">
                                        <i class="fas fa-trash-alt text-danger cursor-pointer" onclick="DeleteRole('delete','<?php echo $value['codrole']; ?>','optionsrole')"></i>
                                    </div>
                                </div>
                            </div>
                        <?php } $i++; ?>
                    <?php } ?>   
                    </div>    
                <?php } ?>
            </div>
        </div>
        <div class="col-12 col-sm-7 col-md-7 col-lg-7 col-xl-8">
            <div id="content_role">      
            </div>
        </div>
    </div>
</div>