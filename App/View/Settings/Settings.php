
<!-- Layouts -->
<?php include './App/View/Layouts/url.php'; ?>
<!DOCTYPE html>
<html>

<!-- Header -->
<?php include $backurlview.'Layouts/header.php'; ?>
<!-- /.Header -->

<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
  <?php include $backurlview.'Layouts/navbar.php'; ?>
  <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <?php include $backurlview.'Layouts/menu.php'; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Configuración</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item">Configuración</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title"></h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <!-- Content -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-primary card-outline card-outline-tabs">
                            <div class="card-header p-0 border-bottom-0 div_tabs"  id="div_tabs">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="a_user" data-toggle="pill" role="tab">Usuarios<img class="d-none" src="./App/assets/img/loading-icon.svg" alt=""></a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="a_program" data-toggle="pill" role="tab">Programas<img class="d-none" src="./App/assets/img/loading-icon.svg" alt=""></a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="a_role" data-toggle="pill" role="tab">Roles<img class="d-none" src="./App/assets/img/loading-icon.svg" alt=""></a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link " id="a_menu" data-toggle="pill" role="tab">Menú<img class="d-none" src="./App/assets/img/loading-icon.svg" alt=""></a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link " id="a_submenu" data-toggle="pill" role="tab">SubMenú<img class="d-none" src="./App/assets/img/loading-icon.svg" alt=""></a>
                                    </li>
                                </ul>
                            </div>
                            <div class="card-body">
                                <div class="tab-content" id="div_tab_content_general">
                                        
                                </div>
                            </div>
                            <!-- /.card -->
                        </div>
                    </div>
                    <!-- /.row -->
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <!-- <div class="float-right d-none d-sm-block">
      <b>Version</b> 3.0.5
    </div>
    <strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io">AdminLTE.io</a>.</strong> All rights
    reserved.-->
  </footer>
                                                
  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->


<!-- Footer -->
<?php include $backurlview.'Layouts/footer.php'; ?>

<!-- Validate JS -->
<script src="<?php echo $backurl;?>js/Settings/validate.js"></script>

<!-- Functions JS -->
<script src="<?php echo $backurl;?>js/Settings/functions.js"></script>

<!-- Events JS -->
<script src="<?php echo $backurl;?>js/Settings/events.js"></script>


</body>
</html>
