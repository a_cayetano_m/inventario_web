
/*DESCOMENTAR PARA BLOQUEAR*/
/*Swal.fire({
    title: "Estamos presentando dificultades técnicas",
    text:"Por favor, espera unas horas para su solución.",
    icon: 'warning',
    showConfirmButton: false,
    allowOutsideClick: false,
    closeOnClickOutside: false
})*/
var MAX_TIME_SEARCH=31;

$("#logo_general").on("click", function(e){
    e.preventDefault()
    window.open('https://www.tailoy.com.pe', '_blank');
});

$("#a_cerrarsesion").on("click", function(){
    cerrarsession();
});
$(".a_cerrarsesion").on("click", function(){
    cerrarsession();
});
function countChars(obj,max){
    var maxLength = max;
    var strLength = obj.value.length;
    
    if(strLength > maxLength){
        document.getElementById("charNum").innerHTML = '<span style="color: red;">'+strLength+' de '+maxLength+' caracteres</span>';

    }else{
        document.getElementById("charNum").innerHTML = strLength+' de '+maxLength+' caracteres';
    }
}


function passchangeFirst(){
    $.ajax({
        type: "POST",
        url: url_global+"PrimerCambioPass",
        dataType : 'json',
        data: {},
        async:true
    }).done(function(response) { // respuesta
        if(response.validate==false){
            $('#md_first_change').modal('toggle');
        }
    });
}
function cerrarsession(){
   
    Swal.fire({
        title: '¿ Cerrar sesión ? ',
        text:'',
        icon: 'warning',
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si',
        cancelButtonText: 'No'
      }).then((result) => {
        if (result.value) {
            const url = url_global+"cerrarsession";
            let data={};
            let value='';
            
            Swal.fire({
                title: 'Cerrando Sesión',
                html: '<strong>Por favor espere...</strong>',
                onOpen: () => {
                    Swal.showLoading()
                    $.ajax({
                        type: "POST",
                        url: url,
                        dataType : 'json',
                        data: {},
                        async:true
                    }).done(function(response) { // respuesta
        
                        if(response.validate){
                            Swal.fire({
                                icon: 'success',
                                title: "Sesión cerrada",
                                showConfirmButton: false,
                                timer: 2000,
                              }).then(() => {
                                window.location.href='./';
                              });
                        }else{
                            Swal.fire({
                                icon: 'error',
                                text: "No se pudo cerrar sessión, comuníquese con el administrador",
                                showConfirmButton: false,
                                timer: 2000,
                              }).then(() => {
                                window.location.href='./';
                              });
                        }
                    });
                }
              })
            
        }
      })
}
 

function RangeExportReport(fechas,tipo){ //RANGO DE MAXIMO 1 MES
    var array_fechas=fechas.split('-');
    var fecha1=array_fechas[0].split("/")[2].trim()+"-"+array_fechas[0].split("/")[1].trim()+"-"+array_fechas[0].split("/")[0].trim();
    var fecha2 =array_fechas[1].split("/")[2].trim()+"-"+array_fechas[1].split("/")[1].trim()+"-"+array_fechas[1].split("/")[0].trim();
    fecha1 = moment(fecha1);
    fecha2 = moment(fecha2);
    
    switch(tipo) {
        case 'reporte':
            var diff=fecha2.diff(fecha1,'days');
            if(diff<MAX_DAY_PER_MONTH){
                return true;
            }else{
                return false;
            }
        break;
        case 'historial':
            var diff=fecha2.diff(fecha1,'days');
            if(diff<MAX_DAY_PER_MONTH_2){
                return true;
            }else{
                return false;
            }
        break;
    }
} 
/* CAMBIO DE PRIMERA CONTRASEÑA */

passchangeFirst();


/* ACTIVE EXTRA FUNCTIONS */
$(function () {
    $('[data-toggle="tooltip"]').tooltip()
  })
  
  $('.selectpicker').selectpicker();