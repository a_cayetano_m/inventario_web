$(document).ready(()=>{
    data={
        type:'user',
        url_type:'User/index',
        url_data:'setting',
        idContent:'div_tab_content_general',
    }
    $("#a_"+data.type+">img").removeClass("d-none");
    LoadContent(data);
});
$("#div_tabs").on('click','#a_user',(e)=> {
    
    data={
        type:'user',
        url_type:'User/index',
        url_data:'setting',
        idContent:'div_tab_content_general',
    }
    $("#a_"+data.type+">img").removeClass("d-none");
    LoadContent(data);

});
$("#div_tabs").on('click','#a_menu',(e)=> {
    
    data={
        type:'menu',
        url_type:'Menu/index',
        url_data:'setting',
        idContent:'div_tab_content_general',
    }
    $("#a_"+data.type+">img").removeClass("d-none");
    LoadContent(data);

});
$("#div_tabs").on('click','#a_role',(e)=> {
    
    data={
        type:'role',
        url_type:'Role/index',
        url_data:'setting',
        idContent:'div_tab_content_general',
    }
    $("#a_"+data.type+">img").removeClass("d-none");
    LoadContent(data);
    
});
$("#div_tabs").on('click','#a_submenu',(e)=> {
    
    data={
        type:'submenu',
        url_type:'Submenu/index',
        url_data:'setting',
        idContent:'div_tab_content_general',
    }
    $("#a_"+data.type+">img").removeClass("d-none");
    LoadContent(data);
    
});
$("#div_tabs").on('click','#a_program',(e)=> {
    
    data={
        type:'program',
        url_type:'Program/index',
        url_data:'setting',
        idContent:'div_tab_content_general',
    }
    $("#a_"+data.type+">img").removeClass("d-none");
    LoadContent(data);

});
/*---------------------------------ROLE------------------------------------------*/
$("#div_tab_content_general").on('click','#btn_role_add',(e)=> {
    let ipt_role_name=$("#ipt_role_name").val();
    let ipt_role_est=$("#ipt_role_est").val();
    let ipt_menu=$("#ipt_menu").val();
    let validate=false;
    $("#ipt_role_name").removeClass("is-invalid");
    $("#ipt_role_est").removeClass("is-invalid");
    $("#ipt_menu").removeClass("is-invalid");
    if(ipt_role_name=='' || ipt_role_name==null || ipt_role_name===undefined){
        validate=true;
        $("#ipt_role_name").addClass("is-invalid");
        toastr.error("Sebe ingresar un nombre de rol");
    }
    if(ipt_role_est=='' || ipt_role_est==null || ipt_role_est===undefined){
        validate=true;
        $("#ipt_role_est").addClass("is-invalid");
        toastr.error("Debe seleccionar algún estado");
    }
    if(ipt_menu=='' || ipt_menu==null || ipt_menu===undefined){
        validate=true;
        $("#ipt_menu").addClass("is-invalid");
        toastr.error("Debe seleccionar alguna opción de menó");
    }
    if(!validate){
        data={
            type:'create',
            ipt_role_name:ipt_role_name,
            ipt_role_est:ipt_role_est,
            ipt_menu:ipt_menu,
            url_data:'optionsrole',
    
        }
        $("#btn_role_add").html("Guardar<img src='App/assets/img/loading-icon.svg'>");
        SendData(data);
        data={
            type:'role',
            url_type:'Role/index',
            url_data:'setting',
            idContent:'div_tab_content_general',
        }
       // $("#a_"+data.type+">img").removeClass("d-none");
        LoadContent(data);
    }
});
$("#div_tab_content_general").on('click','#btn_role_update',(e)=> {
    let ipt_role_name=$("#ipt_role_name").val();
    let ipt_role_est=$("#ipt_role_est").val();
    let ipt_role_code=$("#ipt_role_code").val();
    let ipt_menu=$("#ipt_menu").val();
    let validate=false;
    $("#ipt_role_name").removeClass("is-invalid");
    $("#ipt_role_est").removeClass("is-invalid");
    $("#ipt_role_code").removeClass("is-invalid");
    if(ipt_role_name=='' || ipt_role_name==null || ipt_role_name===undefined){
        validate=true;
        $("#ipt_role_name").addClass("is-invalid");
        toastr.error("Sebe ingresar un nombre de rol");
    }
    if(ipt_role_est=='' || ipt_role_est==null || ipt_role_est===undefined){
        validate=true;
        $("#ipt_role_est").addClass("is-invalid");
        toastr.error("Debe seleccionar algún estado");
    }
    if(ipt_role_code=='' || ipt_role_code==null || ipt_role_code===undefined){
        validate=true;
        $("#ipt_role_code").addClass("is-invalid");
    }
    if(!validate){
        data={
            type:'update',
            ipt_role_code:ipt_role_code,
            ipt_role_name:ipt_role_name,
            ipt_role_est:ipt_role_est,
            ipt_menu:ipt_menu,
            url_data:'optionsrole',
    
        }
        $("#btn_role_update").html("Guardar<img src='App/assets/img/loading-icon.svg'>");
        SendData(data);
       
        data={
            type:'role',
            url_type:'Role/index',
            url_data:'setting',
            idContent:'div_tab_content_general',
        }
        //$("#a_"+data.type+">img").removeClass("d-none");
        LoadContent(data);
    }
});
$("#div_tab_content_general").on('click','#btn_menu_add',(e)=> {
    let ipt_menu_name=$("#ipt_menu_name").val();
    let ipt_menu_orden=$("#ipt_menu_orden").val();
    let ipt_icon_name=$("#ipt_icon_name").val();
    let ipt_route_name=$("#ipt_route_name").val();
    let ipt_type_name=$("#ipt_type_name").val();
   
    let validate=false;
    $("#ipt_menu_name").removeClass("is-invalid");
    $("#ipt_menu_orden").removeClass("is-invalid");
    $("#ipt_icon_name").removeClass("is-invalid");
    $("#ipt_route_name").removeClass("is-invalid");
    $("#ipt_type_name").removeClass("is-invalid");
    if(ipt_menu_name=='' || ipt_menu_name==null || ipt_menu_name===undefined){
        validate=true;
        $("#ipt_menu_name").addClass("is-invalid");
        toastr.error("Sebe ingresar un nombre de menu");
    }
    if(ipt_menu_orden=='' || ipt_menu_orden==null || ipt_menu_orden===undefined){
        validate=true;
        $("#ipt_menu_orden").addClass("is-invalid");
        toastr.error("Debe darle un número de orden");
    }
    if(ipt_menu_orden.length>2  ){
        validate=true;
        $("#ipt_menu_orden").addClass("is-invalid");
        toastr.error("Número de orden máximo 2 dígitos");
    }
    if(ipt_type_name=='' || ipt_type_name==null || ipt_type_name===undefined){
        validate=true;
        $("#ipt_type_name").addClass("is-invalid");
        toastr.error("Debe seleccionar el tipo");
    }
    if(ipt_icon_name=='' || ipt_icon_name==null || ipt_icon_name===undefined){
        validate=true;
        $("#ipt_icon_name").addClass("is-invalid");
        toastr.error("Debe ingresar un class font-awesome");
    }
    if(ipt_route_name=='' || ipt_route_name==null || ipt_route_name===undefined){
        validate=true;
        $("#ipt_route_name").addClass("is-invalid");
        toastr.error("Debe ingresar una ruta para el menu");
    }
    if(!validate){
        data={
            type:'create',
            ipt_menu_name:ipt_menu_name,
            ipt_menu_orden:ipt_menu_orden,
            ipt_icon_name:ipt_icon_name,
            ipt_route_name:ipt_route_name,
            ipt_type_name:ipt_type_name,
            url_data:'optionsmenu',
    
        }
        $("#btn_menu_add").html("Guardar<img src='App/assets/img/loading-icon.svg'>");
        SendData(data);
        data={
            type:'menu',
            url_type:'Menu/index',
            url_data:'setting',
            idContent:'div_tab_content_general',
        }
        $("#a_"+data.type+">img").removeClass("d-none");
        LoadContent(data);
    }
});
$("#div_tab_content_general").on('click','#btn_menu_update',(e)=> {
    console.log($("#ipt_type_name").val())
    let ipt_menu_name=$("#ipt_menu_name").val();
    let ipt_menu_orden=$("#ipt_menu_orden").val();
    let ipt_icon_name=$("#ipt_icon_name").val();
    let ipt_route_name=$("#ipt_route_name").val();
    let ipt_menu_cod=$("#ipt_menu_cod").val();
    let ipt_type_name=$("#ipt_type_name").val();
    
    let validate=false;
    $("#ipt_menu_name").removeClass("is-invalid");
    $("#ipt_menu_orden").removeClass("is-invalid");
    $("#ipt_icon_name").removeClass("is-invalid");
    $("#ipt_route_name").removeClass("is-invalid");
    $("#ipt_type_name").removeClass("is-invalid");
    if(ipt_menu_name=='' || ipt_menu_name==null || ipt_menu_name===undefined){
        validate=true;
        $("#ipt_menu_name").addClass("is-invalid");
        toastr.error("Sebe ingresar un nombre de menu");
    }
    if(ipt_menu_orden=='' || ipt_menu_orden==null || ipt_menu_orden===undefined){
        validate=true;
        $("#ipt_menu_orden").addClass("is-invalid");
        toastr.error("Debe darle un número de orden");
    }
    if(ipt_menu_orden.length>2  ){
        validate=true;
        $("#ipt_menu_orden").addClass("is-invalid");
        toastr.error("Número de orden máximo 2 dígitos");
    }
    if(ipt_type_name=='' || ipt_type_name==null || ipt_type_name===undefined){
        validate=true;
        $("#ipt_type_name").addClass("is-invalid");
        toastr.error("Debe seleccionar el tipo");
    }
    if(ipt_icon_name=='' || ipt_icon_name==null || ipt_icon_name===undefined){
        validate=true;
        $("#ipt_icon_name").addClass("is-invalid");
        toastr.error("Debe ingresar un class font-awesome");
    }
    if(ipt_route_name=='' || ipt_route_name==null || ipt_route_name===undefined){
        validate=true;
        $("#ipt_route_name").addClass("is-invalid");
        toastr.error("Debe ingresar una ruta para el menu");
    }
    if(!validate){
        data={
            type:'update',
            ipt_menu_cod:ipt_menu_cod,
            ipt_menu_name:ipt_menu_name,
            ipt_menu_orden:ipt_menu_orden,
            ipt_icon_name:ipt_icon_name,
            ipt_route_name:ipt_route_name,
            ipt_type_name:ipt_type_name,
            url_data:'optionsmenu',
    
        }
        $("#btn_menu_update").html("Guardar<img src='App/assets/img/loading-icon.svg'>");
        SendData(data);
        data={
            type:'menu',
            url_type:'Menu/index',
            url_data:'setting',
            idContent:'div_tab_content_general',
        }
        $("#a_"+data.type+">img").removeClass("d-none");
        LoadContent(data);
    }
});
/*----------------------------------------------SUBMENU----------------------------------------*/
$("#div_tab_content_general").on('click','#btn_submenu_add',(e)=> {
    let ipt_submenumenu_name=$("#ipt_submenu_name").val();
    let ipt_submenumenu_orden=$("#ipt_submenu_orden").val();
    let ipt_submenuicon_name=$("#ipt_submenuicon_name").val();
    let ipt_submenuroute_name=$("#ipt_submenuroute_name").val();
    let ipt_submenudes_name=$("#ipt_submenudes_name").val();
    let slc_menu_name=$("#slc_menu_name").val();
    let validate=false;
    $("#ipt_submenu_name").removeClass("is-invalid");
    $("#ipt_submenu_orden").removeClass("is-invalid");
    $("#ipt_submenuicon_name").removeClass("is-invalid");
    $("#ipt_submenuroute_name").removeClass("is-invalid");
    $("#ipt_submenudes_name").removeClass("is-invalid");
    $("#slc_menu_name").removeClass("is-invalid");
    if(ipt_submenumenu_name=='' || ipt_submenumenu_name==null || ipt_submenumenu_name===undefined){
        validate=true;
        $("#ipt_submenu_name").addClass("is-invalid");
        toastr.error("Sebe ingresar un nombre de submenu");
    }
    if(ipt_submenumenu_orden=='' || ipt_submenumenu_orden==null || ipt_submenumenu_orden===undefined){
        validate=true;
        $("#ipt_submenu_orden").addClass("is-invalid");
        toastr.error("Debe darle un número de orden");
    }
    if(ipt_submenumenu_orden.length>2  ){
        validate=true;
        $("#ipt_submenu_orden").addClass("is-invalid");
        toastr.error("Número de orden máximo 2 dígitos");
    }
    if(ipt_submenuicon_name=='' || ipt_submenuicon_name==null || ipt_submenuicon_name===undefined){
        if(ipt_submenumenu_name!='null'){
            validate=true;
            $("#ipt_submenuicon_name").addClass("is-invalid");
            toastr.error("Debe ingresar un class font-awesome");
        }
    }
    if(ipt_submenuroute_name=='' || ipt_submenuroute_name==null || ipt_submenuroute_name===undefined){
        validate=true;
        $("#ipt_submenuroute_name").addClass("is-invalid");
        toastr.error("Debe ingresar una ruta para el submenu");
    }
    if(ipt_submenudes_name=='' || ipt_submenudes_name==null || ipt_submenudes_name===undefined){
        validate=true;
        $("#ipt_submenudes_name").addClass("is-invalid");
        toastr.error("Sebe ingresar la descripcion de submenu");
    }
    if(!validate){
        data={
            type:'create',
            ipt_submenumenu_name:ipt_submenumenu_name,
            ipt_submenumenu_orden:ipt_submenumenu_orden,
            ipt_submenuicon_name:ipt_submenuicon_name,
            ipt_submenuroute_name:ipt_submenuroute_name,
            ipt_submenudes_name:ipt_submenudes_name,
            slc_menu_name:slc_menu_name,
            url_data:'optionssubmenu',
    
        }
        $("#btn_submenu_add").html("Guardar<img src='App/assets/img/loading-icon.svg'>");
        SendData(data);
        data={
            type:'submenu',
            url_type:'Submenu/index',
            url_data:'setting',
            idContent:'div_tab_content_general',
        }
        $("#a_"+data.type+">img").removeClass("d-none");
        LoadContent(data);
    }
});
$("#div_tab_content_general").on('click','#btn_submenu_update',(e)=> {
    let ipt_submenu_cod=$("#ipt_submenu_cod").val();
    let ipt_submenumenu_name=$("#ipt_submenu_name").val();
    let ipt_submenumenu_orden=$("#ipt_submenu_orden").val();
    let ipt_submenuicon_name=$("#ipt_submenuicon_name").val();
    let ipt_submenuroute_name=$("#ipt_submenuroute_name").val();
    let slc_menu_name=$("#slc_menu_name").val();
    let validate=false;
    $("#ipt_submenu_name").removeClass("is-invalid");
    $("#ipt_submenu_orden").removeClass("is-invalid");
    $("#ipt_submenuicon_name").removeClass("is-invalid");
    $("#ipt_submenuroute_name").removeClass("is-invalid");
    $("#slc_menu_name").removeClass("is-invalid");
    if(ipt_submenumenu_name=='' || ipt_submenumenu_name==null || ipt_submenumenu_name===undefined){
        validate=true;
        $("#ipt_submenu_name").addClass("is-invalid");
        toastr.error("Sebe ingresar un nombre de submenu");
    }
    if(ipt_submenumenu_orden=='' || ipt_submenumenu_orden==null || ipt_submenumenu_orden===undefined){
        validate=true;
        $("#ipt_submenu_orden").addClass("is-invalid");
        toastr.error("Debe darle un número de orden");
    }
    if(ipt_submenumenu_orden.length>2  ){
        validate=true;
        $("#ipt_submenu_orden").addClass("is-invalid");
        toastr.error("Número de orden máximo 2 dígitos");
    }
    if(ipt_submenuicon_name=='' || ipt_submenuicon_name==null || ipt_submenuicon_name===undefined){
        validate=true;
        $("#ipt_submenuicon_name").addClass("is-invalid");
        toastr.error("Debe ingresar un class font-awesome");
    }
    if(ipt_submenuroute_name=='' || ipt_submenuroute_name==null || ipt_submenuroute_name===undefined){
        validate=true;
        $("#ipt_submenuroute_name").addClass("is-invalid");
        toastr.error("Debe ingresar una ruta para el submenu");
    }
    if(!validate){
        data={
            type:'update',
            ipt_submenumenu_name:ipt_submenumenu_name,
            ipt_submenumenu_orden:ipt_submenumenu_orden,
            ipt_submenuicon_name:ipt_submenuicon_name,
            ipt_submenuroute_name:ipt_submenuroute_name,
            slc_menu_name:slc_menu_name,
            ipt_submenu_cod:ipt_submenu_cod,
            url_data:'optionssubmenu',
    
        }
        $("#btn_submenu_add").html("Guardar<img src='App/assets/img/loading-icon.svg'>");
        SendData(data);
        data={
            type:'submenu',
            url_type:'Submenu/index',
            url_data:'setting',
            idContent:'div_tab_content_general',
        }
        $("#a_"+data.type+">img").removeClass("d-none");
       LoadContent(data);
        
    }
});
/*-----------------------------------------USERS------------------------------------------*/

$("#div_tab_content_general").on('click','#btn_user_add',(e)=> {

    let slc_menu_user=$("#slc_menu_user").val();
   // let slc_sup_user=$("#slc_sup_user").val();
    let slc_role=$("#slc_role").val();
    let slc_area=$("#slc_area").val();
    let ipt_razon=$("#ipt_razon").val();
    let ipt_tlf=$("#ipt_tlf").val();
    let slc_status=$("#slc_status").val();
    let ipt_email=$("#ipt_email").val();
    let ipt_app=$("#ipt_app").val();
    let ipt_name=$("#ipt_name").val();
    let ipt_pass=$("#ipt_pass").val();
    let ipt_user=$("#ipt_user").val();
    let ipt_dni=$("#ipt_dni").val();
    let slc_tipo=$("#slc_tipo").val();
    let slc_porcetaje=$("#slc_porcetaje").val();
    let ipt_limit_min=$("#ipt_limit_min").val();
    let ipt_limit_max=$("#ipt_limit_max").val();
    let slc_programs_user=$("#slc_programs_user").val();
    let validate=false;
    if(ipt_user.length==0){
        validate=true;
        toastr.error("Usuario es un dato obligatorio");
    }
    if(ipt_dni.length==0){
        validate=true;
        toastr.error("Dni es un dato obligatorio");
    }
    if(ipt_pass.length==0){
        validate=true;
        toastr.error("Contraseña es un dato obligatorio");
    }
    if(ipt_name.length==0){
        validate=true;
        toastr.error("Nombre es un dato obligatorio");
    }
    if(ipt_razon.length==0){
        validate=true;
        toastr.error("Razon Social es un dato obligatorio");
    }
    if(slc_status.length==0){
        validate=true;
        toastr.error("Debe seleccionar el estado");
    }
    if(slc_programs_user.length==0){
        validate=true;
        toastr.error("Debe seleccionar el programa");
    }
    if(slc_area.length==0){
        validate=true;
        toastr.error("Debe seleccionar el área");
    }
    if(slc_role.length==0){
        validate=true;
        toastr.error("Debe seleccionar uno o más roles");
    }
    if(slc_tipo.length==0){
        validate=true;
        toastr.error("Debe seleccionar el tipo");
    }
    if(slc_porcetaje.length==0){
        validate=true;
        toastr.error("Debe seleccionar el porcentaje");
    }
    if(ipt_limit_min.length==0){
        validate=true;
        toastr.error("Debe seleccionar límite mínimo");
    }
    if(ipt_limit_max.length==0){
        validate=true;
        toastr.error("Debe seleccionar límite max");
    }
    if(slc_menu_user.length==0){
        validate=true;
        toastr.error("Debe seleccionar una opcion del menu/submenu");
    }
    validate=false;
    if(!validate){
        data={
            type:'create',
            slc_menu_user:slc_menu_user,
           // slc_sup_user:slc_sup_user,
            slc_role:slc_role,
            slc_area:slc_area,
            slc_programs_user:slc_programs_user,
            slc_status:slc_status,
            ipt_email:ipt_email,
            ipt_app:ipt_app,
            ipt_pass:ipt_pass,
            ipt_name:ipt_name,
            ipt_dni:ipt_dni,
            ipt_user:ipt_user,
            ipt_razon:ipt_razon,
            ipt_tlf:ipt_tlf,
            slc_porcetaje:slc_porcetaje,
            slc_tipo:slc_tipo,
            ipt_limit_max:ipt_limit_max,
            ipt_limit_min:ipt_limit_min,
            url_data:'optionsuser',
        }
        $("#btn_user_add").html("Guardar<img src='App/assets/img/loading-icon.svg'>");
        SendData(data);
        $("#btn_user_add").html("Guardar");
        /*data={
            type:'user',
            url_type:'User/index',
            url_data:'setting',
            idContent:'div_tab_content_general',
        }
        $("#a_"+data.type+">img").removeClass("d-none");
        LoadContent(data);*/
    }
});
$("#div_tab_content_general").on('click','#btn_user_update',(e)=> {

    let ipt_cod=$("#ipt_cod").val();
    let slc_menu_user=$("#slc_menu_user").val();
    //let slc_sup_user=$("#slc_sup_user").val();
    let ipt_razon=$("#ipt_razon").val();
    let slc_sucursal=$("#slc_sucursal").val();
    let ipt_tlf=$("#ipt_tlf").val();
    let slc_role=$("#slc_role").val();
    let slc_area=$("#slc_area").val();
    let slc_status=$("#slc_status").val();
    let ipt_email=$("#ipt_email").val();
    let ipt_app=$("#ipt_app").val();
    let ipt_name=$("#ipt_name").val();
    let ipt_pass=$("#ipt_pass").val();
    let ipt_dni=$("#ipt_dni").val();
    let ipt_user=$("#ipt_user").val();
    let slc_tipo=$("#slc_tipo").val();
    let slc_porcetaje=$("#slc_porcetaje").val();
    let ipt_limit_min=$("#ipt_limit_min").val();
    let ipt_limit_max=$("#ipt_limit_max").val();
    let slc_programs_user=$("#slc_programs_user").val();
    let validate=false;
    if(ipt_dni.length==0){
        validate=true;
        toastr.error("Dni es un dato obligatorio");
    }
    if(ipt_pass.length==0){
        validate=true;
        toastr.error("Contraseña es un dato obligatorio");
    }
    if(ipt_name.length==0){
        validate=true;
        toastr.error("Nombre es un dato obligatorio");
    }
    if(ipt_razon.length==0){
        validate=true;
        toastr.error("Razon Social es un dato obligatorio");
    }
    /*if(ipt_app.length==0){
        validate=true;
        toastr.error("Apellido es un dato obligatorio");
    }*/
   /* if(ipt_email.length==0){
        validate=true;
        toastr.error("Correo es un dato obligatorio");
    }*/
    if(slc_status.length==0){
        validate=true;
        toastr.error("Debe seleccionar el estado");
    }
    if(slc_area.length==0){
        validate=true;
        toastr.error("Debe seleccionar el área");
    }
    if(slc_programs_user.length==0){
        validate=true;
        toastr.error("Debe seleccionar el programa");
    }
    if(slc_role.length==0){
        validate=true;
        toastr.error("Debe seleccionar uno o más roles");
    }
    if(slc_tipo.length==0){
        validate=true;
        toastr.error("Debe seleccionar el tipo");
    }
    if(slc_porcetaje.length==0){
        validate=true;
        toastr.error("Debe seleccionar el porcentaje");
    }
    if(ipt_limit_min.length==0){
        validate=true;
        toastr.error("Debe seleccionar límite mínimo");
    }
    if(ipt_limit_max.length==0){
        validate=true;
        toastr.error("Debe seleccionar límite max");
    }
    if(slc_menu_user.length==0){
        validate=true;
        toastr.error("Debe seleccionar una opcion del menu/submenu");
    }
    if(!validate){
        data={
            type:'update',
            ipt_cod:ipt_cod,
            slc_menu_user:slc_menu_user,
            ipt_razon:ipt_razon,
            slc_role:slc_role,
            slc_area:slc_area,
            slc_status:slc_status,
            slc_programs_user:slc_programs_user,
            ipt_email:ipt_email,
            ipt_app:ipt_app,
            ipt_pass:ipt_pass,
            ipt_name:ipt_name,
            ipt_dni:ipt_dni,
            ipt_user:ipt_user,
            slc_sucursal:slc_sucursal,
            ipt_tlf:ipt_tlf,
            slc_porcetaje:slc_porcetaje,
            slc_tipo:slc_tipo,
            ipt_limit_max:ipt_limit_max,
            ipt_limit_min:ipt_limit_min,
            url_data:'optionsuser',
        }
        $("#btn_user_update").html("Guardar<img src='App/assets/img/loading-icon.svg'>");
        SendData(data);
        data={
            type:'user',
            url_type:'User/index',
            url_data:'setting',
            idContent:'div_tab_content_general',
        }
        $("#a_"+data.type+">img").removeClass("d-none");
        LoadContent(data);
    }
});

/*-----------------------------------------PROGRAM------------------------------------------*/

$("#div_tab_content_general").on('click','#btn_program_add',(e)=> {

    let ipt_orden=$("#ipt_orden").val();
    let ipt_description=$("#ipt_description").val();
    let slc_status=$("#slc_status").val();
    let slc_proveedores=$("#slc_proveedores").val();
    let validate=false;
    console.log(slc_proveedores)
    if(ipt_orden.length==0){
        validate=true;
        toastr.error("Orden es un dato obligatorio");
    }
    if(slc_proveedores.length==0){
        validate=true;
        toastr.error("Proveedor es un dato obligatorio");
    }
   
    if(ipt_description.length==0){
        validate=true;
        toastr.error("Descripción es un dato obligatorio");
    }
    if(slc_status.length==0){
        validate=true;
        toastr.error("Estado es un dato obligatorio");
    }
    if(!validate){
        data={
            type:'create',
            ipt_orden:ipt_orden,
            ipt_description:ipt_description,
            slc_status:slc_status,
            slc_proveedores:slc_proveedores,
            url_data:'optionsprogram',
        }
        $("#btn_program_add").html("Guardar<img src='App/assets/img/loading-icon.svg'>");
        SendData(data);
        data={
            type:'program',
            url_type:'Programs/index',
            url_data:'setting',
            idContent:'div_tab_content_general',
        }
        $("#a_"+data.type+">img").removeClass("d-none");
        LoadContent(data);
    }
});
$("#div_tab_content_general").on('click','#btn_program_update',(e)=> {

    let ipt_cod=$("#ipt_cod").val();
    let ipt_orden=$("#ipt_orden").val();
    let ipt_description=$("#ipt_description").val();
    let slc_status=$("#slc_status").val();
    let slc_proveedores=$("#slc_proveedores").val();
    let validate=false;
    if(ipt_orden.length==0){
        validate=true;
        toastr.error("Orden es un dato obligatorio");
    }
    if(ipt_description.length==0){
        validate=true;
        toastr.error("Descripción es un dato obligatorio");
    }
    if(slc_status.length==0){
        validate=true;
        toastr.error("Estado es un dato obligatorio");
    }
    if(slc_proveedores.length==0){
        validate=true;
        toastr.error("Proveedor es un dato obligatorio");
    }
    
    if(!validate){
        data={
            type:'update',
            ipt_cod:ipt_cod,
            ipt_orden:ipt_orden,
            ipt_description:ipt_description,
            slc_status:slc_status,
            slc_proveedores:slc_proveedores,
            url_data:'optionsprogram',
        }
        $("#btn_program_update").html("Guardar<img src='App/assets/img/loading-icon.svg'>");
        SendData(data);
        data={
            type:'program',
            url_type:'Program/index',
            url_data:'setting',
            idContent:'div_tab_content_general',
        }
        $("#a_"+data.type+">img").removeClass("d-none");
        LoadContent(data);
    }
});

