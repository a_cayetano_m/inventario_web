function LoadContent(data){
    $.ajax({
        type:"POST",
        url: url_global+data.url_data,
        dataType: "html",
        async:true,
        data: data,
        success: function (response) {
            $("#"+data.idContent).html(response);
            $("#a_"+data.type+">img").addClass("d-none");
        }
    });
}
function SendData(data){
    $.ajax({
        type:"POST",
        url: url_global+data.url_data,
        dataType: "json",
        async:true,
        data: data,
        success: function (response) {
            if(response.validate){
                toastr.success(response.msg)
            }else{
                toastr.error(response.msg)
            }
        }
    });
}
function ReturnData(data,id){
    $.ajax({
        type:"POST",
        url: url_global+data.url_data,
        dataType: "json",
        async:true,
        data: data,
        success: function (response) {
            console.log(response);
            if(response.validate){
               $("#"+id).html("");
            }else{
                toastr.error(response.msg)
            }
        }
    });
}

function OptionRole(option,idRole,url_data){
    console.log(OptionRole);
    data={
        type:option, //show
        url_data:url_data,
        url_type:option,// show
        idRole:idRole, //2
        idContent:'content_role'
    }
    $("#content_role").html("<img src='App/assets/img/loading-icon.svg'>")
    ShowView(data);
    $("#vert-tabs-tab > .container").children().removeClass('active');
    $("#vert-tabs-tab-"+data.idRole).addClass('active');
    
}
function DeleteRole(option,idRole,url_data){
    Swal.fire({
        title: '¿Seguro(a) de eliminar rol?',
        text: "Ningún usuario debe tener este rol",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#28a745',
        cancelButtonColor: '#dc3545',
        confirmButtonText: 'Si,eliminar',
        cancelButtonText: 'Cancelar'
      }).then((result) => {
        if(result.value){
            data={
                type:'delete',
                ipt_role_code:idRole,
                url_data:'optionsrole',
            }
            SendData(data);
            data={
                type:'role',
                url_type:'Role/index',
                url_data:'setting',
                idContent:'div_tab_content_general',
            }
            LoadContent(data);
        }
      })
}
function OptionMenu(option,idMenu,url_data){
    data={
        type:option, //show
        url_data:url_data,
        url_type:option,// show
        idMenu:idMenu, //2
        idContent:'content_menu'
    }
    $("#content_menu").html("<img src='App/assets/img/loading-icon.svg'>")
    ShowView(data);
    $("#vert-tabs-tab > .container").children().removeClass('active');
    $("#vert-tabs-tab-"+data.idMenu).addClass('active');
    
}
function DeleteMenu(option,idMenu,url_data){
    Swal.fire({
        title: '¿Seguro(a) de eliminar menu?',
        text: "Ningún usuario debe tener este menu",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#28a745',
        cancelButtonColor: '#dc3545',
        confirmButtonText: 'Si,eliminar',
        cancelButtonText: 'Cancelar'
      }).then((result) => {
        if(result.value){
            data={
                type:'delete',
                ipt_menu_code:idMenu,
                url_data:'optionsmenu',
            }
            SendData(data);
            
            data={
                type:'menu',
                url_type:'Menu/index',
                url_data:'setting',
                idContent:'div_tab_content_general',
            }
            LoadContent(data);
        }
      })
}
function OptionSubmenu(option,idSubmenu,url_data){
    data={
        type:option, //show
        url_data:url_data,
        url_type:option,// show
        idSubmenu:idSubmenu, //2
        idContent:'content_submenu'
    }
    $("#content_submenu").html("<img src='App/assets/img/loading-icon.svg'>")
    ShowView(data);
    $("#vert-tabs-tab > .container").children().removeClass('active');
    $("#vert-tabs-tab-"+data.idSubmenu).addClass('active');
    
}
function DeleteSubmenu(option,idSubmenu,url_data){
    Swal.fire({
        title: '¿Seguro(a) de eliminar submenu?',
        text: "Ningún usuario debe tener este submenu",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#28a745',
        cancelButtonColor: '#dc3545',
        confirmButtonText: 'Si,eliminar',
        cancelButtonText: 'Cancelar'
      }).then((result) => {
          console.log(result);
        if(result.value){
            data={
                type:'delete',
                ipt_submenu_code:idSubmenu,
                url_data:'optionssubmenu',
            }
            SendData(data);
            
            data={
                type:'submenu',
                url_type:'Submenu/index',
                url_data:'setting',
                idContent:'div_tab_content_general',
            }
            LoadContent(data);
        }
      })
}
function OptionUser(option,idUser,url_data){
    data={
        type:option, //show
        url_data:url_data,
        url_type:option,// show
        idUser:idUser, //2
        idContent:'content_user'
    }
    $("#content_user").html("<img src='App/assets/img/loading-icon.svg'>")
    ShowView(data);
    $("#vert-tabs-tab > .container").children().removeClass('active');
    $("#vert-tabs-tab-"+data.idUser).addClass('active');
    
}
function DataUser(option,url_data){
    data={
        type:option, //show
        url_data:url_data,
        idContent:'content_user'
    }
    ReturnData(data);
}
function validateuser(){
    let user=$("#ipt_user").val();
    let url='validateuser';
    $.ajax({
        type:"POST",
        url: url_global+url,
        dataType: "json",
        async:true,
        data: {
            user:user,
        },
        success: function (response) {
            if(response.validate){
                $("#ipt_user").addClass("is-invalid")
            }else{
                $("#ipt_user").removeClass("is-invalid")
            }
        }
    });
}

/*----OptionsView----*/
function ShowView(data){
    $.ajax({
        type:"POST",
        url: url_global+data.url_data,
        dataType: "html",
        async:true,
        data: data,
        success: function (response) {
            $("#"+data.idContent).html(response);
        }
    });
}
function OptionsView(option,id,url_data,div_content){
    data={
        type:option, //show
        url_data:url_data,
        url_type:option,// show
        id:id, //2
        idContent:div_content
    }
    $("#content").html("<img src='App/assets/img/loading-icon.svg'>")
    ShowView(data);
    $("#vert-tabs-tab > .container").children().removeClass('active');
    $("#vert-tabs-tab-"+data.id).addClass('active');  
}

function SendEmail(id,url_data,option,user){
    Swal.fire({
        title: '¿Enviar correo de confirmación?',
        html: "Verificar que la información del usuario <span style='font-weight:bold'>"+user+"</span> se encuentre cargada<br><span style='font-size:14px'>*El proveedor recibirá sus credenciales.</span>",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#28a745',
        cancelButtonColor: '#dc3545',
        confirmButtonText: 'Si, enviar',
        cancelButtonText: 'Cancelar'
    }).then((result) => {
        if(result.value){
            $.ajax({
                type:"POST",
                url: url_global+url_data,
                dataType: "html",
                async:true,
                data: {
                    "id":id,
                    "option":option
                },
                success: function (response) {
                   
                }
            });
        }
    })
    
}