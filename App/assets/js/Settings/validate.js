

$("#div_tab_content_general").on('focus','#ipt_role_name',(e)=> {
    $('#ipt_role_name').focusout(function() {
        var patron = /[A-Za-zñÑ-áéíóúÁÉÍÓÚ\s/-]/;
        for (i = 0; i < this.value.length; i++) {
            if(!patron.test(this.value.substring(i, i+1))){
                $('#ipt_role_name').val("");
            }
        }
    });
});

$("#div_tab_content_general").on('focus','#ipt_route_name',(e)=> {
    $('#ipt_route_name').focusout(function() {
        var patron = /[A-Za-zñÑ-áéíóúÁÉÍÓÚ\s/]/;
        for (i = 0; i < this.value.length; i++) {
            if(!patron.test(this.value.substring(i, i+1))){
                $('#ipt_route_name').val("");
            }
        }
    });
});
$("#div_tab_content_general").on('focusout','#ipt_dni',(e)=> {
    var patron = /[0-9]/;
    var value=$('#ipt_dni').val();
    for (i = 0; i < value.length; i++) {
        if(!patron.test(value.substring(i, i+1))){
            $('#ipt_dni').val("");
        }
    }
});
$("#div_tab_content_general").on('keyup','#ipt_dni',(e)=> {
    var dInput = $('#ipt_dni').val();
    var reg = /^(?:\+|-)?\d+$/;
    if(!reg.test(dInput)){
        $('#ipt_dni').val(dInput.substr(0,dInput.length-1))
    }
});

$("#div_tab_content_general").on('focusout','#ipt_email',(e)=> {
    var patron = /^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/;
    if(!patron.test(this.value)){
        $('#t_email').val("");
    }
});
$("#div_tab_content_general").on('focusout','#ipt_app',(e)=> {
    var patron = /[A-Za-zñÑ-áéíóúÁÉÍÓÚ\s/]/;
    var value=$('#ipt_app').val();
    for (i = 0; i < value.length; i++) {
        if(!patron.test(value.substring(i, i+1))){
            $('#ipt_app').val("");
        }
    }
});
$("#div_tab_content_general").on('keyup','#ipt_app',(e)=> {
    var dInput =  $('#ipt_app').val();
        
    var reg = /^([0-9+-._*/#$=()&!¨´|°";:,%<>{}¿?¡[]{0,60})$/;
    if(reg.test(dInput.slice(-1))){
        $('#ipt_app').val(dInput.substr(0,dInput.length-1))
    }
    if(dInput.slice(-1)==' '){
        console.log(dInput);
        if(dInput.split(" ").length>3){
            $('#ipt_app').val(dInput.substr(0,dInput.length-1))
        }
    }
});
$("#div_tab_content_general").on('focusout','#ipt_name',(e)=> {
    var patron = /[A-Za-zñÑ-áéíóúÁÉÍÓÚ\s/]/;
    var value=$('#ipt_name').val();
    for (i = 0; i < value.length; i++) {
        if(!patron.test(value.substring(i, i+1))){
            $('#ipt_name').val("");
        }
    }
});
$("#div_tab_content_general").on('keyup','#ipt_name',(e)=> {
    var dInput =  $('#ipt_name').val();
        
    var reg = /^([0-9+-._*/#$=()&!¨´|°";:,%<>{}¿?¡[]{0,60})$/;
    if(reg.test(dInput.slice(-1))){
        $('#ipt_name').val(dInput.substr(0,dInput.length-1))
    }
    if(dInput.slice(-1)==' '){
        if(dInput.split(" ").length>3){
            $('#ipt_name').val(dInput.substr(0,dInput.length-1))
        }
    }
});


/*--------------------------------------- VALIDATE MENU -------------------------------------------*/
$("#div_tab_content_general").on('focusout','#ipt_menu_name',(e)=> {
    var patron = /[A-Za-zñÑ-áéíóúÁÉÍÓÚ\s/./]/;
    var value=$('#ipt_menu_name').val();
    for (i = 0; i < value.length; i++) {
        if(!patron.test(value.substring(i, i+1))){
            $('#ipt_menu_name').val("");
        }
    }
});
$("#div_tab_content_general").on('keyup','#ipt_menu_name',(e)=> {
    var dInput =  $('#ipt_menu_name').val();
    var reg = /^([0-9+_*/#$=()&!¨´|°";:,%<>{}¿?¡[]{0,60})$/;
    if(reg.test(dInput.slice(-1))){
        $('#ipt_menu_name').val(dInput.substr(0,dInput.length-1))
    }
});
$("#div_tab_content_general").on('focusout','#ipt_route_name',(e)=> {
    var patron = /[a-z\s/]/;
    var value=$('#ipt_route_name').val();
    for (i = 0; i < value.length; i++) {
        if(!patron.test(value.substring(i, i+1))){
            $('#ipt_route_name').val("");
        }
    }
});
$("#div_tab_content_general").on('keyup','#ipt_route_name',(e)=> {
    var dInput =  $('#ipt_route_name').val();
    var reg = /^([A-ZñÑ-áéíóúÁÉÍÓÚ\s/0-9+-._*/#$=()&!¨´|°";:,%<>{}¿?¡[]{0,60})$/;
    if(reg.test(dInput.slice(-1))){
        $('#ipt_route_name').val(dInput.substr(0,dInput.length-1))
    }
});
$("#div_tab_content_general").on('focusout','#ipt_menu_orden',(e)=> {
    var patron = /[0-9]/;
    var value=$('#ipt_menu_orden').val();
    for (i = 0; i < value.length; i++) {
        if(!patron.test(value.substring(i, i+1))){
            $('#ipt_menu_orden').val("");
        }
    }
});
$("#div_tab_content_general").on('keyup','#ipt_menu_orden',(e)=> {
    var dInput = $('#ipt_menu_orden').val();
    var reg = /^(?:\+|-)?\d+$/;
    if(!reg.test(dInput)){
        $('#ipt_menu_orden').val(dInput.substr(0,dInput.length-1))
    }
});



/*--------------------------------------- VALIDATE SUBMENU -------------------------------------------*/