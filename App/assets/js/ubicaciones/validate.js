$('#ipt_dni').focusout(function() {
    var patron = /[0-9]/;
    for (i = 0; i < this.value.length; i++) {
        if(!patron.test(this.value.substring(i, i+1))){
            $('#ipt_dni').css("borderColor","#dc3545");
        }
    }  
});
$('#ipt_dni').keyup(function() {
    var dInput = this.value;
    var reg = /^(?:\+|-)?\d+$/;
    if(!reg.test(dInput)){
        $('#ipt_dni').val(
            function(index,value){
                return value.substr(0,value.length-1);
            })
    }
});
$('#ipt_nropedido').focusout(function() {
    var patron = /[0-9]/;
    for (i = 0; i < this.value.length; i++) {
        if(!patron.test(this.value.substring(i, i+1))){
            $('#ipt_nropedido').css("borderColor","#dc3545");
        }
    } 
});
$('#ipt_nropedido').keyup(function() {
    var dInput = this.value;
    var reg = /^(?:\+|-)?\d+$/;
    if(!reg.test(dInput)){
        $('#ipt_nropedido').val(
            function(index,value){
                return value.substr(0,value.length-1);
            })
    }
});
$('#ipt_nroreclamo').keyup(function() {
    var dInput = this.value;
    var reg = /^([-._*/#$=()&!¨´|°" ;:,%<>{}¿?¡[]{0,60})$/;
    if(reg.test(dInput.slice(-1))){
        $('#ipt_apellido').val(
            function(index,value){
                return value.substr(0,value.length-1);
            })
    }
});
$('#ipt_nombre').keyup(function(e) {

   
    var dInput = this.value;
    var reg = /^([0-9+-._*/#$=()&!¨´|°";:,%<>{}¿?¡[]{0,60})$/;
    if(reg.test(dInput.slice(-1))){
        $('#ipt_nombre').val(
            function(index,value){
                return value.substr(0,value.length-1);
            })
    }
    if(dInput.slice(-1)==' '){
        if(dInput.split(" ").length>3){
            $('#ipt_nombre').val(
                function(index,value){
                    return value.substr(0,value.length-1);
            })
        }
    }
});
$('#ipt_apellido').keyup(function() {
    var dInput = this.value;
    var reg = /^([0-9+-._*/#$=()&!¨´|°" ;:,%<>{}¿?¡[]{0,60})$/;
    if(reg.test(dInput.slice(-1))){
        $('#ipt_apellido').val(
            function(index,value){
                return value.substr(0,value.length-1);
            })
    }
    if(dInput.slice(-1)==' '){
         if(dInput.split(" ").length>3){
             $('#ipt_apellido').val(
                 function(index,value){
                     return value.substr(0,value.length-1);
             })
         }
     }

});
$('#ipt_apellido').focusout(function() {
    var validado=true;
    for (i = 0; i < this.value.length; i++) {
        var patron = /([A-Za-zñÑ-áéíóúÁÉÍÓÚ\s/])/;
        if(!patron.test(this.value.substring(i, i+1))){
            validado=false;
            $('#ipt_apellido').val("");
        }
    }
});
$('#ipt_email').focusout(function() {
    var patron = /^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/ ;
    if(!patron.test(this.value)){
        $('#ipt_email').val("");
    }
});
$('#ipt_telefono').keyup(function() {
    var dInput = this.value;
    var reg = /^(?:\+|-)?\d+$/;
    if(!reg.test(dInput)){
        $('#ipt_telefono').val(
            function(index,value){
                return value.substr(0,value.length-1);
            })
    }
});
$('#ipt_telefono').focusout(function() {
    var patron = /[0-9]{0,9}/;
    for (i = 0; i < this.value.length; i++) {
        if(!patron.test(this.value.substring(i, i+1))){
            console.log(this.value.substring(i, i+1));
            $('#ipt_telefono').val("");
        }
    }
});