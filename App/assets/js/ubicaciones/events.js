
let pathname=window.location.pathname; // Ejm: /proyecto/pagina actual
let array_pathname=pathname.split('/');
let optmenu=array_pathname[2].split('-')[0]+"_"+array_pathname[2].split('-')[1];
let li_padre=$("#ul_"+optmenu).parent(); 
let hijos=$("#ul_"+optmenu).children();
li_padre.addClass("menu-open");
hijos.css("display",'block');


$("#btn_buscarperiodo").on("click", function(){
    buscarperiodo();
    
});

$("#check_anterior").on("click", function(){
    let periodos=$("#ipt_periodo").val();
    if(periodos!=''){
        buscarperiodo();
    }
});


/*---------------------------------------------- DATATABLES -------------------------------------------- */

/*
var font_size='12px';
$("#tbl_infoproveedores").css('font-size',font_size);
$(".pagination ").css('font-size',font_size);

let width= $( window ).width();
let fixedColumns=0;
if(width<700){
    fixedColumns=1
}else{
    fixedColumns=3
}
var tbl_infoproveedores=$("#tbl_infoproveedores").DataTable({
    scrollY:        "300px",
    scrollX:        true,
    scrollCollapse: true,
    paging:         false,
    "searching": false,
    fixedColumns:   {
        left: fixedColumns,
    },
    "order": [],
    "language": {
        "zeroRecords": "No hay registros para mostrar",
        "lengthMenu": "Registros por página  _MENU_ ",
        "info": "",
        "infoEmpty": "",
        "emptyTable": "",
        "infoFiltered": "(filtrado de _MAX_ registros)",
        "search":"Filtrar: ",
        "paginate": {
            "first":      "Primero",
            "last":       "Último",
            "next":       "Siguiente",
            "previous":   "Anterior"
        },
    }
} );

$('#tbl_infoproveedores').on( 'page.dt', function () {
    $('html, body').animate({
        scrollTop: 0
    }, 300);
} );
$('[data-filter-type="date-range"]').daterangepicker({
    autoUpdateInput: true,
    applyClass: 'btn-sm btn-success',
    cancelClass: 'btn-sm btn-default',
    startDate: moment().startOf('hour'),
    endDate: moment().startOf('hour').add(24, 'hour'),
    locale: {
        format: 'DD/MM/YYYY',
        applyLabel: 'Aplicar',
        cancelLabel: 'Limpiar',
        fromLabel: 'Desde',
        toLabel: 'Hasta',
        customRangeLabel: 'Seleccionar rango',
        daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
            'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre',
            'Diciembre'],
        firstDay: 1,
       
    }
});

*/