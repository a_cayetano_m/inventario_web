
 /*

function formatPeriodo(periodo){
    if(periodo!=''){
        let periodos=periodo.split("-");
        console.log(periodos)
        let array_periodo_inicio=periodos[0].trim().split("/");
        let array_periodo_final=periodos[1].trim().split("/");
        let periodo_inicio=array_periodo_inicio[2].trim()+"-"+array_periodo_inicio[1].trim()+"-"+array_periodo_inicio[0].trim();
        let periodo_final=array_periodo_final[2].trim()+"-"+array_periodo_final[1].trim()+"-"+array_periodo_final[0].trim();
        periodos=[periodo_inicio,periodo_final];
        return periodos;
    }
}

function ExportarArchivo(tipo){
    switch (tipo) {
        case 1:
            var total=$("#total").val()
            Swal.fire({
                title: '¿Exportar archivo Excel?',
                html: "Esto puede tardar varios minutos <br><span style='font-size:14px'>*Será necesario desbloquear el archivo</span>",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#28a745',
                cancelButtonColor: '#dc3545',
                confirmButtonText: 'Si, exportar',
                cancelButtonText: 'Cancelar'
            }).then((result) => {
                if(result.value){
                    let periodos=$("#ipt_periodo").val();
                    let check_anterior=0;
                    if( $('#check_anterior').is(':checked') ) {
                        check_anterior=1;
                    }
                    if(periodos!=''){
                        $("#ipt_periodo").removeClass('is-invalid')
                        //window.location.href=url_global+"Services/Folder/Reporte.php?reporte="+tipo+"&fechas="+periodos+"&check_anterior="+check_anterior;
                        window.location.href=url_global+"comercial-ventas-excel?reporte="+tipo+"&fechas="+periodos+"&check_anterior="+check_anterior;
                    }else{
                        $("#ipt_periodo").addClass('is-invalid')
                    }
                }
            })
            break;
        case 2:
            Swal.fire({
                title: '¿Exportar archivo Excel?',
                html: "Esto puede tardar varios minutos <br><span style='font-size:14px'>*Será necesario desbloquear el archivo</span>",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#28a745',
                cancelButtonColor: '#dc3545',
                confirmButtonText: 'Si, exportar',
                cancelButtonText: 'Cancelar'
            }).then((result) => {
                if(result.value){
                    //window.location.href=url_global+"Services/Folder/Reporte.php?reporte="+tipo;
                    window.location.href=url_global+"comercial-stock-excel?reporte="+tipo;
                    
                }
            })
            
            break;
        case 3:
            var total=$("#total").val()
            Swal.fire({
                title: '¿Exportar archivo CSV?',
                html: "Esto puede tardar varios minutos <br><span style='font-size:14px'>*Será necesario desbloquear el archivo</span>",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#28a745',
                cancelButtonColor: '#dc3545',
                confirmButtonText: 'Si, exportar',
                cancelButtonText: 'Cancelar'
            }).then((result) => {
                if(result.value){
                    let periodos=$("#ipt_periodo").val();
                    let check_anterior=0;
                    if( $('#check_anterior').is(':checked') ) {
                        check_anterior=1;
                    }
                    if(periodos!=''){
                        $("#ipt_periodo").removeClass('is-invalid')
                        //window.location.href=url_global+"Services/Folder/Reporte.php?reporte="+tipo+"&fechas="+periodos;
                        window.location.href=url_global+"comercial-ventas-zip?reporte="+tipo+"&fechas="+periodos+"&check_anterior="+check_anterior;
                    }else{
                        $("#ipt_periodo").addClass('is-invalid')
                    }
                }
            })
            break;
        case 4:
            Swal.fire({
                title: '¿Exportar archivo CSV?',
                html: "Esto puede tardar varios minutos <br><span style='font-size:14px'>*Será necesario desbloquear el archivo</span>",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#28a745',
                cancelButtonColor: '#dc3545',
                confirmButtonText: 'Si, exportar',
                cancelButtonText: 'Cancelar'
            }).then((result) => {
                if(result.value){
                    //window.location.href=url_global+"Services/Folder/Reporte.php?reporte="+tipo;
                    window.location.href=url_global+"comercial-stock-zip?reporte="+tipo;
                    
                }
            })
            
            break;
    }
}

function buscarperiodo(){
    let periodos=formatPeriodo($("#ipt_periodo").val());
    let check_anterior=false;
    if( $('#check_anterior').is(':checked') ) {
        check_anterior=true;
    }
    var fecha1 = moment(periodos[0]);
    var fecha2 = moment(periodos[1]);
    let dias=fecha2.diff(fecha1, 'days')
    if(dias<=MAX_TIME_SEARCH){
        
        //document.getElementById("frm_buscarperiodo").submit();
        let parametros={
            pagina:'',
            url:'comercial-ventas-carga',
            ipt_periodo:$("#ipt_periodo").val(),
            check_anterior:check_anterior
        }
        getData(parametros);
    }else{
        Swal.fire({
            title: 'No se pudo completar la búsqueda',
            text:'Máximo '+MAX_TIME_SEARCH+' días de diferencia en la consulta',
            icon: 'warning',
            showConfirmButton: true,
        })
    }
}
function getDataPagination(pagina,url,fechas=''){
    let parametros={
        pagina:pagina,
        url:url,
        ipt_periodo:fechas,
        
    }
    getData(parametros);
}
function getData(parametros){
    if(parametros.fechas!=''){
        $(".icono-search").addClass("d-none");
        $(".icono-load").removeClass("d-none");
    }
    $.ajax({
        type:"POST",
        url: url_global+parametros.url,
        dataType: "json",
        async:true,
        data: parametros,
        success: function (response) {
            tbl_infoproveedores.clear().draw();
            if(response['vista']=='1'){
                $("#pag_inicio").val(response['data']['pag_inicio']);
                $("#pag_fin").val(response['data']['pag_fin']);
                $("#total").val(response['data']['total']);
                $("#div_paginacion").html(response['paginacion'])
                $("#div_registros").html("Encontrados "+response['data']['total']+" registros / páginas: "+response['data']['total_paginas'])
                
            }else{
                $(".page-item").removeClass('active');
            }
            $(".icono-search").removeClass("d-none");
            $(".icono-load").addClass("d-none");
            let data_actual=response['data']['data_actual'];
            let data_anterior=response['data']['data_anterior'];

            for(var i=0;i<data_actual.length;i++){
                tbl_infoproveedores.row.add(data_actual[i]).draw();
            }
            for(var i=0;i<data_anterior.length;i++){
                tbl_infoproveedores.row.add(data_anterior[i]).draw();
            }
            $(".page_"+parametros.pagina+".page-item").addClass('active');
        }
    });
    $(".page_previus").removeClass('disabled');
}
function getNextData_ventas(){
    let pag_inicio=parseInt($("#pag_inicio").val());
    let pag_fin=parseInt($("#pag_fin").val());
    let total=parseInt($("#total").val());
    let max_paginas=parseInt($("#max_paginas").val());
    let pagina_actual=$(".page-item.active .page-link").html();
    if(total>max_paginas){
        for (let i = 1; i <= max_paginas; i++) {
            $("#page_"+i+" .page-link").html(parseInt($("#page_"+i+" .page-link").html())+1);
        }
        getDataPagination(parseInt(pag_fin)+1,'comercial-ventas-carga');
        $("#pag_inicio").val(pag_inicio+1);
        $("#pag_fin").val(pag_fin+1);
        $("#page_"+max_paginas).addClass('active');
    }
}
function getPreviousData_ventas(){
    let pag_inicio=parseInt($("#pag_inicio").val());
    let pag_fin=parseInt($("#pag_fin").val());
    let max_paginas=parseInt($("#max_paginas").val());
    let pagina_actual=$(".page-item.active .page-link").html();
    if(pag_inicio>1){
        getDataPagination(parseInt(pag_fin)-1,'comercial-ventas-carga');
        for (let i = 1; i <= max_paginas; i++) {
            $("#page_"+i+" .page-link").html(parseInt($("#page_"+i+" .page-link").html())-1);
        }
        $("#pag_inicio").val(pag_inicio-1);
        $("#pag_fin").val(pag_fin-1);
        $("#page_"+max_paginas).addClass('active');
    }
   
   
}

function getNextData_stock(){
    let pag_inicio=parseInt($("#pag_inicio").val());
    let pag_fin=parseInt($("#pag_fin").val());
    let max_paginas=parseInt($("#max_paginas").val());
    let pagina_actual=$(".page-item.active .page-link").html();
    for (let i = 1; i <= max_paginas; i++) {
        $("#page_"+i+" .page-link").html(parseInt($("#page_"+i+" .page-link").html())+1);
    }
    getData(parseInt(pag_fin)+1,'comercial-stock');
    $("#pag_inicio").val(pag_inicio+1);
    $("#pag_fin").val(pag_fin+1);
    $("#page_"+max_paginas).addClass('active');
   
}
function getPreviousData_stock(){
    let pag_inicio=parseInt($("#pag_inicio").val());
    let pag_fin=parseInt($("#pag_fin").val());
    let max_paginas=parseInt($("#max_paginas").val());
    let pagina_actual=$(".page-item.active .page-link").html();
    if(pag_inicio>1){
        getData(parseInt(pag_fin)-1,'comercial-stock');
        for (let i = 1; i <= max_paginas; i++) {
            $("#page_"+i+" .page-link").html(parseInt($("#page_"+i+" .page-link").html())-1);
        }
        $("#pag_inicio").val(pag_inicio-1);
        $("#pag_fin").val(pag_fin-1);
        $("#page_"+max_paginas).addClass('active');
    }
   
   
}

*/