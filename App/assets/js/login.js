

const btn_login=document.querySelector("#btn_login");
const ipt_pass=document.querySelector("#ipt_pass");


//LISTENER LOGIN

btn_login.addEventListener('click',login);
ipt_pass.addEventListener('keypress',keypress_login);

//FUNCIONS LOGIN
function keypress_login(e){
    if (e.which == 13) {
        login();
    }
}
async function fetch_functions(url,formData){
    try {
        let data=formData;
        let init={
            method:'POST',
            headers: {},
            body:data
        };
        let response= await fetch(url, init);
        if (response.ok) {
            var respuesta = await response.json();
            return respuesta;
        }else{
            throw new Error(response.statusText);
        }
    } catch (err) {
        console.log("Error al realizar la petición AJAX : " + err.message);
    }
}

$("#btn_send_rest").on("click", function(){
    $(".icono-loading-1").removeClass("d-none");
    let parametros={
        'email': $("#ipt_mail").val()
    }
    $.ajax({
        type:"POST",
        url: url_global+'RecuperarPass',
        dataType: "json",
        data: parametros,
        success: function (response) {
           
            if(response.validate){
                $("#message-1").addClass("text-success")
                $("#ipt_mail").val('')
            }else{
                $("#message-1").addClass("text-danger")
            }
            $(".icono-loading-1").addClass("d-none");
            $("#message-1").html(response.message);
        }
    });
    
});
$("#btn_send_change").on("click", function(){
    $(".icono-loading-2").removeClass("d-none");
    let parametros={
        'pass_old': $("#ipt_pass_old").val(),
        'pass_new': $("#ipt_pass_new").val(),
        'email': $("#ipt_mail_change").val()
    }
    $.ajax({
        type:"POST",
        url: url_global+'CambiarPass',
        dataType: "json",
        async:true,
        data: parametros,
        success: function (response) {
            if(response.validate){
                $("#message-2").addClass("text-success")
                $("#ipt_pass_old").val('')
                $("#ipt_pass_new").val('')
                $("#ipt_mail_change").val('')
            }else{
                $("#message-2").addClass("text-danger")
            }
            $(".icono-loading-2").addClass("d-none");
            $("#message-2").html(response.message);
        }
    });
    
});

function login(){
    let user=$("#ipt_user");
    let pass=$("#ipt_pass");
    let url = url_global+"crearsession";
    if(user!='' && pass!=''){
        //USO DE AJAX JQUERY
        var formData = new FormData();
        formData.append("user",user.val());
        formData.append("pass",pass.val());
        $(".icono-loading").removeClass("d-none");
        fetch_functions(url,formData).then(value=>{
            $("#txt_inactivo").addClass("d-none"); 
            $(".icono-loading").addClass("d-none");
            if(value.validate == 1){
                Swal.fire({
                    title: value.msg,
                    icon: 'success',
                    showConfirmButton: false,
                    timer:2000
                    }).then(() => {
                        window.location.href='./inicio';
                    })
            }else if(value.validate==2){
                $("#txt_inactivo").removeClass("d-none");
                Swal.close();
                user.addClass('is-invalid');
                pass.addClass('is-invalid');
            }else if(value.validate==3){ 
                $("#txt_inactivo").addClass("d-none"); 
                Swal.close();
                user.addClass('is-invalid');
                pass.addClass('is-invalid');
            }
        });
        
    }else{
        console.log("es null");
    }
}  


$(document).ready(function() {
    var width=$( window ).width();
    if(width<786){
        $("#div-web").addClass("d-none");
        $("#div-web").removeClass("d-flex");
    }else{
        console.log("web")
        $("#div-mobile").removeClass("d-flex");
        $("#div-mobile").addClass("d-none");
    }
});
$(window).resize(function() {
    var width=$( window ).width();
    if(width<786){
        $("#div-mobile").removeClass("d-none");
        $("#div-web").removeClass("d-flex");
        $("#div-web").addClass("d-none");
    }else{
        $("#div-web").removeClass("d-none");
        $("#div-mobile").removeClass("d-flex");
        $("#div-mobile").addClass("d-none");
    }
});
 
