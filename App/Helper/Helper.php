<?php 

namespace App\Helper;
use App\Model\PROV06\PROV06;
use App\Model\PROV06\PROV06DA;
class Helper{

    public static function message($type,$key){
        $retorno="";
        if(key_exists($type,DEFAULT_DATA['messages'])){
            if(key_exists($key,DEFAULT_DATA['messages'][$type])){
                $retorno=utf8_encode(DEFAULT_DATA['messages'][$type][$key]);
            }else{
                $retorno=utf8_encode("Error al capturar el mensaje por ".$key);
            }
        }else{
            $retorno=utf8_encode("Error al capturar el mensaje por ".$type);
        }
        return utf8_decode($retorno);
    }
    public static function get_session_global(){
        
        return CONFIG['session']['global'];
    }
    public static function view($key,$url){ //tipo de vista(buscar) y url del modulo(extornos)
        $view="";
      /* TENER EN CUENTA */
        /* SI HAY DOS ROLES PARA UN SUBMODuLO EN EL MISMO MODULO : buscar-basico y buscar-sueprvisor , esto tomara el primero debido a que es el nivel mas bajo */
       
        foreach($_SESSION[CONFIG['session']['global']]['USER']['ROLE'] as $k=>$v){
            
            if($v['RUTAMENU']==$url){
                
                $arrayrole=explode("-",$v['NOMBREROLE']); //ejem: extornos-buscar-basico / requerimiento-buscar-basico
                if($arrayrole[0]==$key){
                    $view=$key."/".$arrayrole[1];
                    break;
                }
            }
           
        }
        return $view;
       
    }
    public static function DiffDate($type,$date1,$date2){
        $obj_date1 = new \DateTime($date1);
        $obj_date2 = new \DateTime($date2);
        $diff = $obj_date1->diff($obj_date2);
        $variable="";
        switch ($type) {
            case 'days':
                $variable=$diff->days;
                break;
            
        }
        return $variable;
    }
    public function format_new_id(Array $array_cod){
        
        $part_cod=implode(" ",$array_cod[0]);
        if($part_cod == 0){
            $part_cod=1;
        }else{
            $part_cod=implode(" ",$array_cod[0]);
            $part_cod=$part_cod+1;
        }
       
        return $part_cod;
    }
    public static function format_ubg($ubg){ //15205100
        if(strlen($ubg)==7){
            $ubg = "0".substr($ubg,0,-2); 
        }else if(strlen($ubg)==8){
            $ubg = substr($ubg,0,-2); 
        }
        return $ubg;
    }
    public static function formatoFechaHora($fecha,$hora){
        return substr($fecha,-2,2)."/".substr($fecha,4,2)."/".substr($fecha,0,4)." ".substr($hora,0,2).":".substr($hora,2,2).":".substr($hora,-2,2);
    }
    public static function formatoName($name){
        return ucwords(mb_strtolower(utf8_encode(trim($name))));
    }
    public static function formatoPeriodo($fecha){
        return "SEM ".substr($fecha,4,2)."-".substr($fecha,0,4);
    }
    public static function formatField($count,$prefix,$cast,$action=''){
        $fields='';
        for($i=1;$i<=$count; $i++){
            if($i==1){
                $fields=$fields.''.$cast.'('.$action.'('.$prefix.''.str_pad($i, 3, "0", STR_PAD_LEFT).')) as '.$prefix.''.str_pad($i, 3, "0", STR_PAD_LEFT).'';
            }else{
                $fields=$fields.','.$cast.'('.$action.'('.$prefix.''.str_pad($i, 3, "0", STR_PAD_LEFT).')) as '.$prefix.''.str_pad($i, 3, "0", STR_PAD_LEFT).'';
            }
        }
        return $fields;
    } 
    public static function AddLog($values){
       
        $PROV06=new PROV06();
        $PROV06DA=new PROV06DA();

        $status=$values['status'];
        $user=$values['user'];
        $fcr=$values['fcr'];

        $cod=$PROV06DA->NuevoCodigoLog();
        $PROV06->PLCOD=$cod;
        $PROV06->PUCOD=$user;
        $PROV06->PLFCR=$fcr;
        $PROV06->PAECOD=$status;
        $PROV06DA->InsertarLog($PROV06);
    }

}