<?php

namespace App\Model\INV24;

use System\Core\Model;

class INV24DA extends Model
{
    private $table = "STLW10.INV24";
    private $typeDB = "odbc";
    private $rta = [];

    public function __contruct()
    {
        parent::__construct();
    }

    public function create($obj)
    {
        $sql = "INSERT INTO " . $this->table . "(LOCACION,USERNAME,SUCURSAL,COD_AS,CANTIDAD,FCREACION,INDICADOR) VALUES ";
        $sql_ = [];
        $in = NULL;
        foreach ($obj as $vv) {
			$as_cod = (int)trim($vv['COD_AS']);
            $as_cod = str_pad($as_cod, 6, "0", STR_PAD_LEFT);
			$in[$as_cod] = $as_cod;			
            $sucu = $vv['SUCURSAL'];
            if($sucu <=9) {
                $sucu = str_pad($sucu, 2, "0", STR_PAD_LEFT);
            }
            $sql_[] = "('" . $vv['LOCACION'] . "','" . $vv['USERNAME'] . "','" . $sucu . "','" . $as_cod . "'," . $vv['CANTIDAD'] . ",'" . $vv['FCREACION'] . "','" . $vv['INDICADOR'] . "')";
        }
        $va = $sql.implode(", ",$sql_);
        $instancia = parent::getIntancia($this->typeDB);
        $instancia->createConexionODBC();
        $conexion = $instancia->getConexionODBC();
        $this->rta = $instancia->executeQueryODBC($va, $conexion); //funcion para reemplazar
        $instancia->disposeODBC();
        
        return $this->rta;
    }

    public function create_($obj)
    {
        $sql = "INSERT INTO " . $this->table . "(LOCACION,USERNAME,SUCURSAL,COD_AS,CANTIDAD,FCREACION,INDICADOR) VALUES ";
        $sql_ = [];
        $in = NULL;
        foreach ($obj as $vv) {
			$as_cod = (int)trim($vv['COD_AS']);
            $as_cod = str_pad($as_cod, 6, "0", STR_PAD_LEFT);
			$in[$as_cod] = $as_cod;
            $sucu = $vv['SUCURSAL'];
            if($sucu <=9) {
                $sucu = str_pad($sucu, 2, "0", STR_PAD_LEFT);
            }
            $sql_[] = "('" . $vv['LOCACION'] . "','" . $vv['USERNAME'] . "','" . $sucu . "','" . $as_cod . "'," . $vv['CANTIDAD'] . ",'" . $vv['FCREACION'] . "','" . $vv['INDICADOR'] . "')";
        }
        $va = $sql.implode(", ",$sql_);
        $instancia = parent::getIntancia($this->typeDB);
        $instancia->createConexionODBC();
        $conexion = $instancia->getConexionODBC();
        $this->rta = $instancia->executeQueryODBC($va, $conexion); //funcion para reemplazar
        $instancia->disposeODBC();
        
        return $this->rta;
    }

    public function update_($obj = NULL) {
        $sql = "INSERT INTO " . $this->table . "(LOCACION,USERNAME,SUCURSAL,COD_AS,CANTIDAD,FCREACION,INDICADOR,USERMOD,FECMODI) VALUES ";
        $sql_ = [];
        
        foreach ($obj as $vv) {
			$as_cod = (int)trim($vv['COD_AS']);
            $as_cod = str_pad($as_cod, 6, "0", STR_PAD_LEFT);
            $sucu = $vv['SUCURSAL'];
            if($sucu <=9) {
                $sucu = str_pad($sucu, 2, "0", STR_PAD_LEFT);
            }
            $sql_[] = "('" . $vv['LOCACION'] . "','" . $vv['USERNAME'] . "','" . $sucu . "','" . $as_cod . "'," . $vv['CANTIDAD'] . ",'" . $vv['FCREACION'] . "','" . $vv['INDICADOR'] . "','" . $vv['USERMOD'] . "','" . $vv['FECMODI'] . "')";
        }
        $va = $sql.implode(", ",$sql_);
        $instancia = parent::getIntancia($this->typeDB);
        $instancia->createConexionODBC();
        $conexion = $instancia->getConexionODBC();
        $this->rta = $instancia->executeQueryODBC($va, $conexion); //funcion para reemplazar
        $instancia->disposeODBC();
        
        return $this->rta;
    }

    public function update($obj = NULL) {
        $sql = "INSERT INTO " . $this->table . "(LOCACION,USERNAME,SUCURSAL,COD_AS,CANTIDAD,FCREACION,INDICADOR,USERMOD,FECMODI) VALUES ";
        $sql_ = [];
        
        foreach ($obj as $vv) {
			$as_cod = (int)trim($vv['COD_AS']);
            $as_cod = str_pad($as_cod, 6, "0", STR_PAD_LEFT);
			
            $sucu = $vv['SUCURSAL'];
            if($sucu <=9) {
                $sucu = str_pad($sucu, 2, "0", STR_PAD_LEFT);
            }
            $sql_[] = "('" . $vv['LOCACION'] . "','" . $vv['USERNAME'] . "','" . $sucu . "','" . $as_cod . "'," . $vv['CANTIDAD'] . ",'" . $vv['FCREACION'] . "','" . $vv['INDICADOR'] . "','" . $vv['USERMOD'] . "','" . $vv['FECMODI'] . "')";
        }
        $va = $sql.implode(", ",$sql_);
        $instancia = parent::getIntancia($this->typeDB);
        $instancia->createConexionODBC();
        $conexion = $instancia->getConexionODBC();
        $this->rta = $instancia->executeQueryODBC($va, $conexion); //funcion para reemplazar
        $instancia->disposeODBC();
        
        return $this->rta;
    }

    public function delete($suc,$locacion){
        //ODBC 
        $instancia=parent::getIntancia($this->typeDB);
        $instancia->createConexionODBC();
        $conexion=$instancia->getConexionODBC();
		if($suc <=9) {
            $suc = str_pad($suc, 2, "0", STR_PAD_LEFT);
        }
        $sql="DELETE FROM ".$this->table." WHERE SUCURSAL = '".$suc."' AND LOCACION='".$locacion."'";
        
        $data=$instancia->executeQueryODBC($sql,$conexion); //funcion para reemplazar
        $instancia->disposeODBC();
        return $data;
    }
    
    public function showProductsLocations($params){
        //ODBC 
        $instancia=parent::getIntancia($this->typeDB);
        $instancia->createConexionODBC();
        $conexion=$instancia->getConexionODBC();
        $sql="SELECT COD_AS,TRANSLATE(ARDES,'aeiouAEIOUnN','áéíóúÁÉÍÓÚñÑ') AS DESCRIPCION,ARUMA AS UM_BASE,CANTIDAD,FCREACION FROM ".$this->table." inv left join STLDTA.OP01 art on art.ARCOD=CAST(COD_AS AS DECIMAL(10,0)) WHERE inv.SUCURSAL = ".$params['SUCURSAL']." AND inv.INDICADOR=1 AND inv.LOCACION='".$params['LOCACION']."'";
        
        $data=$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar
        $instancia->disposeODBC();
        return $data;
    }

    public function getProducts($params){
        //ODBC 
		//print_r("Tus datos");
		//print_r($params);
		$sss = "ean.B7EAN='".$params['CODIGO']."'";
		$can_len = strlen(trim($params['CODIGO']));
		if($can_len <7) {
			$sss = "ARCOD='".$params['CODIGO']."'";
		}
        $instancia=parent::getIntancia($this->typeDB);
        $instancia->createConexionODBC();
        $conexion=$instancia->getConexionODBC();
        $sql = "SELECT ARCOD AS COD_AS,TRANSLATE(ARDES,'aeiouAEIOUnN','áéíóúÁÉÍÓÚñÑ') AS DESCRIPCION,ARUMA AS UM_BASE,UMFAC AS FACTOR, B7EAN AS COD_EAN
        FROM STLDTA.OP01 art
        INNER JOIN STLDTA.OPB7 ean ON ean.B7CTL = art.ARCOD
        INNER JOIN STLDTA.OP03 ON B7UND = UMCOD
        WHERE ( ".$sss." ) limit 1";
        
		//print_r($sql);
        $data=$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar
        $instancia->disposeODBC();
		if(!empty($data[0]['DESCRIPCION'])) {
			$data[0]['DESCRIPCION'] = utf8_encode($data[0]['DESCRIPCION']);
		}
        return !empty($data[0]) ? $data : NULL;
    }

    public function ver_articulo($params){
        //ODBC 
		//print_r("Tus datos");
		//print_r($params);
		$sss = "ean.B7EAN='".$params['CODIGO']."'";
		$can_len = strlen(trim($params['CODIGO']));
		if($can_len <7) {
			$sss = "ARCOD='".$params['CODIGO']."'";
		}
        $instancia=parent::getIntancia($this->typeDB);
        $instancia->createConexionODBC();
        $conexion=$instancia->getConexionODBC();
        $sql = "SELECT ARCOD AS COD_AS,TRANSLATE(ARDES,'aeiouAEIOUnN','áéíóúÁÉÍÓÚñÑ') AS DESCRIPCION,ARUMA AS UM_BASE,UMFAC AS FACTOR, B7EAN AS COD_EAN
        FROM STLDTA.OP01 art
        INNER JOIN STLDTA.OPB7 ean ON ean.B7CTL = art.ARCOD
        INNER JOIN STLDTA.OP03 ON B7UND = UMCOD
        WHERE ( ".$sss." ) limit 1";
		
        $data=$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar
		if(!empty($data[0]['DESCRIPCION'])) {
			$data[0]['DESCRIPCION'] = utf8_encode($data[0]['DESCRIPCION']);
		}			
        $instancia->disposeODBC();
        return !empty($data[0]) ? $data : NULL;
    }
	
	public function pre_inv_act($suc,$locacion) {
        $instancia=parent::getIntancia($this->typeDB);
		$instancia->createConexionODBC();
		$conexion=$instancia->getConexionODBC();
		if($suc <=9) {
            $suc = str_pad($suc, 2, "0", STR_PAD_LEFT);
        }
		$sql = "SELECT COD_AS, CANTIDAD,USERNAME,FCREACION,USERMOD,FECMODI FROM ".$this->table." WHERE LOCACION='".$locacion."' AND SUCURSAL='".$suc."'";
		$data=$instancia->executeQueryShowODBC($sql,$conexion);
		if(!empty($data[0]['COD_AS'])) {
			foreach ($data as $vv) {
				$cod_as_ = (int)trim($vv['COD_AS']);
				$this->rta[$cod_as_]['USERNAME'] = trim($vv['USERNAME']);
				$this->rta[$cod_as_]['FCREACION'] = trim($vv['FCREACION']);
				$this->rta[$cod_as_]['CANTIDAD'] = trim($vv['CANTIDAD']);
				$this->rta[$cod_as_]['USERMOD'] = trim($vv['USERMOD']);
				$this->rta[$cod_as_]['FECMODI'] = trim($vv['FECMODI']);
			}
		}

        return !empty($this->rta) ? $this->rta : NULL;
    }
	
	public function pre_inv_act_($suc,$locacion) {
        $instancia=parent::getIntancia($this->typeDB);
		$instancia->createConexionODBC();
		$conexion=$instancia->getConexionODBC();
		$sql = "SELECT COD_AS, CANTIDAD,USERNAME,FCREACION,USERMOD,FECMODI FROM ".$this->table." WHERE LOCACION='".$locacion."' AND SUCURSAL='".$suc."'";
		$data=$instancia->executeQueryShowODBC($sql,$conexion);
		if(!empty($data[0]['COD_AS'])) {
			foreach ($data as $vv) {
				$cod_as_ = (int)trim($vv['COD_AS']);
				$this->rta[$cod_as_]['USERNAME'] = trim($vv['USERNAME']);
				$this->rta[$cod_as_]['FCREACION'] = trim($vv['FCREACION']);
				$this->rta[$cod_as_]['CANTIDAD'] = trim($vv['CANTIDAD']);
				$this->rta[$cod_as_]['USERMOD'] = trim($vv['USERMOD']);
				$this->rta[$cod_as_]['FECMODI'] = trim($vv['FECMODI']);
			}
		}
        return !empty($this->rta) ? $this->rta : NULL;
    }
}
