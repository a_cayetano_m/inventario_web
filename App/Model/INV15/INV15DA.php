<?php 
    namespace App\Model\INV15;

    use App\Model\User\User;
    use App\Model\Time\Time;
    use System\Core\Model;

    class INV15DA extends Model{

        private $table="STLDTA.INV15";
        private $table_2="STLDTA.INV14";
        private $table_3="STLDTA.INV10";
        private $typeDB="odbc";
        private $instancia="";
        private $user_array=[];
        
        public function __contruct(){
            parent::__construct();
        }
        public function Create($data){
           
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();
            $sql="INSERT INTO ".$this->table."(
                MKRLCOD,MKUCOD,MKSECOD) VALUES ";
            
            foreach($data['MKSECOD'] as $key=>$value){
                if($key==(count($data['MKSECOD'])-1)){
                    $sql=$sql."(
                        '" . $data['MKRLCOD']. "', 
                        '" . $data['MKUCOD']. "', 
                        '" . $value. "')";
                }else{
                    $sql=$sql."(
                        '" . $data['MKRLCOD']. "', 
                        '" . $data['MKUCOD']. "', 
                        '" . $value. "'),";
                }
            }
            $array =$instancia->executeQueryODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $array;
        }

        public function CreateMassive($array_obj){
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();

            $sql="INSERT INTO ".$this->table."(
                MKRLCOD,MKUCOD) VALUES ";
            foreach($array_obj as $key=>$obj){
               if($key==0){
                    $sql=$sql."(
                        '" . $obj->obtener('MKRLCOD') . "', 
                        '" . $obj->obtener('MKUCOD'). "')";
                }else{
                    $sql=$sql.",";
                    $sql=$sql."(
                        '" . $obj->obtener('MKRLCOD') . "', 
                        '" . $obj->obtener('MKUCOD'). "')";
               }
            }
            
            $array =$instancia->executeQueryODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $array;
        }
        public function getRole($id){ //traer roles de usuario
           
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();
            $sql="SELECT c.MKRLCOD AS IDROLE, c.MKRLNOM AS NOMBREROLE, p.MKMERU AS RUTAMENU FROM ".$this->table." m  INNER JOIN ".$this->table_2." c ON m.MKRLCOD=c.MKRLCOD INNER JOIN ".$this->table_3." p ON c.MKMECOD=p.MKMECOD WHERE MKUCOD='".$id."'";
            $array=$instancia->executeQueryShowODBC($sql,$conexion);
            $instancia->disposeODBC();
            $respuesta=array();
            if(count($array)>0){
                $respuesta=$array;
            } 
            return $respuesta;
       }
        public function Delete($data){
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();
            $sql="DELETE FROM ".$this->table." WHERE MKRLCOD=".$data['MKRLCOD']."";

            $array =$instancia->executeQueryODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $array;
        }
        public function Validate($idRole){
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();
            $sql="SELECT * FROM ".$this->table." WHERE MKRLCOD=".$idRole."";
            $array =$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $array;
        }
        public function DeleteByUser($coduser){
            //ODBC 
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();
            $sql="DELETE FROM ".$this->table." WHERE MKUCOD='".$coduser."'";
            $array=$instancia->executeQueryODBC($sql,$conexion);
            $instancia->disposeODBC();
            return $array;
        }
    }

?>