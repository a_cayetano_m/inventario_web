<?php
    namespace App\Model\INV15;
    class INV15{

        private $MKRLCOD;   // Código del rol       //NUMERIC(3,0)
        private $MKUCOD;    // Código del usuario   //NUMERIC(3,0)
        private $MKSECOD;   // Código de la acción       //NUMERIC(3,0)
        public function __contruct(){

        }
        public function __set($valor,$dato){
            $dato = utf8_decode($dato);
            $chars = array("'", '"',  "'", "", "\'", '\"', '*');
            $dato = str_replace($chars, "",$dato);
            $dato = strip_tags($dato); 
            $dato = stripslashes($dato); 
            
            switch($valor){
                case'MKRLCOD':$this->MKRLCOD=$dato;break;
                case'MKUCOD':$this->MKUCOD=$dato;break;
                case'MKSECOD':$this->MKSECOD=$dato;break;
            }
        }
            
          public function obtener($valor){
            switch($valor){
                case'MKRLCOD': return $this->MKRLCOD; break;
                case'MKUCOD': return $this->MKUCOD; break;
                case'MKSECOD': return $this->MKSECOD; break;
            }
        }
        
        


    }




?>