<?php 
    namespace App\Model\INV05;

    use App\Model\User\User;
    use App\Model\Time\Time;
    use App\Helper\Helper as HelperMethod;
    use System\Core\Model;

    class INV05DA extends Model{

        private $table="STLDTA.INV05";
        private $typeDB="odbc";
        private $instancia="";
        private $user_array=[];
        
        public function __contruct(){
            parent::__construct();
        }
        public function getConteos($sucursal){
            //ODBC 
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();
            $sql="SELECT CONTEO,TURNO FROM ". $this->table." WHERE TURNO='1' AND SUCURSAL='".$sucursal."'";
            $data =$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $data;
        }

        public function resetConteo($suc){
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();

            $sql="UPDATE ".$this->table." SET TURNO = 0";
            $data=$instancia->executeQueryODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $data;
        }

        public function updateConteo2($suc,$min,$max){
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();

            $sql="UPDATE ".$this->table." SET TURNO = 1, MINVALO = ".$min.", MAXVALO = ".$max.", CONTADORES = 0 WHERE CONTEO = 2 AND SUCURSAL = '".$suc."'";

            $data=$instancia->executeQueryODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $data;
        }

        public function createConteo1($suc,$inv){
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();

            $sql="INSERT INTO STLDTA.INV05 VALUES('1','1','".$suc."','".$inv."',0,0,0)";

            $data=$instancia->executeQueryODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $data;
        }
        public function createConteo1_2($suc,$inv){
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();

            $sql="INSERT INTO STLDTA.INV05 VALUES('2','0','".$suc."','".$inv."',0,0,0)";

            $data=$instancia->executeQueryODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $data;
        }
        public function createConteo1_3($suc,$inv){
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();

            $sql="INSERT INTO STLDTA.INV05 VALUES('3','0','".$suc."','".$inv."',0,0,0)";

            $data=$instancia->executeQueryODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $data;
        }

        public function getContadores($sucursal){
            //ODBC 
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();
            $sql="SELECT CONTADORES FROM ". $this->table." WHERE CONTEO='2' AND SUCURSAL='".$sucursal."'";
            $data =$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $data;
        }

        public function updateContadores($suc,$con){
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();

            $sql="UPDATE ".$this->table." SET CONTADORES = ".$con." WHERE CONTEO = 2 AND SUCURSAL = '".$suc."'";

            $data=$instancia->executeQueryODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $data;
        }
        
    }

?>