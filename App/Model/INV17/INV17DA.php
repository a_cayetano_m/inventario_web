<?php 
    namespace App\Model\INV17;

    use App\Model\User\User;
    use App\Model\Time\Time;
    use System\Core\Model;

    class INV17DA extends Model{

        private $table="STLDTA.INV17";
        private $table2="STLDTA.INV06";
        private $table3="STLDTA.OP08";
        private $typeDB="odbc";
        private $instancia="";
        private $user_array=[];
        
        public function __contruct(){
            parent::__construct();
        }
        public function getUsuarios($bib,$suc,$alm){
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();

            $sql="SELECT USERNAME,ZONA,STATUS FROM ".$table." WHERE SUCURSAL =".$suc;
            //print_r($sql);
            //die();
            $data =$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $data;
        }

        public function crea($suc,$user,$inv,$zon){
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();

            $sql="INSERT INTO STLDTA.INV17 VALUES ('".$user."','".$suc."','01','99','".$zon."','0','".$inv."')";
            //print_r($sql);
            //die();
            $data =$instancia->executeQueryODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $data;
        }

        public function elimina($suc,$user,$inv,$zon){
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();

            $sql="DELETE STLDTA.INV17 WHERE USERNAME = '".$user."' AND SUCURSAL = '".$suc."' AND ZONA = '".$zon."' AND INVENTARIO = '".$inv."'";
            //print_r($sql);
            //die();
            $data =$instancia->executeQueryODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $data;
        }

        public function getZonas($suc,$inv,$user){
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();
			
			$sql = "SELECT substr(LOCAcION,1,1) as loca FROM STLW10.INV24 WHERE SUCURSAL='".$suc."' GROUP by substr(LOCAcION,1,1)";
            $ubi =$instancia->executeQueryShowODBC($sql,$conexion);
            $sq_ = "";
            if(!empty($ubi[0]['loca'])) {
                $sq_ = " AND ZONA <> '".$ubi[0]['loca']."'";
            }

            $sql="SELECT DISTINCT ZONA FROM STLDTA.INV17 WHERE SUCURSAL = '".$suc."' AND INVENTARIO = '".$inv."' AND USERNAME = '".$user."'".$sq_;
            
            $data =$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $data;
        }
    }

?>