<?php 
    namespace App\Model\INV02;

    use App\Model\User\User;
    use App\Model\Time\Time;
    use App\Helper\Helper as HelperMethod;
    use System\Core\Model;

    class INV02DA extends Model{

        private $table="STLDTA.INV02";
        private $typeDB="odbc";
        private $instancia="";
        private $user_array=[];
        
        public function __contruct(){
            parent::__construct();
        }
       
        public function create($obj){
           
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();
            $sql="INSERT INTO ".$this->table."(LOCACION,ZONA,MUEBLE,NIVEL,SEPARACION,STATUS,SUCURSAL,CODTIENDA,USERNAME,FCREACION) VALUES ";
            foreach($obj as $key=>$value){
                if($key==0){
                    $sql=$sql."";
                }else{
                    $sql=$sql.",";
                }
                $sql=$sql."('" . $value->get('LOCACION') . "',
                '" . $value->get('ZONA') . "',
                '" . $value->get('MUEBLE') . "',
                '" . $value->get('NIVEL') . "',
                '" . $value->get('SEPARACION') . "',
                '" . $value->get('STATUS') . "',
                '" . $value->get('SUCURSAL') . "',
                '" . $value->get('CODTIENDA') . "',
                '" . $value->get('USERNAME') . "',
                '" . $value->get('FCREACION'). "')";
            }
           
            $response =$instancia->executeQueryODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $response;
        }
        public function getLocations(INV02 $obj){
            //ODBC 
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();
            $sql="SELECT LOCACION FROM ". $this->table." WHERE SUCURSAL='".$obj->get('SUCURSAL')."'";
            if($obj->get('LOCACION')!=''){
                $sql=$sql."AND LOCACION LIKE '".$obj->get('LOCACION')."%'";
            }
            $data =$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $data;
        }
        public function validateLocation(INV02 $obj){
            //ODBC 
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();
            $sql="SELECT DISTINCT(LOCACION) FROM ". $this->table." WHERE SUCURSAL='".$obj->get('SUCURSAL')."' AND LOCACION= '".$obj->get('LOCACION')."'";
            $data =$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $data;
        }
        public function deleteLocations($params){
            //ODBC 
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();
            $sql="DELETE FROM ". $this->table." WHERE SUCURSAL='".$params['SUCURSAL']."' AND 
            LOCACION IN ('".$params['LOCATIONS']."')";
           
            $data =$instancia->executeQueryODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $data;
        }
        public function deleteLocation($params){
            //ODBC 
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();
            $sql="DELETE FROM ".$this->table." WHERE LOCACION='".$params['LOCACION']."' AND SUCURSAL='".$params['SUCURSAL']."'";
            
            $response=$instancia->executeQueryODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $response;
        }

        public function deleteTotal($sucursal){
            //ODBC 
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();
            $sql="DELETE FROM ".$this->table." WHERE SUCURSAL='".$sucursal."'";
            $response=$instancia->executeQueryODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $response;
        }

        public function validaExistente($locacion,$sucursal){
            //ODBC 
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();
            $sql="SELECT DISTINCT(LOCACION) FROM ". $this->table." WHERE SUCURSAL='".$sucursal."' AND LOCACION= '".$locacion."'";
            $data =$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $data;
        }
        
        
    }

?>