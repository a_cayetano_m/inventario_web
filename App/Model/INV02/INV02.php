<?php
    namespace App\Model\INV02;
    class INV02{ 
        private $LOCACION; // VARCHAR (50),
        private $ZONA; // VARCHAR (5),
        private $MUEBLE; // VARCHAR (5),
        private $NIVEL; // VARCHAR (5),
        private $SEPARACION; // VARCHAR (5),
        private $STATUS; // VARCHAR (2),
        private $SUCURSAL; // VARCHAR (5),
        private $CODTIENDA; // VARCHAR (15),
        private $USERNAME; // VARCHAR (20),
        private $FCREACION; // VARCHAR (50)
        public function __contruct(){
           
        }
        public function __set($valor,$dato){
            $dato = utf8_decode($dato);
            $chars = array("'", '"',  "'", "", "\'", '\"', '*');
            $dato = str_replace($chars, "",$dato);
            $dato = strip_tags($dato); 
            $dato = stripslashes($dato); 
            
            switch($valor){
                case'LOCACION':$this->LOCACION=$dato;break;
                case'ZONA':$this->ZONA=$dato;break;
                case'MUEBLE':$this->MUEBLE=$dato;break;
                case'NIVEL':$this->NIVEL=$dato;break;
                case'SEPARACION':$this->SEPARACION=$dato;break;
                case'STATUS':$this->STATUS=$dato;break;
                case'SUCURSAL':$this->SUCURSAL=$dato;break;
                case'CODTIENDA':$this->CODTIENDA=$dato;break;
                case'USERNAME':$this->USERNAME=$dato;break;
                case'FCREACION':$this->FCREACION=$dato;break;
            }
        }
            
          public function get($valor){
            switch($valor){
                case'LOCACION': return $this->LOCACION; break;
                case'ZONA': return $this->ZONA; break;
                case'MUEBLE': return $this->MUEBLE; break;
                case'NIVEL': return $this->NIVEL; break;
                case'SEPARACION': return $this->SEPARACION; break;
                case'STATUS': return $this->STATUS; break;
                case'SUCURSAL': return $this->SUCURSAL; break;
                case'CODTIENDA': return $this->CODTIENDA; break;
                case'USERNAME': return $this->USERNAME; break;
                case'FCREACION': return $this->FCREACION; break;
            }
        }
        
        


    }




?>