<?php

namespace App\Model\LOCACION;

use App\Model\User\User;
use App\Model\Time\Time;
use App\Helper\Helper as HelperMethod;
use PhpOffice\PhpSpreadsheet\Worksheet\Table;
use System\Core\Model;

class LOCACIONDA extends Model
{

    private $table = "";
    private $typeDB = "odbc";
    private $instancia = "";
    private $rta = [];

    public function __contruct()
    {
        parent::__construct();
    }

    public function deleteLocation($params, $table)
    {
        //ODBC 
        $instancia = parent::getIntancia($this->typeDB);
        $instancia->createConexionODBC();
        $conexion = $instancia->getConexionODBC();
        $table = trim($table);
        $sql = "DELETE FROM " . $table . ".TMERC WHERE CDZON='" . $params['ZONA'] . "' AND CDMUE='" . $params['MUEBLE'] . "' AND CDNIV='" . $params['NIVEL'] . "'";
        $response = $instancia->executeQueryODBC($sql, $conexion); //funcion para reemplazar
        $instancia->disposeODBC();
        return $response;
    }

    public function deleteLocationUnidad($params, $table)
    {
        //ODBC 
        $instancia = parent::getIntancia($this->typeDB);
        $instancia->createConexionODBC();
        $conexion = $instancia->getConexionODBC();
        $sql = "DELETE FROM " . $table . ".TMERC WHERE 
            CDZON='" . $params['ZONA'] . "' AND CDMUE='" . $params['MUEBLE'] . "' AND CDNIV='" . $params['NIVEL'] . "' AND CDALM=" . $params['ALM'] . " AND TO_NUMBER(CDART)='" . (int)$params['AS'] . "'";
        $response = $instancia->executeQueryODBC($sql, $conexion); //funcion para reemplazar
        $instancia->disposeODBC();
        return $response;
    }

    public function create($obj, $table)
    {
        $instancia = parent::getIntancia($this->typeDB);
        $instancia->createConexionODBC();
        $conexion = $instancia->getConexionODBC();
        $biblio = trim($table);
        $sql = "INSERT INTO " . $table . ".TMERC (CDALM,CDZON,CDMUE,CDNIV,CDART,CDLIN,CDSUB) VALUES ";
        foreach ($obj as $key => $value) {
            if ($key == 0) {
                $sql = $sql . "";
            } else {
                $sql = $sql . ",";
            }
            $sql = $sql . "('" . $value->get('CDALM') . "',
                '" . $value->get('CDZON') . "',
                '" . $value->get('CDMUE') . "',
                '" . $value->get('CDNIV') . "',
                '" . $value->get('CDART') . "',
                '" . $value->get('CDLIN') . "',
                '" . $value->get('CDSUB') . "')";
        }

        $response = $instancia->executeQueryODBC($sql, $conexion); //funcion para reemplazar
        $instancia->disposeODBC();
        return $response;
    }

    public function obtieneBiblioteca($suc)
    {
        //ODBC 
        $instancia = parent::getIntancia($this->typeDB);
        $instancia->createConexionODBC();
        $conexion = $instancia->getConexionODBC();

        $sql = "SELECT STLDTA.DD01.DDLIB AS BIBLIOTECA FROM STLDTA.DD01 WHERE STLDTA.DD01.DDTIE = " . $suc;

        //print($sql);
        //die();

        $data = $instancia->executeQueryShowODBC($sql, $conexion); //funcion para reemplazar
        $instancia->disposeODBC();
        return $data;
    }

    public function add_location($params, $table)
    {
        $instancia = parent::getIntancia($this->typeDB);
        $instancia->createConexionODBC();
        $conexion = $instancia->getConexionODBC();
        if ($params['SUCU'] <= 9) {
            $params['SUCU'] = str_pad($params['SUCU'], 2, "0", STR_PAD_LEFT);
        }
        $sql = "INSERT INTO " . $table . ".TMERC (CDALM, CDZON, CDMUE, CDNIV, CDART, CDLIN, CDSUB) SELECT '" . $params['ALM'] . "','" . $params['ZONA'] . "','" . $params['MUEBLE'] . "','" . $params['NIVEL'] . "',LPAD(LPAD(art.ARCOD,6,'0'),15,' ' ),ARLIN,ARSLN FROM STLDTA.OP01 art WHERE art.ARCOD IN (SELECT COD_AS FROM STLW10.INV24 WHERE SUCURSAL='" . $params['SUCU'] . "' AND LOCACION='" . $params['LOCA'] . "')";
        $response = $instancia->executeQueryODBC($sql, $conexion); //funcion para reemplazar
        $instancia->disposeODBC();
        return $response;
    }

    public function add_conteo_locacion($params, $bib)
    {
        $location = $params['loca'];
        $cdzon = substr($location, 0, 1);
        $cdmue = substr($location, 1, 2);
        $cdniv = substr($location, 4, 3);
        //$LOCACION->CDART = str_pad(str_pad($v['CODIGO_AS'], 6, "0", STR_PAD_LEFT), 15, " ", STR_PAD_LEFT);
        //SELECT RICOD FROM TIENDA88.OP22 WHERE RINUM='' AND RICLO='' NOT IN (SELECT CODART FROM TIENDA88.TMERC WHERE CDZON='' AND CDMUE='' AND CDNIV='')

    }

    public function insert_location($params)
    {
        $alm = str_pad($params['sucursal'], 3, "0", STR_PAD_LEFT);
        $zona = substr($params['location'], 0, 1);
        $mueble = substr($params['location'], 1, 2);
        $nivel = substr($params['location'], 4, 3);
        $bilio = $params['biblioteca'];
        $cod_art = $params['cod_art'];

        $instancia = parent::getIntancia($this->typeDB);
        $instancia->createConexionODBC();
        $conexion = $instancia->getConexionODBC();

        $sql = "INSERT INTO " . $bilio . ".TMERC (CDALM, CDZON, CDMUE, CDNIV, CDART, CDLIN, CDSUB)
                SELECT '" . $alm . "','" . $zona . "','" . $mueble . "','" . $nivel . "',LPAD(art.ARCOD,15,' ' ),ARLIN,ARSLN FROM STLDTA.OP01 art WHERE art.ARCOD = " . $cod_art;
        $response = $instancia->executeQueryODBC($sql, $conexion); //funcion para reemplazar
        $instancia->disposeODBC();
        return $response;
    }

    public function validar_locacion($params)
    {
        $alm = str_pad($params['sucursal'], 3, "0", STR_PAD_LEFT);
        $zona = substr($params['location'], 0, 1);
        $mueble = substr($params['location'], 1, 2);
        $nivel = substr($params['location'], 4, 3);
        $bilio = $params['biblioteca'];
        $cod_art = $params['cod_art'];

        $instancia = parent::getIntancia($this->typeDB);
        $instancia->createConexionODBC();
        $conexion = $instancia->getConexionODBC();

        $sql = "select CDART from  " . $bilio . ".TMERC where CDALM ='" . $alm . "' and CDZON='" . $zona . "' and CDMUE ='" . $mueble . "' and  CDNIV ='" . $nivel . "' and CDART ='" . $cod_art . "' FETCH FIRST 1 ROWS ONLY";

        $response = $instancia->executeQueryShowODBC($sql, $conexion); //funcion para reemplazar
        $instancia->disposeODBC();
        return !empty($response[0]['CDART']) ? true : false;
    }

    public function delete_locaciones($biblio, $params) {
        $this->rta = false;
        if(!empty($biblio)  && !empty($params)) {
            $instancia = parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion = $instancia->getConexionODBC();
            $sql = "DELETE FROM ".$biblio.".TMERC where CDZON='".$params['ZONA']."' AND CDMUE='".$params['MUEBLE']."' AND CDNIV='".$params['NIVEL']."' AND TO_NUMBER(CDART) in (".implode(',',$params['c_as']).")";
            $this->rta = $instancia->executeQueryODBC($sql, $conexion); //funcion para reemplazar
            $instancia->disposeODBC();
        }
        return $this->rta;
    }

    public function add_loca_ciones($biblio, $params = null) {
        $this->rta = false;
        if(!empty($biblio)  && !empty($params)) {
            $instancia = parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion = $instancia->getConexionODBC();
            $sql = "INSERT INTO ".$biblio.".TMERC (CDALM, CDZON, CDMUE, CDNIV, CDART, CDLIN, CDSUB) SELECT '" . $params['ALM'] . "','" . $params['ZONA'] . "','" . $params['MUEBLE'] . "','" . $params['NIVEL'] . "',LPAD(LPAD(art.ARCOD,6,'0'),15,' ' ),ARLIN,ARSLN FROM STLDTA.OP01 art WHERE TO_NUMBER(ARCOD) IN (".implode(",",$params['c_as']).")";
            $this->rta = $instancia->executeQueryODBC($sql, $conexion); //funcion para reemplazar
            $instancia->disposeODBC();
        }
        return (!empty($this->rta)) ? true : false;
    }
}
