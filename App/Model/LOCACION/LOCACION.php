<?php
    namespace App\Model\LOCACION;
    class LOCACION{ 
        private $CDALM; // CHAR(3)
        private $CDZON; // CHAR(8)
        private $CDMUE; // CHAR(8)
        private $CDNIV; // CHAR(8)
        private $CDART; // CHAR(15)
        private $CDLIN; // NUMERIC(4,0)
        private $CDSUB; // NUMERIC(4,0)

        public function __contruct(){
           
        }
        public function __set($valor,$dato){
            $dato = utf8_decode($dato);
            $chars = array("'", '"',  "'", "", "\'", '\"', '*');
            $dato = str_replace($chars, "",$dato);
            $dato = strip_tags($dato); 
            $dato = stripslashes($dato); 
            
            switch($valor){
                case'CDALM':$this->CDALM=$dato;break;
                case'CDZON':$this->CDZON=$dato;break;
                case'CDMUE':$this->CDMUE=$dato;break;
                case'CDNIV':$this->CDNIV=$dato;break;
                case'CDART':$this->CDART=$dato;break;
                case'CDLIN':$this->CDLIN=$dato;break;
                case'CDSUB':$this->CDSUB=$dato;break;
            }
        }
            
          public function get($valor){
            switch($valor){
                case'CDALM': return $this->CDALM; break;
                case'CDZON': return $this->CDZON; break;
                case'CDMUE': return $this->CDMUE; break;
                case'CDNIV': return $this->CDNIV; break;
                case'CDART': return $this->CDART; break;
                case'CDLIN': return $this->CDLIN; break;
                case'CDSUB': return $this->CDSUB; break;
            }
        }
        
        


    }




?>