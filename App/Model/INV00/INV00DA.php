<?php

namespace App\Model\INV00;

use App\Model\User\User;
use App\Model\Time\Time;
use App\Helper\Helper as HelperMethod;
use System\Core\Model;

class INV00DA extends Model
{

    private $table = "STLDTA.INV00";
    private $typeDB = "odbc";
    private $instancia = "";
    private $user_array = [];

    public function __contruct()
    {
        parent::__construct();
    }
    public function getUserComplete($obj)
    {
        //ODBC
        $instancia = parent::getIntancia($this->typeDB);
        $instancia->createConexionODBC();
        $conexion = $instancia->getConexionODBC();

        $sql = "SELECT 
            STLDTA.INV00.ID AS COD,
            STLDTA.INV00.NAME AS NAME, 
            STLDTA.INV00.LASTNAME AS LASTNAME,
            STLDTA.INV00.SUCURSAL AS SUCURSAL, 
            STLDTA.INV00.EMAIL AS EMAIL,
            STLDTA.INV00.USERNAME AS USERNAME,
            (SELECT STLDTA.DD01.DDLIB FROM STLDTA.DD01 WHERE STLDTA.DD01.DDTIE=STLDTA.INV00.SUCURSAL limit 1) AS CODTIENDA,
            STLDTA.INV00.STATUS AS EST,
            STLDTA.INV00.TYPE,
            STLDTA.INV12.MKSMCOD AS TIPOSUBMENU, 
            STLDTA.INV12.MKSMNOM AS SUBMENU, 
            STLDTA.INV12.MKSMRUT AS RUTASUBMENU,
            STLDTA.INV12.MKSMICON AS ICONSUBMENU,
            STLDTA.INV10.MKMECOD AS TIPOMENU, 
            STLDTA.INV10.MKMENOM AS MENU,
            STLDTA.INV10.MKMEICON AS ICONMENU,
            STLDTA.INV10.MKMERU AS RUTAMENU,
            STLDTA.INV09.MKTUNOM AS TIPOUSUARIONOMB,
            STLDTA.INV09.MKTUAB AS TIPOUSUARIOABRV
            FROM " . $this->table . " INNER JOIN STLDTA.INV13
            ON STLDTA.INV00.ID = STLDTA.INV13.MKUCOD INNER JOIN STLDTA.INV12
            ON STLDTA.INV13.MKSMCOD  = STLDTA.INV12.MKSMCOD INNER JOIN STLDTA.INV10
            ON STLDTA.INV12.MKMECOD  = STLDTA.INV10.MKMECOD INNER JOIN STLDTA.INV11
            ON STLDTA.INV00.STATUS  = STLDTA.INV11.MKESCOD INNER JOIN STLDTA.INV09
            ON STLDTA.INV00.TYPE  = STLDTA.INV09.MKTUCOD  WHERE STLDTA.INV00.USERNAME='" . $obj->get('USERNAME') . "' AND PASSWORD='" . $obj->get('PASSWORD') . "' ORDER BY STLDTA.INV10.MKMEORD,STLDTA.INV12.MKSMORD";

        $data = $instancia->executeQueryShowODBC($sql, $conexion); //funcion para reemplazar

        $instancia->disposeODBC();

        return $data;
    }
    public function getUser($obj)
    {
        //ODBC 
        $instancia = parent::getIntancia($this->typeDB);
        $instancia->createConexionODBC();
        $conexion = $instancia->getConexionODBC();

        $sql = "SELECT 
            STLDTA.INV00.ID AS COD,
            STLDTA.INV00.NAME AS NAME, 
            STLDTA.INV00.LASTNAME AS LASTNAME,
            STLDTA.INV00.SUCURSAL AS SUCURSAL, 
            STLDTA.INV00.EMAIL AS EMAIL,
            STLDTA.INV00.USERNAME AS USERNAME,
            STLDTA.INV00.SUCURSAL AS SUCURSAL,
            STLDTA.INV00.STATUS AS EST,
            STLDTA.INV00.TYPE AS TYPE,
            STLDTA.INV00.SESION AS SESION
            FROM " . $this->table . "  WHERE STLDTA.INV00.USERNAME='" . $obj->get('USERNAME') . "' AND STLDTA.INV00.PASSWORD='" . $obj->get('PASSWORD') . "'";

        $data = $instancia->executeQueryShowODBC($sql, $conexion); //funcion para reemplazar
        $instancia->disposeODBC();

        return $data;
    }

    public function getUserActive($suc)
    {
        //ODBC 
        $instancia = parent::getIntancia($this->typeDB);
        $instancia->createConexionODBC();
        $conexion = $instancia->getConexionODBC();

        $sql = "SELECT USERNAME,SESION
            FROM " . $this->table . "  WHERE SUCURSAL='" . $suc . "'";

        $data = $instancia->executeQueryShowODBC($sql, $conexion); //funcion para reemplazar
        $instancia->disposeODBC();

        return $data;
    }

    public function updateSesion($obj)
    {
        //ODBC 
        $instancia = parent::getIntancia($this->typeDB);
        $instancia->createConexionODBC();
        $conexion = $instancia->getConexionODBC();

        $sql = "UPDATE " . $this->table . " SET SESION = '1'  WHERE STLDTA.INV00.USERNAME='" . $obj->get('USERNAME') . "' AND STLDTA.INV00.PASSWORD='" . $obj->get('PASSWORD') . "' AND TYPE = '2'";

        $data = $instancia->executeQueryShowODBC($sql, $conexion); //funcion para reemplazar
        $instancia->disposeODBC();

        return $data;
    }

    public function getUserSesion($obj)
    {
        //ODBC 
        $instancia = parent::getIntancia($this->typeDB);
        $instancia->createConexionODBC();
        $conexion = $instancia->getConexionODBC();

        $sql = "SELECT 
            STLDTA.INV00.ID AS COD,
            STLDTA.INV00.NAME AS NAME, 
            STLDTA.INV00.LASTNAME AS LASTNAME,
            STLDTA.INV00.SUCURSAL AS SUCURSAL, 
            STLDTA.INV00.EMAIL AS EMAIL,
            STLDTA.INV00.USERNAME AS USERNAME,
            STLDTA.INV00.SUCURSAL AS SUCURSAL,
            STLDTA.INV00.STATUS AS EST,
            STLDTA.INV00.TYPE AS TYPE
            FROM " . $this->table . "  WHERE STLDTA.INV00.USERNAME='" . $obj->get('USERNAME') . "' AND STLDTA.INV00.PASSWORD='" . $obj->get('PASSWORD') . "' AND SESION = '0'";

        $data = $instancia->executeQueryShowODBC($sql, $conexion); //funcion para reemplazar
        $instancia->disposeODBC();

        return $data;
    }

    public function closeSession($obj)
    {
        //ODBC 
        $instancia = parent::getIntancia($this->typeDB);
        $instancia->createConexionODBC();
        $conexion = $instancia->getConexionODBC();

        $sql = "UPDATE " . $this->table . " SET SESION = '0'  WHERE STLDTA.INV00.USERNAME='" . $obj->get('USERNAME') . "'";
        $data = $instancia->executeQueryShowODBC($sql, $conexion); //funcion para reemplazar
        $instancia->disposeODBC();

        return $data;
    }

    public function deleteUsers($suc)
    {
        //ODBC 
        $instancia = parent::getIntancia($this->typeDB);
        $instancia->createConexionODBC();
        $conexion = $instancia->getConexionODBC();

        $sql = "DELETE " . $this->table . " WHERE SUCURSAL = '" . $suc . "' AND TYPE = '2'";

        //print_r($sql);
        //die();
        $data = $instancia->executeQueryODBC($sql, $conexion); //funcion para reemplazar
        $instancia->disposeODBC();

        return $data;
    }

    public function createUsers($suc, $user, $id)
    {
        //ODBC 
        $instancia = parent::getIntancia($this->typeDB);
        $instancia->createConexionODBC();
        $conexion = $instancia->getConexionODBC();

        $sql = "INSERT INTO " . $this->table . " VALUES (" . $id . ",'" . $user . "','" . $user . "','','','','2','" . $suc . "','','','" . date("Y-m-d H:i:s") . "','1','0')";

        $data = $instancia->executeQueryODBC($sql, $conexion); //funcion para reemplazar
        $instancia->disposeODBC();

        return $data;
    }

    public function userInv($suc)
    {
        //ODBC 
        $instancia = parent::getIntancia($this->typeDB);
        $instancia->createConexionODBC();
        $conexion = $instancia->getConexionODBC();

        $sql = "SELECT * FROM " . $this->table . " WHERE SUCURSAL = '" . $suc . "' AND TYPE = '2'";

        $data = $instancia->executeQueryShowODBC($sql, $conexion); //funcion para reemplazar
        $instancia->disposeODBC();

        return $data;
    }

    public function refreshSession($user)
    {
        //ODBC 
        $instancia = parent::getIntancia($this->typeDB);
        $instancia->createConexionODBC();
        $conexion = $instancia->getConexionODBC();

        $sql = "UPDATE " . $this->table . " SET SESION = '0'  WHERE STLDTA.INV00.USERNAME='" . $user . "'";
        $data = $instancia->executeQueryShowODBC($sql, $conexion); //funcion para reemplazar
        $instancia->disposeODBC();

        return $data;
    }

    public function updateName($suc, $user, $name, $cargo)
    {
        //ODBC 
        $instancia = parent::getIntancia($this->typeDB);
        $instancia->createConexionODBC();
        $conexion = $instancia->getConexionODBC();

        $sql = "UPDATE " . $this->table . " SET NAME = '" . $name . "', LASTNAME = '" . $cargo . "' WHERE SUCURSAL = '" . $suc . "' AND USERNAME = '" . $user . "'";

        //print_r($sql);
        //die();
        $data = $instancia->executeQueryODBC($sql, $conexion); //funcion para reemplazar
        $instancia->disposeODBC();

        return $data;
    }
}
