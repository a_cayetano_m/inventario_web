<?php
    namespace App\Model\INV00;
    class INV00{ //INFORMACION DE USUARIOS
        
        private $ID; // NUMERIC(8,0)
        private $USERNAME; // VARCHAR (50),
        private $PASSWORD; // VARCHAR (50),
        private $NAME; // VARCHAR(50)
        private $LASTNAME; // VARCHAR(150)
        private $EMAIL; // VARCHAR(150)
        private $TYPE; // VARCHAR(2)
        private $SUCURSAL; // VARCHAR(5)
        private $CODTIENDA; // VARCHAR(15)
        private $DESTIENDA; // VARCHAR(150)
        private $STATUS; // VARCHAR(3)
        private $FCRACION; // VARCHAR(50)
        public function __contruct(){
           
        }
        public function __set($valor,$dato){
            $dato = utf8_decode($dato);
            $chars = array("'", '"',  "'", "", "\'", '\"', '*');
            $dato = str_replace($chars, "",$dato);
            $dato = strip_tags($dato); 
            $dato = stripslashes($dato); 
            
            switch($valor){
                case'ID':$this->ID=$dato;break;
                case'USERNAME':$this->USERNAME=$dato;break;
                case'PASSWORD':$this->PASSWORD=$dato;break;
                case'NAME':$this->NAME=$dato;break;
                case'LASTNAME':$this->LASTNAME=$dato;break;
                case'EMAIL':$this->EMAIL=$dato;break;
                case'TYPE':$this->TYPE=$dato;break;
                case'SUCURSAL':$this->SUCURSAL=$dato;break;
                case'CODTIENDA':$this->CODTIENDA=$dato;break;
                case'DESTIENDA':$this->DESTIENDA=$dato;break;
                case'STATUS':$this->STATUS=$dato;break;
                case'FCREACION':$this->FCREACION=$dato;break;
            }
        }
            
          public function get($valor){
            switch($valor){
                case'ID': return $this->ID; break;
                case'USERNAME': return $this->USERNAME; break;
                case'PASSWORD': return $this->PASSWORD; break;
                case'NAME': return $this->NAME; break;
                case'LASTNAME': return $this->LASTNAME; break;
                case'EMAIL': return $this->EMAIL; break;
                case'TYPE': return $this->TYPE; break;
                case'SUCURSAL': return $this->SUCURSAL; break;
                case'CODTIENDA': return $this->CODTIENDA; break;
                case'DESTIENDA': return $this->DESTIENDA; break;
                case'STATUS': return $this->STATUS; break;
                case'FCREACION': return $this->FCREACION; break;
            }
        }
        
        


    }




?>