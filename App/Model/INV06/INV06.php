<?php
    namespace App\Model\INV06;
    class INV06{ 
        private $LOCACION; // VARCHAR (50);
        private $USERNAME; // VARCHAR (50);
        private $SUCURSAL; // VARCHAR (5);
        private $COD_AS; // VARCHAR (20);
        private $CANTIDAD; // NUMERIC (8,0);
        private $FCREACION; // VARCHAR (50);
        private $INDICADOR; // VARCHAR (2);
 
        public function __contruct(){
           
        }
        public function __set($valor,$dato){
            $dato = utf8_decode($dato);
            $chars = array("'", '"',  "'", "", "\'", '\"', '*');
            $dato = str_replace($chars, "",$dato);
            $dato = strip_tags($dato); 
            $dato = stripslashes($dato); 
            
            switch($valor){
                case'LOCACION':$this->LOCACION=$dato;break;
                case'USERNAME':$this->USERNAME=$dato;break;
                case'SUCURSAL':$this->SUCURSAL=$dato;break;
                case'COD_AS':$this->COD_AS=$dato;break;
                case'CANTIDAD':$this->CANTIDAD=$dato;break;
                case'FCREACION':$this->FCREACION=$dato;break;
                case'INDICADOR':$this->INDICADOR=$dato;break;
            }
        }
            
          public function get($valor){
            switch($valor){
                case'LOCACION': return $this->LOCACION; break;
                case'USERNAME': return $this->USERNAME; break;
                case'SUCURSAL': return $this->SUCURSAL; break;
                case'COD_AS': return $this->COD_AS; break;
                case'CANTIDAD': return $this->CANTIDAD; break;
                case'FCREACION': return $this->FCREACION; break;
                case'INDICADOR': return $this->INDICADOR; break;
            }
        }
        
        


    }




?>