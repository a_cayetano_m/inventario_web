<?php 
    namespace App\Model\INV06;

    use App\Model\User\User;
    use App\Model\Time\Time;
    use App\Helper\Helper as HelperMethod;
    use System\Core\Model;

    class INV06DA extends Model{

        private $table="STLDTA.INV06";
        private $typeDB="odbc";
        private $instancia="";
        private $user_array=[];
        
        public function __contruct(){
            parent::__construct();
        }
       
        public function create($obj){
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();
            $sql="INSERT INTO ".$this->table."(LOCACION,USERNAME,SUCURSAL,COD_AS,CANTIDAD,FCREACION,INDICADOR) VALUES ";
            foreach($obj as $key=>$value){
                if($key==0){
                    $sql=$sql."";
                }else{
                    $sql=$sql.",";
                }
                $sql=$sql."('" . $value->get('LOCACION') . "',
                '" . $value->get('USERNAME') . "',
                '" . $value->get('SUCURSAL') . "',
                '" . $value->get('COD_AS') . "',
                '" . $value->get('CANTIDAD') . "',
                '" . $value->get('FCREACION') . "',
                '" . $value->get('INDICADOR'). "')";
            }

            $response =$instancia->executeQueryODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $response;
        }
        public function showProductsLocations($params){
            //ODBC 
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();
            $sql="SELECT * FROM ".$this->table." WHERE SUCURSAL = ".$params['SUCURSAL']." AND LOCACION='".$params['LOCACION']."'";
            
            $data=$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $data;
        }
        public function searchProductEAN($ean,$sucursal){
            //ODBC 
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();
            
            $sql="SELECT ARCOD AS CODIGO_AS,B7EAN AS EAN,TRANSLATE(ARDES,'aeiouAEIOUnN','áéíóúÁÉÍÓÚñÑ') AS DESCRIPCION ,ARUMA AS UNIDAD_BASE, UMFAC AS EAN_FACTOR
                        FROM STLDTA.OP01
                        INNER JOIN STLDTA.OPB7 ON B7CTL = ARCOD
                        INNER JOIN STLDTA.OP03 ON B7UND = UMCOD
                        INNER JOIN STLDTA.TMS4 ON S4ART = ARCOD
                        WHERE S4SUC = ".$sucursal." AND B7EAN in (".$ean.")";
            $data=$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $data;
        }
        public function searchProductAS($as,$sucursal){
            //ODBC 
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();
            
            $sql="SELECT ARCOD AS CODIGO_AS,TRANSLATE(TRANSLATE(ARDES,'aeiouAEIOUnN','áéíóúÁÉÍÓÚñÑ'),'aeiouAEIOUnN','áéíóúÁÉÍÓÚñÑ') AS DESCRIPCION, ARUMA AS UNIDAD_BASE
                        FROM STLDTA.OP01
                        INNER JOIN STLDTA.OPB7 ON B7CTL = ARCOD
                        INNER JOIN STLDTA.OP03 ON B7UND = UMCOD
                        INNER JOIN STLDTA.TMS4 ON S4ART = ARCOD
                        WHERE S4SUC = ".$sucursal." AND ARCOD in (".$as.") GROUP BY ARCOD,TRANSLATE(ARDES,'aeiouAEIOUnN','áéíóúÁÉÍÓÚñÑ'),ARUMA";
            $data=$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar
            
            $instancia->disposeODBC();
            return $data;
        }
        public function deleteContentLocation($location,$sucursal){
            //ODBC 
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();
            $sql="DELETE FROM ".$this->table." WHERE LOCATION='".$location."' AND SUCURSAL='".$sucursal."'";
            $response=$instancia->executeQueryODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $response;
        }
        public function getLocationMaterial($params){
            //ODBC 
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();
            
            $sql="SELECT COD_EAN,COD_AS,DESCRIPCION,FACTOR,UNIDAD_BASE FROM ".$this->table." WHERE LOCACION='".$params['LOCACION']."' AND SUCURSAL=".$params['SUCURSAL']."";
            
            $data=$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar
            
            $instancia->disposeODBC();
            return $data;
        }
        public function getAllDataSurcursal(){
            //ODBC 
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();
            $sql="SELECT ARCOD AS CODIGO_AS,B7EAN AS EAN,TRANSLATE(ARDES,'aeiouAEIOUnN','áéíóúÁÉÍÓÚñÑ') AS DESCRIPCION ,ARUMA AS UNIDAD_BASE, UMFAC AS EAN_FACTOR
            FROM STLDTA.OP01
            INNER JOIN STLDTA.OPB7 ON B7CTL = ARCOD
            INNER JOIN STLDTA.OP03 ON B7UND = UMCOD
            INNER JOIN STLDTA.TMS4 ON S4ART = ARCOD
            WHERE S4SUC = 052";
            $data=$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $data;
           
        }

        public function horaFinal($suc,$zona){
            //ODBC 
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();
            $sql="SELECT FCREACION FROM STLDTA.INV06 WHERE SUBSTRING(LOCACION,1,1) = '".$zona."' AND SUCURSAL = '".$suc."' ORDER BY FCREACION DESC FETCH FIRST 1 ROW ONLY";
            $data=$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $data;
           
        }

        public function getDataInventario($suc,$locacion){
            //ODBC 
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();
            $sql="SELECT SUBSTRING(LOCACION,1,3)||' '||SUBSTRING(LOCACION,5,1)||'-'||SUBSTRING(LOCACION,6,2) AS LOCACION,COD_AS,CANTIDAD FROM STLDTA.INV06 WHERE SUCURSAL = '".$suc."' AND LOCACION = '".$locacion."'";
            $data=$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $data;
           
        }

        public function horaFinal2($suc){
            //ODBC 
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();
            $sql="SELECT FCREACION FROM STLDTA.INV06 WHERE SUCURSAL = '".$suc."' ORDER BY FCREACION DESC FETCH FIRST 1 ROW ONLY";
            $data=$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $data;
           
        }

        public function delete($suc,$locacion){
            //ODBC 
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();
            $sql="DELETE FROM ".$this->table." WHERE SUCURSAL = '".$suc."' AND LOCACION='".$locacion."'";
            
            $data=$instancia->executeQueryODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $data;
        }

        public function delete_conteo2($suc,$locacion,$cod_as){
            //ODBC 
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();
            $sql="DELETE FROM STLDTA.INV08 WHERE SUCURSAL = '".$suc."' AND LOCACION='".$locacion."' AND COD_AS = '".$cod_as."'";
            
            $data=$instancia->executeQueryODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $data;
        }

        public function create_conteo2($obj){
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();
            $sql="INSERT INTO STLDTA.INV08 (LOCACION,USERNAME,SUCURSAL,COD_AS,CANTIDAD,FCREACION,INDICADOR) VALUES ";
            foreach($obj as $key=>$value){
                if($key==0){
                    $sql=$sql."";
                }else{
                    $sql=$sql.",";
                }
                $sql=$sql."('" . $value->get('LOCACION') . "',
                '" . $value->get('USERNAME') . "',
                '" . $value->get('SUCURSAL') . "',
                '" . $value->get('COD_AS') . "',
                '" . $value->get('CANTIDAD') . "',
                '" . $value->get('FCREACION') . "',
                '" . $value->get('INDICADOR'). "')";
            }

            $response =$instancia->executeQueryODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $response;
        }
        public function getDataInventario2($suc,$locacion){
            //ODBC 
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();
            $sql="SELECT SUBSTRING(LOCACION,1,3)||' '||SUBSTRING(LOCACION,5,1)||'-'||SUBSTRING(LOCACION,6,2) AS LOCACION,COD_AS,CANTIDAD FROM STLDTA.INV08 WHERE SUCURSAL = '".$suc."' AND LOCACION = '".$locacion."'";
            $data=$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $data;
           
        }
        
    }

?>