<?php 
    namespace App\Model\TMERC;

    use App\Model\User\User;
    use App\Model\Time\Time;
    use App\Helper\Helper as HelperMethod;
    use System\Core\Model;

    class TMERCDA extends Model{

        private $table="TMERC";
        private $typeDB="odbc";
        private $instancia="";
        private $user_array=[];
        
        public function __contruct(){
            parent::__construct();
        }
        public function insertarRegistro($bib){
            //ODBC 
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();
            $sql="INSERT INTO STLDTA.INV02 (LOCACION, ZONA, MUEBLE, NIVEL, SEPARACION, STATUS, SUCURSAL, CODTIENDA, USERNAME, FCREACION) 
            SELECT 
            SUBSTR(CDZON,1,1)||SUBSTR(CDMUE,1,2)||'-'||CDNIV AS LOCACIONES,
            SUBSTR(CDZON,1,1) AS ZONA, 
            SUBSTR(CDMUE,1,2) AS MUEBLE,
            SUBSTR(CDNIV,1,1) AS NIVEL,
            SUBSTR(CDNIV,2,2) AS SEPARACION,
            2 AS STATUS,
            SUBSTR(CDALM,2,2) AS SUCURSAL,
            '' AS CODTIENDA,
            '' AS USERNAME,
            '' AS FCREACION
            FROM ".$bib.".". $this->table;
            $array =$instancia->executeQueryODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $data;
        }
        
    }

?>