<?php 
    namespace App\Model\INV16;

    use App\Model\User\User;
    use App\Model\Time\Time;
    use System\Core\Model;

    class INV16DA extends Model{

        private $table="STLDTA.INV16";
        private $table2="STLDTA.INV06";
        private $table3="STLDTA.OP08";
        private $typeDB="odbc";
        private $instancia="";
        private $user_array=[];
        
        public function __contruct(){
            parent::__construct();
        }
        public function Create($bib,$suc,$alm,$monto,$inv){
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();
            $sql="INSERT INTO ".$this->table." 
            SELECT *
            FROM (
                SELECT ".$suc.", RICOD, 
                       (STQFI - STQRE) AS DISPONIBLE,
                       LISPR1 AS PRECIO,
                       ((STQFI - STQRE) * LISPR1) AS VALOR,
                       SUM(RIC11) AS CONTEO,
                       (SUM(RIC11) * LISPR1) AS VALORCONTEO,
                       (SUM(RIC11) - (STQFI - STQRE)) AS DIFCAN,
                       (SUM(RIC11) * LISPR1) - ((STQFI - STQRE) * LISPR1) AS DIFVAL
                FROM ".$bib.".OP22
                INNER JOIN STLDTA.OP08 ON RICOD = STCOD 
                INNER JOIN ".$bib.".TLIST1 ON LISCOD = LPAD(LPAD(RICOD,6,'0' ),15,' ' )
                WHERE STALM = '".$alm."' AND LISCL1 = 'L05' AND LISCAN = 1 AND RINUM = ".$inv."
                GROUP BY ".$suc.", RICOD, STQFI, STQRE, LISPR1
            ) AS Subquery
            WHERE (VALOR - VALORCONTEO) > ".$monto." OR (VALOR - VALORCONTEO) < -".$monto;
            //print_r($sql);
            //die();
            $array =$instancia->executeQueryODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $array;
        }

        public function getValues($suc){
            //ODBC 
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();
            $sql="SELECT MAX(DIFVAL) AS MAXIMO, MIN(DIFVAL) AS MINIMO, COUNT(*) AS CANTIDAD_REGISTROS FROM STLDTA.INV16 WHERE SUCURSAL = '".$suc."'";
            $data =$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $data;
        }
    }

?>