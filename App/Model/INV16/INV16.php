<?php
    namespace App\Model\INV16;
    class INV16{

        private $SUCURSAL;   
        private $CODAS;    
        private $STOCKDISPO;
        private $PRECIO;   
        private $VALORCOD; 
        private $CONTEO1;  
        private $VALORCONT;
        private $DIFCANT;  
        private $DIFVAL;   
        public function __contruct(){

        }
        public function __set($valor,$dato){
            $dato = utf8_decode($dato);
            $chars = array("'", '"',  "'", "", "\'", '\"', '*');
            $dato = str_replace($chars, "",$dato);
            $dato = strip_tags($dato); 
            $dato = stripslashes($dato); 
            
            switch($valor){
                case'SUCURSAL':$this->SUCURSAL=$dato;break;
                case'CODAS':$this->CODAS=$dato;break;
                case'STOCKDISPO':$this->STOCKDISPO=$dato;break;
                case'PRECIO':$this->PRECIO=$dato;break;
                case'VALORCOD':$this->VALORCOD=$dato;break;
                case'CONTEO1':$this->CONTEO1=$dato;break;
                case'VALORCONT':$this->VALORCONT=$dato;break;
                case'DIFCANT':$this->DIFCANT=$dato;break;
                case'DIFVAL':$this->DIFVAL=$dato;break;
            }
        }
            
          public function obtener($valor){
            switch($valor){
                case'SUCURSAL': return $this->SUCURSAL; break;
                case'CODAS': return $this->CODAS; break;
                case'STOCKDISPO': return $this->STOCKDISPO; break;
                case'PRECIO': return $this->PRECIO; break;
                case'VALORCOD': return $this->VALORCOD; break;
                case'CONTEO1': return $this->CONTEO1; break;
                case'VALORCONT': return $this->VALORCONT; break;
                case'DIFCANT': return $this->DIFCANT; break;
                case'DIFVAL': return $this->DIFVAL; break;
            }
        }
        
        


    }




?>