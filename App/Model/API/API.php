<?php
namespace App\Model\API;

use System\Core\Model;

class API extends Model{

	public function sendMail_Post($url,$data){
        $ch = curl_init($url);
        $headers = array();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch,CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($ch);
    }
    public function sendEmail($to,$data){

        
        $Adress=$to;
        $url=CONFIG['correo']['api_tailoy']['url'];
        $token=CONFIG['correo']['api_tailoy']['token'];
        $Username=CONFIG['correo']['access']['username'];
        $Password=CONFIG['correo']['access']['password'];
        $From=CONFIG['correo']['from'];
        $Filenames=array();
        $Files=array();
        $Subject=$data['asunto'];
        $Debug=1;    
        $ReplyTo=array();
        $CC=array();
        $BCC=array();

        $Body=$data['body'];
    
        $data=array(
            'Token'=> $token,
            'Adress'=>json_encode($Adress),
            'Username'=>$Username,
            'Password'=>$Password,
            'From'=>json_encode($From),
            'Filenames'=>json_encode($Filenames),
            'Files'=>json_encode($Files),
            'Subject'=>$Subject,
            'Body'=>$Body,
            'Debug'=>$Debug,
            'ReplyTo'=>json_encode($ReplyTo),
            'CC'=>json_encode($CC),
            'BCC'=>json_encode($BCC)
        );

        $respuesta=$this->sendMail_Post($url,$data);
        
    }
    
}?>
