<?php 
    namespace App\Model\INV10;

    use System\Core\Model;
    use App\Model\INV10\INV10;
    

    class INV10DA extends Model{
        private $table="STLDTA.INV10";
        private $typeDB="odbc";
        private $ins="";
        private $user_array=[];
        
        public function __contruct(){
            parent::__construct();
        }
        
        public function ShowAllMenu(){
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();
            $sql="SELECT MKMECOD AS CODMENU,MKMENOM AS NAMEMENU,MKMEORD AS ORDENMENU FROM ".$this->table." ORDER BY MKMEORD";
            $response =$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $response;
        }
        public function ShowXMenu($idMenu){
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();
            $sql="SELECT MKMECOD AS CODMENU, MKMENOM AS NAMEMENU,MKMEORD AS ORDENMENU, MKMEICON AS ICONMENU,MKMERU AS ROUTEMENU, MKMETYP AS TYPEMENU FROM ".$this->table." WHERE MKMECOD=".$idMenu."";
            $response =$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $response;
        }
        public function NewCode(){

            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();
            $sql="SELECT IFNULL(MAX(MKMECOD),0) AS NEWCOD FROM ".$this->table;
            $array =$instancia->executeQueryShowODBC($sql,$conexion);
            $part_cod=implode(" ",$array[0]);
            if($part_cod==0){
                $part_cod=1;
            }else{
                $part_cod=implode(" ",$array[0]);
                $part_cod=$part_cod+1;
            }
            $instancia->disposeODBC();
            return $part_cod;
        }
        public function CreateMenu(PROV10 $obj){
           
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();
            $sql="INSERT INTO ".$this->table."(
                MKMECOD,MKMENOM,MKMEORD,MKMEICON,MKMERU,MKMEFCR,MKMETYP) VALUES (
                '" . $obj->obtener('MKMECOD') . "', 
                '" . $obj->obtener('MKMENOM') . "', 
                '" . $obj->obtener('MKMEORD') . "', 
                '" . $obj->obtener('MKMEICON') . "', 
                '" . $obj->obtener('MKMERU') . "',
                '" . $obj->obtener('MKMEFCR') . "', 
                '" . $obj->obtener('MKMETYP'). "') ";
            $array =$instancia->executeQueryODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $array;
        }
        public function UpdateMenu(PROV10 $obj){
           
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();
            $sql="UPDATE ".$this->table." SET MKMENOM='".$obj->obtener('MKMENOM')."',MKMEORD='".$obj->obtener('MKMEORD')."',MKMETYP='".$obj->obtener('MKMETYP')."',MKMEICON='".$obj->obtener('MKMEICON')."',MKMERU='".$obj->obtener('MKMERU')."' WHERE MKMECOD=".$obj->obtener('MKMECOD')."";
            $array =$instancia->executeQueryODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $array;
        }
        public function DeleteMenu($idMenu){
           
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();
            $sql="DELETE FROM ".$this->table." WHERE MKMECOD=".$idMenu."";
            $array =$instancia->executeQueryODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $array;
        }
        public function Validate($idMenu){
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();
            $sql="SELECT * FROM MK12 WHERE MKMECOD=".$idMenu."";
            $array =$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $array;
        }
        public function getRoute($idMenu){
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();
            $sql="SELECT MKMERU AS ROUTEMENU FROM ".$this->table." WHERE MKMECOD=".$idMenu."";
            $array =$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $array;
        }
        public function ShowMenu(){
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();
            $sql="SELECT MKMECOD AS ROUTECODE,MKMERU AS NAMEROUTE FROM ".$this->table."";
            $response =$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $response;
        }
       
    }

?>