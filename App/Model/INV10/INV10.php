<?php
    namespace App\Model\INV10;
    
    class INV10{ //Table Menu
        private $MKMECOD; // Codigo del menu            //NUMERIC(3,0)
        private $MKMENOM; // Nombre del menu            //VARCHAR(25)
        private $MKMEORD; // Numero de orden del menu   //VARCHAR(2)
        private $MKMEICON; // Icono del menu            //VARCHAR(25)
        private $MKMERU; // Ruta del menu               //VARCHAR(25)
        private $MKMEFCR; // Fecha creación del menu     //VARCHAR(25)
        private $MKMETYP; // Tipo de menú (con opciones o sin opciones)              //VARCHAR(25)
        public function __contruct(){

        }
        public function __set($valor,$dato){
            $dato = utf8_decode($dato);
            $chars = array("'", '"',  "'", "", "\'", '\"', '*');
            $dato = str_replace($chars, "",$dato);
            $dato = strip_tags($dato); 
            $dato = stripslashes($dato); 
            
            switch($valor){
                case'MKMECOD':$this->MKMECOD=$dato;break;
                case'MKMENOM':$this->MKMENOM=$dato;break;
                case'MKMEORD':$this->MKMEORD=$dato;break;
                case'MKMEICON':$this->MKMEICON=$dato;break;
                case'MKMERU':$this->MKMERU=$dato;break;
                case'MKMEFCR':$this->MKMEFCR=$dato;break;
                case'MKMETYP':$this->MKMETYP=$dato;break;
            }
        }
            
          public function obtener($valor){
            switch($valor){
                case'MKMECOD': return $this->MKMECOD; break;
                case'MKMENOM': return $this->MKMENOM; break;
                case'MKMEORD': return $this->MKMEORD; break;
                case'MKMEICON': return $this->MKMEICON; break;
                case'MKMERU': return $this->MKMERU; break;
                case'MKMEFCR': return $this->MKMEFCR; break;
                case'MKMETYP': return $this->MKMETYP; break;
            }
        }
        
        


    }




?>