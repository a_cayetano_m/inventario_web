<?php 
    namespace App\Model\INV07;

    use App\Model\User\User;
    use App\Model\Time\Time;
    use System\Core\Model;

    class INV07DA extends Model{

        private $table="STLDTA.INV07";
        private $table2="STLDTA.INV16";
        private $table3="STLDTA.INV03";
        private $table4="STLDTA.OPB7";
        private $table5="STLDTA.OP01";
        private $table6="STLDTA.OP03";
        private $typeDB="odbc";
        private $instancia="";
        private $user_array=[];
        
        public function __contruct(){
            parent::__construct();
        }
        public function Create1($suc){
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();
            $sql="INSERT INTO ".$this->table." 
                SELECT LOCACION,'inv',D.SUCURSAL,COD_AS,B7EAN,TRANSLATE(ARDES,'aeiouAEIOUnN','áéíóúÁÉÍÓÚñÑ'), UMFAC,'',0 FROM ".$this->table2." D
                INNER JOIN ".$this->table3." L ON D.CODAS = L.COD_AS AND D.SUCURSAL = L.SUCURSAL
                INNER JOIN ".$this->table4." ON CODAS = B7CTL
                INNER JOIN ".$this->table5." ON CODAS = ARCOD
                INNER JOIN ".$this->table6." ON B7UND = UMCOD
                WHERE D.SUCURSAL = ".$suc." AND L.SUCURSAL = '".$suc."'
                GROUP BY LOCACION,D.SUCURSAL,COD_AS,B7EAN,TRANSLATE(ARDES,'aeiouAEIOUnN','áéíóúÁÉÍÓÚñÑ'),UMFAC
                ORDER BY COD_AS,UMFAC ASC";
            $array =$instancia->executeQueryODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $array;
        }
        public function Create2($suc){
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();
            $sql="INSERT INTO ".$this->table." 
                SELECT LOCACION,'inv',D.SUCURSAL,COD_AS,B7EAN,TRANSLATE(ARDES,'aeiouAEIOUnN','áéíóúÁÉÍÓÚñÑ'), UMFAC,'',0 FROM ".$this->table2." D
                INNER JOIN ".$this->table3." L ON D.CODAS = L.COD_AS AND D.SUCURSAL = L.SUCURSAL
                INNER JOIN ".$this->table4." ON CODAS = B7CTL
                INNER JOIN ".$this->table5." ON CODAS = ARCOD
                INNER JOIN ".$this->table6." ON B7UND = UMCOD
                WHERE D.SUCURSAL = '".$suc."' AND D.DIFVAL >= 200 AND L.SUCURSAL = '".$suc."'
                GROUP BY LOCACION,D.SUCURSAL,COD_AS,B7EAN,TRANSLATE(ARDES,'aeiouAEIOUnN','áéíóúÁÉÍÓÚñÑ'),UMFAC
                ORDER BY COD_AS,UMFAC ASC";
            $array =$instancia->executeQueryODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $array;
        }

        public function getFirstCod($suc){
            //ODBC 
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();

            $sql="SELECT COD_AS FROM STLDTA.INV07 WHERE SUCURSAL = '".$suc."' AND USERNAME = 'inv' AND CONTEO = 0 ORDER BY COD_AS asc limit 1 ";
           
            $data=$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar

            $instancia->disposeODBC();
            return $data;
        }

        public function update($suc,$cod,$user,$con){
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();
            $fechaHora = date('Y-m-d H:i:s');
            $date = trim($fechaHora);
            $sql="UPDATE STLDTA.INV07 SET USERNAME = '".$user."', FCREACION = '".$date."' 
            WHERE SUCURSAL = '".$suc."' AND COD_AS IN (SELECT DISTINCT CODAS
            FROM (
                SELECT D.CODAS,D.DIFVAL
                FROM STLDTA.INV16 D
                INNER JOIN STLDTA.INV07 P ON D.SUCURSAL = P.SUCURSAL AND D.CODAS = P.COD_AS
                WHERE D.SUCURSAL = '".$suc."' AND P.USERNAME = 'inv' AND P.CONTEO = 0
                GROUP BY D.CODAS,D.DIFVAL
                ORDER BY D.DIFVAL ASC
                LIMIT ".$con."
            ) )";

            //print_r($sql);
            $array =$instancia->executeQueryODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $array;
        }

        public function getLocationInv($suc,$user){
            //ODBC 
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();

            $sql="SELECT DISTINCT P.LOCACION FROM STLDTA.INV07 P
            WHERE P.SUCURSAL = '".$suc."' AND P.USERNAME='".$user."' AND CONTEO = 0 ";
           
            $data=$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar

            $instancia->disposeODBC();
            return $data;
        }

        public function updateConteo($suc,$cod,$loc){
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();
            $fechaHora = date('Y-m-d H:i:s');
            $date = trim($fechaHora);
            $sql="UPDATE STLDTA.INV07 SET CONTEO = 1 WHERE SUCURSAL = '".$suc."' AND COD_AS = '".$cod."' AND LOCACION = '".$loc."'";

            $array =$instancia->executeQueryODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $array;
        }
    }

?>