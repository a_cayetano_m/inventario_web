<?php
    namespace App\Model\OP69;
    class OP69{

        private $RGCIA;   
        private $RGALM;    
        private $RGNUM;
        private $RGZON;   
        private $RGFEC; 
        private $RGEST;  
        private $RGTIP;
        private $RGCAT;  
        private $RGDES;   
        private $RGHAS; 
        public function __contruct(){

        }
        public function __set($valor,$dato){
            $dato = utf8_decode($dato);
            $chars = array("'", '"',  "'", "", "\'", '\"', '*');
            $dato = str_replace($chars, "",$dato);
            $dato = strip_tags($dato); 
            $dato = stripslashes($dato); 
            
            switch($valor){
                case'RGCIA':$this->RGCIA=$dato;break;
                case'RGALM':$this->RGALM=$dato;break;
                case'RGNUM':$this->RGNUM=$dato;break;
                case'RGZON':$this->RGZON=$dato;break;
                case'RGFEC':$this->RGFEC=$dato;break;
                case'RGEST':$this->RGEST=$dato;break;
                case'RGTIP':$this->RGTIP=$dato;break;
                case'RGCAT':$this->RGCAT=$dato;break;
                case'RGDES':$this->RGDES=$dato;break;
                case'RGHAS':$this->RGHAS=$dato;break;
            }
        }
            
          public function obtener($valor){
            switch($valor){
                case'RGCIA': return $this->RGCIA; break;
                case'RGALM': return $this->RGALM; break;
                case'RGNUM': return $this->RGNUM; break;
                case'RGZON': return $this->RGZON; break;
                case'RGFEC': return $this->RGFEC; break;
                case'RGEST': return $this->RGEST; break;
                case'RGTIP': return $this->RGTIP; break;
                case'RGCAT': return $this->RGCAT; break;
                case'RGDES': return $this->RGDES; break;
                case'RGHAS': return $this->RGHAS; break;
            }
        }
        
        


    }




?>