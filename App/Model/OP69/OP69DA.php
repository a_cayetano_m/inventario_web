<?php 
    namespace App\Model\OP69;

    use App\Model\User\User;
    use App\Model\Time\Time;
    use System\Core\Model;

    class OP69DA extends Model{

        private $table="OP69";
        private $typeDB="odbc";
        private $instancia="";
        private $user_array=[];
        
        public function __contruct(){
            parent::__construct();
        }

        public function getInventario($bib){
            //ODBC 
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();
            $sql="SELECT RGNUM AS INVENTARIO, RGALM AS ALMACEN, RGFEC AS FECHA, RGEST AS ESTADO FROM ".$bib.".". $this->table." WHERE RGFEC >= 20220101 ORDER BY RGFEC DESC fetch first 10 rows only";
            //print_r($sql);
            //die();
            $data =$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $data;
        }

        public function validateInv($bib,$inv){
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();
            $sql ="SELECT * FROM ".$bib.".".$this->table." WHERE RGNUM = ".$inv." AND RGEST < 3 AND RGCAT = 2";
            //die();
            $data =$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $data;
        }

        public function getStatus($bib,$inv){
            //ODBC
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();
            $sql="SELECT RGEST FROM ".$bib.".".$this->table." WHERE RGNUM = ".$inv;
            $data = $instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $data;
        }

        public function get_info_last_invt($bib) {
            //ODBC
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();
            $sql="SELECT RGNUM, RGEST FROM ".$bib.".".$this->table." WHERE RGCAT = 2 order by RGFEC desc, RGNUM DESC FETCH FIRST 1 ROWS ONLY";
            $data = $instancia->executeQueryShowODBC($sql,$conexion);//funcion para reemplazar
            $instancia->disposeODBC();
            //echo"<pre>";print_r($sql);print_r($data);echo"</pre>";
            return !empty($data[0]['RGNUM']) ? $data[0] : '';
        }

        public function getinv($bib,$inv) {
            //ODBC
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();
            $sql="SELECT RGEST FROM ".$bib.".".$this->table." WHERE RGCAT = 2 and RGNUM='".$inv."' order by RGFEC desc, RGNUM DESC FETCH FIRST 1 ROWS ONLY";
            $data = $instancia->executeQueryShowODBC($sql,$conexion);//funcion para reemplazar
            $instancia->disposeODBC();
            return !empty($data[0]['RGNUM']) ? $data[0]['RGEST'] : 0;
        }
    }
?>