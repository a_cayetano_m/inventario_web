<?php 
    namespace App\Model\PRODUCTOS;

    use App\Model\User\User;
    use App\Model\Time\Time;
    use App\Helper\Helper as HelperMethod;
    use System\Core\Model;

    class PRODUCTOSDA extends Model{

        private $table="";
        private $typeDB="odbc";
        private $instancia="";
        private $rta=[];
        
        public function __contruct(){
            parent::__construct();
        }
       
        public function showProduct($params){
            //ODBC
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();
            $sql="SELECT ARCOD AS CODIGO_AS, TRANSLATE(ARDES,'aeiouAEIOUnN','áéíóúÁÉÍÓÚñÑ') AS DESCRIPCION 
            FROM STLDTA.OP01
            INNER JOIN STLDTA.OPB7 ON B7CTL = ARCOD
            INNER JOIN STLDTA.OP03 ON B7UND = UMCOD
            WHERE (ARCOD=".$params['MATERIAL']." OR B7EAN='".$params['MATERIAL']."')
            GROUP BY ARCOD, TRANSLATE(ARDES,'aeiouAEIOUnN','áéíóúÁÉÍÓÚñÑ')";
            $data =$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $data;
        }
        public function showDetailsProduct($params){
            //ODBC
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();
            $sql="SELECT ARCOD AS CODIGO_AS,ARUMA AS UNIDAD_BASE,B7EAN AS EAN,
            UMFAC AS EAN_FACTOR, TRANSLATE(ARDES,'aeiouAEIOUnN','áéíóúÁÉÍÓÚñÑ') AS DESCRIPCION,ARLIN AS LIN,ARSLN AS SUBLIN
            FROM STLDTA.OP01
            INNER JOIN STLDTA.OPB7 ON B7CTL = ARCOD
            INNER JOIN STLDTA.OP03 ON B7UND = UMCOD
            INNER JOIN STLDTA.TMS4 ON S4ART = ARCOD
            WHERE S4SUC = ".$params['SUCURSAL']." AND ";
            
            if(strlen($params['MATERIAL'])>0 && strlen($params['MATERIAL_EAN'])>0){
                $sql=$sql. "(ARCOD IN (".$params['MATERIAL'].") OR B7EAN IN (".$params['MATERIAL_EAN']."))";
            }
            else if(strlen($params['MATERIAL'])>0 && strlen($params['MATERIAL_EAN'])==0){
                $sql=$sql. "ARCOD IN (".$params['MATERIAL'].")";
            }
            else if(strlen($params['MATERIAL_EAN'])>0 && strlen($params['MATERIAL'])==0){
                $sql=$sql. "B7EAN IN (".$params['MATERIAL_EAN'].")";
            }
            $data =$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $data;
        }

        public function showDetailsProductAllBaseEAN($params){
            //ODBC           
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();
            $sql="SELECT DISTINCT(ARCOD)  AS CODIGO_AS
            FROM STLDTA.OP01
            INNER JOIN STLDTA.OPB7 ON B7CTL = ARCOD
            INNER JOIN STLDTA.OP03 ON B7UND = UMCOD
            INNER JOIN STLDTA.TMS4 ON S4ART = ARCOD
            WHERE B7EAN IN ('".$params['MATERIAL_EAN']."') LIMIT 1";
            $data =$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $data;
        }

        public function showProductEj(){
            //ODBC
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();
            $sql="SELECT ARCOD AS CODIGO_AS, TRANSLATE(ARDES,'aeiouAEIOUnN','áéíóúÁÉÍÓÚñÑ') AS DESCRIPCION 
            FROM STLDTA.OP01
            INNER JOIN STLDTA.OPB7 ON B7CTL = ARCOD
            INNER JOIN STLDTA.OP03 ON B7UND = UMCOD
            INNER JOIN STLDTA.TMS4 ON S4ART = ARCOD
            WHERE S4SUC = 332 
            AND (ARCOD=887961763072 OR B7EAN='887961763072')
            GROUP BY ARCOD, TRANSLATE(ARDES,'aeiouAEIOUnN','áéíóúÁÉÍÓÚñÑ')";
            $data =$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $data;
        }

        public function actualizaDesc($art){
            //ODBC
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();
            $sql="SELECT ARCOD AS CODIGO, REPLACE(ARDES,'''','') AS DESCRIPCION ,'1' AS FACTOR ,ARUMA AS UNIDAD,B7EAN AS EAN
            FROM STLDTA.OP01
            FULL JOIN STLDTA.OPB7 ON ARCOD = B7CTL AND ARUMA = B7UND
            WHERE ARCOD = ".$art." LIMIT 1";
            $data =$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $data;
        }

        public function actualizaDescEAN($art){
            //ODBC
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();
            $sql="SELECT ARCOD AS CODIGO,REPLACE(ARDES,'''','') AS DESCRIPCION,UMFAC AS FACTOR,ARUMA AS UNIDAD,B7EAN AS EAN FROM STLDTA.OPB7 
            INNER JOIN STLDTA.OP03 ON B7UND = UMCOD
            INNER JOIN STLDTA.OP01 ON B7CTL = ARCOD
            WHERE B7EAN = '".$art."'";
            $data =$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $data;
        }

        public function buscar_codigo_as_desde_ean($params =  NULL) {
            $this->rta = [];
            if(!empty($params)) {
                $instancia=parent::getIntancia($this->typeDB);
                $instancia->createConexionODBC();
                $conexion=$instancia->getConexionODBC();
                $sql="SELECT ARCOD as COD_AS,TO_NUMBER(B7EAN) as EAN
                FROM STLDTA.OP01
                INNER JOIN STLDTA.OPB7 ON B7CTL = ARCOD
                INNER JOIN STLDTA.OP03 ON B7UND = UMCOD
                INNER JOIN STLDTA.TMS4 ON S4ART = ARCOD
                WHERE TO_NUMBER(B7EAN) IN ('".implode(",",$params)."')";
                $data =$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar
                $instancia->disposeODBC();
                if(!empty($data[0]['COD_AS'])) {
                    foreach($data as $vv) {
                        $this->rta[$vv['EAN']] = $vv['COD_AS'];
                    }
                }
                return $this->rta;        
            }
        }        
    }

?>