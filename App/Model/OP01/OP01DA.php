<?php 
    namespace App\Model\OP01;

    use App\Model\User\User;
    use App\Model\Time\Time;
    use System\Core\Model;

    class OP01DA extends Model{

        private $table="OP01";
        private $typeDB="odbc";
        private $instancia="";
        private $user_array=[];
        
        public function __contruct(){
            parent::__construct();
        }

        public function existCod($cod){
            //ODBC 
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();
            $sql="SELECT * FROM STLDTA.OP01 WHERE ARCOD = ".$cod;
            //print_r($sql);
            //die();
            $data =$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $data;
        }

        public function validateInv($bib,$inv){
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();
            $sql ="SELECT * FROM ".$bib.".OP22 WHERE RGNUM = ".$inv." AND RGEST < 3 AND RGTIP = 8";

            //print_r($sql);
            //die();
            $data =$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $data;
        }

        public function validateExist($bib,$inv,$locacion,$codigo){
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();
            $sql ="SELECT * FROM ".$bib.".OP22 WHERE RGNUM = ".$inv." AND RICLO = '".$locacion."' AND RICOD = ".$codigo;

            //print_r($sql);
            //die();
            $data =$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $data;
        }

        public function update($bib,$inv,$locacion,$codigo,$cantidad,$user,$fecha,$hora){
            //ODBC 
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();
            $sql="UPDATE ".$bib.".OP22 RIEST = 1 , RIC11 = ".$cantidad.", RIUS1 = '".$user."', RIFE1 = ".$fecha.", RIHR1 = ".$hora." 
            WHERE RGNUM = ".$inv." AND RICLO = '".$locacion."' AND RICOD = ".$codigo;
            $response=$instancia->executeQueryODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $response;
        }
    }

?>