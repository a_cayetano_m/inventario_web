<?php 
    namespace App\Model\OP22;

    use App\Model\User\User;
    use App\Model\Time\Time;
    use System\Core\Model;

    class OP22DA extends Model{

        private $table="OP22";
        private $typeDB="odbc";
        private $instancia="";
        private $user_array=[];
        
        public function __contruct(){
            parent::__construct();
        }

        public function getPagina($bib,$inv){
            //ODBC 
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();
            $sql="SELECT RIPAG AS PAGINA FROM ".$bib.".OP22 WHERE RINUM = ".$inv." ORDER BY RIPAG DESC FETCH FIRST 1 ROW ONLY";
            //print_r($sql);
            //die();
            $data =$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $data;
        }

        public function validateInv($bib,$inv){
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();
            $sql ="SELECT * FROM ".$bib.".OP22 WHERE RGNUM = ".$inv." AND RGEST < 3 AND RGTIP = 8";

            //print_r($sql);
            //die();
            $data =$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $data;
        }

        public function validateExist($bib,$inv,$locacion,$codigo){
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();
            $sql ="SELECT * FROM ".$bib.".OP22 WHERE RINUM = ".$inv." AND RICLO = '".$locacion."' AND RICOD = ".$codigo;

            //print_r($sql);
            //die();
            $data =$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $data;
        }

        public function update($bib,$inv,$locacion,$codigo,$cantidad,$user,$fecha,$hora){
            //ODBC 
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();
            $sql="UPDATE ".$bib.".OP22 SET RIEST = 1 , RIC11 = ".$cantidad.", RIUS1 = '".$user."', RIFE1 = ".$fecha.", RIHR1 = ".$hora." 
            WHERE RINUM = ".$inv." AND RICLO = '".$locacion."' AND RICOD = ".$codigo;
            $response=$instancia->executeQueryODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $response;
        }

        public function insert($bib,$inv,$locacion,$codigo,$cantidad,$user,$fecha,$hora,$pagina,$secuencia,$suc){
            //ODBC 
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();
            $sql="INSERT INTO ".$bib.".OP22
            SELECT SACIA,SASUB,".$inv.",".$pagina.",".$secuencia.",'".$locacion."',0,".$fecha.",ARCOD,ARDES,ARUMA,LISPR1,STQFI,0,0,1,'',".$cantidad.",0,0,0,0, 0, ARLIN, ARSLN, ARFAM, ARSFM, '".$user."',".$fecha.", ".$hora.", '', 0, 0, '', 0, 0, '', 0, 0, '', 0, 0, '', 0, 0 
            FROM STLDTA.OP02
            INNER JOIN STLDTA.OP08 ON SACIA = STCIA AND SASUB = STALM
            INNER JOIN STLDTA.OP01 ON ARCOD = STCOD
            INNER JOIN ".$bib.".TLIST1 ON LISCOD = ARCOD 
            WHERE SASUC = ".$suc." AND SATIP = 'F' AND ARCOD = ".$codigo." AND LISCL1 = 'L05' AND LISCAN = 1";

            //print_r($sql);
            //die();
            $response=$instancia->executeQueryODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $response;
        }

        public function update_conteo2($bib,$inv,$locacion,$codigo,$cantidad,$user,$fecha,$hora){
            //ODBC 
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();
            $sql="UPDATE ".$bib.".OP22 SET RIEST = 2 , RIC22 = ".$cantidad.", RIUS4 = '".$user."', RIFE4 = ".$fecha.", RIHR4 = ".$hora." 
            WHERE RINUM = ".$inv." AND RICLO = '".$locacion."' AND RICOD = ".$codigo;
            //print_r($sql);
            //die();
            $response=$instancia->executeQueryODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $response;
        }
    }

?>