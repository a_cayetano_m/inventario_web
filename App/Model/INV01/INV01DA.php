<?php 
    namespace App\Model\INV01;

    use App\Model\User\User;
    use App\Model\Time\Time;
    use App\Helper\Helper as HelperMethod;
    use System\Core\Model;

    class INV01DA extends Model{

        private $table="STLDTA.INV01";
        private $typeDB="odbc";
        private $instancia="";
        private $user_array=[];
        
        public function __contruct(){
            parent::__construct();
        }
       
        public function getInfoLocaciones(){
            //ODBC 
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();
            $sql="SELECT * FROM ". $this->table."";
            $data =$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $data;
        }
       
        
        
    }

?>