<?php 
    namespace App\Model\INV20;

    use App\Model\User\User;
    use App\Model\Time\Time;
    use System\Core\Model;

    class INV20DA extends Model{

        private $table="STLDTA.INV20";
        private $table2="STLDTA.INV06";
        private $table3="STLDTA.OP08";
        private $typeDB="odbc";
        private $instancia="";
        private $user_array=[];
        
        public function __contruct(){
            parent::__construct();
        }
        public function getData($suc,$inv){
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();

            $sql="SELECT SUCURSAL, INVENTARIO, ZONA, MUEBLETOT, MUEBLECON, AVANCE, ESTADO, LINEASTOT, LINEASCON,ROUND((CAST(LINEASCON AS DOUBLE) / CAST(LINEASTOT AS DOUBLE)) * 100, 2) AS AVANCE2, TIME(SUBSTRING(HORAINI,12,8)) AS HORAINI, TIEMPOTRA, ESTIMADOTER, INVENTARIODORCUR FROM STLDTA.INV20 WHERE SUCURSAL ='".$suc."' AND INVENTARIO = '".$inv."'";
            
            //print_r($sql);
            //die();
            $data =$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $data;
        }

        public function create($suc,$inv){
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();
            $inv=str_pad($inv, 10, "0", STR_PAD_LEFT);
            $sql="INSERT INTO STLDTA.INV20
            SELECT '".$suc."','".$inv."',SUBSTRING(LOCACION,1,1),COUNT(DISTINCT SUBSTRING(LOCACION,1,3)),0,0,0,COUNT(DISTINCT COD_AS),0,'','','',0 FROM STLDTA.INV03
            WHERE SUCURSAL = '".$suc."' AND SUBSTRING(LOCACION,1,1) <> ''
            GROUP BY SUBSTRING(LOCACION,1,1) 
            ORDER BY SUBSTRING(LOCACION,1,1)";
            
            //print_r($sql);
            //die();
            $data =$instancia->executeQueryODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $data;
        }

        public function getLocationInv($suc,$inv){
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();
			$sql = "SELECT substr(LOCAcION,1,1) as loca FROM STLW10.INV24 WHERE SUCURSAL='" . (int)$suc . "' GROUP by substr(LOCAcION,1,1)";
			$ubi = $instancia->executeQueryShowODBC($sql, $conexion);
			$sq_ = "";
			if (!empty($ubi[0]['LOCA'])) {
				$sq_ = " AND ZONA <> '" . $ubi[0]['LOCA'] . "'";
			}
            $sql="SELECT DISTINCT ZONA FROM STLDTA.INV20 WHERE SUCURSAL ='".$suc."' AND INVENTARIO = '".$inv."'". $sq_;
            
            //die();
            $data =$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $data;
        }

        public function segMueble($suc,$inv){
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();

            $sql="SELECT DISTINCT ZONA,MUEBLETOT,LINEASTOT FROM STLDTA.INV20 WHERE SUCURSAL ='".$suc."' AND INVENTARIO = '".$inv."' ORDER BY ZONA ASC";
            
            //print_r($sql);
            //die();
            $data =$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $data;
        }

        public function update($suc,$inv,$zona,$a,$b,$c,$d,$e,$f,$g,$h){
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();

            $sql="UPDATE STLDTA.INV20
            SET MUEBLECON = '".$a."', AVANCE = '".$b."', ESTADO = '".$c."', LINEASCON = '".$d."', HORAINI = '".$e."', TIEMPOTRA = '".$f."', ESTIMADOTER = '".$g."', INVENTARIODORCUR = '".$h."'
            WHERE SUCURSAL = '".$suc."' AND INVENTARIO = '".$inv."' AND ZONA = '".$zona."'";
            
            //print_r($sql);
            //die();
            $data =$instancia->executeQueryODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $data;
        }

        public function avanceTotal($suc,$inv){
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();

            $sql="
            SELECT 
              t1.MUEBLETOT,
              t1.MUEBLECON,
              (CAST(t1.MUEBLECON AS DOUBLE) / CAST(t1.MUEBLETOT AS DOUBLE)) * 100 AS AVANCE,
              t1.LINEASTOT,
              t1.LINEASCON,
              t2.FCREACION
            FROM 
              (SELECT 
                SUM(MUEBLETOT) AS MUEBLETOT,
                SUM(MUEBLECON) AS MUEBLECON,
                SUM(LINEASTOT) AS LINEASTOT,
                SUM(LINEASCON) AS LINEASCON
              FROM STLDTA.INV20
              WHERE SUCURSAL = '".$suc."' AND INVENTARIO = '".$inv."') t1
            JOIN 
              (SELECT FCREACION FROM STLDTA.INV04 WHERE SUCURSAL = '".$suc."' AND C1=1 ORDER BY FCREACION ASC FETCH FIRST 1 ROW ONLY) t2
            ON 1=1";
            
            //print_r($sql);
            //die();
            $data =$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $data;
        }
    }

?>