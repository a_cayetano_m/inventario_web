<?php

    namespace App\Model\Session;

    class Session{

        public function init(){
            session_start();
        }
        public function active(){
            $session_global=CONFIG['session']['global'];
            
            if(empty($_SESSION[$session_global])){
                return false;
            }else{
                return true;
            }
        }
        public function add($key,$value){
            $_SESSION[$key]=$value;
        }
        public function addAll($key,$data){
            $_SESSION[strtoupper($key)]=$data;
        }
        public function get($key){
            if(!empty($_SESSION[$key])){
                return  $_SESSION[$key];
            }else{
                if(is_array($_SESSION[$key]) && count($_SESSION[$key])==0){
                    return  $_SESSION[$key];
                }else{
                    return  null;
                }
            }
        }
        public function set($key_global,$data,$key=''){
            if($key==''){
                $_SESSION[$key_global]=$data;
            }else{
                $_SESSION[$key_global][$key]=$data;
            }
            
        }
        public function getAll(){
            return $_SESSION;
        }
        public function remove($key){
            if(!empty($_SESSION[$key])){
                unset($_SESSION[$key]);
            }
        }
        public function sessionArray($key,$data){
            $this->add($key,$data);
        }
        public function close(){
            session_unset();
            session_destroy();
        }
    }






?>