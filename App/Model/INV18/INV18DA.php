<?php 
    namespace App\Model\INV18;

    use App\Model\User\User;
    use App\Model\Time\Time;
    use System\Core\Model;

    class INV18DA extends Model{

        private $table="STLDTA.INV18";
        private $table2="STLDTA.INV06";
        private $table3="STLDTA.OP08";
        private $typeDB="odbc";
        private $instancia="";
        private $user_array=[];
        
        public function __contruct(){
            parent::__construct();
        }
        public function getUsuarios($suc,$inv){
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();

            $sql="SELECT I.USERNAME,C.NAME,C.LASTNAME, I.SUCURSAL, I.INVENTARIO, A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z, MUEBLELEI, TIME(SUBSTRING(HORAINICIO,12,8)) AS HORAINICIO, TIME(SUBSTRING(HORAULTCARGA,12,8)) AS HORAULTCARGA, I.STATUS,I.LINEACON
            FROM STLDTA.INV18 I
            INNER JOIN STLDTA.INV00 C ON I.SUCURSAL = C.SUCURSAL AND I.USERNAME = C.USERNAME
            WHERE I.SUCURSAL ='".$suc."' AND I.INVENTARIO = '".$inv."'";
            //print_r($sql);
            //die();
            $data =$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $data;
        }

        public function createUsersLocations($suc,$user,$inv){
            //ODBC   
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();

            $sql="INSERT INTO STLDTA.INV18 VALUES ('".$user."', '".$suc."', '".$inv."', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0','0')";

            $data =$instancia->executeQueryODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();

            return $data;
        }

        public function quitaZona($suc,$user,$inv,$zon){
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();

            $sql="UPDATE STLDTA.INV18 SET ".$zon." = '' WHERE SUCURSAL = '".$suc."' AND USERNAME = '".$user."' AND INVENTARIO = '".$inv."'";

            //print_r($sql);
            //die();
            $data =$instancia->executeQueryODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();

            return $data;
        }

        public function agregaZona($suc,$user,$inv,$zon){
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();

            $sql="UPDATE STLDTA.INV18 SET ".$zon." = 'X' WHERE SUCURSAL = '".$suc."' AND USERNAME = '".$user."' AND INVENTARIO = '".$inv."'";

            //print_r($sql);
            //die();
            $data =$instancia->executeQueryODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();

            return $data;
        }

        public function update($suc,$user,$inv,$a,$b,$c,$d,$e){
            //ODBC 
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();

            $sql="UPDATE STLDTA.INV18 SET MUEBLELEI = '".$a."', HORAINICIO = '".$b."', HORAULTCARGA = '".$c."', STATUS = '".$d."', LINEACON = '".$e."'
            WHERE USERNAME = '".$user."' AND SUCURSAL = '".$suc."' AND INVENTARIO = '".$inv."'";

            //print_r($sql);
            //die();
            $data =$instancia->executeQueryODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();

            return $data;
        }

        public function getUsuarios2($suc,$inv){
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();

            $sql="SELECT I.USERNAME
            FROM STLDTA.INV18 I
            INNER JOIN STLDTA.INV00 C ON I.SUCURSAL = C.SUCURSAL AND I.USERNAME = C.USERNAME
            WHERE I.SUCURSAL ='".$suc."' AND I.INVENTARIO = '".$inv."'";
            //print_r($sql);
            //die();
            $data =$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $data;
        }

        public function exist($suc,$inv,$user,$zona){
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();

            $sql="SELECT * FROM STLDTA.INV17 WHERE SUCURSAL = '".$suc."' AND INVENTARIO = '".$inv."' AND USERNAME = '".$user."' AND ZONA = '".$zona."'";
            //print_r($sql);
            //die();
            $data =$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $data;
        }
    }

?>