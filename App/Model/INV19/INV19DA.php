<?php 
    namespace App\Model\INV19;

    use App\Model\User\User;
    use App\Model\Time\Time;
    use System\Core\Model;

    class INV19DA extends Model{

        private $table="STLDTA.INV19";
        private $table2="STLDTA.INV06";
        private $table3="STLDTA.OP08";
        private $typeDB="odbc";
        private $instancia="";
        private $user_array=[];
        private $rta = [];
        public function __contruct(){
            parent::__construct();
        }
        public function getInventario($suc){
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();

            $sql="SELECT INVENTARIO FROM STLDTA.INV19 WHERE SUCURSAL ='".$suc."' AND STATUS = '0'";
            
            //print_r($sql);
            //die();
            $data =$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $data;
        }

        public function createInventario($suc,$inv){
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();

            $sql="INSERT INTO STLDTA.INV19 VALUES ('".$suc."','".$inv."','0')";
            
            //print_r($sql);
            //die();
            $data=$instancia->executeQueryODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $data;
        }

        public function existInv($suc,$inv){
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();

            $sql="SELECT INVENTARIO FROM STLDTA.INV19 WHERE SUCURSAL ='".$suc."' AND  INVENTARIO = '".$inv."'";
            
            //print_r($sql);
            //die();
            $data=$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $data;
        }

        public function lastInv($suc = NULL) {
            if(!empty($suc)) {
                $instancia=parent::getIntancia($this->typeDB);
                $instancia->createConexionODBC();
                $conexion=$instancia->getConexionODBC();
                $sql = "SELECT INVENTARIO FROM STLDTA.INV19 WHERE sucursal='".$suc."' AND STATUS=0 ORDER BY INVENTARIO desc";
                $data=$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar
                $this->rta = !empty($data[0]['INVENTARIO']) ? $data[0]['INVENTARIO'] : NULL;
                $instancia->disposeODBC();
            }
			return $this->rta;
		}

        public function verificarInventario($suc) {
            $this->rta = [];
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();

            $sql="SELECT * FROM STLDTA.INV19 WHERE SUCURSAL ='".$suc."' ORDER BY INVENTARIO DESC fetch first 1 rows only";

            $this->rta =$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return (!empty($this->rta[0]) && $this->rta['status']==0) ? $this->rta : NULL;
        }

        public function update_inventario($sucu, $inv, $estado) {
            $this->rta = [];
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();

            $sql="update STLDTA.INV19 set STATUS='".$estado."' WHERE SUCURSAL ='".$sucu."' and  INVENTARIO='".$inv."'";

            $this->rta =$instancia->executeQueryODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return ($this->rta) ? $this->rta : NULL;
        }
    }

?>