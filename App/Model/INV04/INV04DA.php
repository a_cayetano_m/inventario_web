<?php 
    namespace App\Model\INV04;

    use App\Model\User\User;
    use App\Model\Time\Time;
    use App\Helper\Helper as HelperMethod;
    use System\Core\Model;

    class INV04DA extends Model{

        private $table="STLDTA.INV04";
        private $table2="STLDTA.INV03";
        private $typeDB="odbc";
        private $instancia="";
        private $rta=[];
        
        public function __contruct(){
            parent::__construct();
        }
        public function getLocationInv($params){
            if(!empty($params['INVENTARIO'])) {
                //ODBC 
                $instancia=parent::getIntancia($this->typeDB);
                $instancia->createConexionODBC();
                $conexion=$instancia->getConexionODBC();
                $sql="SELECT DISTINCT LOCACION FROM ".$this->table." WHERE SUCURSAL = ".$params['SUCURSAL']." AND INVENTARIO='" . $params['INVENTARIO'] . "'
                AND USERNAME='".$params['USERNAME']."' AND C".$params['CONTEO']."=0";
                $this->rta=$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar
                $instancia->disposeODBC();
            }            
            return $this->rta;
        }
        public function getLocationMaterialFirstCount($params){
            //ODBC 
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();

            $sql="SELECT up.LOCACION,up.COD_EAN,up.COD_AS,up.DESCRIPCION,up.FACTOR,up.UNIDAD_BASE,up.INDICADOR 
            FROM ".$this->table." ul INNER JOIN STLDTA.INV03 up ON ul.LOCACION=up.LOCACION AND ul.SUCURSAL = up.SUCURSAL WHERE 
            ul.SUCURSAL='".$params['SUCURSAL']."' AND ul.USERNAME = '".$params['USERNAME']."'";
           
            $data=$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar

            $instancia->disposeODBC();
            return $data;
        }
        public function getLocationMaterialSecondCount($params){
            //ODBC 
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();

            
            $sql="SELECT LOCACION,COD_EAN,COD_AS,DESCRIPCION,FACTOR,ARUMA AS UNIDAD_BASE,1 AS INDICADOR FROM STLDTA.INV07 
            INNER JOIN STLDTA.OP01 ON COD_AS = ARCOD
            WHERE SUCURSAL='".$params['SUCURSAL']."' AND USERNAME = '".$params['USERNAME']."' AND CONTEO = 0";
            $data=$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $data;
        }
        public function updateCont($params){ // conteo
            //ODBC 
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();

            $sql="UPDATE ".$this->table." SET C".$params['CONTEO']."=1 WHERE 
            LOCACION='".$params['LOCATION']."' AND USERNAME='".$params['USERNAME']."' AND SUCURSAL='".$params['SUCURSAL']."'";

            $data=$instancia->executeQueryODBC($sql,$conexion); //funcion para reemplazar

            $instancia->disposeODBC();
            return $data;
        }

        public function Create($suc,$inv){
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();
            $inv_=str_pad($inv, 10, "0", STR_PAD_LEFT);
            $sql="INSERT INTO ".$this->table." 
                SELECT 'inv',LOCACION,0,0,0,SUCURSAL,CURRENT DATE,'".$inv."' FROM ".$this->table2."
                WHERE SUCURSAL = ".$suc."
                GROUP BY LOCACION,SUCURSAL";
            $array =$instancia->executeQueryODBC($sql,$conexion); //funcion para reemplazar
            $sql="UPDATE ".$this->table." SET INVENTARIO='".$inv_."' WHERE INVENTARIO='".$inv."' AND SUCURSAL='".$suc."'";
            $instancia->disposeODBC();
            return $array;
        }

        public function consultaObtenerLocAsignado($params){
            if(!empty($params['INVENTARIO'])) {
                //ODBC 
                $instancia = parent::getIntancia($this->typeDB);
                $instancia->createConexionODBC();
                $conexion = $instancia->getConexionODBC();
				$sucu = $params['INVENTARIO'];
                $sql = "SELECT * FROM STLDTA.INV04 WHERE 
                SUCURSAL='" . $params['SUCURSAL'] . "' AND INVENTARIO='" . $params['INVENTARIO'] . "' AND USERNAME = '" . $params['USERNAME'] . "' AND C1 = 0";
                //print($sql);
                //die();
                $this->rta = $instancia->executeQueryShowODBC($sql, $conexion); //funcion para reemplazar
                $instancia->disposeODBC();
            }            
            return $this->rta;
        }

        public function consultaObtenerNiveles($params){
            if(!empty($params['INVENTARIO'])) {
                //ODBC 
                $instancia=parent::getIntancia($this->typeDB);
                $instancia->createConexionODBC();
                $conexion=$instancia->getConexionODBC();

                $sql="SELECT NIVEL1 AS NIVINF, NIVEL2 AS NIVSUP, ZONA FROM STLDTA.INV17 WHERE INVENTARIO='".$params['INVENTARIO']."' AND USERNAME = '".$params['USERNAME']."' AND SUCURSAL = '".$params['SUCURSAL']."' AND STATUS = '0'
                ORDER BY ZONA ASC
                FETCH FIRST 1 ROWS ONLY";
                //die();
                $this->rta=$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar
                $instancia->disposeODBC();
            }            
            return $this->rta;
        }

        public function consultaExistenLocPend($zona,$nivinf,$nivsup,$suc,$numinv){
            //ODBC 
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();
            
            $zona = (int) $zona;
            $nivsup = (int) $nivsup;
            $nivinf = (int) $nivinf;

            $sql="SELECT SUBSTRING(LOCACION,1,1) AS ZONA, SUBSTRING(LOCACION,2,2) AS NIVEL FROM STLDTA.INV04 WHERE INVENTARIO='".$numinv."' AND SUBSTRING(LOCACION,1,1) = '".$zona."' 
            AND CAST(SUBSTRING(LOCACION,2,2) AS INT) >= ".$nivinf." AND CAST(SUBSTRING(LOCACION,2,2) AS INT) <= ".$nivsup."
            AND C1 = 0 AND SUCURSAL = '".$suc."' AND USERNAME = 'inv'
            ORDER BY LOCACION ASC
            FETCH FIRST 1 ROWS ONLY";
            
            $data=$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $data;
        }

        public function actualizaEstadoZonaNivel($suc,$user,$zona,$nivinf,$nivsup,$numinv){ // conteo
            //ODBC 
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();

            $sql="UPDATE STLDTA.INV17 SET STATUS = 1 WHERE SUCURSAL = '".$suc."' AND USERNAME = '".$user."'
                AND ZONA = '".$zona."' AND NIVEL1 = ".$nivinf." AND NIVEL2 = ".$nivsup." AND INVENTARIO='".$numinv."'";

            $data=$instancia->executeQueryODBC($sql,$conexion); //funcion para reemplazar

            $instancia->disposeODBC();
            return $data;
        }

        public function asignoLocacionUsuario($suc,$user,$zona,$nivel,$numinv){ // conteo
            //ODBC 
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();
            $fechaHora = date('Y-m-d H:i:s');
            $date = trim($fechaHora);
            $sql="UPDATE STLDTA.INV04 SET USERNAME = '".$user."' , FCREACION = '".$date."' WHERE SUBSTRING(LOCACION,1,1) = '".$zona."' AND SUBSTRING(LOCACION,2,2) = ".$nivel." AND SUCURSAL = '".$suc."' AND C1 = 0 AND INVENTARIO='".$numinv."'";
            //print($sql);
            //die();
            $data=$instancia->executeQueryODBC($sql,$conexion); //funcion para reemplazar

            $instancia->disposeODBC();
            return $data;
        }

        public function locationsPend($suc,$inv){
            //ODBC 
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();

            $sql="SELECT P.LOCACION,D.USERNAME,LPAD(COUNT(DISTINCT C.COD_AS), 2, '0') AS TOTAL FROM STLDTA.INV02 P 
            LEFT JOIN STLDTA.INV03 C ON P.SUCURSAL = C.SUCURSAL AND P.LOCACION = C.LOCACION
            LEFT JOIN STLDTA.INV04 D ON P.SUCURSAL = D.SUCURSAL AND P.LOCACION = D.LOCACION
            WHERE P.SUCURSAL = '".$suc."' AND P.LOCACION <> '' AND D.C1 = 0
            GROUP BY P.LOCACION,D.USERNAME
            ORDER BY P.LOCACION ASC";

            //print($sql);
            //die();
            
            $data=$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $data;
        }

        public function muebleContado($zona,$suc,$inv){
            //ODBC 
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();

            $sql="SELECT COUNT(*) AS TOTAL
            FROM (
            SELECT t1.col1, t1.col2, t1.count_substring,
              (SELECT COUNT(DISTINCT LOCACION)
               FROM STLDTA.INV04
               WHERE SUBSTRING(LOCACION,1,1) = '".$zona."' AND SUBSTRING(LOCACION,2,2) = t1.col2
                 AND SUCURSAL = '".$suc."' AND C1 = 1 AND INVENTARIO = '".$inv."') AS conteo_distinto
            FROM (
              SELECT SUBSTRING(LOCACION,1,1) AS col1, SUBSTRING(LOCACION,2,2) AS col2, COUNT(SUBSTRING(LOCACION,2,2)) AS count_substring
              FROM STLDTA.INV04
              WHERE SUBSTRING(LOCACION,1,1) = '".$zona."' AND SUCURSAL = '".$suc."' AND INVENTARIO = '".$inv."'
              GROUP BY SUBSTRING(LOCACION,1,1), SUBSTRING(LOCACION,2,2)
            ) AS t1
            ORDER BY t1.col2) AS t2
            WHERE t2.conteo_distinto = t2.count_substring";

            //print($sql);
            //die();
            
            $data=$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $data;
        }

        public function lineaContado($zona,$suc,$inv){
            //ODBC 
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();

            $sql="SELECT COUNT(DISTINCT C.COD_AS) AS TOTAL FROM STLDTA.INV04 B
            INNER JOIN STLDTA.INV03 C ON B.LOCACION = C.LOCACION AND B.SUCURSAL = C.SUCURSAL
            WHERE SUBSTRING(B.LOCACION,1,1) = '".$zona."' AND C1 = 1 AND B.SUCURSAL = '".$suc."' AND B.INVENTARIO = '".$inv."'";

            //print($sql);
            //die();
            
            $data=$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $data;
        }

        public function horaInicio($zona,$suc,$inv){
            //ODBC 
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();

            $sql="SELECT FCREACION FROM STLDTA.INV04 WHERE SUCURSAL = '".$suc."' AND INVENTARIO = '".$inv."' AND SUBSTRING(LOCACION,1,1) = '".$zona."' AND C1 = 1 ORDER BY FCREACION ASC FETCH FIRST 1 ROW ONLY";

            //print($sql);
            //die();
            
            $data=$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $data;
        }

        public function invActivos($zona,$suc,$inv){
            //ODBC 
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();

            $sql="SELECT COUNT(DISTINCT USERNAME) AS TOTAL FROM STLDTA.INV04 WHERE SUCURSAL = '".$suc."' AND INVENTARIO = '".$inv."' AND SUBSTRING(LOCACION,1,1) = '".$zona."' AND C1 = 0 AND USERNAME <> 'inv'";

            //print($sql);
            //die();
            
            $data=$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $data;
        }

        public function muebleContadoInv($inventariador,$suc,$inv){
            //ODBC 
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();

            $sql="SELECT COUNT(*) AS TOTAL
            FROM (
            SELECT t1.col1, t1.col2, t1.count_substring,
              (SELECT COUNT(DISTINCT LOCACION)
               FROM STLDTA.INV04
               WHERE USERNAME = '".$inventariador."' AND SUBSTRING(LOCACION,2,2) = t1.col2
                 AND SUCURSAL = '".$suc."' AND C1 = 1 AND INVENTARIO = '".$inv."') AS conteo_distinto
            FROM (
              SELECT SUBSTRING(LOCACION,1,1) AS col1, SUBSTRING(LOCACION,2,2) AS col2, COUNT(SUBSTRING(LOCACION,2,2)) AS count_substring
              FROM STLDTA.INV04
              WHERE USERNAME = '".$inventariador."' AND SUCURSAL = '".$suc."' AND INVENTARIO = '".$inv."'
              GROUP BY SUBSTRING(LOCACION,1,1), SUBSTRING(LOCACION,2,2)
            ) AS t1
            ORDER BY t1.col2) AS t2
            WHERE t2.conteo_distinto = t2.count_substring";

            //print($sql);
            //die();
            
            $data=$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $data;
        }

        public function horaInicioInv($usuario,$suc,$inv){
            //ODBC 
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();

            $sql="SELECT FCREACION FROM STLDTA.INV04 WHERE SUCURSAL = '".$suc."' AND INVENTARIO = '".$inv."' AND USERNAME = '".$usuario."' ORDER BY FCREACION ASC FETCH FIRST 1 ROW ONLY";

            //print($sql);
            //die();
            
            $data=$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $data;
        }

        public function horaUltimoInv($usuario,$suc,$inv){
            //ODBC 
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();

            $sql="SELECT FCREACION FROM STLDTA.INV04 WHERE SUCURSAL = '".$suc."' AND INVENTARIO = '".$inv."' AND USERNAME = '".$usuario."' ORDER BY FCREACION DESC FETCH FIRST 1 ROW ONLY";

            //print($sql);
            //die();
            
            $data=$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $data;
        }

        public function totalMueble($suc,$inv,$zonas){
            //ODBC 
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();


            foreach ($zonas as $key => $value) {
                if($key < count($zonas)-1){
                    $in = $in."'".trim($value["ZONA"])."',";
                }else {
                    $in = $in."'".trim($value["ZONA"])."'";
                }
            }
            

            $sql="SELECT COUNT(DISTINCT SUBSTRING(LOCACION,1,3)) AS TOTAL FROM STLDTA.INV04 WHERE 
            SUCURSAL='".$suc."' AND INVENTARIO = '".$inv."' AND SUBSTRING(LOCACION,1,1) IN (".$in.")";

            $data=$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $data;
        }

        public function totalMuebleLeido($suc,$inv,$zonas){
            //ODBC 
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();


            foreach ($zonas as $key => $value) {
                if($key < count($zonas)-1){
                    $in = $in."'".trim($value["ZONA"])."',";
                }else {
                    $in = $in."'".trim($value["ZONA"])."'";
                }
            }
            

            $sql="SELECT COUNT(*) AS TOTAL
            FROM (
            SELECT t1.col1, t1.col2, t1.count_substring,
              (SELECT COUNT(DISTINCT LOCACION)
               FROM STLDTA.INV04
               WHERE SUBSTRING(LOCACION,1,1) IN (".$in.") AND SUBSTRING(LOCACION,1,3) = t1.col2
                 AND SUCURSAL = '".$suc."' AND C1 = 1 AND INVENTARIO = '".$inv."') AS conteo_distinto
            FROM (
              SELECT SUBSTRING(LOCACION,1,1) AS col1, SUBSTRING(LOCACION,1,3) AS col2, COUNT(SUBSTRING(LOCACION,1,3)) AS count_substring
              FROM STLDTA.INV04
              WHERE SUBSTRING(LOCACION,1,1) IN (".$in.") AND SUCURSAL = '".$suc."' AND INVENTARIO = '".$inv."'
              GROUP BY SUBSTRING(LOCACION,1,1), SUBSTRING(LOCACION,1,3)
            ) AS t1
            ORDER BY t1.col2) AS t2
            WHERE t2.conteo_distinto = t2.count_substring";

            $data=$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $data;
        }

        public function lineaContadoInv($user,$suc,$inv){
            //ODBC 
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();

            $sql="SELECT COUNT(DISTINCT C.COD_AS) AS TOTAL FROM STLDTA.INV04 B
            INNER JOIN STLDTA.INV03 C ON B.LOCACION = C.LOCACION AND B.SUCURSAL = C.SUCURSAL
            WHERE C1 = 1 AND B.SUCURSAL = '".$suc."' AND B.USERNAME = '".$user."' AND B.INVENTARIO = '".$inv."'";

            //print($sql);
            //die();
            
            $data=$instancia->executeQueryShowODBC($sql,$conexion); //funcion para reemplazar
            $instancia->disposeODBC();
            return $data;
        }

        public function updateCont2($params){ // conteo
            //ODBC 
            $instancia=parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion=$instancia->getConexionODBC();

            $sql="UPDATE ".$this->table." SET C2 = 1 WHERE 
            LOCACION='".$params['LOCATION']."' AND SUCURSAL='".$params['SUCURSAL']."'";

            $data=$instancia->executeQueryODBC($sql,$conexion); //funcion para reemplazar

            $instancia->disposeODBC();
            return $data;
        }

    }

?>