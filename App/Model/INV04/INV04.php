<?php
    namespace App\Model\INV04;
    class INV04{ 
        private $USERNAME; // VARCHAR (20);
        private $LOCATION; // VARCHAR (50);
        private $C1; // NUMERIC (8,0);
        private $C2; // NUMERIC  (8,0);
        private $C3; // NUMERIC  (8,0);
        private $SUCURSAL; // VARCHAR (5);
        private $FCREACION; // VARCHAR(50)
 
        public function __contruct(){
           
        }
        public function __set($valor,$dato){
            $dato = utf8_decode($dato);
            $chars = array("'", '"',  "'", "", "\'", '\"', '*');
            $dato = str_replace($chars, "",$dato);
            $dato = strip_tags($dato); 
            $dato = stripslashes($dato); 
            
            switch($valor){
                case'USERNAME':$this->USERNAME=$dato;break;
                case'LOCATION':$this->LOCATION=$dato;break;
                case'C1':$this->C1=$dato;break;
                case'C2':$this->C2=$dato;break;
                case'C3':$this->C3=$dato;break;
                case'SUCURSAL':$this->SUCURSAL=$dato;break;
                case'FCREACION':$this->FCREACION=$dato;break;
            }
        }
            
          public function get($valor){
            switch($valor){
                case'USERNAME': return $this->USERNAME; break;
                case'LOCATION': return $this->LOCATION; break;
                case'C1': return $this->C1; break;
                case'C2': return $this->C2; break;
                case'C3': return $this->C3; break;
                case'SUCURSAL': return $this->SUCURSAL; break;
                case'FCREACION': return $this->FCREACION; break;
            }
        }
        
        


    }




?>