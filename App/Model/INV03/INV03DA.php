<?php

namespace App\Model\INV03;

use App\Model\User\User;
use App\Model\Time\Time;
use App\Helper\Helper as HelperMethod;
use System\Core\Model;

class INV03DA extends Model
{

    private $table = "STLDTA.INV03";
    private $typeDB = "odbc";
    private $instancia = "";
    private $rta = [];

    public function __contruct()
    {
        parent::__construct();
    }

    public function create($obj)
    {

        $instancia = parent::getIntancia($this->typeDB);
        $instancia->createConexionODBC();
        $conexion = $instancia->getConexionODBC();
        $sql = "INSERT INTO " . $this->table . "(LOCACION,COD_EAN,COD_AS,DESCRIPCION,SUCURSAL,USERNAME,FACTOR,UNIDAD_BASE,FCREACION,INDICADOR) VALUES ";
        foreach ($obj as $key => $value) {
            if ($key == 0) {
                $sql = $sql . "";
            } else {
                $sql = $sql . ",";
            }
            $sql = $sql . "('" . $value->get('LOCACION') . "',
                '" . $value->get('COD_EAN') . "',
                '" . $value->get('COD_AS') . "',
                '" . $value->get('DESCRIPCION') . "',
                '" . $value->get('SUCURSAL') . "',
                '" . $value->get('USERNAME') . "',
                '" . $value->get('FACTOR') . "',
                '" . $value->get('UNIDAD_BASE') . "',
                '" . $value->get('FCREACION') . "',
                '" . $value->get('INDICADOR') . "')";
        }

        $response = $instancia->executeQueryODBC($sql, $conexion); //funcion para reemplazar
        $instancia->disposeODBC();
        return $response;
    }
    public function showProductsLocations($params)
    {
        //ODBC 
        $instancia = parent::getIntancia($this->typeDB);
        $instancia->createConexionODBC();
        $conexion = $instancia->getConexionODBC();
        $sql = "SELECT LOCACION,COD_AS,SUCURSAL,FACTOR,UNIDAD_BASE FROM " . $this->table . " WHERE SUCURSAL = " . $params['SUCURSAL'] . " AND LOCACION='" . $params['LOCACION'] . "' GROUP BY LOCACION,COD_AS,SUCURSAL,FACTOR,UNIDAD_BASE";
        $data = $instancia->executeQueryShowODBC($sql, $conexion); //funcion para reemplazar
        $instancia->disposeODBC();
        return $data;
    }
    public function validateProduct($params)
    {
        //ODBC 
        $instancia = parent::getIntancia($this->typeDB);
        $instancia->createConexionODBC();
        $conexion = $instancia->getConexionODBC();
        $sql = "SELECT count(1) as cont FROM " . $this->table . " WHERE SUCURSAL = " . $params['SUCURSAL'] . " AND LOCACION='" . $params['LOCACION'] . "' AND COD_EAN='" . $params['COD_EAN'] . "' AND COD_AS='" . $params['COD_AS'] . "'";

        $data = $instancia->executeQueryShowODBC($sql, $conexion); //funcion para reemplazar
        $instancia->disposeODBC();
        return $data;
    }
    public function validateProductCount($params)
    {
        //ODBC 
        $instancia = parent::getIntancia($this->typeDB);
        $instancia->createConexionODBC();
        $conexion = $instancia->getConexionODBC();
        $sql = "SELECT count(1) as cont FROM " . $this->table . " WHERE SUCURSAL = " . $params['SUCURSAL'] . " AND LOCACION='" . $params['LOCACION'] . "' AND COD_EAN='" . $params['COD_EAN'] . "' AND COD_AS='" . $params['COD_AS'] . "'";

        $data = $instancia->executeQueryShowODBC($sql, $conexion); //funcion para reemplazar
        $instancia->disposeODBC();
        return $data;
    }
    public function ExistProduct($params)
    {
        //ODBC 

        $instancia = parent::getIntancia($this->typeDB);
        $instancia->createConexionODBC();
        $conexion = $instancia->getConexionODBC();
        $sql = "SELECT count(1) as cont FROM " . $this->table . " WHERE SUCURSAL = " . $params['SUCURSAL'] . " AND LOCACION='" . $params['LOCACION'] . "' AND (COD_EAN='" . $params['MATERIAL'] . "' OR COD_AS='" . $params['MATERIAL'] . "')";
        $data = $instancia->executeQueryShowODBC($sql, $conexion); //funcion para reemplazar
        $instancia->disposeODBC();
        return $data;
    }
    public function searchProductEAN($ean, $sucursal)
    {
        //ODBC 
        $instancia = parent::getIntancia($this->typeDB);
        $instancia->createConexionODBC();
        $conexion = $instancia->getConexionODBC();

        $sql = "SELECT ARCOD AS CODIGO_AS,B7EAN AS EAN,TRANSLATE(ARDES,'aeiouAEIOUnN','áéíóúÁÉÍÓÚñÑ') AS DESCRIPCION ,ARUMA AS UNIDAD_BASE, UMFAC AS EAN_FACTOR
                        FROM STLDTA.OP01
                        INNER JOIN STLDTA.OPB7 ON B7CTL = ARCOD
                        INNER JOIN STLDTA.OP03 ON B7UND = UMCOD
                        INNER JOIN STLDTA.TMS4 ON S4ART = ARCOD
                        WHERE S4SUC = " . $sucursal . " AND B7EAN in (" . $ean . ")";
        $data = $instancia->executeQueryShowODBC($sql, $conexion); //funcion para reemplazar
        $instancia->disposeODBC();
        return $data;
    }
    public function searchProductAS($as, $sucursal)
    {
        //ODBC 
        $instancia = parent::getIntancia($this->typeDB);
        $instancia->createConexionODBC();
        $conexion = $instancia->getConexionODBC();

        $sql = "SELECT ARCOD AS CODIGO_AS,TRANSLATE(ARDES,'aeiouAEIOUnN','áéíóúÁÉÍÓÚñÑ') AS DESCRIPCION, ARUMA AS UNIDAD_BASE
                        FROM STLDTA.OP01
                        INNER JOIN STLDTA.OPB7 ON B7CTL = ARCOD
                        INNER JOIN STLDTA.OP03 ON B7UND = UMCOD
                        INNER JOIN STLDTA.TMS4 ON S4ART = ARCOD
                        WHERE S4SUC = " . $sucursal . " AND ARCOD in (" . $as . ") GROUP BY ARCOD,TRANSLATE(ARDES,'aeiouAEIOUnN','áéíóúÁÉÍÓÚñÑ'),ARUMA";
        $data = $instancia->executeQueryShowODBC($sql, $conexion); //funcion para reemplazar

        $instancia->disposeODBC();
        return $data;
    }
    public function deleteContentLocation($params)
    {
        //ODBC 
        $instancia = parent::getIntancia($this->typeDB);
        $instancia->createConexionODBC();
        $conexion = $instancia->getConexionODBC();
        $sql = "DELETE FROM " . $this->table . " WHERE LOCACION='" . $params['LOCACION'] . "' AND SUCURSAL=" . $params['SUCURSAL'] . "";
        $response = $instancia->executeQueryODBC($sql, $conexion); //funcion para reemplazar
        $instancia->disposeODBC();
        return $response;
    }
    public function deleteContentLocationUnidad($params)
    {
        //ODBC 
        $instancia = parent::getIntancia($this->typeDB);
        $instancia->createConexionODBC();
        $conexion = $instancia->getConexionODBC();
        $sql = "DELETE FROM " . $this->table . " WHERE LOCACION='" . $params['LOCACION'] . "' AND SUCURSAL=" . $params['SUCURSAL'] . " AND COD_AS='" . $params['AS_NOFORMAT'] . "'";
        $response = $instancia->executeQueryODBC($sql, $conexion); //funcion para reemplazar
        $instancia->disposeODBC();
        return $response;
    }
    public function getLocationMaterial($params)
    {
        //ODBC 
        $instancia = parent::getIntancia($this->typeDB);
        $instancia->createConexionODBC();
        $conexion = $instancia->getConexionODBC();

        $sql = "SELECT COD_EAN,COD_AS,DESCRIPCION,FACTOR,UNIDAD_BASE FROM " . $this->table . " WHERE LOCACION='" . $params['LOCACION'] . "' AND SUCURSAL=" . $params['SUCURSAL'] . "";

        $data = $instancia->executeQueryShowODBC($sql, $conexion); //funcion para reemplazar

        $instancia->disposeODBC();
        return $data;
    }
    public function getAllDataSurcursal()
    {
        //ODBC 
        $instancia = parent::getIntancia($this->typeDB);
        $instancia->createConexionODBC();
        $conexion = $instancia->getConexionODBC();
        $sql = "SELECT ARCOD AS CODIGO_AS,B7EAN AS EAN,TRANSLATE(ARDES,'aeiouAEIOUnN','áéíóúÁÉÍÓÚñÑ') AS DESCRIPCION ,ARUMA AS UNIDAD_BASE, UMFAC AS EAN_FACTOR
            FROM STLDTA.OP01
            INNER JOIN STLDTA.OPB7 ON B7CTL = ARCOD
            INNER JOIN STLDTA.OP03 ON B7UND = UMCOD
            INNER JOIN STLDTA.TMS4 ON S4ART = ARCOD
            WHERE S4SUC = 039";
        $data = $instancia->executeQueryShowODBC($sql, $conexion); //funcion para reemplazar
        $instancia->disposeODBC();
        return $data;
    }

    public function searchEan($ean, $sucursal)
    {
        //ODBC 
        $instancia = parent::getIntancia($this->typeDB);
        $instancia->createConexionODBC();
        $conexion = $instancia->getConexionODBC();
        $sql = "SELECT COUNT(1) as CANTIDAD FROM " . $this->table . " WHERE COD_EAN='" . $ean . "' AND SUCURSAL='" . $sucursal . "'";
        $data = $instancia->executeQueryShowODBC($sql, $conexion); //funcion para reemplazar
        $instancia->disposeODBC();
        return $data;
    }

    public function searchAs($as, $sucursal)
    {
        //ODBC 
        $instancia = parent::getIntancia($this->typeDB);
        $instancia->createConexionODBC();
        $conexion = $instancia->getConexionODBC();
        $sql = "SELECT COUNT(1) as CANTIDAD FROM " . $this->table . " WHERE COD_AS='" . $as . "' AND SUCURSAL='" . $sucursal . "'";
        $data = $instancia->executeQueryShowODBC($sql, $conexion); //funcion para reemplazar
        $instancia->disposeODBC();
        return $data;
    }

    public function delete_locaciones_inv($params = null)
    {
        $this->rta = false;
        if (!empty($params)) {
            $instancia = parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion = $instancia->getConexionODBC();
            $params['sucu'] = strlen($params['sucu']) == 1 ? "0".$params['sucu'] : $params['sucu'];
            $sql = "DELETE FROM " . $this->table . " where SUCURSAL='".$params['sucu']."' AND LOCACION='".$params['loca']."' AND TO_NUMBER(COD_AS) IN (" . implode(',', $params['c_as']) . ")";
            $this->rta = $instancia->executeQueryODBC($sql, $conexion); //funcion para reemplazar
            $instancia->disposeODBC();
        }
        return (!empty($this->rta)) ? true : false;
    }

    public function add_locaciones($params = null)
    {
        $this->rta = false;
        if(!empty($params)) {
            //ODBC 
            $params['sucu'] = strlen($params['sucu']) == 1 ? "0".$params['sucu'] : $params['sucu'];
            $fecha = date("Y-m-d H:m:s");
            $instancia = parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion = $instancia->getConexionODBC();
            $sql = "INSERT INTO " . $this->table . " SELECT 
                '".$params['loca']."',
                B7EAN,
                ARCOD,
                '".$params['sucu']."',
                '".$params['user']."',
                '".$fecha."',
                TRANSLATE(ARDES,'aeiouAEIOUnN','áéíóúÁÉÍÓÚñÑ') AS DESCRIPCION,
                UMFAC,
                ARUMA,
                '0'
                FROM STLDTA.OP01
                INNER JOIN STLDTA.OPB7 ON B7CTL = ARCOD
                INNER JOIN STLDTA.OP03 ON B7UND = UMCOD
                WHERE TO_NUMBER(ARCOD) IN (".implode(",",$params['c_as']).")";
            $this->rta = $instancia->executeQueryODBC($sql, $conexion); //funcion para reemplazar
            $instancia->disposeODBC();
        }        
        return (!empty($this->rta)) ? true : false;
    }

    public function upd_locacion_indicador($params = NULL) {
        $this->rta = false;
        if(!empty($params)) {
            //ODBC 
            $instancia = parent::getIntancia($this->typeDB);
            $instancia->createConexionODBC();
            $conexion = $instancia->getConexionODBC();
            $sql="UPDATE ".$this->table." SET INDICADOR='1' WHERE SUCURSAL='".$params['sucu']."' AND LOCACION='".$params['loca']."' AND TO_NUMBER(COD_EAN) IN (" . implode(',', $params['c_ea']) . ")";
            $this->rta = $instancia->executeQueryODBC($sql, $conexion); //funcion para reemplazar
            $instancia->disposeODBC();
        }
        return (!empty($this->rta)) ? true : false;
    }
}
