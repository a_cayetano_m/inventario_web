<?php 
    namespace App\Model\Files;

    use System\Core\Model;

    class Files extends Model{

        public function __contruct(){
            parent::__construct();
        }
        
        public function CreateFile($folder,$content){
          if(!file_exists($folder)){
            $myfile = fopen($folder, "w");
            fwrite($myfile,$content);
            fclose($myfile);
            $valor=true;
          }else{
              $valor=false;
          }
        }
        public function CreateFolder($folder){
          if(!file_exists($folder)){
            if(mkdir($folder,0777,true)){
              $valor=true;
            }
          }else{
              $valor=false;
          }
          return $valor;
        }
        public function ImageBase64($Base64Img){
          list(, $Base64Img) = explode(';', $Base64Img);
          list(, $Base64Img) = explode(',', $Base64Img);
          $Base64Img = base64_decode($Base64Img);
          return $Base64Img;
        }
       
    }

?>